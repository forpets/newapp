export const getReportAlbumPath = (reportId, userId) => {
  return getReportPath(reportId, userId) + '/album/'
}
export const getReportPath = (reportId, userId) => {
  return `${getUserPath(userId)}/reports/${reportId}`
}
export const getReportProfilePhotoPath = (reportId, userId) => {
  return getReportPath(reportId, userId) + '/pet.jpg'
}

export const getPetAlbumPath = (petId, userId) => {
  return getPetPath(petId, userId) + '/album/'
}
export const getPetProfilePhotoPath = (petId, userId) => {
  return getPetPath(petId, userId) + '/pet.jpg'
}
export const getPetPath = (petId, userId) => {
  return `${getUserPath(userId)}/pets/${petId}`
}

export const getUserPath = (userId) => {
  return `/user/${userId}`
}

export const getUserProfilePicPath = (userId) => {
  return `${getUserPath(userId)}/profile.jpg`
}

export default {
  getReportAlbumPath,
  getReportPath,
  getReportProfilePhotoPath,

  getPetAlbumPath,
  getPetProfilePhotoPath,
  getPetPath,

  getUserProfilePicPath,
  getUserPath
}
