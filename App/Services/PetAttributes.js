
export const findAllKinds = () => {
  return petKinds
}
export const findAllGenders = () => {
  return petGenders
}

export const findBreedsFor = (kindValue = null) : Array<Breed> => {
  switch (kindValue) {
    case 'gato':
      return catBreeds
    case 'perro':
      return dogBreeds
    default:
      return otherBreeds
  }
}

export const findColorsFor = (kindValue = null) : Array<Breed> => {
  switch (kindValue) {
    case 'gato':
      return catColors
    case 'perro':
      return dogColors
    default:
      return otherColors
  }
}

export const findAllFoods = () => {
  // return petCastrated
  return petFoods
}

export const findAllCastratedOptions = () => {
  return petCastrated
}

export default {
  findAllKinds,
  findAllGenders,
  findBreedsFor,
  findAllFoods,
  findAllCastratedOptions,
  findColorsFor
}

const petKinds = [
    {value: 'gato', label: 'Gato'},
    {value: 'perro', label: 'Perro'},
    {value: 'otro', label: 'Otro'}
]

const catColors = [{'value': 'CC1', 'label': 'Chinchilla O Silver Shaded'}, {'value': 'CC2', 'label': 'Shaded O-Shell Cameo'}, {'value': 'CC3', 'label': 'Shaded O Shell Tortoiseshell'}, {'value': 'CC4', 'label': 'Golden Chinchilla O Shaded'}, {'value': 'CC5', 'label': 'Azul Humo'}, {'value': 'CC6', 'label': 'Negro Humo'}, {'value': 'CC7', 'label': 'Cameo Humo'}, {'value': 'CC8', 'label': 'Torti Humo'}, {'value': 'CC9', 'label': 'Blue Cream Humo'}, {'value': 'CC10', 'label': 'Silver Tabby'}, {'value': 'CC11', 'label': 'Brown Tabby'}, {'value': 'CC12', 'label': 'Red Tabby'}, {'value': 'CC13', 'label': 'Cameo Tabby'}, {'value': 'CC14', 'label': 'Cream Tabby'}, {'value': 'CC15', 'label': 'Silver Patched Tabby'}, {'value': 'CC16', 'label': 'Brown Patched Tabby'}, {'value': 'CC17', 'label': 'Blue Patched Tabby'}, {'value': 'CC18', 'label': 'Negro'}, {'value': 'CC19', 'label': 'Azul'}, {'value': 'CC20', 'label': 'Rojo'}, {'value': 'CC21', 'label': 'Crema'}, {'value': 'CC22', 'label': 'Torti'}, {'value': 'CC23', 'label': 'Torti Diluida'}, {'value': 'CC24', 'label': 'Bicolor Negro'}, {'value': 'CC25', 'label': 'Bicolor Azul'}, {'value': 'CC26', 'label': 'Bicolor Rojo'}, {'value': 'CC27', 'label': 'Bicolor Crema'}, {'value': 'CC28', 'label': 'Calico'}, {'value': 'CC29', 'label': 'Calico Diluida'}]

const dogColors = [{'value': 'CD1', 'label': 'Abigarrado'}, {'value': 'CD2', 'label': 'Albaricoque'}, {'value': 'CD3', 'label': 'Albino'}, {'value': 'CD4', 'label': 'Amarillo'}, {'value': 'CD5', 'label': 'Arlequín'}, {'value': 'CD6', 'label': 'Ascob'}, {'value': 'CD7', 'label': 'Atigrado'}, {'value': 'CD8', 'label': 'Avellana'}, {'value': 'CD9', 'label': 'Azul'}, {'value': 'CD10', 'label': 'Azul grisáceo o grizzle'}, {'value': 'CD11', 'label': 'Azul mirlo'}, {'value': 'CD12', 'label': 'Belton'}, {'value': 'CD13', 'label': 'Bleiz'}, {'value': 'CD14', 'label': 'Bronce'}, {'value': 'CD15', 'label': 'Café'}, {'value': 'CD16', 'label': 'Caoba'}, {'value': 'CD17', 'label': 'Cervato'}, {'value': 'CD18', 'label': 'Cobre'}, {'value': 'CD19', 'label': 'Collar'}, {'value': 'CD20', 'label': 'Con anteojos'}, {'value': 'CD21', 'label': 'Cortado'}, {'value': 'CD22', 'label': 'Champaña'}, {'value': 'CD23', 'label': 'Chocolate'}, {'value': 'CD24', 'label': 'Encendido'}, {'value': 'CD25', 'label': 'Ensillado'}, {'value': 'CD26', 'label': 'Flameado'}, {'value': 'CD27', 'label': 'Fuego'}, {'value': 'CD28', 'label': 'Golondrino'}, {'value': 'CD29', 'label': 'Gris carbonado'}, {'value': 'CD30', 'label': 'Hígado'}, {'value': 'CD31', 'label': 'Isabela'}, {'value': 'CD32', 'label': 'Leonado'}, {'value': 'CD33', 'label': 'Lila'}, {'value': 'CD34', 'label': 'Manchado'}, {'value': 'CD35', 'label': 'Manteado'}, {'value': 'CD36', 'label': 'Marcado'}, {'value': 'CD37', 'label': 'Marcas de lápiz'}, {'value': 'CD38', 'label': 'Marrón'}, {'value': 'CD39', 'label': 'Máscara'}, {'value': 'CD40', 'label': 'Matices'}, {'value': 'CD41', 'label': 'Moteado'}, {'value': 'CD42', 'label': 'Naranja'}, {'value': 'CD43', 'label': 'Paja'}, {'value': 'CD44', 'label': 'Pardo'}, {'value': 'CD45', 'label': 'Particolor'}, {'value': 'CD46', 'label': 'Pincelado'}, {'value': 'CD47', 'label': 'Plateado'}, {'value': 'CD48', 'label': 'Porcelana'}, {'value': 'CD49', 'label': 'Roano'}, {'value': 'CD50', 'label': 'Rubio carbonado'}, {'value': 'CD51', 'label': 'Sal y pimienta'}, {'value': 'CD52', 'label': 'Sepia'}, {'value': 'CD53', 'label': 'Tricolor'}]


const otherColors = [{'value': 'OC1', 'label': 'Abigarrado'}, {'value': 'OC2', 'label': 'Arena'}, {'value': 'OC3', 'label': 'Atigrado'}, {'value': 'OC4', 'label': 'Blanco'}, {'value': 'OC5', 'label': 'Blue'}, {'value': 'OC6', 'label': 'Canela'}, {'value': 'OC7', 'label': 'Caoba'}, {'value': 'OC8', 'label': 'Chocolate'}, {'value': 'OC9', 'label': 'Crema'}, {'value': 'OC10', 'label': 'Dorado'}, {'value': 'OC11', 'label': 'Durazno'}, {'value': 'OC12', 'label': 'Fuego'}, {'value': 'OC13', 'label': 'Gris'}, {'value': 'OC14', 'label': 'Leonado'}, {'value': 'OC15', 'label': 'Lila'}, {'value': 'OC16', 'label': 'Marron Claro'}, {'value': 'OC17', 'label': 'Marrón'}, {'value': 'OC18', 'label': 'Naranja'}, {'value': 'OC19', 'label': 'Negro'}, {'value': 'OC20', 'label': 'Plata'}, {'value': 'OC21', 'label': 'Red Nose'}, {'value': 'OC22', 'label': 'Rojo'}]

const petCastrated = [
  {value: '1', label: 'Si'},
  {value: '0', label: 'No'}
]

const petFoods = [{'value': 'F1', 'label': 'Agility'}, {'value': 'F2', 'label': 'Biopet'}, {'value': 'F3', 'label': 'Box'}, {'value': 'F4', 'label': 'Brascorp'}, {'value': 'F5', 'label': 'Chacal'}, {'value': 'F6', 'label': 'Chasque'}, {'value': 'F7', 'label': 'Dizano'}, {'value': 'F8', 'label': 'Dog Chow'}, {'value': 'F9', 'label': 'Dog Selection'}, {'value': 'F10', 'label': 'Dogui'}, {'value': 'F11', 'label': 'Dr Cossia'}, {'value': 'F12', 'label': 'Dr Perrot'}, {'value': 'F13', 'label': 'Estampa'}, {'value': 'F14', 'label': 'Eukanuba'}, {'value': 'F15', 'label': 'Excellent'}, {'value': 'F16', 'label': 'Iams'}, {'value': 'F17', 'label': 'Infinity'}, {'value': 'F18', 'label': 'Ken-L'}, {'value': 'F19', 'label': 'Kongo'}, {'value': 'F20', 'label': 'Kume'}, {'value': 'F21', 'label': 'Mira'}, {'value': 'F22', 'label': 'MV'}, {'value': 'F23', 'label': 'Nucon'}, {'value': 'F24', 'label': 'Old Prince'}, {'value': 'F25', 'label': 'Pedigree'}, {'value': 'F26', 'label': 'Pro Plan'}, {'value': 'F27', 'label': 'Provet'}, {'value': 'F28', 'label': 'Purina'}, {'value': 'F29', 'label': 'Raza'}, {'value': 'F30', 'label': 'Rosco'}, {'value': 'F31', 'label': 'Royal Canin'}, {'value': 'F32', 'label': 'Sieger'}, {'value': 'F33', 'label': 'Smug'}, {'value': 'F34', 'label': 'The Green Dog'}, {'value': 'F35', 'label': 'Tiernitos'}, {'value': 'F36', 'label': 'Top Nutrition'}, {'value': 'F37', 'label': 'Vitalcan'}, {'value': 'F38', 'label': 'Voraz'}]

const petGenders = [
  {value: 'hembra', label: 'HEMBRA'},
  {value: 'macho', label: 'MACHO'}
]

const otherBreeds = [{'value': 'Oth', 'label': 'Otra'}]

const dogBreeds = [{'value': 'D1', 'label': 'Airedale Terrier'}, {'value': 'D2', 'label': 'Akita Inu'}, {'value': 'D3', 'label': 'Alaskan Malamute'}, {'value': 'D4', 'label': 'American Cocker Spaniel'}, {'value': 'D5', 'label': 'American Pit bull Terrier'}, {'value': 'D6', 'label': 'Australian Terrier'}, {'value': 'D7', 'label': 'Balkan Hound'}, {'value': 'D8', 'label': 'Basenji'}, {'value': 'D9', 'label': 'Basset Hound'}, {'value': 'D10', 'label': 'Beagle'}, {'value': 'D11', 'label': 'Bearded Collie'}, {'value': 'D12', 'label': 'Beauceron'}, {'value': 'D13', 'label': 'Bichón Habanero'}, {'value': 'D14', 'label': 'Bichón Maltés'}, {'value': 'D15', 'label': 'Bloodhound o Perro de San Huberto'}, {'value': 'D16', 'label': 'Bobtail'}, {'value': 'D17', 'label': 'Border Terrier'}, {'value': 'D18', 'label': 'Borzoi'}, {'value': 'D19', 'label': 'Boston Terrier'}, {'value': 'D20', 'label': 'Bóxer'}, {'value': 'D21', 'label': 'Boyero Australiano'}, {'value': 'D22', 'label': 'Boyero de Flandes'}, {'value': 'D23', 'label': 'Braco Alemán'}, {'value': 'D24', 'label': 'Braco Francés'}, {'value': 'D25', 'label': 'Bretón Español'}, {'value': 'D26', 'label': 'Bull Terrier'}, {'value': 'D27', 'label': 'Bulldog Francés'}, {'value': 'D28', 'label': 'Bulldog Inglés'}, {'value': 'D29', 'label': 'Bullmastiff'}, {'value': 'D30', 'label': 'Caniche'}, {'value': 'D31', 'label': 'Carlino'}, {'value': 'D32', 'label': 'Chart Polski'}, {'value': 'D33', 'label': 'Chihuahua'}, {'value': 'D34', 'label': 'Chin Japonés'}, {'value': 'D35', 'label': 'Chow chow'}, {'value': 'D36', 'label': 'Cimarrón Uruguayo'}, {'value': 'D37', 'label': 'Cocker Spaniel Inglés'}, {'value': 'D38', 'label': 'Collie'}, {'value': 'D39', 'label': 'Crestado Chino'}, {'value': 'D40', 'label': 'Dálmata'}, {'value': 'D41', 'label': 'Deutsch Drahthaar'}, {'value': 'D42', 'label': 'Doberman'}, {'value': 'D43', 'label': 'Dogo Alemán'}, {'value': 'D44', 'label': 'Dogo Argentino'}, {'value': 'D45', 'label': 'Dogo de Burdeos'}, {'value': 'D46', 'label': 'Fila Brasileño'}, {'value': 'D47', 'label': 'Fox Terrier'}, {'value': 'D48', 'label': 'Foxhound Inglés'}, {'value': 'D49', 'label': 'Galgo Español'}, {'value': 'D50', 'label': 'Golden Retriever'}, {'value': 'D51', 'label': "Gos D'Atura"}, {'value': 'D52', 'label': 'Grifón de Bohemia'}, {'value': 'D53', 'label': 'Hovawart'}, {'value': 'D54', 'label': 'Husky Siberiano'}, {'value': 'D55', 'label': 'Iceland Sheepdog'}, {'value': 'D56', 'label': 'Irish Wolfhound'}, {'value': 'D57', 'label': 'Jack Russell Terrier'}, {'value': 'D58', 'label': 'Kelpie Australiano'}, {'value': 'D59', 'label': 'Kuvasz'}, {'value': 'D60', 'label': 'Labrador Retriever'}, {'value': 'D61', 'label': 'Lebrel Afgano'}, {'value': 'D62', 'label': 'Lebrel Escocés'}, {'value': 'D63', 'label': 'Leonberger'}, {'value': 'D64', 'label': 'Lhasa Apso'}, {'value': 'D65', 'label': 'Mastín de los Pirineos'}, {'value': 'D66', 'label': 'Otterhound'}, {'value': 'D67', 'label': 'Pastor Alemán'}, {'value': 'D68', 'label': 'Pastor Belga'}, {'value': 'D69', 'label': 'Pastor Ganadero Australiano'}, {'value': 'D70', 'label': 'Pastor Garafiano'}, {'value': 'D71', 'label': 'Papillón'}, {'value': 'D72', 'label': 'Pequinés'}, {'value': 'D73', 'label': 'Pembroke Welsh Corgi'}, {'value': 'D74', 'label': 'Perro de Agua Español'}, {'value': 'D75', 'label': 'Perro de Agua Francés'}, {'value': 'D76', 'label': 'Perro sin Pelo Mexicano o Xoloitzcuintle'}, {'value': 'D77', 'label': 'Perro sin Pelo del Perú'}, {'value': 'D78', 'label': 'Petit Basset Griffon'}, {'value': 'D79', 'label': 'Pinscher'}, {'value': 'D80', 'label': 'Podenco Canario'}, {'value': 'D81', 'label': 'Podenco Ibicenco'}, {'value': 'D82', 'label': 'Pointer Inglés'}, {'value': 'D83', 'label': 'Pomerania'}, {'value': 'D84', 'label': 'Presa Canario'}, {'value': 'D85', 'label': 'Puli'}, {'value': 'D86', 'label': 'Ratón Bodeguero Andaluz'}, {'value': 'D87', 'label': 'Retriever de pelo rizado'}, {'value': 'D88', 'label': 'Rottweiler'}, {'value': 'D89', 'label': 'San Bernardo'}, {'value': 'D90', 'label': 'Samoyedo'}, {'value': 'D91', 'label': 'Schnauzer'}, {'value': 'D92', 'label': 'Scottish Terrier'}, {'value': 'D93', 'label': 'Setter Irlandés'}, {'value': 'D94', 'label': 'Shar Pei'}, {'value': 'D95', 'label': 'Shetland Sheepdog'}, {'value': 'D96', 'label': 'Shih Tzu'}, {'value': 'D97', 'label': 'Spinone italiano'}, {'value': 'D98', 'label': 'Teckel'}, {'value': 'D99', 'label': 'Terranova'}, {'value': 'D100', 'label': 'Terrier Australiano'}, {'value': 'D101', 'label': 'Terrier Checo'}, {'value': 'D102', 'label': 'Terrier Japonés'}, {'value': 'D103', 'label': 'Terrier Tibetano'}, {'value': 'D104', 'label': 'Tosa Inu'}, {'value': 'D105', 'label': 'Weimaraner'}, {'value': 'D106', 'label': 'West Highland White Terrier'}, {'value': 'D107', 'label': 'Yorkshire Terrier'}]

const catBreeds = [{'value': 'C1', 'label': 'Abisinio'}, {'value': 'C2', 'label': 'American Curl'}, {'value': 'C3', 'label': 'Azul ruso'}, {'value': 'C4', 'label': 'American shorthair'}, {'value': 'C5', 'label': 'American wirehair'}, {'value': 'C6', 'label': 'Angora turco'}, {'value': 'C7', 'label': 'Africano doméstico'}, {'value': 'C8', 'label': 'Bengala'}, {'value': 'C9', 'label': 'Bobtail japonés'}, {'value': 'C10', 'label': 'Bombay'}, {'value': 'C11', 'label': 'Bosque de Noruega'}, {'value': 'C12', 'label': 'Brazilian Shorthair'}, {'value': 'C13', 'label': 'Brivon de pelo corto'}, {'value': 'C14', 'label': 'Brivon de pelo largo'}, {'value': 'C15', 'label': 'British Shorthair'}, {'value': 'C16', 'label': 'Burmés'}, {'value': 'C17', 'label': 'Burmilla'}, {'value': 'C18', 'label': 'Cornish rex'}, {'value': 'C19', 'label': 'California Spangled'}, {'value': 'C20', 'label': 'Ceylon'}, {'value': 'C21', 'label': 'Cymric'}, {'value': 'C22', 'label': 'Chartreux'}, {'value': 'C23', 'label': 'Deutsch Langhaar'}, {'value': 'C24', 'label': 'Devon rex'}, {'value': 'C25', 'label': 'Dorado africano'}, {'value': 'C26', 'label': 'Don Sphynx'}, {'value': 'C27', 'label': 'Dragon Li'}, {'value': 'C28', 'label': 'Europeo Común'}, {'value': 'C29', 'label': 'Exótico de Pelo Corto'}, {'value': 'C30', 'label': 'Gato europeo bicolor'}, {'value': 'C31', 'label': 'FoldEx'}, {'value': 'C32', 'label': 'German Rex'}, {'value': 'C33', 'label': 'Habana brown'}, {'value': 'C34', 'label': 'Himalayo'}, {'value': 'C35', 'label': 'Korat'}, {'value': 'C36', 'label': 'Khao Manee'}, {'value': 'C37', 'label': 'Maine Coon'}, {'value': 'C38', 'label': 'Manx'}, {'value': 'C39', 'label': 'Mau egipcio'}, {'value': 'C40', 'label': 'Munchkin'}, {'value': 'C41', 'label': 'Ocicat'}, {'value': 'C42', 'label': 'Oriental'}, {'value': 'C43', 'label': 'Oriental de pelo largo'}, {'value': 'C44', 'label': 'Ojos azules'}, {'value': 'C45', 'label': 'PerFold1​'}, {'value': 'C46', 'label': 'Persa Americano o Moderno'}, {'value': 'C47', 'label': 'Persa Clásico o Tradicional2​'}, {'value': 'C48', 'label': 'Peterbald'}, {'value': 'C49', 'label': 'Pixie Bob'}, {'value': 'C50', 'label': 'Ragdoll'}, {'value': 'C51', 'label': 'Sagrado de Birmania'}, {'value': 'C52', 'label': 'Scottish Fold'}, {'value': 'C53', 'label': 'Selkirk rex'}, {'value': 'C54', 'label': 'Serengeti'}, {'value': 'C55', 'label': 'Seychellois'}, {'value': 'C56', 'label': 'Siamés'}, {'value': 'C57', 'label': 'Siamés Moderno'}, {'value': 'C58', 'label': 'Siamés Tradicional'}, {'value': 'C59', 'label': 'Siberiano'}, {'value': 'C60', 'label': 'Snowshoe'}, {'value': 'C61', 'label': 'Sphynx'}, {'value': 'C62', 'label': 'Tonkinés'}, {'value': 'C63', 'label': 'Van Turco'}]
