import R from 'ramda'

export const findOne = (id) => {
  return findAll().find((item) => item.name === id)
}

export const findAll = () : Array => {
  return [
    {
      name: 'lost',
      label: 'Perdid',
      color: '#0EE7C9',
      iconImage: require('../Images/Categories/veterinaria.png'),
      starredImage: require('../Images/Categories/veterinariaStar.png'),
      mapMarker: require('../Images/Categories/veterinariaMarker.png'),
      mapMarkerSelected: require('../Images/Categories/veterinariaMarkerSelected.png')
    },
    {
      name: 'found',
      label: 'Encontrad',
      color: '#8F5827',
      iconImage: require('../Images/Categories/peluquerias.png'),
      starredImage: require('../Images/Categories/peluqueriasStar.png'),
      mapMarker: require('../Images/Categories/peluqueriasMarker.png'),
      mapMarkerSelected: require('../Images/Categories/peluqueriasMarkerSelected.png')
    }
  ]
}

export const getTypeText = (report) => {
  let text = (report.reportType == 'lost') ? 'Perdid' : 'Encontrad'
  text += (report.gender == 'macho') ? 'o' : 'a'
  return text
}

export default {findOne, findAll, getTypeText}
