export type Vaccine = {
  id: number,
  name: string,
  description: string
}

export const findOne = (id:number) : Vaccine => {
  return findAll().find((item) => item.id === id)
}

export const findAll = () : Array<Vaccine> => {
  return [
    {
      id: 1,
      name: 'Quíntuple',
      description: 'Combinación de antígenos contra el Distémper o Moquillo, la Hepatitis y tos de las perreras (ambas causadas por Adenovirus), la Parainfluenza, la Parvovirosis y contra el Coronavirus.'
    }, {

      id: 2,
      name: 'Séxtuple',
      description: 'Protege contra Distémper, Parvovirus, Parainfluenza, Adenovirus II, Hepatitis Infecciosa (Adeno I) y Leptospira.'
    }, {

      id: 3,
      name: 'Antirrábica',
      description: 'Brinda protección contra el virus causante de la rabia.'
    }, {

      id: 4,
      name: 'Trivalente',
      description: 'En el caso de gatos, protege frente a Rinotraqueítis, Calicivirus y Panleucopenia. En los perros, la vacuna trivalente puede ser para Moquillo, Hepatitis y Leptospira; o para Moquillo, Hepatitis y Parvovirus.'
    }, {

      id: 5,
      name: 'Leucemia felina',
      description: 'Protege contra el retrovirus VLFe (FeLV en inglés) que se transmite entre los gatos infectados a través de la saliva o las secreciones nasales. '
    }, {

      id: 6,
      name: 'Bordetella',
      description: 'Vacuna contra la comúnmente llamada tos de las perreras, enfermedad respiratoria contagiosa.'
    }
  ]
}
