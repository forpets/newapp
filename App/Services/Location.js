import { eventChannel, END } from 'redux-saga'
import { PermissionsAndroid } from 'react-native'
import { call } from 'redux-saga/effects'

const { geolocation } = navigator
const WATCH_POSITION_OPTIONS = {
  enableHighAccuracy: true,
  timeout: 10000,
  maximumAge: 1000,
  distanceFilter: 10,
  useSignificantChanges: true
}

// Define the function to open the location listener.
export const locationChangeChannel = () => {
  // Return the event channel.
  return eventChannel((emit) => {
    const onError = (error) => {
      console.tron.display({name: 'locationChangeChannel', preview: error, value: {error, WATCH_POSITION_OPTIONS}, important: true})
      // Close the channel after any errors in `watchPosition`.
      return emit(END)
    }

    // Invokes the `emit` callback whenever the location changes.
    const watchId = geolocation.watchPosition(emit, onError, WATCH_POSITION_OPTIONS)

    // The `eventChannel` call should return the unsubscribe
    // function, this will stop watching of the location of the user.
    return () => navigator.geolocation.clearWatch(watchId)
  })
}

export function getCurrentPosition () {
  return new Promise((resolve, reject) => {
    geolocation.getCurrentPosition((position) => {
      resolve(position)
    }, (error) => {
      reject(new Error('getCurrentPosition error: ', JSON.stringify(error)))
    })
  })
}

export function * requestLocationAccess () : boolean {
  try {
    const granted = yield call(PermissionsAndroid.request,
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Permitir a ForPets acceder a su ubicación?',
        'message': 'Active su GPS para que podamos mostrarle los espacios cercanos a su ubicación' }
      )

    // Granted === true in older androids
    if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
      return true
    } else {
      return false
    }
  } catch ({code, message}) {
    console.tron.display({name: 'requestLocationAccess', preview: message, value: {code, message}, important: true})
    return false
  }
}

export default {locationChangeChannel, getCurrentPosition, requestLocationAccess}
