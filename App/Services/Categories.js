// @flow
import type {PlaceCategory} from '../FlowTypes/PlaceCategory'
import R from 'ramda'

export const findOne = (id:number) : PlaceCategory => {
  return findAll().find((item) => item.value === id)
}

// Attention!
// If you are going to add more categories:
// 1. update firestore.rules
// 2. update ValidationRules
// 3. Make sure to add images
// 4. Update cloud functions/index.js

export const findAll = () : Array<PlaceCategory> => {
  return [
    {value: 1,
      label: 'Veterinaria',
      color: '#0EE7C9',
      iconImage: require('../Images/Categories/veterinaria.png'),
      starredImage: require('../Images/Categories/veterinariaStar.png'),
      mapMarker: require('../Images/Categories/veterinariaMarker.png'),
      mapMarkerSelected: require('../Images/Categories/veterinariaMarkerSelected.png')
    },
    {value: 2,
      label: 'Peluquería',
      color: '#8F5827',
      iconImage: require('../Images/Categories/peluquerias.png'),
      starredImage: require('../Images/Categories/peluqueriasStar.png'),
      mapMarker: require('../Images/Categories/peluqueriasMarker.png'),
      mapMarkerSelected: require('../Images/Categories/peluqueriasMarkerSelected.png')

    },
    {value: 3,
      label: 'Paseadores',
      color: '#FFCC00',
      iconImage: require('../Images/Categories/paseadores.png'),
      starredImage: require('../Images/Categories/paseadoresStar.png'),
      mapMarker: require('../Images/Categories/paseadoresMarker.png'),
      mapMarkerSelected: require('../Images/Categories/paseadoresMarkerSelected.png')
    },
    {value: 4,
      label: 'Alojamientos',
      color: '#010474',
      iconImage: require('../Images/Categories/alojamientos.png'),
      starredImage: require('../Images/Categories/alojamientosStar.png'),
      mapMarker: require('../Images/Categories/alojamientosMarker.png'),
      mapMarkerSelected: require('../Images/Categories/alojamientosMarkerSelected.png')
    },
    {value: 5,
      label: 'Entrenadores',
      color: '#F87100',
      iconImage: require('../Images/Categories/entrenadores.png'),
      starredImage: require('../Images/Categories/entrenadoresStar.png'),
      mapMarker: require('../Images/Categories/entrenadoresMarker.png'),
      mapMarkerSelected: require('../Images/Categories/entrenadoresMarkerSelected.png')
    },
    {value: 6,
      label: 'Refugio',
      color: '#0099FF',
      iconImage: require('../Images/Categories/refugios.png'),
      starredImage: require('../Images/Categories/refugiosStar.png'),
      mapMarker: require('../Images/Categories/refugiosMarker.png'),
      mapMarkerSelected: require('../Images/Categories/refugiosMarkerSelected.png')
    },
    {value: 7,
      label: 'Guarderías',
      color: '#8E29D2',
      iconImage: require('../Images/Categories/guarderias.png'),
      starredImage: require('../Images/Categories/guarderiasStar.png'),
      mapMarker: require('../Images/Categories/guarderiasMarker.png'),
      mapMarkerSelected: require('../Images/Categories/guarderiasMarkerSelected.png')
    },
    {value: 8,
      label: 'Cuidadores',
      color: '#F74596',
      iconImage: require('../Images/Categories/cuidadores.png'),
      starredImage: require('../Images/Categories/cuidadoresStar.png'),
      mapMarker: require('../Images/Categories/cuidadoresMarker.png'),
      mapMarkerSelected: require('../Images/Categories/cuidadoresMarkerSelected.png')
    },
    {value: 9,
      label: 'Restaurants/Bares',
      color: '#B5ACA0',
      iconImage: require('../Images/Categories/restaurantes_bares.png'),
      starredImage: require('../Images/Categories/restaurants_baresStar.png'),
      mapMarker: require('../Images/Categories/restaurants_baresMarker.png'),
      mapMarkerSelected: require('../Images/Categories/restaurants_baresMarkerSelected.png')
    },
    {value: 10,
      label: 'Pet Shops',
      color: '#EE5034',
      iconImage: require('../Images/Categories/petshops.png'),
      starredImage: require('../Images/Categories/petshopsStar.png'),
      mapMarker: require('../Images/Categories/petshopsMarker.png'),
      mapMarkerSelected: require('../Images/Categories/petshopsMarkerSelected.png')
    },
    {value: 11,
      label: 'Plazas',
      color: '#63CC53',
      iconImage: require('../Images/Categories/plazas.png'),
      starredImage: require('../Images/Categories/plazasStar.png'),
      mapMarker: require('../Images/Categories/plazasMarker.png'),
      mapMarkerSelected: require('../Images/Categories/plazasMarkerSelected.png')
    }
  ]
}

export const idsToString = (ids : Array<number>) : string => {
  const categories = R.map(R.prop('label'), R.map(findOne, ids))
  return R.join(', ', categories)
}

export const idToString = (id : number, params = {}) : string => {
  const cat = findOne(id)
  const {uppercase} = params

  let label = cat ? cat.label : ''
  if (uppercase) {
    label = label.toUpperCase()
  }
  return label
}

export default {findOne, findAll, idsToString, idToString}
