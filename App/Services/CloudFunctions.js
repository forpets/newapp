import apisauce from 'apisauce'

const create = (baseURL = 'https://us-central1-dazzling-torch-8669.cloudfunctions.net/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })

  const filterPlaces = (filter, location) => api.post('filterPlaces', {filter, location, debug: __DEV__})
  const searchPlaces = (filter, location, search) => api.post('filterPlaces', {filter, location, search, debug: __DEV__})

  const filterLostFound = (filter, location) => api.post('filterLostFound', {filter, location, debug: __DEV__})

  return {
    filterPlaces,
    searchPlaces,
    filterLostFound
  }
}

export default {
  create
}
