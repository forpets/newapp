// @flow
import geolib from 'geolib'
import type {Place} from '../FlowTypes/Place'

export function getDistanceToPlace (place : Place, currentLocation : Location) : number {
  const location1 = getCoordsFromPlace(place)
  const location2 = extractLocation(currentLocation)

  if (!location1 || !location2) {
    console.tron.display({
      name: 'getDistanceToPlace',
      preview: 'Trying to get distance to Place without proper location data.',
      value: {location2, place, location1},
      important: true})
    return null
  }
  const meters = geolib.getDistance(location1, location2, 100)
  return Math.round(meters / 100) / 10 // Show kilometers 0.0
}

export function getCoordsFromPlace (place : Place) : Location {
  if (!place.location) {
    console.tron.error('getCoordsFromPlace: Received place with empty location data. <Place>:' + JSON.stringify(place))
    return null
  }

  const [latitude, longitude] = place.location
  if (!longitude || !latitude) {
    console.tron.error('getCoordsFromPlace: Received place with malformed location data. <Place>:' + JSON.stringify(place))
    return null
  }
  return {longitude, latitude}
}

export function extractLocation (location) {
  if (!location) {
    // console.tron.error('extractLocation: Received falsy location. <Location>:' + JSON.stringify(location))
    return null
  }
  const { latitude, longitude } = location
  if (!longitude || !latitude) {
    // console.tron.error('extractLocation: Received place with malformed location data. <Location>:' + JSON.stringify(location))
    return null
  }

  return {latitude, longitude}
}

export function isPlaceWithinRadius (place : Place, currentLocation : Location, radius : number) : boolean {
  const location1 = getCoordsFromPlace(place)
  const location2 = extractLocation(currentLocation)

  if (!location1 || !location2) {
    console.tron.display({
      name: 'isPlaceWithinRadius',
      preview: 'Trying to get distance to Place without proper location data.',
      value: {location2, place, location1, radius},
      important: true})
    return false
  }

  return geolib.isPointInCircle(location1, location2, radius)
}

export default {getDistanceToPlace}
