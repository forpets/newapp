import R from 'ramda'

export default function (schedule) {
  if (!schedule || R.isEmpty(schedule)) {
    return ''
  }

  const {hours, h24} = schedule
  const days = [...schedule.days].sort() // Immutable
  const daysArr = ['', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb', 'Dom']
  const numToDay = (int) => daysArr[int]
  const daysLabel = R.join(', ', R.map(numToDay, days))
  let hoursLabel = ''

  if (h24) {
    hoursLabel = '00:00-24:00'
  } else if (hours && R.length(hours) === 2) {
    hoursLabel = `${hours[0]}-${hours[1]}`
  }
  return daysLabel + ' ' + hoursLabel
}
