// @flow

export const stringToDate = (text : string) : Date => {
  if (!text) return null

  let birthdateSplited = text.split('/')

  let day = parseInt(birthdateSplited[0])
  let month = parseInt(birthdateSplited[1])
  let year = parseInt(birthdateSplited[2])

  return new Date(year, month, day)
}

export const dateToString = (date : Date) : string => {
  var mm = date.getMonth() + 1 // getMonth() is zero-based
  var dd = date.getDate()

  return [
    // (dd > 9 ? '' : '0') + dd,
    // (mm > 9 ? '' : '0') + mm,
    dd, mm, date.getFullYear()
  ].join('/')
}
