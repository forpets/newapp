// leave off @2x/@3x
const images = {
  boneDog: require('../Images/fondohueso.png'),
  headerLogo: require('../Images/headerLogo.png'),
  homeIcon: require('../Images/Menu/menu_inicio.png'),
  menuDirectorio: require('../Images/Menu/menu_directorio.png'),
  menuLostFound: require('../Images/Menu/menu_pye.png'),
  menuMascotas: require('../Images/Menu/menu_mascotas.png'),
  menuNotas: require('../Images/Menu/menu_notas.png'),
  menuMiPerfil: require('../Images/Menu/menu_mi_perfil.png'),
  menuAcercaDe: require('../Images/Menu/menu_acerca_de.png'),
  menuTyc: require('../Images/Menu/menu_tyc.png'),
  menuCerrarSesion: require('../Images/Menu/menu_cerrar_sesión.png'),
  menuCrearEspacio: require('../Images/Menu/crear_espacio.png'),
  logoLogin: require('../Images/Icons/logo_login.png'),
  fb: require('../Images/Icons/facebook.png'),
  // backButton: require('../Images/Icons/back-button.png'),
  // closeButton: require('../Images/Icons/close-button.png'),

  petsEmpty: require('../Images/empty_mascotas.png'),
  petsDefaultImage: require('../Images/default_pet.jpg'),
  notesEmpty: require('../Images/empty_notas.png'),
  homeBackground: require('../Images/homeBackground.jpg'),

  loginBackground: require('../Images/fondo_huesos.png'),

  login: {
    lock: require('../Images/lock_login.png')
  },

  pets: {
    medical: require('../Images/Mascotas/mascotas_info_medica.png'),
    vaccines: require('../Images/Mascotas/mascotas_info_vacunas.png'),
    photos: require('../Images/Mascotas/mascotas_fotos.png'),
    about: require('../Images/Mascotas/mascotas_sobre.png'),
    noPets: require('../Images/Mascotas/mascotas_empty.png'),
    uploadPhoto: require('../Images/Mascotas/mascotas_card_imagen.png')
  },
  directories: {
    vetBagEmergency: require('../Images/Directories/logo_veterinarias.png'),
    anuncio: require('../Images/Directories/anuncio-white.png')
  },
  directorio: {
    star: require('../Images/Directorio/directorio_star.png'), // FIXME: remove
    reloj: require('../Images/Directorio/directorio_reloj_abierto.png'),
    sign1: require('../Images/Directorio/directorio_comentarios_pesos_white_1.png'),
    sign2: require('../Images/Directorio/directorio_comentarios_pesos_white_2.png'),
    sign3: require('../Images/Directorio/directorio_comentarios_pesos_white_3.png'),

    currencyFilter1: require('../Images/Directorio/directorio_filtro_precios_1.png'),
    currencyFilter2: require('../Images/Directorio/directorio_filtro_precios_2.png'),
    currencyFilter3: require('../Images/Directorio/directorio_filtro_precios_3.png'),

    anuncio: require('../Images/Directorio/destacado.png'),
    galeriaAdd: require('../Images/Directorio/crear_galeria.png'),
    emergencia: require('../Images/Directorio/directorio_individual_emergencia.png'),
    editarEspacio: require('../Images/Directorio/editar_espacio.png'),
    history: require('../Images/Directorio/latest_search.png'),
    thanksForTheComments: require('../Images/Directorio/directorio_comentarios_gracias.png'), // FIXME: remove
    placeNoPhoto: require('../Images/Directorio/placeNoPhotoPlaceholder.png'),
    locationFilter: require('../Images/Directorio/location_filter.png'),
    relevanceFilter: require('../Images/Directorio/relevance_filter.png'),
    locationFilterDisable: require('../Images/Directorio/location_filter_disable.png'),
    relevanceFilterDisable: require('../Images/Directorio/relevance_filter_disable.png'),
    locationGuideBlack: require('../Images/Directorio/location_guide_black.png')
  },
  crearEspacio: {
    on: require('../Images/Directorio/directorio_filtro_emergencia_full.png'),
    off: require('../Images/Directorio/directorio_filtro_emergencia_empty.png'),
    success: require('../Images/Directorio/success_rojo.png')
  },

  notes: {
    empty: require('../Images/Notas/notes_empty.png'),
    trash: require('../Images/Notas/notas_trash.png'),
    photos: require('../Images/Notas/photos.png'),
    step: require('../Images/Notas/step.png')
  },
  markers: {
   
  },
  avatar: require('../Images/avatar.png'),
  white: require('../Images/white.jpg')
}

export default images
