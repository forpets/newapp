import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  noteCreateRequest: ['petId', 'data'],
  noteUpdateRequest: ['id', 'petId', 'data', 'oldNote'],
  noteSaveSuccess: ['payload'],
  noteSaveFailure: null
})

export const NoteSaveTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  petId: null,
  id: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

export const request = (state, { petId, data }) =>
  state.merge({ fetching: true, data, petId, payload: null, id: null })

export const update = (state, { id, petId, data, oldNote }) =>
  state.merge({ fetching: true, data, id, petId, payload: null })

export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

export const failure = state =>
  state.merge({ fetching: false, error: true, petId: null, id: null, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NOTE_CREATE_REQUEST]: request,
  [Types.NOTE_UPDATE_REQUEST]: request,
  [Types.NOTE_SAVE_SUCCESS]: success,
  [Types.NOTE_SAVE_FAILURE]: failure
})
