// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { getPet } from './PetsRedux'
import R from 'ramda'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  vaccineSaveRequest: ['data', 'petId'],
  vaccineUpdateRequest: ['data', 'petId', 'petVaccineId'],
  vaccineSaveSuccess: ['payload'],
  vaccineSaveFailure: null
})

export const VaccineSaveTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data, petId }) =>
  state.merge({ fetching: true, data, payload: null })

export const updateRequest = (state, { data, petId, petVaccineId }) => {
  console.tron.warn({data, petId, petVaccineId})
  return state.merge({ fetching: true, data, payload: null })
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VACCINE_SAVE_REQUEST]: request,
  [Types.VACCINE_UPDATE_REQUEST]: updateRequest,
  [Types.VACCINE_SAVE_SUCCESS]: success,
  [Types.VACCINE_SAVE_FAILURE]: failure
})

// ------------------ Selectors ----------------
export const getVaccinesArray = (state : State, petId : number) : Array<Vaccine> => {
  const pet = getPet(state, petId)
  if (!pet) return []
  if (!pet.vaccines) return []
  // console.tron.warn({pet})

  const vaccines = pet.vaccines.asMutable()
  const vaccinesArray = []
  const length = Object.keys(vaccines).length
  // console.tron.warn({vaccines})

  if (length > 0) {
    const iterator = (val, key) => { vaccinesArray.push({...val, id: key, petId}) }
    R.forEachObjIndexed(iterator, vaccines)
  }

  return vaccinesArray
}

export const getVaccine = (state: State, petId: number, petVaccineId: number) => {
  // const pet = getPet(state, petId)
  // if (!pet) return null
  // const {vaccines = {}} = pet
  // return vaccines[petVaccineId]
  const vaccines = getVaccinesArray(state, petId)
  if (!vaccines) return null
  return vaccines.find((item) => item.id === petVaccineId)
}
