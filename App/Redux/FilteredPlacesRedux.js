import { createReducer, createActions } from 'reduxsauce'
import AppConfig from '../Config/AppConfig'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  filteredPlacesRequest: null,
  filteredPlacesApply: ['filter'],
  filteredPlacesApplyDefault: null,
  filteredPlacesSuccess: ['payload'],
  filteredPlacesFailure: null,
  filteredPlacesSetMeta: [ 'coords' ]
  // filteredPlacesStartFetching: null
})

export const FilteredPlacesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  payload: null,
  error: null,
  coords: null,
  filter: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) => state.merge({fetching: true})

export const apply = (state, { filter }) => state.merge({ fetching: true, filter })

export const startFetching = (state) => state.merge({fetching: true})

export const applyDefault = (state) => {
  const filter = AppConfig.defaultFilter
  return state.merge({ fetching: true, filter, payload: null })
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const setMeta = (state, {coords}) =>
  state.merge({coords})

// export const filterByLocation = (state, {coords}) => state.merge({fetching: true, error: null, coords})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FILTERED_PLACES_REQUEST]: request,
  [Types.FILTERED_PLACES_APPLY]: apply,
  [Types.FILTERED_PLACES_SUCCESS]: success,
  [Types.FILTERED_PLACES_FAILURE]: failure,
  [Types.FILTERED_PLACES_APPLY_DEFAULT]: applyDefault,
  [Types.FILTERED_PLACES_SET_META]: setMeta
  // [Types.FILTERED_PLACES_START_FETCHING]: startFetching
})

/* --------------- Selectors -------------- */

export const getFilteredPlaces = (state) => {
  return state.filteredPlaces.payload || []
}

export const getFilteredPlacesFetching = (state) => {
  return state.filteredPlaces.fetching
}

export const getPlaceFilter = (state) => {
  return state.filteredPlaces.filter
}
