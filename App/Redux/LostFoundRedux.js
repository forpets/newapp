import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  reportCreateRequest: ['data', 'imagePickerResponse'],
  reportUpdateRequest: ['data', 'imagePickerResponse', 'id'],
  reportDeleteRequest: ['data', 'id'],
  reportPhotoUploadRequest: ['imagePickerResponse', 'id'],
  reportFetchRequest: null,
  reportFetchSuccess: ['data'],
  reportSaveSuccess: ['payload'],
  reportSaveFailure: null
})

export const LostFoundTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  reports: {},
  photoData: null,
  data: null,
  fetching: null,
  payload: null,
  error: null,
  id: null
})

/* ------------- Reducers ------------- */

export const createReport = (state, { data }, imagePickerResponse) =>
  state.merge({ fetching: true, data, payload: null })

export const updateReport = (state, { data }, imagePickerResponse, id) =>
  state.merge({ fetching: true, data, id, payload: null })

export const deleteReport = (state, { data }, id) =>
  state.merge({ fetching: true, data, id, payload: null })

export const uploadReportPhoto = (state, { data }) =>
  state.merge({ fetching: true, photoData: data, payload: null })

export const fetchReports = (state, { data }) =>
  state.merge({ fetching: true, reports: {} })

// successful api fetch
export const successFetch = (state, action) => {
  const { data } = action
  const reports = data.payload ? data.payload : {}
  return state.merge({ fetching: false, error: null, reports })
}

// successful api CRUD
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload, id: null })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REPORT_CREATE_REQUEST]: createReport,
  [Types.REPORT_UPDATE_REQUEST]: updateReport,
  [Types.REPORT_DELETE_REQUEST]: deleteReport,
  [Types.REPORT_PHOTO_UPLOAD_REQUEST]: uploadReportPhoto,
  [Types.REPORT_FETCH_SUCCESS]: successFetch,
  [Types.REPORT_FETCH_FAILURE]: failure,
  [Types.REPORT_SAVE_SUCCESS]: success,
  [Types.REPORT_SAVE_FAILURE]: failure
})

/* ------------- Selectors ------------- */

export const getReport = (state, id) : Object => {
  if (!(state.lostFound && state.lostFound.reports)) return null
  if (!id) return null
  // console.tron.log({state, id})
  const report = state.lostFound.reports[id]
  // console.tron.log({report})
  return {...report, id} || null
}