import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import R from 'ramda'
import { getFilteredPlaces } from './FilteredPlacesRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placeSearchRequest: ['searchTerm'],
  placeSearchSuccess: ['results'],
  placeSearchFailure: ['error'],
  placeSearchClear: null,
  placeSearchBarShow: null,
  placeSearchBarHide: null,
  placeSearchRerun: null
})

export const PlaceSearchTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  results: [],
  searchTerm: null,
  fetching: null,
  error: null,
  searchHistory: [],
  showSearchBar: false
})

/* ------------- Reducers ------------- */

export const request = (state, { searchTerm }) => {
  return state.merge({ fetching: true, searchTerm }) // Avoid flickers
  // return state.merge({ fetching: true, searchTerm, results: [] })
}

export const success = (state, action) => {
  const { results } = action
  return state.merge({ fetching: false, error: null, results })
}

export const failure = (state, {error}) =>
  state.merge({ fetching: false, error, results: [] })

// Clears search - updates history
export const clear = (state) => {
  const searchHistory = rotateHistory(state)
  return state.merge({
    fetching: false,
    error: null,
    results: [], 
    searchTerm: null,
    searchHistory})
}

export const showSearchBar = (state) => state.merge({showSearchBar: true})
export const hideSearchBar = (state) => state.merge({showSearchBar: false})
export const rerun = (state) => state
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACE_SEARCH_REQUEST]: request,
  [Types.PLACE_SEARCH_SUCCESS]: success,
  [Types.PLACE_SEARCH_FAILURE]: failure,
  [Types.PLACE_SEARCH_CLEAR]: clear,
  [Types.PLACE_SEARCH_BAR_SHOW]: showSearchBar,
  [Types.PLACE_SEARCH_BAR_HIDE]: hideSearchBar,
  [Types.PLACE_SEARCH_RERUN]: rerun
})

/* --------------- Selectors ----------------- */
export const getSearchActive = (state) => {
  // return (!R.isEmpty(state.placesSearch))
  return Boolean(state.placeSearch.searchTerm)
}

export const getSearchFinished = (state) =>
  Boolean(state.placeSearch.searchTerm && !state.placeSearch.fetching)

export const getPlaceSearchFetching = (state) =>
  state.placeSearch.fetching

export const getCurrentSearchTerm = (state) => {
  return state.placeSearch.searchTerm
}

export const getSearchHistory = (state) => {
  return state.placeSearch.searchHistory
}

export const getShowSearchBar = (state) => state.placeSearch.showSearchBar

export const getSearchedPlaces = (state) => state.placeSearch.results

/* ---------------- Helper func ------------------- */
const rotateHistory = (state) => {
  const {searchTerm, searchHistory} = state
  const history = searchHistory.asMutable()
  if (history.length >= 3) { history.pop() }
  history.unshift(searchTerm)
  return Immutable(history)
}
