import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import R from 'ramda'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  petsRequest: null,
  petsSuccess: ['data'],
  petsFailure: null,
  petsNoUser: null
})

export const PetsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ fetching: true, data: {} })

// successful api lookup
export const success = (state, action) => {
  const { data } = action
  const pets = data.payload ? data.payload : {}
  return state.merge({ fetching: false, error: null, data: pets })
}
export const noUser = (state, action) => {
  return state.merge({ fetching: false, error: null, data: null })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, data: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PETS_REQUEST]: request,
  [Types.PETS_SUCCESS]: success,
  [Types.PETS_FAILURE]: failure,
  [Types.PETS_NO_USER]: noUser
})

// Selectors
export const getPetsArray = (state) : Array<Pet> => {
  if (!(state.pets && state.pets.data)) return []
  const pets = state.pets.data.asMutable()
  const length = Object.keys(pets).length

  if (length > 0) {
    let petList = []

    const iterator = (val, key) => { petList.push({...val, id: key}) }
    R.forEachObjIndexed(iterator, pets)
    return petList
  }
  return []
}

export const getPet = (state, id) : Object => {
  if (!(state.pets && state.pets.data)) return null
  if (!id) return null
  // console.tron.log({state, id})
  const pet = state.pets.data[id]
  // console.tron.log({pet})
  return {...pet, id} || null
}

export const getIsFetching = (state) => (state.pets.fetching) 
