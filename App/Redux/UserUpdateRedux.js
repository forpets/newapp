import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  userUpdateRequest: ['data', 'imagePickerResponse', 'uid'],
  userUpdateSuccess: ['payload'],
  userUpdateFailure: null
})

export const UserUpdateTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { user } = action.payload
  let userClean = user ? user.toJSON() : null
  return state.merge({ fetching: false, error: null, payload: userClean })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_UPDATE_REQUEST]: request,
  [Types.USER_UPDATE_SUCCESS]: success,
  [Types.USER_UPDATE_FAILURE]: failure
})
