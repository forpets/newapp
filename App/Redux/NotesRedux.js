import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { getPetsArray } from './PetsRedux'
import R from 'ramda'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  notesRequest: ['data'],
  notesSuccess: ['payload'],
  notesFailure: null
})

export const NotesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NOTES_REQUEST]: request,
  [Types.NOTES_SUCCESS]: success,
  [Types.NOTES_FAILURE]: failure
})

// ------------ Selectors

export const getNotesArray = (state) : Array<Note> => {
  if (!state) return null
  const pets = getPetsArray(state)
  if (!pets) return null

  const resultNotes = []

  // Iterate all pets
  pets.forEach((pet) => {
    const {notes} = pet
    const length = (notes && typeof notes === 'object') ? Object.keys(notes).length : 0

    if (length > 0) {
      // Iterate all notes
      R.forEachObjIndexed((val, key) => {
        const item = {...val, petName: pet.name, petId: pet.id, id: key, petImage: pet.image}
        resultNotes.push(item)
      }
      , notes)
    }
  })

  return resultNotes
}

export const getNote = (state, id) : Note => {
  if (!id) return null
  const notes = getNotesArray(state)
  if (notes.length === 0) return null

  return notes.find((item) => item.id === id)
}
