import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  allUsersRequest: ['data'],
  allUsersSuccess: ['payload'],
  allUsersFailure: null
})

export const AllUsersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: {},
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
// !!! DO NOT CLEAR PAYLOAD
export const request = (state, { data }) =>
  state.merge({ fetching: true, data })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ALL_USERS_REQUEST]: request,
  [Types.ALL_USERS_SUCCESS]: success,
  [Types.ALL_USERS_FAILURE]: failure
})

// --------------- selectors
export const getAllUsersObject = (state) => {
  return state.allUsers.payload
  // return (state.allUsers && state.allUsers.payload) ? state.allUsers.payload : {}
}
