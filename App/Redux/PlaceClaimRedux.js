import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placeClaimRequest: ['data'],
  placeClaimSuccess: ['payload'],
  placeClaimFailure: null
})

export const PlaceClaimTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, error: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACE_CLAIM_REQUEST]: request,
  [Types.PLACE_CLAIM_SUCCESS]: success,
  [Types.PLACE_CLAIM_FAILURE]: failure
})

/* --------------- Selectors ------ */
export const getPlaceClaimError = (state) => {
  return (state.placeClaim.error)
}