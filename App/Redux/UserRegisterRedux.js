import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  userRegisterRequest: ['data', 'imagePickerResponse'],
  userRegisterSuccess: ['payload'],
  userRegisterFailure: ['error']
})

export const UserRegisterTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }, imagePickerResponse) =>
  state.merge({ fetching: true, data, payload: null, error: null })

// successful api lookup
export const success = (state, action) => {
  const { user } = action.payload
  let userClean = user ? user.toJSON() : null
  return state.merge({ fetching: false, error: null, payload: userClean })
}

// Something went wrong somewhere.
export const failure = (state, action) => {
  const {error} = action
  return state.merge({ fetching: false, error, payload: null })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_REGISTER_REQUEST]: request,
  [Types.USER_REGISTER_SUCCESS]: success,
  [Types.USER_REGISTER_FAILURE]: failure
})
