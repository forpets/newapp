import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { getPet } from './PetsRedux'
import R from 'ramda'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  petAlbumsRequest: ['data'],
  petAlbumsSuccess: ['payload'],
  petAlbumsFailure: null,
})

export const PetAlbumsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PET_ALBUMS_REQUEST]: request,
  [Types.PET_ALBUMS_SUCCESS]: success,
  [Types.PET_ALBUMS_FAILURE]: failure
})

// --------------- Selectors

export const getAlbumForPet = (state : State, id) => {
  if (!id) return null
  const pet = getPet(state, id)

  if (!pet) return null
  const {album} = pet
  if (!album) return []

  const length = Object.keys(album).length

  if (length > 0) {
    let albumList = []

    const iterator = (val, key) => {
      albumList.push({...val, id: key})
    }
    R.forEachObjIndexed(iterator, album)
    return albumList
  }
  return []
}
