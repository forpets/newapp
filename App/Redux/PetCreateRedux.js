import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  petCreateRequest: ['data', 'imagePickerResponse'],
  petUpdateRequest: ['data', 'imagePickerResponse', 'id'],
  petSaveSuccess: ['payload'],
  petSaveFailure: null
})

export const PetCreateTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  id: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }, imagePickerResponse) =>
  state.merge({ fetching: true, data, payload: null })

export const updateRequest = (state, { data }, imagePickerResponse, id) =>
  state.merge({ fetching: true, data, id, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload, id: null })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PET_CREATE_REQUEST]: request,
  [Types.PET_UPDATE_REQUEST]: updateRequest,
  [Types.PET_SAVE_SUCCESS]: success,
  [Types.PET_SAVE_FAILURE]: failure
})
