// @flow
import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

import type {StateUser} from './UserRedux'

export type State = {
  nav: StateNav,
  login: StateLogin,
  user: StateUser,
  pets: StatePets
}

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,

  login: require('./LoginRedux').reducer,
  user: require('./UserRedux').reducer,
  allUsers: require('./AllUsersRedux').reducer,

  userRegister: require('./UserRegisterRedux').reducer,
  forgotPassword: require('./ForgotPasswordRedux').reducer,

  pets: require('./PetsRedux').reducer,
  petAlbumUpload: require('./PetAlbumUploadRedux').reducer,
  petAlbumDelete: require('./PetAlbumDeleteRedux').reducer,
  petCreate: require('./PetCreateRedux').reducer,
  petDelete: require('./PetDeleteRedux').reducer,

  noteSave: require('./NoteSaveRedux').reducer,
  noteDelete: require('./NoteDeleteRedux').reducer,

  userUpdate: require('./UserUpdateRedux').reducer,
  medicalInfoSave: require('./MedicalInfoSaveRedux').reducer,
  vaccineSave: require('./VaccineSaveRedux').reducer,

  place: require('./PlaceRedux').reducer,
  placeAdd: require('./PlaceAddRedux').reducer,
  // placesFindAll: require('./PlacesFindAllRedux').reducer,

  filteredPlaces: require('./FilteredPlacesRedux').reducer,
  placeSearch: require('./PlaceSearchRedux').reducer,

  filteredLostFound: require('./FilteredLostFoundRedux').reducer,
  lostFound: require('./LostFoundRedux').reducer,

  placeReviewAdd: require('./PlaceReviewAddRedux').reducer,
  placeReview: require('./PlaceReviewRedux').reducer,
  placeClaim: require('./PlaceClaimRedux').reducer,
  placePhotoUpload: require('./PlacePhotoUploadRedux').reducer,

  location: require('./LocationRedux').reducer
})

export default () => {
  let { store, sagasManager, sagaMiddleware } = configureStore(reducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
