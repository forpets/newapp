// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import type {PlaceReview} from '../FlowTypes/PlaceReview'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placeReviewRequest: ['data'],
  placeReviewSuccess: ['payload'],
  placeReviewFailure: null
})

export const PlaceReviewTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACE_REVIEW_REQUEST]: request,
  [Types.PLACE_REVIEW_SUCCESS]: success,
  [Types.PLACE_REVIEW_FAILURE]: failure
})

/* ----------------------- Selectors --------- */

export const getPlaceReviewsIsFetching = (state) : boolean => {
  return (state.placeReview.fetching)
}

export const getPlaceReviewsArray = (state) : Array<PlaceReview> => {
  const {payload = []} = state.placeReview
  return payload
}
