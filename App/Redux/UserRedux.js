// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
// import {omit} from 'ramda'

import type {State} from './index'

export type User = {

}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  userSet: ['user'],
  userLoggedIn: ['user'],
  userClear: null,
  userLogout: null,
  userSyncRequest: null,
  userLoginGuest: null
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  profile: null,
  isGuestUser: null
})

/* ------------- Reducers ------------- */

export const set = (state, action) => {
  const {user} = action
  // Can receive auth object or user json - divide in the future
  let userClean = null

  if (user && user.toJSON) {
    userClean = user.toJSON()
  } else if (user) {
    userClean = user
  }
  return state.merge({profile: userClean, isGuestUser: false})
}

export const syncRequest = (state) => state

export const clear = (state) => state.merge({profile: null, isGuestUser: null})

export const loginGuest = (state) => state.merge({profile: null, isGuestUser: true})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_SET]: set,
  [Types.USER_LOGGED_IN]: set,
  [Types.USER_CLEAR]: clear,
  [Types.USER_LOGOUT]: clear,
  [Types.USER_SYNC_REQUEST]: syncRequest,
  [Types.USER_LOGIN_GUEST]: loginGuest
})

/* ------------- Selectors ------------- */
export const getCurrentUser = (state : State) : User => {
  if (state.user && state.user.profile) {
    return state.user.profile
  }
  return null
}

export const getIsGuestUser = (state : State) : boolean => {
  if (state.user && state.user.isGuestUser) return true
  return false
}
