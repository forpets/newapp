// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import R from 'ramda'
import type {Place} from './PlaceRedux'
import { getFilteredPlaces } from './FilteredPlacesRedux'
import {getSearchedPlaces} from './PlaceSearchRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placesFindAllRequest: ['data'],
  // placesFindAllSyncing: null,
  placesFindAllSuccess: ['payload'],
  placesFindAllFailure: null
})

export const PlacesFindAllTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// Sync in progress - do not clear current array
// export const syncing = (state) =>
//   state.merge({ fetching: true })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  // NOTE: Do not add payload to store - because we are processing too much records
  return state.merge({ fetching: false, error: null, payload: null })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACES_FIND_ALL_REQUEST]: request,
  // [Types.PLACES_FIND_ALL_SYNCING]: syncing,
  [Types.PLACES_FIND_ALL_SUCCESS]: success,
  [Types.PLACES_FIND_ALL_FAILURE]: failure
})

/* ------------------ Selectors ------------------------ */

// export const getPlacesArray = (state : State) : Array<Place> => {
//   if (!(state.placesFindAll && state.placesFindAll.payload)) return []
//   const places = state.placesFindAll.payload// .asMutable()
//   return places
// }

export const getPlace = (state: State, id: string) : Place => {
  // if (!(state.placesFindAll && state.placesFindAll.payload)) return null
  // if (!id) return null
  // const places = getPlacesArray(state)

  // Now we are loading from filtered places
  let places = getFilteredPlaces(state)
  let place = R.find(R.propEq('id', id), places)
  // If do not find in filtered - search in searched...
  if (!place) {
    places = getSearchedPlaces(state)
    place = R.find(R.propEq('id', id), places)
  }
  return place
}
