import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  locationRequest: ['data'],
  locationSuccess: ['coords'],
  locationFailure: null,
  locationPermissionDenied: null,
  locationPermissionGranted: null,
  locationAccessDeniedAlertDisplayed: null
})

export const LocationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  coords: null,
  error: null,
  locationAccess: null,
  accessDeniedAlertDisplayed: false
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data })

// successful api lookup
export const success = (state, action) => {
  const { coords } = action
  return state.merge({ fetching: false, error: null, coords })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true })

export const permissionDenied = state => state.merge({locationAccess: false})

export const permissionGranted = state => state.merge({locationAccess: true})

export const accessDeniedAlertDisplayed = state => state.merge({accessDeniedAlertDisplayed: true})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOCATION_REQUEST]: request,
  [Types.LOCATION_SUCCESS]: success,
  [Types.LOCATION_FAILURE]: failure,
  [Types.LOCATION_PERMISSION_DENIED]: permissionDenied,
  [Types.LOCATION_PERMISSION_GRANTED]: permissionGranted,
  [Types.LOCATION_ACCESS_DENIED_ALERT_DISPLAYED]: accessDeniedAlertDisplayed
})

/* --------------------- Selector ----------------------- */

export const getCurrentLocation = (state) => {
  return state.location.coords
}

// True = granted
// False = denied
// null = undefined
export const getLocationAccessStatus = (state) => {
  return state.location.locationAccess
}

export const getShouldShowLocationAccessDeniedAlert = (state) => {
  // If location access is DENIED and ALERT was not displayed
  // console.tron.log({access: getLocationAccessStatus(state), accessDenied})
  return (getLocationAccessStatus(state) === false && !state.location.accessDeniedAlertDisplayed)
}
