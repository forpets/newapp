import AppNavigation from '../Navigation/AppNavigation'

export const reducer = (state, action) => {
  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}

export const getCurrentRoute = (state: NavigationState) => (
  state.index !== undefined ? getCurrentRoute(state.routes[state.index]) : state.routeName
)
