import React, { Component } from 'react'
import CheckBox from 'react-native-check-box'
import { View, Text } from 'react-native'
import styles from './Styles/GenericCheckBoxStyle'
import R from 'ramda'
export default class GenericCheckBox extends Component {

  constructor(props) {
    super(props)
  }

  renderCheckboxes = (categories) => {
    const pairs = R.splitEvery(2, categories)
    return pairs.map((pair, i) => this.renderCheckboxPair(pair, i))
  }

  renderCheckbox = (item) => {
   let days = this.props.days

    if (!item) return
    const selected = R.contains(item.value, this.props.secondaryValues)
    const onClick = () => {
      this.props.onPressSecondary(item.value, !selected)
    }
    return (
      <View style={styles.container}>
        <CheckBox
          style={styles.checkbox}
          onClick={onClick}
          isChecked={selected}
          rightText={days.map(day => day)}
          rightTextStyle={selected ? styles.checkBoxClicked : styles.checkboxText}
          checkBoxColor={selected ? '#FF1744' : 'black'}
              />
      </View>
    )
  }
}
