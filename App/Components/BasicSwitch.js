import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text, Image } from 'react-native'
import styles from './Styles/BasicSwitchStyle'
import R from 'ramda'
import { Images } from '../Themes'

export default class BasicSwitch extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  static defaultProps = {
    onValueChange: () => {}
  }

  constructor (props) {
    super(props)
    const {value} = this.props
    this.state = {value: value || false}
  }

  callback = () => {
    const {value = false} = this.state
    const {onValueChange} = this.props

    this.setState({value: !value})

    if (onValueChange && typeof onValueChange === 'function') {
      onValueChange(!(value))
    }
  }

  render () {
    const {value} = this.state

        // <Switch onValueChange={onValueChange} value={value}/>
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.callback}>
          {this.showSelector(value)}
        </TouchableOpacity>
      </View>
    )
  }

  showSelector = (value) => {
    if (value) {
      return (
        <View style={styles.iconContainerOn}>
          <Image source={Images.crearEspacio.on} style={styles.icon} />
        </View>
      )
    } else {
      return (
        <View style={styles.iconContainerOff}>
          <Image source={Images.crearEspacio.off} style={styles.icon} />
        </View>
      )
    }
  }
}
