import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Callout } from 'react-native-maps'
import styles from './Styles/DirectoriesMapCalloutStyles'
import { Images } from '../Themes/index'
import Triangle from 'react-native-triangle'
import CategoryHeader from './CategoryHeader'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import GeoFunctions from '../Transforms/GeoFunctions'

export default class DirectoriesMapCallout extends React.Component {
  constructor (props) {
    super(props)
    this.onPress = this.props.onPress.bind(this, this.props.location)
  }

  render () {
    // FIXME: fix address formating
    const { location, currentLocation } = this.props

    const distance = (currentLocation) ? GeoFunctions.getDistanceToPlace(location, currentLocation) : null
    const distanceText = distance ? `A ${distance} km de distancia` : ''

    const isAdvert = (location.advert)
    const containerStyle = isAdvert ? styles.container : [styles.container, styles.containerNoHeader]

    return (
      <Callout tooltip style={styles.callout} onPress={() => this.props.onPress(location)}>
        {(isAdvert) && <CategoryHeader categoryId={location.category} />}

        <View style={containerStyle}>
          <Text style={styles.textLocation}>{location.name}</Text>
          <Text style={styles.textAddress}>{location.address}</Text>
          <Text style={styles.textDistance}>{distanceText}</Text>

          <View style={styles.footer}>
            {(location.rating) && <IconMD name='star' style={styles.icon} />}
            <Text style={styles.rates}>{location.rating}</Text>
            <View style={styles.open}><Text style={styles.state}>ABIERTO</Text></View>
          </View>
        </View>
        <View style={styles.triangleContainer}>
          <Triangle
            width={18}
            height={10}
            color={'#545555'}
            direction={'down'}
            />
        </View>
      </Callout>
    )
  }
}
