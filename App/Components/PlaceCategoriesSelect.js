// @flow
import React, { Component } from 'react'
import {View, Text, ScrollView, TouchableOpacity} from 'react-native'
import styles from './Styles/PlaceCategoriesSelectStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import CheckBox from 'react-native-check-box'
import CategoryRadioButtons from './CategoryRadioButtons'
import R from 'ramda'
import Modal from 'react-native-modal'
import Categories from '../Services/Categories'

type Props = {
  show: boolean,
  hideModalFunc: Function,
  mainValue: number,
  secondaryValues: Array<number>,

  onChangeMain: Function,
  onPressSecondary: Function
}

export default class PlaceCategoriesSelect extends Component<Props> {
  static defaultProps ={
    show: false,
    hideModalFunc: () => {},

    mainValue: null,
    secondaryValues: [],

    onChangeMain: () => {},
    onPressSecondary: () => {}
  }

  render () {
    const {show, hideModalFunc} = this.props

    const categories = Categories.findAll()
    // console.tron.warn(this.props)

    return (
      <Modal
        style={styles.modal}
        isVisible={show}
        onBackdropPress={hideModalFunc}
        onBackButtonPress={hideModalFunc}
        animationIn='slideInDown'
        animationOut='slideOutUp'
      >

        <ScrollView style={styles.container}>
          <View style={styles.header}>
            <View style={styles.iconArrowWrapper}>
              <IconMD name='arrow-back' size={25} onPress={hideModalFunc} style={styles.iconArrow} />
            </View>
            <TouchableOpacity onPress={hideModalFunc}>
              <View style={styles.headerRightButtonWrapper}>
                <Text style={styles.headerRightButton}>Aplicar</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.mainTitleWrapper}>
            <Text style={styles.mainTitle}>Categorías</Text>
          </View>
          <View style={styles.descriptionWrapper}>
            <Text style={styles.description}>Seleccioná el rubro principal sobre el cual u negocio se desarrolla. (solo una opción)</Text>
          </View>

          <View style={styles.radioButtonContainer}>
            <View style={styles.radioButtonInnerWrapper}>
              <CategoryRadioButtons
                options={categories}
                selectedIndex={this.props.mainValue}
                onChange={this.props.onChangeMain}
              />
            </View>
          </View>

          <View style={styles.descriptionWrapper}>
            <Text style={styles.description}>Seleccioná otros rubros o servicio que tu negocio sobre el cual u negocio brinde. (pueden ser varias opciones)</Text>
          </View>

          <View style={styles.checkBoxesContainer}>
            {this.renderCheckboxes(categories)}
          </View>

        </ScrollView>
      </Modal>
    )
  }

  renderCheckboxes = (categories) => {
    // Group by 2 for rendering e.g.: <checkbox> <checkbox>
    const pairs = R.splitEvery(2, categories)
    return pairs.map((pair, i) => this.renderCheckboxPair(pair, i))
  }

  renderCheckboxPair = (pair, i) => {
    const [first, second] = pair
    return (
      <View key={i} style={styles.checkBoxesWrapper}>
        <View style={styles.checkBoxItem}>
          {this.renderCheckbox(first)}
        </View>
        <View style={styles.checkBoxItem}>
          {this.renderCheckbox(second)}
        </View>
      </View>
    )
  }

  renderCheckbox = (item) => {
    if (!item) return
    const selected = R.contains(item.value, this.props.secondaryValues)
    const onClick = () => {
      this.props.onPressSecondary(item.value, !selected)
    }

    // if (item.value == '')
    return (
      <CheckBox
        key={Math.random(1)} // Hack to force always rerender
        style={styles.checkbox}
        onClick={onClick}
        isChecked={selected}
        rightText={item.label}
        rightTextStyle={selected ? styles.checkBoxClicked : styles.checkboxText}
        checkBoxColor={selected ? '#FF1744' : 'black'}
      />
    )
  }
}
