import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/CategoryRadioButtonsStyle'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
import R from 'ramda'

export default class CategoryRadioButtons extends Component {
  static defaultProps = {
    options: [],
    selectedIndex: null,
    onChange: () => {}
  }

  render () {
    const {options} = this.props

    // Split options in 2 columns
    const [first, second] = R.splitAt(R.length(options) / 2, options)

    return (
      <View style={styles.container}>

        <RadioForm formHorizontal animation style={styles.radioForm}>

          <View style={styles.columnLeft}>
            {first.map((obj) => this.renderButton(obj))}
          </View>

          <View style={styles.columnRight}>
            {second.map((obj) => this.renderButton(obj))}
          </View>

        </RadioForm>

      </View>
    )
  }

  renderButton = (obj) => {
    const {selectedIndex, onChange} = this.props
    const i = obj.value

    return (
      <RadioButton labelHorizontal key={i} wrapStyle={styles.radioButton} >

        <RadioButtonInput
          obj={obj}
          index={i}
          isSelected={selectedIndex === i}
          onPress={onChange}
          borderWidth={0.5}
          buttonInnerColor={'#FF1744'}
          buttonOuterColor={selectedIndex === i ? '#000' : '#000'}
          buttonSize={10}
          buttonOuterSize={20}
          buttonStyle={{borderWidth: 2}}
          buttonWrapStyle={styles.buttonWrapStyle}
            />

        <RadioButtonLabel
          obj={obj}
          index={i}
          labelHorizontal
          onPress={onChange}
          labelStyle={styles.labelStyle}
          labelWrapStyle={styles.labelWrapStyle}
          />
      </RadioButton>
    )
  }
}
