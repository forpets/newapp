import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import styles from './Styles/ReviewPricingInputStyle'
import R from 'ramda'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

export default class ReviewPricingInput extends Component {
  static defaultProps = {
    onPress: () => {},
    value: null
  }

  render () {
    const {renderSelector} = this
    const {value} = this.props

    return (
      <View style={styles.currencySignContainer} >
        {renderSelector(1, value)}
        {renderSelector(2, value)}
        {renderSelector(3, value)}
      </View>
    )
  }

  renderIcon = (selected = false, key) => {
    const style = selected ? styles.iconMoneyEnabled : styles.iconMoneyDisable
    return (<IconMD style={style} name='attach-money' key={key} />)
  }

  renderSelector = (count, currentValue) => {
    const selected = (parseInt(currentValue) === count)
    const style = selected ? styles.currencySignWrapperEnabled : styles.currencySignWrapperDisable
    const icon = (key) => this.renderIcon(selected, key)

    return (
      <TouchableOpacity onPress={() => this.props.onPress(count)}>
        <View style={style} >
          {R.times(icon, count)}
        </View>
      </TouchableOpacity>
    )
  }
}
