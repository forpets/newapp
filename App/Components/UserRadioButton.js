// @flow
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
import styles from './Styles/UserRadioButtonStyle'

type Props = {
  defaultValue?: string,

  fieldName: string,
  parentComp: Object,
  options: Array,
  onPress?: Function,

  label: string,
  buttonInnerColor: string
}

type State = {
  selectedIndex: number
}

export default class UserRadioButton extends Component<Props, State> {
  static defaultProps = {
    defaultValue: null,
    buttonInnerColor: 'black'
  }

  constructor (props : Props) {
    super(props)
    const {defaultValue, options} = props

    const selectedIndex = (defaultValue) ? options.findIndex((obj) => defaultValue === obj.value) : -1
    this.state = {selectedIndex}
  }

  handlePress = (value : string, index : number) => {
    const {parentComp, fieldName, onPress} = this.props

    // console.tron.log({value, options, index})
    this.setState({selectedIndex: index})

    if (typeof onPress === 'function') {
      onPress(value)
    } else if (parentComp && typeof parentComp.setField === 'function') {
      parentComp.setField(fieldName, value)
    } else {
      console.tron.display({
        name: 'UserRadioButton',
        preview: 'parentComp has no function setValue or is undefined',
        value: {type: typeof parentComp.setField}
      })
    }
  }

  render () {
    const {parentComp, fieldName, label, options, buttonInnerColor} = this.props
    const {selectedIndex} = this.state
    const {handlePress} = this
    // If has onPress - can override default action

    // If has error
    const error = parentComp ? parentComp.getError(fieldName) : false

    const styleContainer = !error ? styles.container : styles.containerError
    const styleText = !error ? styles.text : styles.textError

    return (
      <View style={styleContainer}>
        { label && <Text style={styleText}>{label}</Text> }

        <RadioForm formHorizontal animation style={styles.options}>

          {options.map((obj, i) => {
            return <RadioButton labelHorizontal key={i} >
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={selectedIndex === i}
                onPress={handlePress}
                borderWidth={1}
                buttonInnerColor={buttonInnerColor}
                buttonOuterColor={selectedIndex === i ? '#000' : '#000'}
                buttonSize={10}
                buttonOuterSize={20}
                buttonStyle={{borderWidth: 2}}
                buttonWrapStyle={{marginLeft: 10}}
              />

              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal
                onPress={handlePress}
                labelStyle={{fontSize: 16, fontFamily: 'Rubik-Regular', color: '#363636', marginHorizontal: 2}}
                labelWrapStyle={{}}
            />
            </RadioButton>
          })}

        </RadioForm>

      </View>
    )

        // <RadioForm
        //   style={styles.options}
        //   buttonColor={'black'}
        //   labelColor={'black'}
        //   labelStyle={{fontSize: 18, fontFamily: 'Rubik-Regular', color: '#363636', marginHorizontal: 15}}
        //   radio_props={options}
        //   initial={defaultValue}
        //   formHorizontal
        //   onPress={onPress}
        // />
  }
}
