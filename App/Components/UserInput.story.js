import React from 'react'
import { storiesOf } from '@storybook/react-native'

import UserInput from './UserInput'

storiesOf('UserInput')

  .add('Default', () => (

    <UserInput
      parentComp={mockComponentDefault}
      placeholder='Default plac'
      defaulValue='Some value asdasd'
    />

  ))
  .add('Error', () => (

    <UserInput
      parentComp={mockComponentError}
      placeholder='Placeholder asd'
      defaulValue='Some value'
    />

  ))

const mockComponentError = {
  getError: (fieldName : string) => 'Some asda serror:',
  isFocused: (fieldName : string) => false,
  setField: (fieldName : string, fieldValue : string) => { }
}

const mockComponentDefault = {
  getError: (fieldName : string) => '',
  isFocused: (fieldName : string) => false,
  setField: (fieldName : string, fieldValue : string) => { }
}
