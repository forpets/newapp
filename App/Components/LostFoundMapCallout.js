import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Callout } from 'react-native-maps'
import styles from './Styles/DirectoriesMapCalloutStyles'
import { Images } from '../Themes/index'
import Triangle from 'react-native-triangle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import GeoFunctions from '../Transforms/GeoFunctions'
import LostFound from '../Services/LostFound'

export default class LostFoundMapCallout extends React.Component {
  constructor (props) {
    super(props)
    this.onPress = this.props.onPress.bind(this, this.props.location)
  }

  render () {
    const getDateText = (dateString) => {
      const parts = dateString.split('/')
      const date = new Date(parts[2],parseInt(parts[1])-1,parts[0])
      const now = new Date();
      const days = Math.floor((now.getTime() - date.getTime()) / (1000*60*60*24))

      return 'Hace ' + days + ' días'
    }

    // FIXME: fix address formating
    const { location, currentLocation } = this.props

    // const distance = (currentLocation) ? GeoFunctions.getDistanceToPlace(location, currentLocation) : null
    // const distanceText = distance ? `A ${distance} km de distancia` : ''

    const containerStyle = [styles.container, styles.containerNoHeader]

    return (
      <Callout tooltip style={styles.callout} onPress={() => this.props.onPress(location)}>

        <View style={containerStyle}>
          <Text style={styles.textLocation}>{ LostFound.getTypeText(location) }</Text>
          <Text style={styles.textAddress}>{ getDateText(location.date) }</Text>
          <Text style={styles.textDistance}>{ location.kind } - { location.gender }</Text>
        </View>
        <View style={styles.triangleContainer}>
          <Triangle
            width={18}
            height={10}
            color={'#545555'}
            direction={'down'}
          />
        </View>
      </Callout>
    )
  }
}
