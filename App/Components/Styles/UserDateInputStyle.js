import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    marginHorizontal: 25,
    marginVertical: 10,
    flex: 1
  },
  errorText: {
    marginVertical: 4,
    color: 'red',
    fontSize: 13,
    fontFamily: Fonts.type.base
  },
  selectedDate: {
    fontFamily: Fonts.type.base,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: Colors.frost,
    paddingTop: 7,
    paddingBottom: 6
  },
  UnselectedDate: {
    paddingBottom: 6,
    paddingTop: 2,
    fontFamily: Fonts.type.base,
    fontSize: 16,
    color: '#363636',
    borderBottomWidth: 1,
    borderBottomColor: Colors.frost
  },
  UnselectedDateError: {
    fontSize: 16,
    color: '#363636',
    paddingBottom: 6,
    paddingTop: 2,
    fontFamily: Fonts.type.base,
    borderBottomWidth: 1,
    borderBottomColor: 'red'
  },
  placeholderTop: {
    color: 'gray',
    marginTop: 8,
    fontFamily: Fonts.type.base,
    fontSize: 12
  }
})
