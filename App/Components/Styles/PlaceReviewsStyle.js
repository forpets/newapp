import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  blankCommentsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(10)
  },
  blankComments: {
    fontFamily: Fonts.type.base,
    color: 'gray',
    fontStyle: 'italic',
    textAlign: 'left',
    marginHorizontal: moderateScale(20)
  },
  profileAndNameWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    /* borderColor: 'red',
    borderWidth: 1, */
    marginTop: -16,
    width: '70%',
  },
  nameItemWrapper: {
    marginTop: 40
  },
  avatarProfile: {
    borderRadius: 50,
    height: 70,
    width: 70
  },
  nameProfileText: {
    color: '#444444',
    fontSize: moderateScale(14),
    fontFamily: Fonts.type.rubikBold,
  },
  avatarProfileWrapper: {
    marginHorizontal: 20
  },
  nameProfileTextWrapper: {
    marginTop: moderateScale(-10)
  },
  ratingSubtitleWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: moderateScale(5)
  },
  numberRatingSubtitle: {
    marginHorizontal: 10,
    color: '#525252',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(12)
  },
  commentTime: {
    color: '#939393',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(12)
  },
  descriptionComentarioTextWrapper: {
    marginTop: moderateScale(10),
    width: '85%'
  },
  descriptionComentarioText: {
    color: '#444444',
    fontSize: moderateScale(14),
    fontFamily: Fonts.type.base,
    lineHeight: 25
  },
  resenaWrapper: {
    marginHorizontal: moderateScale(102),
    marginVertical: moderateScale(6)
  },
  resenaText: {
    color: '#2962FF',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(15)
  },
  calificacionWrapper: {
    marginHorizontal: 20
  },
  espacioWrapper: {
    marginHorizontal: 20
  },
  espacioText: {
    color: '#8F95E7',
    fontFamily: Fonts.type.base,
    fontWeight: 'bold',
    fontSize: moderateScale(14)
  },
  calificacionText: {
    color: '#4A4A4A',
    fontFamily: Fonts.type.base,
    // fontWeight: 'bold',
    fontSize: moderateScale(14)
  },
  starIcon: {
    color: '#7AC51E',
    fontSize: 17,
    textAlignVertical: 'top'
  },
  dividerLine: {
    borderWidth: 0.5,
    borderColor: 'gray',
    width: '100%',
    marginVertical: 14
  },
  halfDivider: {
    marginHorizontal: moderateScale(102),
    borderWidth: 0.5,
    borderColor: 'gray',
    width: '85%',
    marginVertical: 14
  }
})
