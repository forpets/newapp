import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {Fonts} from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  orderByContainer: {
 /*    borderColor:'red',
    borderWidth:1, */
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconSelected: {
    fontSize: moderateScale(17),
    color: '#FD5381'
  },
  iconSelectedImage: {
    tintColor: '#FD5381',
    height: '100%',
    width: '100%'
  },
  iconUnselectedStar: {
    fontSize: moderateScale(17),
    color: 'black'
  },

  iconContainerImage: {
    height: moderateScale(20),
    width: moderateScale(20)
  },
  iconContainerLocationImage: {
    height: moderateScale(20),
    width: moderateScale(15)
  },
  iconUnselectedImage: {
    height: '100%',
    width: '100%'
  },
  orderByWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: '#9B9B9B',
    borderRightWidth: 1,
    paddingTop: moderateScale(26),
    paddingBottom: moderateScale(26)

  },
  orderInnerLabelWrapper: {
    marginVertical: moderateScale(5),
  },
  orderInnerLabel: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(13),
    color: 'gray'
  },
  orderInnerLabelSelected: {
    fontFamily: Fonts.type.rubikBold,
    fontSize: moderateScale(13),
    color: '#FD5381'
  }

})
