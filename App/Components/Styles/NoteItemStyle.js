import {StyleSheet, Dimensions} from 'react-native'
const {height, width} = Dimensions.get('window')
const containerHeight = height * 0.25
const containerWidth = height * 0.68

export default StyleSheet.create({
  bigContainer: {
    padding: 1,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: 10,
    marginTop: 10,
    elevation: 6,
    borderRadius: 6,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 15,
    height: containerHeight,
    width: containerWidth * 0.80
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    height: '100%',
    width: '100%',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF',
    paddingLeft: 15,
    borderRadius: 10

  },
  date: {
    marginTop: 0,
    height: containerHeight,
    width: containerWidth * 0.1,
    borderRightWidth: 2,
    borderColor: '#7bdbcd'
  },
  month: {
    marginTop: 0,
    marginLeft: 0,
    color: '#7bdbcd'
  },
  day: {
    marginTop: 12,
    marginLeft: 0,
    color: '#7bdbcd',
    fontSize: 20,
    // fontWeight: 'bold'
    fontFamily: 'Rubik-Medium'
  },
  body: {
    flex: 1,
    // borderWidth:1,
    flexDirection: 'column',
    justifyContent: 'space-between',

    height: '100%',
    marginLeft: 18,
    paddingVertical: 12
  },
  title: {
    fontFamily: 'Rubik-Medium',
    fontSize: 18
  },
  description: {
    // borderWidth: 1,
    flex: 1,
    marginTop: 10,
    fontSize: 14,
    color: '#525252',
    paddingRight: 10,
    fontFamily: 'Rubik-Regular'
  },
  petAvatar: {
    width: 40,
    height: 40,
    marginTop: 10,
    borderRadius: 100
  },
  petInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  button: {
    // borderWidth: 1,

  },
  petName: {
    // borderWidth: 1,
    marginTop: 5, // FIXME: flex > center
    marginLeft: 8,
    fontSize: 18,
    fontFamily: 'Rubik-Medium',
    color: '#7bdbcd'
  }
})
