import { StyleSheet } from 'react-native'
import Fonts from '../../Themes/Fonts'
import {ApplicationStyles, Metrics} from '../../Themes'

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 14) : ((Metrics.screenWidth <= 360) ? (fontSize = 16) : (fontSize = 17))
/** ************ Media Queries Block Ends ***************/


export default StyleSheet.create({
  modal: {
    position: 'absolute',
    // zIndex: 2000,
    height: '50%',
    width: '100%',
    bottom: 0,
    left: 0,
    padding: 0,
    margin: 0,
    flex: 1
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white',
/*     borderColor: 'red',
    borderWidth: 1 */
  },

  // TOP ROW
  topRow: {
    flex: 1,
    // paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  topRowDivider: {
    height: 30,
    borderWidth: 0.75,
    borderColor: 'gray'
  },
  topRowText: {
    marginHorizontal: 50,
    color: 'gray',
    fontFamily: Fonts.type.base,
    fontSize
  },

  // BOTTOM ROW
  bottomRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
/*     borderColor: 'green',
    borderWidth: 1 */
  },
  bottomRowButton: {
    flex: 1,
    alignItems: 'center',
    borderColor: 'gray',
    borderTopWidth: 1,
    paddingVertical: 20,
    paddingHorizontal: 30
  },
  bottomButtonLeft: {
    borderRightWidth: 0.5,
    borderColor: 'gray'
  },
  bottomButtonRight: {
    borderLeftWidth: 0.5,
    borderColor: 'gray'
  },
  bottomRowButtonText: {
    color: 'gray',
    fontFamily: Fonts.type.base,
    fontSize
  },
  cancelButton: {
    color: 'black'
  },

  // MIDDLE ROW
  middleRow: {
    flex: 4,
    // borderWidth: 1,
    flexDirection: 'row',
  /*   borderColor: 'blue',
    borderWidth: 1 */
  },
  middleRowCol: {
    flex: 1,
    // borderWidth: 1,
    // borderColor: 'red',
    marginHorizontal: 30,
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  timeItem: {
    // fontFamily: Fonts.type.base,
    fontSize: 20,
    color: '#CCC',
    paddingVertical: 3
    // Do not put margins - it will break getItemLayout
  },
  timeItemSelected: {
    color: '#444'
  }

})
