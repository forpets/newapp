import {StyleSheet, Dimensions} from 'react-native'
const {height, width} = Dimensions.get('window')
const noteListContainerWidth = width * 0.9
const titleWidth = height * 0.5

const styles = StyleSheet.create({
  title: {
    color: '#787878',
    fontSize: 20,
    fontWeight: 'bold',
    width: titleWidth
  },
  container: {
/*     borderColor: 'red',
    borderWidth: 1, */
    marginTop: 3,
    alignItems: 'center',
    marginBottom: 7,
    width: '100%'
  }
})

export default styles
