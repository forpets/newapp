import { StyleSheet } from 'react-native'
import {Colors} from '../../Themes'

const borderRadius = 20
const paddingVertical = 7
const paddingHorizontal = 25
const fontSize = 13
const borderWidth = 1.5

const colorGray = '#444444'

export default StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  leftButton: {
    marginBottom: 5,
    elevation: 3,
    backgroundColor: 'white',
    borderWidth,
    borderColor: colorGray,
    borderTopLeftRadius: borderRadius,
    borderBottomLeftRadius: borderRadius,
    paddingVertical,
    paddingHorizontal
  },
  rightButton: {
    marginBottom: 5,
    elevation: 3,
    backgroundColor: 'white',
    borderWidth,
    borderColor: colorGray,
    borderTopRightRadius: borderRadius,
    borderBottomRightRadius: borderRadius,
    paddingVertical,
    paddingHorizontal
  },
  buttonText: {
    fontSize,
    fontFamily: 'Roboto-Regular',
    color: colorGray
  },

  selectedButton: {
    backgroundColor: colorGray
  },
  selectedText: {
    color: 'white'
  }
})
