import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    height: 300,
    // width: 420
    // height: '100%',
    width: '100%'
  },
  slide: {

  },
  sliderImage: {
    height: '100%',
    width: '100%'
  },

  dotStyle: {
    borderColor: 'white',
    backgroundColor: 'rgba(0,0,0,0)',
    borderWidth: 2,
    width: 11,
    height: 11,
    borderRadius: 4,

    marginLeft: 6,
    marginRight: 6
  },
  activeDotStyle: {
    backgroundColor: 'white',
    borderColor: 'white',
    width: 11,
    height: 11,
    borderRadius: 8,
    borderWidth: 2,

    marginLeft: 6,
    marginRight: 6
  }
})
