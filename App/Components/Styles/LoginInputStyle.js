import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors} from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  inputValid: {
    height: 42,
    borderRadius: 25,
    backgroundColor: Colors.greenInputLight,
    marginHorizontal: Metrics.section,
    marginTop: 8,
    textAlignVertical: 'bottom',
    justifyContent: 'center',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 14,
    paddingLeft: 25,
    fontWeight: 'bold',
    color: 'white'
  },
  inputError: {
    textAlignVertical: 'bottom',
    fontFamily: Fonts.type.base,
    color: '#FF0000',
    borderColor: 'red',
    borderWidth: 2
  },
  error: {
    color: '#FF0000',
    fontSize: 11,
    marginTop: 4,
    marginHorizontal: Metrics.section
  },
  viewStyle: {
    flex: 1
  }
})
