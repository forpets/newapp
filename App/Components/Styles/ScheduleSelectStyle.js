import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters/lib/scalingUtils'
import {Fonts} from '../../Themes'
export default StyleSheet.create({
  container: {
    flex: 1
  },
  modal: {
    padding: 0,
    margin: 0,
    flex: 1,
    backgroundColor: 'white'
  },
  mainTitleWrapper: {
    marginVertical: moderateScale(12),
    marginHorizontal: moderateScale(20)
  },
  mainTitle: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(28),
    color: '#000000'
  },
  iconArrow: {
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(10)
  },
  descriptionWrapper: {
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(20)
  },
  description: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16),
    color: '#9B9B9B'
  },
  checkBoxesContainer: {

  },
  checkbox: {

  },
  dayLabel: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16),
    color: '#000000'
  },
  row: {
    marginVertical: moderateScale(18),
    marginHorizontal: moderateScale(20)
  },
  scheduleRow: {
    marginVertical: moderateScale(18),
    marginHorizontal: moderateScale(20),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
    // borderWidth: 1
  },
  scheduleText: {
    fontFamily: 'Rubik-Regular', fontSize: moderateScale(16), color: '#646860'
  },
  renderCheckboxRowContainer: {
    // marginTop: moderateScale(-67),
    // marginBottom: moderateScale(40),
    marginVertical: moderateScale(18),
    // marginVertical: moderateScale(20),
    marginHorizontal: moderateScale(20),
    marginBottom: moderateScale(40)
  }
})
