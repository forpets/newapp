import { StyleSheet, Dimensions } from 'react-native'

const {height, width} = Dimensions.get('window')
const avatarWH = width * 0.35

// console.tron.log(Dimensions.get('window'))

export default StyleSheet.create({
  image: {
    height: avatarWH,
    width: avatarWH,
    borderRadius: width
  }
})
