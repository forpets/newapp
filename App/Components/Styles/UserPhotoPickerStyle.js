import { StyleSheet, Dimensions } from 'react-native'
import {Fonts} from '../../Themes'

const {height, width} = Dimensions.get('window')
const avatarWH = width * 0.35

export default StyleSheet.create({
  container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: avatarWH,
    width: avatarWH,
    borderRadius: avatarWH,
    backgroundColor: 'white',
    elevation: 4,
    marginVertical: 10,
  },
  avatarText: {
    color: '#d8437a',
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    fontSize: 15
  },
  photo: {
    borderWidth: 0,
    borderRadius: avatarWH,
    height: '100%',
    width: '100%'
  }
})
