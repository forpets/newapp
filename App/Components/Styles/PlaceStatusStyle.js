import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {ApplicationStyles, Metrics} from '../../Themes'

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
let paddingBottom:any
let marginHorizontal:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 9) : ((Metrics.screenWidth <= 360) ? (fontSize = 12) : (fontSize = 14))

paddingBottom = (Metrics.screenWidth <= 320) ? (paddingBottom = 8) : ((Metrics.screenWidth <= 360) ? (paddingBottom = 8) : (paddingBottom = 1))
marginHorizontal = (Metrics.screenWidth <= 320) ? (marginHorizontal = 24) : ((Metrics.screenWidth <= 360) ? (marginHorizontal = 20) : (marginHorizontal = 20))

/** ************ Media Queries Block Ends ***************/

/* console.tron.log(Metrics.screenWidth) */

export default StyleSheet.create({
  container: {

  },
  rateClock: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingTop: moderateScale(15),
    paddingBottom,
    marginHorizontal

  },
  isOpen: {
    fontSize,
    color: '#8acc27',
    fontFamily: 'Rubik-Regular'
  },
  isClosed: {
    fontSize,
    color: 'red',
    fontFamily: 'Rubik-Regular'
  },
  clockIconOpen: {
    fontSize,
    color: '#8ACB27'
  },
  clockIconClosed: {
    fontSize,
    color: 'red'
  }

})
