import { StyleSheet, Dimensions } from 'react-native'
import {Fonts, Colors, Metrics} from '../../Themes'
import { moderateScale } from 'react-native-size-matters/lib/scalingUtils'

const {height, width} = Dimensions.get('window')
const placesContainerHeight = height * 0.35

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 14) : ((Metrics.screenWidth <= 360) ? (fontSize = 16) : (fontSize = 17))
/** ************ Media Queries Block Ends ***************/



const styles = StyleSheet.create({
  container:{
    
  },
  title: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(20),
    color: '#4B4B4B',
    marginHorizontal: moderateScale(8)
  },
  rightTitle: {
    fontFamily: 'Roboto-Regular', // FIXME: roboto regular
    color: '#939393',
    fontSize: moderateScale(13),
    textAlignVertical: 'center'
  },
  directoryContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: placesContainerHeight * moderateScale(0.85)
    // borderWidth: 1
  },

  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: moderateScale(5),
    marginRight: moderateScale(17),
    // borderWidth: 1,
    marginBottom: moderateScale(20)
  }
})

export default styles
