// import { StyleSheet } from 'react-native'

// export default StyleSheet.create({
//   container: {
//     flex: 1
//   }
// })

import {StyleSheet, Dimensions} from 'react-native'
const {height, width} = Dimensions.get('window')
const noteListContainerWidth = width * 0.9
const titleWidth = height * 0.5

const styles = StyleSheet.create({
  title: {
    color: '#787878',
    fontSize: 20,
    fontWeight: 'bold',
    width: titleWidth
  },
  petsContainer: {
    marginTop: 15,
    marginLeft: 20,
    alignItems: 'center',
    marginBottom: 10
  }
})

export default styles
