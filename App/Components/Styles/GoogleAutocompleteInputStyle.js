import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors} from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: Metrics.section
  },
  border: {
    flex: 1,
    borderBottomWidth: 1.5,
    borderColor: '#dce3e5'
  },
  errorBorder: {
    borderColor: 'red'
  },
  errorText: {
    marginTop: 5,
    color: 'red',
    fontSize: 12,
    fontFamily: Fonts.type.base
  },
  // Placeholder
  placeholderTop: {
    color: 'gray',
    marginTop: 6,
    fontFamily: Fonts.type.base,
    fontSize: 12
  },
  placeholderTopError: {
    color: 'red'
  }

})

// STYLES OF GOOGLE AUTOCOMPLETE COMPONENT
// Basicly we are 'clearing' the default styles
export const autocompleteInputStyle = {
  container: {
  },
  textInputContainer: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: 'auto',

    backgroundColor: 'white',
    borderWidth: 0,
    width: '100%'
  },
  textInput: {
    textAlignVertical: 'bottom',
    paddingTop: 0,
    paddingBottom: 4,
    height: 32,
    marginTop: 0,
    marginLeft: 0,
    marginRight: 0,
    borderRadius: 0,
    paddingLeft: 0,
    paddingRight: 0,
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  description: {
    fontFamily: 'Rubik-Regular',
    fontSize: 13
  },
  listView: {
    // marginLeft: 0,
    // marginRight: 0,
    // paddingLeft: 0,
    // paddingRight: 0,
  },
  predefinedPlacesDescription: {
    fontFamily: 'Rubik-Medium',
    fontSize: 16
    // color: '#1faadb'
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: 'gray'
  }

}
