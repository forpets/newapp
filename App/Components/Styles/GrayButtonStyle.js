import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  button: {
    height: 40,
    borderRadius: 25,
    borderWidth: 0,
    marginHorizontal: Metrics.section,
    marginTop:2,
    marginBottom: 10,
    justifyContent: 'center'
  },
  buttonEnabled: {
    backgroundColor: Colors.loginGray
  },
  buttonDisabled: {
    // backgroundColor: Colors.loginDisabled
    backgroundColor: Colors.loginGray
  },
  text: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    margin: 4
  }
})
