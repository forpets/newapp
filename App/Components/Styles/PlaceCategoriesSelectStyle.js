import { StyleSheet } from 'react-native'
import { Fonts, ApplicationStyles, Metrics } from '../../Themes'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 12) : ((Metrics.screenWidth <= 360) ? (fontSize = 14) : (fontSize = 14))
/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  modal: {
    padding: 0,
    margin: 0,
    flex: 1
  },
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: '100%',
    height: '100%',
    zIndex: 1000,
    flex: 1,
    // flex: 1,
    paddingHorizontal: moderateScale(20),
    backgroundColor: 'white'
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: moderateScale(20)
  },
  iconArrowWrapper: {

  },
  iconArrow: {

  },
  headerTitleStyle: {
    ...Fonts.style.header
  },
  headerRightButtonWrapper: {
    marginHorizontal: moderateScale(20)
  },
  headerRightButton: {
    fontFamily: Fonts.type.rubikMedium,
    color: '#FF1744',
    fontSize: moderateScale(14)
  },
  mainTitleWrapper: {
    paddingTop: moderateScale(-10),
    paddingBottom: moderateScale(20)
  },
  mainTitle: {
    fontFamily: Fonts.type.rubikMedium,
    color: '#000000',
    fontSize: moderateScale(20)
  },
  descriptionWrapper: {

  },
  description: {
    fontFamily: Fonts.type.base,
    color: '#9B9B9B',
    fontSize: moderateScale(15)

  },
  radioButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: moderateScale(20),
    width: '96%',
    marginLeft: 'auto',
    marginRight: 'auto'

  },
  radioButtonInnerWrapper: {

  },
  checkBoxesContainer: {
    marginTop: 20,
  },
  checkBoxesWrapper: {
 /*    borderWidth: 1,
    borderColor: 'blue', */
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '97%',
    marginLeft: 'auto',
    marginRight: 'auto'

  },
  checkBoxItem: {
/*     borderWidth: 1,
    borderColor: 'red' */
  },
  checkbox: {
    // flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 0,
    width: 154,
    // borderWidth: 1,
    // width: '100%'
  },
  checkboxText: {
    fontFamily: Fonts.type.base,
    fontSize
  },
  checkBoxClicked: {
    fontFamily: Fonts.type.bold,
    fontSize
  }

})
