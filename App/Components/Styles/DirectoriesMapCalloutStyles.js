import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'

const borderRadius = 5
const elevation = 10
const containerColor = '#545555'

export default StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderRadius,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    backgroundColor: containerColor,
    width: 250,
    height: 120,
    elevation
  },
  containerNoHeader: {
    paddingTop: 10,
    height: 130,
    borderTopLeftRadius: borderRadius,
    borderTopRightRadius: borderRadius
  },
  callout: {
    flex: 1,
    backgroundColor: Colors.transparent
  },

  footer: {
    marginTop: 10,
    marginBottom: 7,
    flex: 1,
    borderRadius,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  icon: {
    color: 'white',
    fontSize: 16,
    marginLeft: 15
    // textAlignVertical: 'center',
  },
  rates: {
    textAlignVertical: 'bottom',
    fontSize: 14,
    color: '#FFF',
    fontFamily: Fonts.type.base,
    marginLeft: 10
  },
  state: {
    fontSize: 14,
    fontFamily: Fonts.type.base,
    color: '#FFF',
    marginHorizontal: 15
  },
  open: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  textLocation: {
    marginHorizontal: 15,
    marginTop: 7,
    fontSize: 20,
    fontFamily: Fonts.type.rubikMedium,
    color: Colors.greenInputLight
  },
  textAddress: {
    marginHorizontal: 15,
    marginTop: 8,
    fontFamily: Fonts.type.base,
    fontSize: 11,
    color: '#A3EC4A'
  },
  textDistance: {
    marginHorizontal: 15,
    marginTop: 7,
    fontFamily: Fonts.type.base,
    fontSize: 10,
    color: '#FFF'
  },
  triangleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 7,
    elevation
    // borderWidth: 1
  }
})
