import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  currencySignContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: moderateScale(20),
    marginHorizontal: moderateScale(-10)
  },
  currencySignWrapperEnabled: {
    paddingLeft: moderateScale(20),
    paddingRight: moderateScale(20),
    paddingTop: moderateScale(1),
    paddingBottom: moderateScale(1),
    flexDirection: 'row',
    justifyContent: 'center',
    borderColor: '#FD5381',
    backgroundColor: '#FF1744',
    borderWidth: 1,
    borderRadius: moderateScale(25),
    width: moderateScale(70)

  },
  currencySignWrapperDisable: {
    marginHorizontal: 2,
    paddingLeft: moderateScale(20),
    paddingRight: moderateScale(20),
    paddingTop: moderateScale(-3),
    paddingBottom: moderateScale(1),
    flexDirection: 'row',
    justifyContent: 'center',
    borderColor: '#FD5381',
    borderWidth: 1,
    borderRadius: moderateScale(25),
    width: moderateScale(70)
  },
  iconMoneyEnabled: {
    color: 'white',
    fontSize: moderateScale(17),
    marginHorizontal: moderateScale(-4)
  },
  iconMoneyDisable: {
    color: '#FF1744',
    fontSize: moderateScale(17),
    marginHorizontal: moderateScale(-4)
  }
})
