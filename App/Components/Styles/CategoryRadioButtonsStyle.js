import { StyleSheet, Dimensions } from 'react-native'
import { Fonts, ApplicationStyles, Metrics } from '../../Themes'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 12) : ((Metrics.screenWidth <= 360) ? (fontSize = 14) : (fontSize = 14))
/** ************ Media Queries Block Ends ***************/

const {width, height} = Dimensions.get('window')
// const width = '100%'

export default StyleSheet.create({
  container: {
    flex: 1,
    width
  },
  containerError: {
// Not used in this comp
  },
  radioForm: {

  },
  columnLeft: {
    flex: 1,
    alignItems: 'flex-start'

  },
  columnRight: {
   /*  borderWidth: 1, */
    flex: 1,
    alignItems: 'flex-start',

  },

  radioButton: {
    height: 28,
    marginVertical: 5
  },
  buttonWrapStyle: {
    marginLeft: 20,
    width: 40
  },
  labelWrapStyle: {
    // borderWidth: 1
  },
  labelStyle: {
    fontFamily: Fonts.type.base,
    fontSize
  }

})
