import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  button: {
    height: 40,
    borderRadius: 25,
    marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    justifyContent: 'center'
  },
  buttonEnabled: {
    backgroundColor: Colors.facebookBlue
  },
  buttonDisabled: {
    backgroundColor: Colors.loginDisabled
  }
})
