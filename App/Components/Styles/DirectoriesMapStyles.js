import { StyleSheet } from 'react-native'
import {ApplicationStyles, Metrics} from '../../Themes/'

export default StyleSheet.create({
  map: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight - Metrics.navBarHeight
  }
})
