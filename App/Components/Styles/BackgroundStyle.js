import { StyleSheet, Dimensions } from 'react-native'

const {width, height} = Dimensions.get('window')

export default StyleSheet.create({
  backgroundImage: {
    resizeMode: 'cover', // or 'stretch'
    width: '100%',
    height: '100%'
  },
  view: {
    position: 'absolute',
    width: width,
    height: height + 900
  }
})
