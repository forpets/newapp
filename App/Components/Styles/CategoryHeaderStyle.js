import { StyleSheet } from 'react-native'
import {Fonts, Colors} from '../../Themes'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

const borderRadius = 5

export default StyleSheet.create({
  sectionHeader: {
    width: '100%',
    flex: 1,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderRadius,
    backgroundColor: '#0EE7C9',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  text1: {
    color: '#FFFFFF',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(10),
    marginHorizontal: -5
  },
  logoImage: {
    height: 22,
    marginHorizontal: 15
  },
  anuncioImage: {
    position: 'absolute',
    right: 0
  }
})
