import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    // height: 35,
    marginHorizontal: Metrics.section,
    marginBottom: Metrics.baseMargin
  },
  picker: {
    marginHorizontal: -7,
    marginTop: -5,
    marginBottom: -9,
    // marginBottom: 0,
    borderWidth: 1
    // padding: 0,
    // marginLeft: -9,
    // paddingBottom: 10,
    // fontFamily: Fonts.type.base, // FIXME: does not show up - hack styles.xml in android
    // fontSize: 17,
    // color: 'red'
  },
  placehoderText: {
    fontFamily: Fonts.type.base,
    fontSize: 16
  },
  placeholderTop: {
    color: 'gray',
    marginTop: 10,
    marginBottom: -8,
    fontFamily: Fonts.type.base,
    fontSize: 12,
    marginHorizontal: 1
  },
  errorText: {
    paddingTop: 4,
    color: 'red',
    borderTopWidth: 1,
    borderTopColor: Colors.frost,
    fontSize: 13
    // borderColor: 'red'
  },
  errorTextError: {
    borderTopColor: 'red'
    // marginHorizontal: Metrics.section,
    // marginBottom: Metrics.baseMargin
  }

})
