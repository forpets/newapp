import { StyleSheet } from 'react-native'

export default StyleSheet.create({

  circle: {
    margin: 10,
    borderRadius: 25,
    width: 25,
    height: 25,
    borderWidth: 2,
    borderColor: 'white',
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  normal: {
  },
  selected: {
    borderWidth: 0,
    backgroundColor: 'red'
  },
  checkIcon: {
    color: 'white',
    marginTop: 4,
    fontSize: 17,
    textAlign: 'center',
    textAlignVertical: 'center'
  }
})
