import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {ApplicationStyles, Metrics} from '../../Themes'
// FIXME: remove margins - auto distribute

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 14) : ((Metrics.screenWidth <= 360) ? (fontSize = 17) : (fontSize = 17))
/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  container: {
    flex: 1
  },
  menuItem: {
    flexDirection: 'row',
    marginTop: 0,
    marginBottom: moderateScale(10)
  },
  menuItemTextDisabled: {
    color: 'gray',
    marginLeft: -45,
    fontSize: 17
  },
  menuItemText: {
    color: '#ccc',
    marginLeft: -45,
    fontSize
  },
  iconContainer: {
    width: 85,
    justifyContent: 'center',
    alignItems: 'flex-start'

  },
  iconSize: {
    resizeMode: 'contain',
    width: 20,
    height: 20
  }
})
