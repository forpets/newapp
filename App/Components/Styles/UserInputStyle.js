import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors} from '../../Themes/'

const height = 32

export default StyleSheet.create({
  container: {
    flex: 1
  },
  // Input state valid
  inputValid: {
    color: 'black',
    marginHorizontal: Metrics.section,
    paddingBottom: 4,
    justifyContent: 'center',
    fontFamily: Fonts.type.base,
    paddingLeft: 0,
    textAlign: 'left',
    fontSize: 16,
    borderBottomWidth: 1,
    height,
    borderColor: '#dce3e5'
    // borderWidth: 1
  },
  // Input state invalid
  inputError: {
    color: 'red',
    borderColor: 'red',
    fontSize: 16,
    fontFamily: Fonts.type.base
  },

  // Bottom error text
  errorText: {
    marginTop: 5,
    color: 'red',
    paddingLeft: 26,
    fontSize: 12,
    fontFamily: Fonts.type.base
    // height: 5,
  },

  // Placeholder
  placeholderTop: {
    color: 'gray',
    marginTop: 6, // MD has 16
    marginHorizontal: Metrics.section,
    fontFamily: Fonts.type.base,
    fontSize: 12
    // borderWidth: 1
  },
  placeholderTopError: {
    color: 'red',

  }

})
