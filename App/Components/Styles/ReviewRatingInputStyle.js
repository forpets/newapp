import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  starMainContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: moderateScale(16)
  },
  iconStar: {
    color: '#FF1744',
    fontSize: moderateScale(26),
    marginHorizontal: moderateScale(-4)
  },
  iconStarDisable: {
    color: '#ABABAB',
    fontSize: moderateScale(26),
    marginHorizontal: moderateScale(-4)
  }
})
