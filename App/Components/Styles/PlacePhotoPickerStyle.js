import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'
export default StyleSheet.create({
  container: {
    flex: 1
  },
  emptyGalleryContainer: {
    borderWidth: 1,
    borderColor: '#f5f5f5',
    elevation: 3,
    backgroundColor: 'white',
    borderRadius: 4,
    width: '98%',
    height: 85,
    marginLeft: 'auto',
    marginRight: 'auto',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: moderateScale(24)
  },
  iconItemWrapper: {
    marginHorizontal: moderateScale(8)
  },
  loadMorePhotosIcon: {
    // fontSize: moderateScale(26)
  },
  textItemWrapper: {

  },
  textItem: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14),
    color: '#525252'
  },
  addMorePicturesWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },
  addMorePictures: {
    color: '#D76675',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  }
})
