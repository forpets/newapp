import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { ApplicationStyles, Fonts } from '../../Themes/'

const preciosWrapper = {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  borderRightColor: '#9B9B9B',
  borderLeftColor: '#9B9B9B',
  height: '100%',
  paddingTop: moderateScale(26),
  paddingBottom: moderateScale(26)
}

const backgroundColor = '#FD5381'

export default StyleSheet.create({
  container: {
    flex: 1
  },

  preciosContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: 'gray',
    borderTopWidth: 1,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  iconUnselectedWrapper: {
    marginHorizontal: moderateScale(-3)
  },
  iconSelectedWrapper: {
    marginHorizontal: moderateScale(-3)
  },
  iconUnselected: {
    fontSize: moderateScale(22)
  },
  iconSelectedPrice: {
    fontSize: moderateScale(22),
    color: 'white'
  },
  preciosWrapper1: {
    ...preciosWrapper
  },
  preciosWrapper2: {
    ...preciosWrapper,
    borderRightWidth: 1,
    borderLeftWidth: 1
  },
  preciosWrapper3: {
    ...preciosWrapper
  },
  preciosWrapper1Selected: {
    ...preciosWrapper,
    backgroundColor
  },
  preciosWrapper2Selected: {
    ...preciosWrapper,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    backgroundColor
  },
  preciosWrapper3Selected: {
    ...preciosWrapper,
    backgroundColor
  }
})
