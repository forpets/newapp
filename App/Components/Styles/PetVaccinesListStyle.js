import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

const marginLeft = 15

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    elevation: 5,
    width: '92%',
    backgroundColor: 'white',
    borderRadius: 3,
    borderColor: '#ddd',

    paddingVertical: 10,
    marginVertical: 10,
    marginLeft: 'auto',
    marginRight: 'auto'
  },

  vaccineName: {
    color: '#00B0FF',
    fontFamily: Fonts.type.rubikMedium,
    marginLeft,
    marginVertical: 10,
    fontSize: 20,
    // borderWidth: 1
  },
  divider: {
    marginVertical: 4,
    // borderBottomWidth: 0.3,
    borderTopWidth: 0.3,
    borderColor: Colors.lightGray,
    width: '100%',
  },
  text: {
    fontFamily: Fonts.type.base,
    fontSize: 16,
    marginHorizontal: 10,
    marginVertical: 10,
    color: '#565656',
    flex: 1
    // borderWidth: 1
  },
  dot: {
    fontSize: 8,
    color: '#CCC',
    marginHorizontal: '3%'
  },
  dotContainer: {
    marginLeft,
    flex: 1,
    // borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  }
})
