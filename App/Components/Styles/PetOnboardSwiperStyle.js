import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    width: 300,
    height: 300,
    borderRadius: 15,
    backgroundColor: 'white'

  },

  swiper: {
    // height: 100,
    marginHorizontal: 'auto',
    marginVertical: 'auto',
    padding: 5,
    paddingBottom: 10,
    height: '70%'

  },
  slide: {
    margin: 10,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  image: {
    marginTop: 30,
    marginBottom: 20
  },
  text: {
    color: 'gray',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    lineHeight: 20
  },
  paginationStyle: {
    // borderWidth: 1,
    // borderColor: 'red',
    // marginTop: 10,
    marginBottom: 0,
    paddingBottom: 0
    // marginTop: 50,
    // paddingBottom: 35
  },

  dotStyle: {
    borderColor: 'gray',
    backgroundColor: 'white',
    borderWidth: 2,
    width: 9,
    height: 9,
    borderRadius: 4,

    marginLeft: 6,
    marginRight: 6
  },
  activeDotStyle: {
    backgroundColor: 'white',
    width: 12,
    height: 12,
    borderRadius: 8,
    borderWidth: 2,

    marginLeft: 6,
    marginRight: 6
  },
  closeButton: {
    textAlign: 'center',
    marginBottom: 30, 
    fontSize: 14,
    fontFamily: 'Rubik-Medium',
    // borderWidth: 1
  }

})
