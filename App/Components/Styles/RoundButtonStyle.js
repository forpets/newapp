import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 25,
    // width: '32%',
    // marginBottom: 10,
    paddingHorizontal: '3%',
    // marginHorizontal: 20
    // marginVertical: -90
  },

  buttonEnabled: {
    backgroundColor: Colors.loginGray
  },
  buttonDisabled: {
    // backgroundColor: Colors.loginDisabled
    backgroundColor: Colors.loginGray
  },
  text: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 14,
    textAlign: 'center',
    margin: 4
  }
})
