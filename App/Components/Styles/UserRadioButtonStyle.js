import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 24,
    marginVertical: 10
  },

  containerError: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: 'red'
  },

  options: {
    justifyContent: 'space-between',
    width: '60%',
    marginHorizontal: 30,
    marginRight: 'auto'
  },

  // Label
  text: {
    paddingBottom: 14,
    color: '#363636',
    fontFamily: 'Rubik-Regular',
    fontSize: 16
    // marginVertical: 25,

  },
  textError: {
    color: 'red',
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  }
})
