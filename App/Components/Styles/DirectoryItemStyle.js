import {StyleSheet, Dimensions} from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {ApplicationStyles, Metrics} from '../../Themes'

const {height, width} = Dimensions.get('window')
const itemWidth = width * 0.6
const itemHeight = height * 0.28

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
let paddingVertical:any
let marginTop:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 9) : ((Metrics.screenWidth <= 360) ? (fontSize = 12) : (fontSize = 14))
paddingVertical = (Metrics.screenWidth <= 320) ? (paddingVertical = 9) : ((Metrics.screenWidth <= 360) ? (paddingVertical = 11) : (paddingVertical = 13))
marginTop = (Metrics.screenWidth <= 320) ? (marginTop = -22) : ((Metrics.screenWidth <= 360) ? (marginTop = -18) : (marginTop = -20))

/** ************ Media Queries Block Ends ***************/

const styles = StyleSheet.create({
  container: {
    // borderWidth: 1,
    height: itemHeight,
    width: itemWidth * moderateScale(0.9),
    marginBottom: moderateScale(7),
    marginLeft: moderateScale(11),
    marginRight: moderateScale(10),
    // paddingLeft: 0,
    elevation: 5,
    backgroundColor: '#ffffff',
    borderRadius: 8
  },

  directoryItem: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
    // borderWidth: 1,
  },

  header: {
    paddingLeft: moderateScale(5),
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    paddingVertical,
    alignItems: 'center',
    flexDirection: 'row'
  },
  content: {
    // borderWidth: 1,
    paddingLeft: moderateScale(20),
    paddingRight: moderateScale(10),
    flexGrow: 0.4,
    height: moderateScale(100)
  },
  title: {
    flex: 1,
   /*  borderWidth:1,
    width:'85%', */
    color: '#ffffff',
    fontSize: moderateScale(12),
    marginLeft: moderateScale(15)
  },
  icon: {
    marginLeft: 17
  },
  backgroundImage: {
    borderWidth: 1,
    borderRadius: 5,
    width: itemWidth - 1,
    height: itemHeight - 1,
    borderColor: '#ffffff'
  },
  itemName: {
    marginTop: (Metrics.screenWidth < 330) ? 13 : 16,
    color: '#444444',
    fontFamily: 'Rubik-Medium',
    fontSize: (Metrics.screenWidth < 330) ? 14 : 17,
    textAlign: 'left'
  },
  itemAge: {
    color: '#ffffff'
  },
  address: {
    textAlign: 'left',
    color: '#646464',
    marginTop: moderateScale(5),
    fontSize: (Metrics.screenWidth < 400) ? 10 : 12,
    fontFamily: 'Rubik-Regular'
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop,
   /*  paddingBottom: moderateScale(5),
    marginBottom: moderateScale(8), */
	  borderTopWidth: 1,
    borderTopColor: '#e6e6e6',
    width: itemWidth
/*     height: itemHeight */
  },
  rate: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: moderateScale(20),
    paddingTop: moderateScale(15),
    paddingBottom: moderateScale(15)
  },
/*   placeStatusWrapper: {
    borderWidth: 1,
    borderColor: 'red',
    height: (Metrics.screenWidth < 500) ? 20 : null,
    width: (Metrics.screenWidth < 500) ? 90 : null
  }, */
  starIcon: {
    color: '#8ACB27',
    fontSize
/*     height: (Metrics.screenWidth < 500) ? 15 : null,
    width: (Metrics.screenWidth < 500) ? 15 : null */
  },
  rateNumber: {
    fontSize,
    marginLeft: moderateScale(7),
    color: '#646464',
    fontFamily: 'Rubik-Regular'
/*     height: (Metrics.screenWidth < 500) ? 15 : null,
    width: (Metrics.screenWidth < 500) ? 15 : null */
  },
  rateClock: {
/*     paddingTop: moderateScale(-15), */
  /*   paddingBottom: moderateScale(10), */
    // paddingTop: (Metrics.screenHeight < 600) ? -3 : -5,
    marginTop: (Metrics.screenWidth < 400) ? 8 : -1,
    marginHorizontal: (Metrics.screenWidth < 400) ? 10 : 5

  },
  isOpen: {
    fontSize: moderateScale(12),
    color: '#8acc27',
    fontFamily: 'Rubik-Regular'
  }

})

export default styles
