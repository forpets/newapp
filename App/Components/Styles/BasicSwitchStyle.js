import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  container: {
    width: 43,
    height: 22

    // flex: 1,
  },
  iconContainerOn: {
    borderRadius: 13,
    backgroundColor: '#FF1744'
  },
  iconContainerOff: {
    borderRadius: 10
  },
  icon: {
    width: 43,
    height: 22
  }
})
