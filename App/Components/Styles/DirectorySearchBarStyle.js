import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {ApplicationStyles, Metrics, Fonts} from '../../Themes/'

const marginLeft = moderateScale(15)

export default StyleSheet.create({
  modal: {
    padding: 0,
    margin: 0
  },
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: '100%',
    height: moderateScale(250),
    zIndex: 1000,
    flex: 1
    // borderWidth: 1,
    // borderColor: 'red',
  },
  searchingTextWrapper: {
    backgroundColor: '#212121',
    height: moderateScale(70),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  wrapperTextForSearching: {
    width: '100%'
    // marginLeft: moderateScale(5)
  },
  searchingText: {
    color: '#FFFFFF',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(20),
    // paddingTop: moderateScale(1),
    paddingBottom: moderateScale(5),
    borderBottomColor: '#9B9B9B',
    borderWidth: 1,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    width: '78%',
    marginLeft: moderateScale(25),
    marginTop: moderateScale(10)
  },
  historyTextWrapper: {
    paddingVertical: 10,
    backgroundColor: 'white'
  },
  itemsWrapper: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: moderateScale(10)
    // marginVertical: moderateScale(3)
  },
  iconWrapper: {
    flex: 1,
    marginHorizontal: moderateScale(15)
   // marginVertical: moderateScale(3)
  },
  icon: {
    // color: '#444444'
  },
  itemHistoryWrapper: {
    flex: 10,
    paddingRight: moderateScale(15)
  },
  historyText: {
    color: 'black',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16)
  },
  arrowBack: {
    marginLeft,
    fontSize: moderateScale(25),
    color: 'white',
    marginTop: moderateScale(20)
  }

})
