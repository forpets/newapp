// import { StyleSheet } from 'react-native'

// export default StyleSheet.create({
//   container: {
//     flex: 1
//   }
// })
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'
import {StyleSheet, Dimensions} from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
const {height, width} = Dimensions.get('window')
const itemWidth = width * 0.9
const itemHeight = height * 0.23
const itemMargin = width * 0.05
const petDetailWidth = itemWidth * 0.35
const petDetailHeight = itemHeight * 0.37
const petDetailMargin = itemWidth * 0.04

const borderRadius = 6

const styles = StyleSheet.create({
  petItem: {
    width: itemWidth,
    height: itemHeight,
    marginRight: 10,
    marginBottom: 10
    // marginHorizontal: 10
    // margin: itemMargin
  },

  overlayStyle: {
    // borderWidth: 2
    position: 'absolute',
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius
  },
  backgroundImage: {
    borderWidth: 1,
    borderRadius,
    width: itemWidth - 1,
    height: itemHeight - 1,
    borderColor: '#ffffff'
  },
  petDetail: {
    position: 'absolute',
    width: petDetailWidth,
    height: petDetailHeight,
    // FIXME: add background image
    backgroundColor: '#343434',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 1,
    left: petDetailMargin,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  itemName: {
    marginTop: 6,
    fontFamily: Fonts.type.rubikMedium, 
    color: '#9de82e',
    fontSize: moderateScale(15)
  },
  itemAge: {
    color: '#ffffff'
  },
  petAvatar: {
    width: itemWidth,
    height: itemHeight
  },
  imageBone:{
    position:'absolute',
    top: 0, 
    right:0,
   // height:'100%',
     width:'85%'
 }

})

export default styles
