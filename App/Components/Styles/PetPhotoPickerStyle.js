import { StyleSheet } from 'react-native'
import {Fonts, Colors} from '../../Themes'

export default StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: 150,
    backgroundColor: 'white',
    elevation: 4,
    borderRadius: 7,
    marginHorizontal: 20,
    marginVertical: 10,
    borderColor: '#ddd',
    borderWidth: 1
  },
  defaultPhotoContainer: {
    paddingVertical: 20,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerTitle: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 14,
    color: Colors.frost,
    marginTop: 15
  },
  photo: {
    borderWidth: 0,
    borderRadius: 7,
    height: '100%',
    width: '100%'
  }
})
