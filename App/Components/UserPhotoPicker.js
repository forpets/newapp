import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/UserPhotoPickerStyle'
import ImagePicker from 'react-native-image-picker'
import {Images} from '../Themes'

type Props = {
  photoUrl?: string,
  onPhotoSelected?: Function,
  containerStyle?: Object,
  bottomComponent: Object,
  showDefaultImage: boolean
}

type State = {
  selectedPhoto?: Object
}

var options = {
  title: 'Subir foto de tu perfil',
  takePhotoButtonTitle: 'Tomar una foto',
  chooseFromLibraryButtonTitle: 'Seleccionar una foto',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  allowEditing: true,
  noData: true,
  quality: 0.7,
  maxWidth: 1000,
  maxHeight: 1000,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

export default class UserPhotoPicker extends Component<Props, State> {
  static defaultProps = {
    photoUrl: null,
    containerStyle: {},
    bottomComponent: <Text style={styles.avatarText}>Cambiar Imagen</Text>,
    showDefaultImage: true
  }

  constructor (props:Props) {
    super(props)
    this.state = {selecetedPhoto: null}
  }

  pickImage = () => {
    // const {pet} = this.props
    const {onPhotoSelected} = this.props

    ImagePicker.showImagePicker(options, (response) => {
      // const {petAlbumUploadRequest} = this.props

      // FIXME: add photo resize??
      if (response.didCancel) {
        console.tron.warn('User cancelled image picker')
      } else if (response.error) {
        console.tron.error('ImagePicker Error: ', response.error)
      } else {
        // console.tron.warn({msg: 'Image object selected ', resp: response})
        this.setState({ selectedPhoto: response })

        if (onPhotoSelected && typeof onPhotoSelected === 'function') {
          onPhotoSelected(response)
        }
      }
    })
  }

  render () {
    const {selectedPhoto} = this.state
    const {photoUrl, containerStyle, bottomComponent, showDefaultImage} = this.props

    const defaultPhoto = showDefaultImage ? Images.avatar : Images.white
    // Default photo vs photo provided by parent
    const preselectedPhoto = !photoUrl ? Images.avatar : {uri: photoUrl}
    // If user has selected a new photo
    const photo = selectedPhoto ? {uri: selectedPhoto.uri} : preselectedPhoto
    const hasPhoto = (selectedPhoto || photoUrl)
    // console.tron.warn({photo})

    return (
      <TouchableOpacity onPress={this.pickImage}>
        <View style={[styles.container, containerStyle]}>
          {hasPhoto &&
            <Image source={photo} style={styles.photo} />
          }

          {!hasPhoto &&
          <Image source={defaultPhoto} style={styles.photo} />
          }
        </View>
        {bottomComponent}
      </TouchableOpacity>
    )
  }
}
