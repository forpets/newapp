// @flow
import React, { Component } from 'react'
import { View, TextInput, Text } from 'react-native'
import styles from './Styles/BasicInputStyle'
import R from 'ramda'

type Props = {
  placeholder?: string,

  style?: Object | Array<Object>,
  errorTextStyle?: Object | Array<Object>,
  viewStyle?: Object | Array<Object>,

  placeholderTextColor?: string,
  onChangeText?: Function,
  onBlur?: Function,
  error: string,
  onSubmitEditing: Function,
  maxLength: number,
  focus: boolean,
  returnKeyType: 'done' | 'go' | 'next' | "search" | 'send',
  blurOnSubmit: boolean,
  secureTextEntry: boolean,
  defaultValue?: string,
  editable: boolean,
  overlayItem?: Object,
  multiline?: boolean,
  maxLength?: number,
  value?: string
}
// TODO: Add keyboard type

export default class BasicLoginInput extends Component<Props> {
  static defaultProps = {
    // Styling props
    style: {},
    errorTextStyle: {},
    viewStyle: {},

    // Textinput proops
    editable: true,
    placeholderTextColor: 'white',
    placeholder: 'Input',
    onChangeText: () => {},
    onBlur: () => {},
    error: '',
    onSubmitEditing: () => {},
    maxLength: 100,
    focus: false,
    returnKeyType: 'next',
    blurOnSubmit: true,
    secureTextEntry: false,
    defaultValue: '',
    overlayItem: null,
    multiline: true,
    keyboardType: 'default',
    value: null // Can use to force value from parent comp
  }
  // blurOnSubmit = false

  focus () {
    this.textInput.focus()
  }

  // showErrorField = (error : string) => {
  //   const {errorTextStyle} = this.props
  //   if (error) {
  //     return (
  //       <Text style={[errorTextStyle]}>{error}</Text>
  //     )
  //   }
  // }

  componentWillReceiveProps (nextProps : Props) {
    const {focus} = nextProps
    focus && this.focus()
  }

  render () {
    const { placeholder, style, onChangeText, onBlur, error, maxLength, onSubmitEditing, returnKeyType,
    blurOnSubmit, secureTextEntry, defaultValue, viewStyle, placeholderTextColor, errorTextStyle, editable,
    overlayItem, multiline, value, keyboardType} = this.props

    const errorText = error || ' '

    // if (placeholder === 'Nombre *') {
    // console.tron.log(this.props)
    // }

    return (
      <View style={viewStyle || styles.container}>
        <TextInput
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          autoGrow={multiline}
          autoCorrect={false}
          placeholder={placeholder}
          style={[style]}
          underlineColorAndroid={'transparent'}
          onChangeText={onChangeText}
          placeholderTextColor={placeholderTextColor}
          onBlur={onBlur}
          onSubmitEditing={onSubmitEditing}
          returnKeyType={returnKeyType}
          blurOnSubmit={blurOnSubmit}
          secureTextEntry={secureTextEntry}
          defaultValue={defaultValue ? String(defaultValue) : ''}

          value={!R.isNil(value) ? String(value) : null}

          ref={(input) => { this.textInput = input }}
          editable={editable}
        />
        {(overlayItem) && overlayItem }
        <Text style={[errorTextStyle]}>{errorText}</Text>
      </View>
    )
  }
}
