import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, Alert } from 'react-native'
import styles from './Styles/DirectorySearchBarStyle'
import Modal from 'react-native-modal'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import {Images} from '../Themes'
import R from 'ramda'

// No se encontraron resultados

export default class DirectorySearchBar extends Component {
  static defaultProps = {
    show: false,
    hideModalFunc: null,
    searchHistory: [],
    startSearchFunc: () => {}
  }

  constructor (props) {
    super(props)
    this.state = {searchText: null}
  }

  handleSearchInput = (searchText) => {
    this.setState({searchText})
  }

  handleStartSearch = (searchText) => {
    if (searchText && R.length(searchText) > 3) {
      this.props.startSearchFunc(searchText)
    } else {
      Alert.alert('Por favor entra mas de 3 caracteres')
    }
  }

  render () {
    const {show, hideModalFunc, searchHistory} = this.props

    return (
      <Modal
        style={styles.modal}
        // isVisible={this.state.isVisible}
        isVisible={show}
        backdropOpacity={0.4}
        onBackdropPress={hideModalFunc}
        onBackButtonPress={hideModalFunc}
        animationIn='slideInDown'
        animationOut='slideOutUp'
        >

        <View style={styles.container}>
          <View style={styles.searchingTextWrapper}>
            <TouchableOpacity onPress={hideModalFunc} >
              <View style={styles.wrapperArrowBack}>
                <IconMD style={styles.arrowBack} name='arrow-back' />
              </View>
            </TouchableOpacity>
            <View style={styles.wrapperTextForSearching}>
              <TextInput
                autoFocus
                underlineColorAndroid={'transparent'}
                style={styles.searchingText}
                placeholderTextColor='#aeaeae'
                placeholder='Buscar'
                onChangeText={this.handleSearchInput}
                onSubmitEditing={() => this.handleStartSearch(this.state.searchText)}
                maxLength={100}
              />
            </View>
          </View>

          {(R.length(searchHistory) > 0) &&
            <View style={styles.historyTextWrapper}>
              {searchHistory.map((item) => this.renderHistoryItem(item))}
            </View>
          }

        </View>
      </Modal>
    )
  }

  renderHistoryItem = (item) => {
    const key = Math.random()
    return (
      <TouchableOpacity
        style={styles.itemsWrapper}
        key={key}
        onPress={() => this.handleStartSearch(item)}
      >
        <View style={styles.iconWrapper}>
          <Image style={styles.icon} source={Images.directorio.history} />
        </View>
        <View style={styles.itemHistoryWrapper}>
          <Text style={styles.historyText} numberOfLines={1} ellipsizeMode='tail'>{item}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
