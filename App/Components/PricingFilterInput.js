import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import styles from './Styles/PricingFilterInputStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import R from 'ramda'

export default class PricingFilterInput extends Component {
  static defaultProps = {
    onChange: () => {},
    value: null
  }

  onChange = (value) => {
    const {onChange} = this.props
    if (value !== this.props.value && onChange && typeof onChange === 'function') {
      onChange(value)
    }
  }

  render () {
    const {renderSelector} = this
    const {value} = this.props

    return (
      <View style={styles.preciosContainer}>
        {renderSelector(1, value)}
        {renderSelector(2, value)}
        {renderSelector(3, value)}
      </View>
    )
  }

  renderIcon = (selected = false, key) => {
    const wrapperStyle = selected ? styles.iconSelectedWrapper : styles.iconUnselectedWrapper
    const iconStyle = selected ? styles.iconSelectedPrice : styles.iconUnselected
    return (
      <View style={wrapperStyle} key={key}>
        <IconMD style={iconStyle} name='attach-money' />
      </View>
    )
  }

  renderSelector = (count, currentValue) => {
    const selected = (parseInt(currentValue) === count)
    const style = selected ? styles[`preciosWrapper${count}Selected`] : styles[`preciosWrapper${count}`]
    const icon = (key) => this.renderIcon(selected, key)

    return (
      <TouchableOpacity onPress={() => this.onChange(selected ? null : count)} style={style}>
        {R.times(icon, count)}
      </TouchableOpacity>
    )
  }
}
