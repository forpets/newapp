import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native'
import styles from './Styles/ReviewRatingInputStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import R from 'ramda'

export default class ReviewRatingInput extends Component {
  static defaultProps = {
    onPress: () => {},
    value: null
  }

  render () {
    const {renderSelector} = this
    const {value} = this.props

    return (
      <View style={styles.starMainContainer}>
        {renderSelector(1, value)}
        {renderSelector(2, value)}
        {renderSelector(3, value)}
        {renderSelector(4, value)}
        {renderSelector(5, value)}
      </View>
    )
  }

  renderSelector = (count, currentValue) => {
    const selected = (parseInt(currentValue) >= count)
    const style = selected ? styles.iconStar : styles.iconStarDisable

    return (
      <TouchableOpacity onPress={() => this.props.onPress(count)}>
        <View>
          <IconMD style={style} name='star' />
        </View>
      </TouchableOpacity>
    )
  }
}
