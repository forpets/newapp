// @flow
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList, Image, Alert } from 'react-native'
import styles from './Styles/PlacePhotoPickerStyle'
import ImagePicker from 'react-native-image-picker'
import R from 'ramda'
import type PlacePhoto from '../Redux/PlaceRedux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import { Images } from '../Themes'
import AppConfig from '../Config/AppConfig'

const pickerOptions = {
  title: 'Subir foto de espacio',
  takePhotoButtonTitle: 'Tomar una foto',
  chooseFromLibraryButtonTitle: 'Seleccionar una foto',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  allowEditing: true,
  noData: true,
  quality: 0.7,
  maxWidth: 1000,
  maxHeight: 1000,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

const maxPhotos = AppConfig.maxPhotosForPlace

type Props = {
  defaultValue: Array<PlacePhoto>
}

type State = {
  photosList: Array<PlacePhoto>
}

export default class PlacePhotoPicker extends Component<Props, State> {
  // // Defaults for props
  static defaultProps = {
    defaultValue: [],
    onUpdate: null
  }

  constructor (props : Props) {
    super(props)
    const {defaultValue} = this.props
    const photosList = (R.length(defaultValue) > 0) ? defaultValue : []

    console.tron.log({props: this.props, photosList})
    this.state = { photosList }
  }

  handleOnUpdate = () => {
    const {photosList} = this.state
    const {onUpdate} = this.props

    // console.tron.log('handle on update')
    if (onUpdate && typeof onUpdate === 'function') {
      onUpdate(photosList)
    }
  }

  pickPhoto = () => {
    const {photosList} = this.state

    const photos = R.reject(R.propEq('markForDelete', true), photosList)
    if (photos && photos.length >= maxPhotos) {
      Alert.alert('', 'No se puede agregar mas que ' + maxPhotos + ' fotos.',
        [ {text: 'OK', onPress: () => {}} ], { cancelable: false })
      return
    }

    ImagePicker.showImagePicker(pickerOptions, (response) => {
      if (response.didCancel) {
        console.tron.warn('User cancelled image picker')
      } else if (response.error) {
        console.tron.error('ImagePicker Error: ', response.error)
      } else {
        const photo : PlacePhoto = {url: response.uri, imagePickerResponse: response}
        photosList.push(photo)
        // console.tron.warn({photosList})
        this.setState({photosList})
        this.handleOnUpdate()
      }
    })
  }

  // Main render method - CONTAINER
  // If has photos > render photos list
  // If no photos > render placeholder
  render () {
    const {photosList = []} = this.state
    // Hide photos that are marked for deletion
    const photos = R.reject(R.propEq('markForDelete', true), photosList)
    const hasPhotos = (photos && photos.length > 0)

    return (
      <View style={styles.container}>
        { hasPhotos && this.renderPhotosList(photos) }
        { !hasPhotos && this.renderEmptyPlaceholder() }
      </View>
    )
  }

  //
  // Placeholder - if does not have photos
  renderEmptyPlaceholder = () => {
    return (
      <TouchableOpacity onPress={this.pickPhoto}>
        <View style={styles.emptyGalleryContainer}>
          <View style={styles.iconItemWrapper}>
            <Image source={Images.directorio.galeriaAdd} style={styles.loadMorePhotosIcon} />
          </View>
          <View style={styles.textItemWrapper}>
            <Text style={styles.textItem}>Crear una galería</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  //
  // If has photos - SHOW list
  // View container with Add more photos button below
  renderPhotosList = (photosList : Array<PlacePhoto>) => {
    return (
      <View>
        <FlatList
          horizontal
          data={photosList}
          renderItem={({item}) => this.renderPhotoItem(item)}
          keyExtractor={(item, index) => index} />
        <TouchableOpacity onPress={this.pickPhoto}>
          <View style={styles.addMorePicturesWrapper}>
            <View>
              <Text style={styles.addMorePictures}>Agregar más fotos</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  //
  // Photo item - used to by renderPhotosList
  //
  renderPhotoItem = (photoItem: PlacePhoto) => {
    const {url} = photoItem
    return (
      <TouchableOpacity onLongPress={() => this.longPressPhoto(photoItem)}>
        <Image source={{uri: url}} style={{height: 100, width: 100, marginRight: 10}} />
      </TouchableOpacity>
    )
  }

  longPressPhoto = (photoItem: PlacePhoto) => {
    Alert.alert('', '¿Deseas eliminar esta foto?',
      [
        {text: 'CANCELAR', onPress: () => {}},
        {text: 'ELIMINAR', onPress: () => this.deletePhoto(photoItem)}
      ], { cancelable: false })
  }

  deletePhoto = (photoItem : PlacePhoto) => {
    const {photosList} = this.state

    if (photoItem && photoItem.imagePickerResponse) {
      // If new photo - just remove
      const newPhotosList: Array<PlacePhoto> = R.without([photoItem], photosList)
      this.setState({photosList: newPhotosList})
    } else if (photoItem && photoItem.url) {
      // If already existing - mark for deletion
      const index = R.findIndex(R.equals(photoItem), photosList)
      const newPhotoItem = {...photoItem, markForDelete: true}
      const newPhotosList = R.update(index, newPhotoItem, photosList)
      // console.tron.warn({newPhotosList, photosList, photoItem, newPhotoItem})
      this.setState({photosList: newPhotosList})
    } else {
      console.tron.error('PlacePhotoPicker: Trying to delete photo without proper photoItem. ' + JSON.stringify(photoItem))
    }
    this.handleOnUpdate()
  }
}
