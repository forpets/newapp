// @flow
import type {Place} from '../FlowTypes/Place'
import type {PlaceReview} from '../FlowTypes/PlaceReview'

import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './Styles/PlaceReviewsStyle'
import R from 'ramda'
import {Images} from '../Themes'
import Timeago from '../Transforms/Timeago'

type Props = {
  place: Place,
  navigation: Object,
  isFetching: boolean,
  reviews: Array<PlaceReview>,
  showCount: number,
  allUsers: Array<UserPublic>,
  currentUser: User
}
type State = {
  expand: boolean
}

export default class PlaceReviews extends Component<Props, State> {
  static defaultProps = {
    showCount: 3,
    allUsers: {},
    reviews: []
  }

  constructor (props:Props) {
    super(props)
    this.state = {expand: false}

    const timeago = Timeago()
    timeago.setLocale('es')
    this.timeagoInstance = timeago
  }

  goNewReviewScreen = () => {
    const {navigate} = this.props.navigation
    const {id} = this.props.place
    if (!this.props.currentUser) return false
    navigate('ReviewNewScreen', {placeId: id})
  }

  goPlaceClaimScreen = () => {
    const {navigate} = this.props.navigation
    const {id} = this.props.place
    navigate('PlaceClaimScreen', {placeId: id})
  }

  toggleExpand = () => {
    const {expand} = this.state
    this.setState({expand: !expand})
  }

  userCanComment = () => {
    const {reviews, currentUser} = this.props

    if (currentUser) {
      if (reviews && R.length(reviews) > 0) {
        // Can not comment 2 times
        return !(R.find(R.propEq('userId', currentUser.uid), reviews))
      } else {
        return true
      }
    } else {
      // Must be logged in
      return false
    }
  }

  render () {
    const {reviews, isFetching, allUsers, currentUser} = this.props
    const userCanComment = this.userCanComment()

    return (
      <View style={styles.container}>

        { (isFetching || !allUsers) // Wait to fetch reviews and users
           ? <ActivityIndicator size='large' color='gray' />
          : (reviews == null || R.isEmpty(reviews))
              ? this.renderNoReviews()
             : this.renderReviews(reviews)
        }

        {userCanComment &&
        <View style={styles.dividerLine} />
        }

        {userCanComment &&
          <TouchableOpacity onPress={this.goNewReviewScreen}>
            <View style={styles.calificacionWrapper}>
              <Text style={styles.calificacionText}>Añardir calificación a este espacio </Text>
            </View>
          </TouchableOpacity>}

        <View style={styles.dividerLine} />

        { (currentUser) &&
        <TouchableOpacity onPress={this.goPlaceClaimScreen}>
          <View style={styles.espacioWrapper}>
            <Text style={styles.espacioText}>Reportar este espacio como propio </Text>
          </View>
        </TouchableOpacity>
        }

      </View>
    )
  }

  renderNoReviews = () => {
    const {place} = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.goNewReviewScreen}>
          <View style={styles.blankCommentsContainer} >
            <View>
              <Text style={styles.blankComments}>
                {place.name} todavía no tiene ningún comentario, si querés podés dejarnos tu opinión <Text style={{fontWeight: 'bold'}}>aquí</Text>
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  renderReviews = (reviews : Array<PlaceReview>) => {
    const {expand} = this.state
    const {showCount} = this.props
    const reviewsCount = R.length(reviews)

    // Has more than 3 reviews and is not already expanded
    const showExpandButton = (reviewsCount > showCount && !expand)

    const reviewsToShow = expand ? reviews : R.splitAt(showCount, reviews)[0]

    return (
      <View>
        {reviewsToShow.map((review, i) => {
          return this.renderReview(review, i)
        })}

        { showExpandButton && <View style={styles.halfDivider} /> }
        { showExpandButton &&
          <View style={styles.resenaWrapper}>
            <TouchableOpacity onPress={this.toggleExpand} >
              <Text style={styles.resenaText}>MÁS RESEÑAS</Text>
            </TouchableOpacity>
          </View>
        }

      </View>
    )
  }

  renderReview = (review : PlaceReview, i :number) => {
    const {timeagoInstance} = this
    const user = this.props.allUsers[review.userId]

    // Default photo
    let userAvatar = Images.avatar
    let name = 'Usuario desconocido'
    const userAvatarProps = {}

    if (user) {
      if (user.photoURL) userAvatar = {uri: user.photoURL}
      name = user.firstname
      if (user.lastname) name = name + ' ' + user.lastname.substring(0, 1) + '.'
    } else {
      userAvatarProps.tintColor = '#CCC'
    }

    return (
      <View style={styles.profileAndNameWrapper} key={i}>

        <View style={styles.avatarProfileWrapper}>
          <Image style={styles.avatarProfile} source={userAvatar} {...userAvatarProps} />
        </View>
        <View style={styles.nameProfileTextWrapper}>
          <View style={styles.nameItemWrapper}>
            <Text style={styles.nameProfileText} ellipsizeMode='tail' numberOfLines={1}>{name}</Text>
          </View>
          <View style={styles.ratingSubtitleWrapper}>
            {parseInt(review.rating) > 0 && <Image style={styles.imagStar} source={Images.directorio.star} /> }
            {parseInt(review.rating) > 0 && <Text style={styles.numberRatingSubtitle} >{review.rating}</Text> }
            <Text style={styles.commentTime}>{timeagoInstance.format(review.createdAt, 'es')}</Text>
          </View>
          <View style={styles.descriptionComentarioTextWrapper}>
            <Text style={styles.descriptionComentarioText}>{review.comment}</Text>
          </View>
        </View>

      </View>
    )
  }
}
