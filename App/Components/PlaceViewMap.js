import React from 'react'
import { View } from 'react-native'
import MapView from 'react-native-maps'
import PlaceViewMapCallout from './PlaceViewMapCallout'
import Styles from './Styles/PlaceViewMapStyles'
import {getCoordsFromPlace} from '../Transforms/GeoFunctions'
import Categories from '../Services/Categories'
import R from 'ramda'
import AppConfig from '../Config/AppConfig'

class PlaceViewMap extends React.Component {
  constructor (props) {
    super(props)

    // If no location - show obelisc
    const location = getCoordsFromPlace(props.place) || AppConfig.defaultLocation
    const locations = [props.place]
    const region = {...location, latitudeDelta: 0.01, longitudeDelta: 0.01}

    this.state = {
      region,
      locations,
      showUserLocation: true
    }

    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentWillReceiveProps (newProps) {
    /* ***********************************************************
    * STEP 3
    * If you wish to recenter the map on new locations any time the
    * props change, do something like this:
    *************************************************************/
    // this.setState({
    //   region: calculateRegion(newProps.locations, { latPadding: 0.1, longPadding: 0.1 })
    // })
  }

  onRegionChange (newRegion) {
    /* ***********************************************************
    * STEP 4
    * If you wish to fetch new locations when the user changes the
    * currently visible region, do something like this:
    *************************************************************/
    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta / 2,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta / 2,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta / 2,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta / 2
    // }
    // Fetch new data...
  }

  calloutPress (location) {
    /* ***********************************************************
    * STEP 5
    * Configure what will happen (if anything) when the user
    * presses your callout.
    *************************************************************/

    // console.tron.log(location) // Reactotron
  }

  renderMapMarkers = (location : Place) => {
    const {selectedLocation} = this.state
    // Do not render if does not has coordinates
    if (!location.location || R.isEmpty(location.location)) {
      return
    }
    const [latitude, longitude] = location.location
    const category : PlaceCategory = Categories.findOne(location.category)

    // Set category marker
    let image = category.mapMarker

    if (selectedLocation && selectedLocation === location.id) {
      // TODO: get category marker if selected
      image = category.mapMarkerSelected
    }

    return (
      <MapView.Marker
        image={image}
        key={location.id}
        coordinate={{latitude, longitude}}
         />
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        initialRegion={this.state.region}
        onRegionChangeComplete={this.onRegionChange}
        showsUserLocation={this.state.showUserLocation}

        showsMyLocationButton

        rotateEnabled={false}
        pitchEnabled={false}
        showTraffic={false}

        maxZoomLevel={20}
        minZoomLevel={12}
      >
        {this.state.locations.map((location) => this.renderMapMarkers(location))}
      </MapView>
    )
  }
}

export default PlaceViewMap
