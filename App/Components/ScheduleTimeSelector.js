import React, { Component } from 'react'
import { View, Text, Alert, TouchableOpacity, FlatList } from 'react-native'
import styles from './Styles/ScheduleTimeSelectorStyle'
import Modal from 'react-native-modal'
import R from 'ramda'
import { findAll } from '../Services/Vaccines'

const height = 30

const timeGen = (i) => {
  const div = String(Math.floor(i / 4)).padStart(2, '0')
  const mod = String((i % 4) * 15).padStart(2, '0')
  return `${div}:${mod}`
}
const paddedItems = ['', '', '', '', '']
// const availableTimes = [...paddedItems, R.times(timeGen, 4 * 24), ...paddedItems]
const availableTimes = R.times(timeGen, 4 * 24)

export default class ScheduleTimeSelector extends Component {
  static defaultProps = {
    defaultValue: [null, null],
    onChange: () => {}
  }

  constructor (props) {
    super(props)
    const [opensAt, closesAt] = props.defaultValue || [null, null]
    this.state = { opensAt, closesAt }
    this.renderTimeList = this.renderTimeList.bind(this)
  }

  handleViewableItemsChange = ({viewableItems, changed, ...other}) => {
    const middle = viewableItems[Math.round((viewableItems.length - 1) / 2)]

    if (!this.props.show) return // Disable when modal is hidden
    if (middle) {
      // hacky but react does not allow dynamic handler
      const varName = middle.key ? middle.key.split('#')[0] : null
      console.tron.log({viewableItems, changed, other, varName})
      if (varName) this.setState({[varName]: middle.item})
    }
  }

  onConfirm = () => {
    const {opensAt, closesAt} = this.state
    if (opensAt >= closesAt) {
      Alert.alert('Error', 'El horario de apertura no puede ser mayor al de cierre')
    } else {
      this.props.onChange([opensAt, closesAt])
      this.props.hideModalFunc()
    }
  }

  onCancel = () => {
    // Reset state to default values
    const [opensAt, closesAt] = this.props.defaultValue || [null, null]
    // console.tron.log({opensAt, closesAt})
    this.setState({opensAt, closesAt})
    this.props.hideModalFunc()
  }

  getItemLayout = (data, index) => {
    return {length: height, offset: height * index, index}
  }

  // ON INIT COMP - do autoscroll?
  // componentDidUpdate (nextProps) {
  //   if (nextProps.defaultValue) {
  //     const [opensAt = null, closesAt = null] = nextProps.defaultValue
  //     const {show} = nextProps

  //     if (opensAt && closesAt && show) {
  //       const ref = !!(this._opensAt)

  //       if (ref) {
  //         console.tron.log('REF TRU ' + JSON.stringify(opensAt))
  //         // this._opensAt.scrollToEnd()
  //         // this._opensAt.scrollToItem({opensAt, viewPosition: 0.)
  //         // this._closesAt.scrollToItem(closesAt)
  //         // this._opensAt.scrollToIndex({ 10, viewPosition: 0.5 })
  //       }
  //     }
  //   }
  // }

  render () {
    // console.tron.log(this.state)
    return (
      <Modal
        style={styles.modal}
        isVisible={this.props.show}
        animationIn='slideInUp'
        animationOut='slideOutDown'
        onBackdropPress={this.onCancel}
        onBackButtonPress={this.onCancel}
        >
        <View style={styles.container}>
          <View style={styles.topRow}>
            <Text style={styles.topRowText}>Abre a las</Text>
            <View style={styles.topRowDivider} />
            <Text style={styles.topRowText}>Cierra a las</Text>
          </View>

          <View style={styles.middleRow} >
            <View style={styles.middleRowCol} >{this.renderTimeList('opensAt')}</View>
            <View style={styles.middleRowCol} >{this.renderTimeList('closesAt')}</View>
          </View>

          <View style={styles.bottomRow}>
            <TouchableOpacity
              onPress={this.onCancel}
              style={[styles.bottomRowButton, styles.bottomButtonLeft]}
            >
              <Text style={[styles.bottomRowButtonText, styles.cancelButton]}>CANCELAR</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.onConfirm}
              style={[styles.bottomRowButton, styles.bottomButtonRight]}
            >
              <Text style={styles.bottomRowButtonText}>LISTO</Text>
            </TouchableOpacity>
          </View>
        </View>

      </Modal>
    )
  }

  renderTimeList = (varName) => {
    const selectedItem = this.state[varName]
    const keyExtractor = (item) => varName + '#' + item

    const itemStyle = (item) =>
      (item === selectedItem) ? [ styles.timeItem, styles.timeItemSelected ] : styles.timeItem

    const findIndex = R.findIndex((i) => i === selectedItem, availableTimes)
    const initialScrollIndex = findIndex < 0 ? 0 : findIndex

    return (
      <FlatList
      //   contentInset={{top: 100, left: 0, bottom: 0, right: 0}}
        // ref={(fl) => { this[`_${varName}`] = fl }}
        // overScrollMode='always'
        getItemLayout={this.getItemLayout}
        data={availableTimes}
        windowSize={8}
        initialNumToRender={10}
        initialScrollIndex={initialScrollIndex}
        scrollEventThrottle={300}
        snapToInterval={100}
        extraData={selectedItem}
        keyExtractor={keyExtractor}
        showsVerticalScrollIndicator={false}
        onViewableItemsChanged={this.handleViewableItemsChange}
        // onViewableItemsChanged={(...args) => this.handleViewableItemsChange(...args, varName)}
        // viewabilityConfig={{ itemVisiblePercentThreshold: 50 }}
        snapToAlignment={'center'}

        renderItem={({item, idx}) =>
          <Text style={[itemStyle(item), {height}]}>{item}</Text>
      }
    />
    )
  }
}
