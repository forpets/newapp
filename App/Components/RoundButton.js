// @flow
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import styles from './Styles/RoundButtonStyle'
import BasicButton from './BasicButton'

type Props = {
    onPress: Function,
    disabled?: boolean,
    text: string,
    color: string,
    bgColor: string,
    bgColorDisabled: string
  }

export default class RoundButton extends Component<Props> {
  static defaultProps = {
    onPress: () => {},
    disabled: false,
    text: ''
  }

  render () {
    const {disabled, text, color, bgColor} = this.props
    const {bgColorDisabled = bgColor} = this.props

    const buttonStyle = disabled
      ? [styles.button, styles.buttonDisabled, this.props.styles]
      : [styles.button, styles.buttonEnabled, this.props.styles]

    const backgroundColor = disabled ? bgColorDisabled : bgColor

    return (
      <View style={styles.container}>
        <BasicButton
          onPress={this.props.onPress}
          disabled={this.props.disabled}
          styles={[buttonStyle, {backgroundColor}]}>

          <Text style={[styles.text, {color: color}]}>{text}</Text>
        </BasicButton>
      </View>
    )
  }
}
