import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/PetsCarouselItemStyle'
import {Images} from '../Themes'
import SelectorCircle from './SelectorCircle'

export default class PetsCarouselItem extends Component {
  static defaultProps = {
    clickFunction: null,
    selectable: false
  }

  constructor (props) {
    super(props)
    this.state = {selected: false}
  }

  printImage = (image) => {
    const img = image ? {uri: image} : Images.petsDefaultImage
    return <Image source={img} style={StyleSheet.flatten([styles.backgroundImage, styles.petAvatar])} />
  }

  touchAction = (id, name) => {
    const {navigate, clickFunction, selectable} = this.props
    const {selected} = this.state

    // Invert selected state
    if (selectable) {
      this.setState({selected: !selected})
    }

    if (!clickFunction && id) {
      return navigate('PetProfileScreen', {id})
    } else {
      // Send callback with id, select state
      return clickFunction(id, name, !selected)
    }
  }

  render () {
    const {additionalContent, selectable} = this.props
    const {id, name, image} = this.props.pet
    const {selected} = this.state

    const petName = name ? name.substring(0, 10) : 'PetName'

    return (
      <TouchableOpacity onPress={() => this.touchAction(id, name)} key={id} underlayColor='#ffffff'>
        <View style={styles.petItem}>

          {this.printImage(image)}
          {selectable && <View style={styles.overlayStyle}>
            <SelectorCircle selected={selected} />
          </View>}

          <View style={styles.petDetail}>
            <Image style={styles.imageBone} source={Images.boneDog} />
            <Text style={styles.itemName}> {petName} </Text>
            <Text style={styles.itemAge}>
              {/* 6 años FIXME: add YEAR CALCULATOR */}
            </Text>
          </View>
          {additionalContent && additionalContent({id, name})}
        </View>
      </TouchableOpacity>
    )
  }
}
