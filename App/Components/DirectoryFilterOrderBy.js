import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/DirectoryFilterOrderByStyle'
import {Images} from '../Themes'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

export default class DirectoryFilterOrderBy extends Component {
  static defaultProps = {
    value: null,
    onChange: () => {}
  }

  onPress = (itemValue) => {
    const {onChange} = this.props
    if (itemValue !== this.props.value && onChange && typeof onChange === 'function') {
      onChange(itemValue)
    }
  }

  render () {
    return (
      <View style={styles.orderByContainer}>
        {this.renderLocationSelect('nearness')}
        {this.renderRevelanceSelect('revelance')}
        {this.renderRatingSelect('rating')}
      </View>
    )
  }

  renderRatingSelect = (itemValue) => {
    const selected = (itemValue === this.props.value)
    // const containerStyle = selected ? styles.iconContainer : styles.iconContainer
    const containerStyle = styles.iconContainer
    const iconStyle = selected ? styles.iconSelected : styles.iconUnselectedStar

    const icon = (
      <View style={containerStyle}><IconMD style={iconStyle} name='star' /></View>
    )

    return this.renderSelector('Puntaje', icon, itemValue, selected)
  }

  renderRevelanceSelect = (itemValue) => {
    const selected = (itemValue === this.props.value)
    const containerStyle = selected ? styles.iconContainerImage : styles.iconContainerImage
    const iconStyle = selected ? styles.iconSelectedImage : styles.iconUnselectedImage

    const icon = (
      <View style={containerStyle}>
        <Image style={iconStyle} source={Images.directorio.relevanceFilterDisable} />
      </View>
    )
    return this.renderSelector('Relevancia', icon, itemValue, selected)
  }

  renderLocationSelect = (itemValue) => {
    const selected = (itemValue === this.props.value)
    const containerStyle = selected ? styles.iconContainerLocationImage : styles.iconContainerLocationImage
    const iconStyle = selected ? styles.iconSelectedImage : styles.iconUnselectedImage

    const icon = (
      <View style={containerStyle}>
        <Image style={iconStyle} source={Images.directorio.locationFilterDisable} />
      </View>
    )
    return this.renderSelector('Cercanía', icon, itemValue, selected)
  }

  renderSelector = (label, child, itemValue, selected) => {
    // <TouchableOpacity onPress={() => this.onPress(itemValue)}>
    const textStyle = selected ? styles.orderInnerLabelSelected : styles.orderInnerLabel
    return (
      <TouchableOpacity style={styles.orderByWrapper} onPress={() => this.onPress(itemValue)}>
        {child}
        <View style={styles.orderInnerLabelWrapper}>
          <Text style={textStyle}>{label}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
