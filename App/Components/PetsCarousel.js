import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, FlatList } from 'react-native'
import styles from './Styles/PetsCarouselStyle'
import PetsCarouselItem from './PetsCarouselItem'

export default class PetsCarousel extends Component {
  static defaultProps = {
    horizontal: true,
    selectable: false,
    additionalContent: null,
    clickFunction: null
  }

  showList = () => {
    let {list, horizontal, additionalContent, navigate} = this.props
    let key = 0
    if (!list.length) { return }

    return (
      <FlatList
        horizontal={horizontal}
        showsHorizontalScrollIndicator={false}
        removeClippedSubviews
        data={list}
        extraData={this.props}
        renderItem={({item}) => <PetsCarouselItem
          pet={item}
          navigate={navigate}
          additionalContent={additionalContent} />}
        keyExtractor={() => key++}
      />
    )
  }

  render () {
    return (
        <View style={styles.petsContainer}>
          {this.showList()}
        </View>
    )
  }
}
