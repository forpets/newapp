import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native'
import styles from './Styles/BasicButtonStyle'
import colors from '../Themes/Colors'

type Props = {
  children: any,
  onPress: Function,
  disabled?: boolean,
  underlayColor: string
}

export default class BasicButton extends Component<Props> {
  static defaultProps = {
    onPress: () => {},
    disabled: false,
    underlayColor: colors.loginGreen
  }

  render () {
    // TODO: add styles optional
    // TODO: add styles IF DISABLED
    const {disabled} = this.props
    const onPress = disabled ? () => {} : this.props.onPress

    const style = disabled ? [styles.button, styles.buttonDisabled, this.props.styles]
      : [styles.button, styles.buttonEnabled, this.props.styles]

    const underlayColor = disabled ? styles.buttonDisabled.backgroundColor : this.props.underlayColor
    const activeOpacity = disabled ? 1 : 0.2

    return (
      <TouchableOpacity
        activeOpacity={activeOpacity}
        underlayColor={underlayColor}
        style={style}
        onPress={onPress}>
        {this.props.children}
      </TouchableOpacity>
    )
  }
}
