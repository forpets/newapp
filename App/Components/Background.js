import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Image, Text } from 'react-native'
import styles from './Styles/BackgroundStyle'
import {Images} from '../Themes'

export default class Background extends Component {
  // static defaultProps = {
  //   image: Images.homeBackground
  // }

  render () {
    let {image} = this.props

    if (!image) {
      image = Images.homeBackground
    }
    
    return (
      <View style={styles.view}>
        <Image style={styles.backgroundImage} source={image} />
      </View>
    )
  }
}
