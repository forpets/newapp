// @flow
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, DatePickerAndroid } from 'react-native'
import styles from './Styles/UserDateInputStyle'
import {stringToDate, dateToString} from '../Transforms/DateConvert'
import { VaccineSaveTypes } from '../Redux/VaccineSaveRedux'

type Props = {
  defaultValue?: string,
  fieldName: string,
  parentComp: Object,
  placeholder: string,
  onSubmitEditing?: Function,
  maxDate?: Date,
  minDate?: Date
}

type State = {
  selectedDate: string,
}

export default class UserDateInput extends Component<Props, State> {
  constructor (props: Props) {
    super(props)

    this.state = {
      selectedDate: props.defaultValue || ''
    }
  }

  openPicker = async () => {
    const {parentComp, fieldName, onSubmitEditing, maxDate, minDate} = this.props
    const {selectedDate} = this.state

    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: (selectedDate) ? stringToDate(selectedDate) : new Date(),
        maxDate: maxDate || new Date(2030, 12, 12), // RN should fix that
        minDate: minDate || new Date(1900, 12, 12)
      })

      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        const selectedDate = new Date(year, month, day)
        const selectedDateStr = dateToString(selectedDate)

        // Abort if selectedDate > maxDate
        if (maxDate && maxDate < selectedDate) { return }
        this.setState({selectedDate: selectedDateStr})

        // Update field in parent component
        if (parentComp && typeof parentComp.setField === 'function') {
          parentComp && parentComp.setField(fieldName, selectedDateStr)
        }

        // Trigger on submit editing
        if (typeof onSubmitEditing === 'function') { onSubmitEditing() }
      }
    } catch ({code, message}) {
      // console.tron.log({state: this.state, parseddate: stringToDate(this.state.selectedDate), props: this.props.maxDate})
      console.tron.display({name: 'Date Input',
        preview: 'Error opening: ' + code,
        value: {code,
          message,
          state: this.state,
          parsedDate: stringToDate(this.state.selectedDate),
          maxDate: this.props.maxDate}})
    }
  }

  render () {
    const {parentComp, fieldName, placeholder} = this.props
    const {selectedDate} = this.state

    // Field is in error state?
    const error = parentComp ? parentComp.getError(fieldName) : false

    const containerStyle = styles.container
    const unselectedDateError = !error ? styles.UnselectedDate : styles.UnselectedDateError
    // Show placeholder?
    const topPlaceholder = (selectedDate) ? placeholder : ' '
    const errorText = error || ' '

    return (
      <View style={containerStyle}>
        <TouchableOpacity onPress={this.openPicker} style={styles.container2}>
          <Text style={styles.placeholderTop}>{topPlaceholder}</Text>
          {selectedDate
            ? <Text style={styles.selectedDate}>{String(selectedDate)}</Text>
            : <Text style={unselectedDateError}>{String(placeholder)}</Text>}
        </TouchableOpacity>
        <Text style={styles.errorText}>{errorText}</Text>
      </View>
    )
  }
}
