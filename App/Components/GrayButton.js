import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { Text } from 'react-native'
import styles from './Styles/GrayButtonStyle'
import BasicButton from './BasicButton'

type Props = {
  children: any,
  onPress: Function,
  disabled?: boolean,
  text: string
}

export default class GrayButton extends Component<Props> {
  static defaultProps = {
    onPress: () => {},
    disabled: false,
    text: ''
  }

  render () {
    const {disabled, text} = this.props

    const buttonStyle = disabled ? [styles.button, styles.buttonDisabled, this.props.styles]
    : [styles.button, styles.buttonEnabled, this.props.styles]

    return (
      <BasicButton
        onPress={this.props.onPress}
        disabled={this.props.disabled}
        styles={buttonStyle}>
        <Text style={styles.text}>{text}</Text>
      </BasicButton>
    )
  }
}
