import React, { Component } from 'react'
import { View } from 'react-native'
import styles from './Styles/SelectorCircleStyle'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class SelectorCircle extends Component {
  static defaultProps = {
    selected: false
  }

  render () {
    const {selected} = this.props

    return (selected
    ? <View style={[styles.circle, styles.selected]} >
      <Icon name='check' style={styles.checkIcon} />
    </View>
    : <View style={[styles.circle, styles.normal]} />)
  }
}
