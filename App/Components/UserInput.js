// @flow
import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import styles from './Styles/UserInputStyle'
import BasicInput from './BasicInput'
import { Text, View } from 'react-native'

type Props = {
  parentComp: Object, // TODO: RN COMP
  fieldName: string,
  placeholder: string,
  onSubmitEditing: Function,
  secureTextEntry?: boolean,
  defaultValue?: string,
  editable?: boolean,
  returnKeyType?: 'done' | 'go' | 'next' | "search" | 'send',
  overlayItem?: Object,
  placeholderTextColor?: 'string',
  hideTopPlaceholder?: boolean,
  multiline?: boolean,
  maxLength?: number,
  keyboardType?: 'default' | 'numeric' | 'email-address' | 'phone-pad',
  value?: string // Force value from props
}

export default class UserInput extends Component<Props> {
  static defaultProps = {
    parentComp: null,
    fieldName: 'input',

    placeholder: 'Input',
    onSubmitEditing: () => {},
    maxLength: 50,
    focus: false,
    returnKeyType: 'next',
    blurOnSubmit: true,
    secureTextEntry: false,
    editable: true,
    defaultValue: '',
    overlayItem: null,
    placeholderTextColor: 'black',
    hideTopPlaceholder: false,
    multiline: false,
    keyboardType: 'default',
    value: null // Can use to force value from parent prop
  }

  constructor (props : Props) {
    super(props)
    this.state = {showTopPlaceholder: (props.defaultValue)}
  }

  getError = () : string => {
    const {fieldName, parentComp} = this.props
    return (parentComp && typeof parentComp.getError === 'function') ? parentComp.getError(fieldName) : ''
  }

  isFocused = () : boolean => {
    const {fieldName, parentComp} = this.props
    return (parentComp && typeof parentComp.isFocused === 'function') ? parentComp.isFocused(fieldName) : false
  }

  setField = (text : string) => {
    const {fieldName, parentComp} = this.props
    if (parentComp && typeof parentComp.setField === 'function') {
      parentComp.setField(fieldName, text)
    }
  }

  handleChangeText = (text : string) => {
    this.setField(text)
    this.setState({showTopPlaceholder: Boolean(text)})
  }

  render () {
    const {onSubmitEditing,
      defaultValue,
      placeholder,
      secureTextEntry,
      returnKeyType,
      editable,
      overlayItem,
      placeholderTextColor,
      hideTopPlaceholder,
      multiline,
      maxLength,
      value,
      keyboardType
    } = this.props
    const {showTopPlaceholder} = this.state

    const error = this.getError()
    const {handleChangeText} = this

    // If input is invalid merge input valid + input error
    const style = !error ? styles.inputValid : [styles.inputValid, styles.inputError]
    // If input is invalid merge input valid + input error
    // const styleBorderInput = !error ? styles.inputBorder : styles.inputBorderError
    // If input
    const topPlaceholderStyle = !error ? styles.placeholderTop : [styles.placeholderTop, styles.placeholderTopError]
    const errorTextStyle = styles.errorText
    const viewStyle = {flex: 1}

    // Text for top placeholder
    let topPlaceholder = (showTopPlaceholder && !hideTopPlaceholder) ? placeholder : ' '
    // If value set from outside - force topPlaceholder
    if (value) {
      topPlaceholder = placeholder
    }

    if (this.props.fieldName === 'categoryLabel') {
      // console.tron.warn(this.props)
    }

    return (
      <View>

        <Text style={topPlaceholderStyle}>{topPlaceholder}</Text>
        <BasicInput
          style={style}
          errorTextStyle={errorTextStyle}
          viewStyle={viewStyle}
          maxLength={maxLength}
          keyboardType={keyboardType}

          editable={editable}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          placeholderTextColor={placeholderTextColor}
          returnKeyType={returnKeyType}
          error={error}
          focus={this.isFocused()}
          onChangeText={handleChangeText}
          onSubmitEditing={onSubmitEditing}
          defaultValue={defaultValue}
          value={value}

          overlayItem={overlayItem}

          multiline={multiline}
      />
      </View>
    )
  }
}
