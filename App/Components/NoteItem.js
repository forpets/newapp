import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/NoteItemStyle'
import {Images} from '../Themes'

class NoteItem extends Component {
  clickAction = () => {
    let {navigate, note} = this.props
    // console.tron.log('NOTEid: '+ note.id)
    navigate('NotesNewScreen', {id: note.id})
  }

  printImage = (image) => {
      // let {image} = this.state.currentPet;
      // console.log(image);
    if (image) {
      return <Image
        source={{uri: image}}
        style={styles.petAvatar}
        />
    }
    return <Image source={Images.petsDefaultImage} style={styles.petAvatar} />
  }

  render () {
    let {note} = this.props
    let {title, description, createdAt, petName, petImage} = note

    let months = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC']
    var mainDate = new Date(createdAt)

    return (
      <TouchableOpacity style={styles.button} onPress={this.clickAction}>
        <View style={styles.bigContainer}>
          <View style={styles.container}>

            <View style={styles.date}>
              <Text style={styles.day}>{mainDate.getDate()}</Text>
              <Text style={styles.month}>{months[mainDate.getMonth()]}</Text>
            </View>

            <View style={styles.body}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.description} ellipsizeMode='tail' numberOfLines={2} >{description}</Text>
              <View style={styles.petInfo}>
                {this.printImage(petImage)}
                <Text style={styles.petName}> {petName} </Text>
              </View>
            </View>

          </View>
        </View>
      </TouchableOpacity>
    )
  }
  }

export default NoteItem
