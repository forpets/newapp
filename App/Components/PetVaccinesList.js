import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import type Vaccine from '../Services/Vaccines'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import styles from './Styles/PetVaccinesListStyle'
import * as Vaccines from '../Services/Vaccines'
import Icon from 'react-native-vector-icons/FontAwesome'
import R from 'ramda'

export default class PetVaccinesList extends Component {
  // // Defaults for props
  static defaultProps = {
    // someSetting: false
  }

  renderItem = ({item}) => {
    const vaccine = Vaccines.findOne(item.vaccineId)
    const {navigation} = this.props
    // console.tron.log({vaccine, item, Vaccines})
    if (!vaccine) return

    return (
      <TouchableOpacity onPress={() => navigation.navigate('PetVaccineNewScreen', {petVaccineId: item.id, petId: item.petId})} >
        <View style={styles.container}>
          <Text style={styles.vaccineName}>{vaccine.name}</Text>
          <View style={styles.divider} />

          <View style={styles.dotContainer}>
            <Icon name='circle' style={styles.dot} />
            <Text style={styles.text} numberOfLines={3}>{vaccine.description}</Text>
          </View>

          { (item.applicationDate && R.length(item.applicationDate) > 1)
          ? <View style={styles.dotContainer}>
            <Icon name='circle' style={styles.dot} />
            <Text style={styles.text}>Ultima aplicacion: {item.applicationDate}</Text>
          </View>
          : null }

          { (item.nextApplicationDate && R.length(item.nextApplicationDate) > 1)
            ? <View style={styles.dotContainer}>
              <Icon name='circle' style={styles.dot} />
              <Text style={styles.text}>Proxima aplicacion: {item.nextApplicationDate}</Text>
            </View>
          : null}
        </View>
      </TouchableOpacity>
    )
  }

  render () {
    const {data} = this.props

    return (
      <FlatList
        style={{flex: 1}}
        data={data}
        keyExtractor={(item, index) => item.id}
        renderItem={this.renderItem}
      />
    )
  }
}
