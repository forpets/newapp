import React from 'react'
import { View } from 'react-native'
import MapView from 'react-native-maps'
import SetLocationMapCallout from './SetLocationMapCallout'
import Styles from './Styles/SetLocationMapStyles'

// Generate this MapHelpers file with `ignite generate map-utilities`
// import { calculateRegion } from '../Lib/MapHelpers'

class SetLocationMap extends React.Component {

  static defaultProps = {
    onRegionChangeCallback: () => {}
  }

  constructor (props) {
    super(props)

    const locations = [ { title: '', latitude: 37.78825, longitude: -122.4324 } ]
    // * `ignite generate map-utilities`
    // const region = calculateRegion(locations, { latPadding: 0.05, longPadding: 0.05 })
    const region = { latitude: -34.6156625, longitude: -58.5033382, latitudeDelta: 0.1, longitudeDelta: 0.1 }

    this.state = {
      region,
      locations,
      showUserLocation: false
    }
    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentWillReceiveProps (newProps) {
    /* ***********************************************************
    * STEP 3
    * If you wish to recenter the map on new locations any time the
    * props change, do something like this:
    *************************************************************/
    // this.setState({
    //   region: calculateRegion(newProps.locations, { latPadding: 0.1, longPadding: 0.1 })
    // })
  }

  onRegionChange (newRegion) {
    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta / 2,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta / 2,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta / 2,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta / 2
    // }
    // console.tron.warn({searchRegion})
    const {onRegionChangeCallback} = this.props
    const {longitude, latitude} = newRegion
    const locations = [ {title: '', latitude, longitude} ]
    this.setState({locations})

    if (onRegionChangeCallback && typeof onRegionChangeCallback === 'function') {
      onRegionChangeCallback(newRegion)
    }
  }

  calloutPress (location) {
    // console.tron.log(location) // Reactotron
  }

  renderMapMarkers (location) {
    // <SetLocationMapCallout location={location} onPress={this.calloutPress} />
    return (
      <MapView.Marker key={location.title} coordinate={{latitude: location.latitude, longitude: location.longitude}} />
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        initialRegion={this.state.region}
        onRegionChangeComplete={this.onRegionChange}
        showsUserLocation={this.state.showUserLocation}
      >
        {this.state.locations.map((location) => this.renderMapMarkers(location))}
      </MapView>
    )
  }
}

export default SetLocationMap
