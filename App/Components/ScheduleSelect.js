// @flow
import type {PlaceSchedule} from '../FlowTypes/PlaceSchedule'
import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import styles from './Styles/ScheduleSelectStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import Modal from 'react-native-modal'
import CheckBox from 'react-native-check-box'
import R from 'ramda'
import { moderateScale } from 'react-native-size-matters/lib/scalingUtils'
import ScheduleTimeSelector from './ScheduleTimeSelector'

type Props = {
  show: boolean,
  hideModalFunc: Function,
  value: PlaceSchedule,
  onChange: Function
}

type State = PlaceSchedule

export default class ScheduleSelect extends Component<Props, State> {
  static defaultProps = {
    value: {
      days: [],
      h24: false,
      hours: null
    },
    show: false,
    onChange: () => {}
  }

  dataDays = [
      {value: 1, label: 'Lunes'},
      {value: 2, label: 'Martes'},
      {value: 3, label: 'Miercoles'},
      {value: 4, label: 'Jueves'},
      {value: 5, label: 'Viernes'},
      {value: 6, label: 'Sabado'},
      {value: 7, label: 'Domingo'}
  ]

  constructor (props:Props) {
    super(props)

    const {days = [], h24 = false, hours = null} = props.value

    this.state = {
      days,
      h24,
      hours,
      timeSelectModal: false
    }
  }

  // Some tricky logic to avoid wasteful rerenders
  shouldComponentUpdate (nextProps, nextState) {
    const filter = R.pick(['show', 'value'])
    let shouldUpdate = !R.equals(filter(nextProps), filter(this.props))
    // console.tron.log({ next: JSON.stringify(filter(nextProps)), this: JSON.stringify(filter(this.props)), shouldUpdate})
    if (shouldUpdate) return true

    // console.tron.log({nextState: JSON.stringify(nextState), state: JSON.stringify(this.state), shouldUpdate: !R.equals(nextState, this.state) })
    if (!R.equals(nextState, this.state)) {
      // If data updated - push props to parent, do not rerender now
      this.triggerOnChange(nextState)
    // if (this.state.timeSelectModal !== nextState.timeSelectModal) return true
      return true
    }
    return false
  }

  triggerOnChange = (state) => { this.props.onChange(R.pickAll(['days', 'h24', 'hours'], state)) }

  handleScheduleDays = (day:number) : void => {
    const {days} = this.state
    let newDays = null // Creating new object to avoid references
    if (R.contains(day, days)) {
      newDays = R.without([day], days)
    } else {
      newDays = [...days, day].sort()
    }
    // this.setState({days}, this.triggerOnChange)
    this.setState({days: newDays})
  }

  handle24h = () : void => {
    this.setState({h24: !this.state.h24})
    if (!this.state.h24) {
      this.setState({hours: null})
    }
  }

  openTimeSelectModal = () : void => { this.setState({timeSelectModal: true}) }
  closeTimeSelectModal = () : void => { this.setState({timeSelectModal: false}) }

  updateHours = (hours) => {
    this.setState({hours, h24: false})
  }

  render () {
    const {show, hideModalFunc} = this.props
    const {hours} = this.state

    return (
      <Modal
        style={styles.modal}
        isVisible={show}
        animationIn='slideInDown'
        animationOut='slideOutUp'
        onBackdropPress={hideModalFunc}
        onBackButtonPress={hideModalFunc}
      >

        <ScrollView style={styles.container}>
          <View style={styles.header}>
            <View style={styles.iconArrowWrapper}>
              <IconMD name='arrow-back' size={25} onPress={hideModalFunc} style={styles.iconArrow} />
            </View>
          </View>

          <View style={styles.mainTitleWrapper}>
            <Text style={styles.mainTitle}>Días y horarios</Text>
          </View>
          <View style={styles.descriptionWrapper}>
            <Text style={styles.description}>Días que abre el espacio</Text>
          </View>

          <View style={styles.checkBoxesContainer}>
            {this.dataDays.map(day => this.renderCheckbox(day))}

            <TouchableOpacity
              style={styles.scheduleRow}
              onPress={this.openTimeSelectModal}>
              <Text style={styles.scheduleText} >ESTABLECER HORARIO</Text>
              <Text style={styles.scheduleText} >{(hours) && hours[0] + ' - ' + hours[1]}</Text>
            </TouchableOpacity>

            {this.render24hCheckbox()}

          </View>

        </ScrollView>

        <ScheduleTimeSelector
          show={this.state.timeSelectModal}
          hideModalFunc={this.closeTimeSelectModal}
          onChange={this.updateHours}
          defaultValue={this.state.hours}
         />

      </Modal>
    )
  }

  renderCheckbox = (item) => {
    if (!item) return
    const selected = R.contains(item.value, this.state.days)

    return (
      <TouchableOpacity style={styles.row} key={item.value}>
        <CheckBox
          style={styles.checkbox}
          isChecked={selected}
          leftText={item.label}
          onClick={() => this.handleScheduleDays(item.value)}
          leftTextStyle={styles.dayLabel}
          checkBoxColor={selected ? '#FF1744' : 'black'}
        />
      </TouchableOpacity>
    )
  }

  render24hCheckbox= () => {
    const selected = this.state.h24

    // Hack to force rerender checkbox
    return (
      <TouchableOpacity style={styles.renderCheckboxRowContainer}>
        <CheckBox
          key={Math.random(1)} // Hack to force rerender
          style={styles.checkbox}
          isChecked={selected}
          leftText={'Abierto 24 horas'}
          leftTextStyle={styles.dayLabel}
          checkBoxColor={selected ? '#FF1744' : 'black'}
          onClick={this.handle24h}
        />
      </TouchableOpacity>
    )
  }
}
