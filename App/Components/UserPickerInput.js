// @flow
import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Picker, Text } from 'react-native'
import styles from './Styles/UserPickerInputStyle'

type Props = {
  selectedValue?: string,
  fieldName: string,
  parentComp: Object,
  optionsForSelect: Array,
  placeholder: string,
}

type State = {
  showTopPlaceholder: boolean
}

export default class UserPickerInput extends Component<Props, State> {
  static defaultProps = {
    selectedValue: null
  }

  constructor (props : Props) {
    super(props)
    // const selectedIndex = (defaultValue) ? options.findIndex((obj) => defaultValue === obj.value) : -1
    // this.state = {selectedIndex}
    this.state = {
      showTopPlaceholder: Boolean(props.selectedValue)
    }
  }

  showOptions = () => {
    const { optionsForSelect = [], selectedValue } = this.props

    if (optionsForSelect && optionsForSelect.length > 0) {
      return optionsForSelect.map((item, index) => {
        // const color = (selectedValue === item.value) ? 'red' : 'black'
        const color = 'black'
        if (item && item.label) {
          return <Picker.Item
            label={item.label}
            value={item.value}
            key={item.value}
            color={color}
          />
        } else {
          // If label = null
          return <Picker.Item label='???' value='' key='?' />
        }
      })
    } else {
      return <Picker.Item label='' value='' key='1' />
    }
  }

  handlePick = (itemValue : string, itemIndex : number) => {
    const {parentComp, fieldName} = this.props
    // Show placeholder when item is not default value not empty
    if (itemIndex) {
      this.setState({showTopPlaceholder: (itemIndex !== 0 && Boolean(itemValue))})
      parentComp && parentComp.setField(fieldName, itemValue)
    }
  }

  render () {
    const {selectedValue, parentComp, fieldName, placeholder} = this.props

    // If field is in error state?
    const error = parentComp ? parentComp.getError(fieldName) : false

    const errorStyle = error ? [styles.errorText, styles.errorTextError] : styles.errorText
    const topPlaceholder = this.state.showTopPlaceholder ? placeholder : ' '
    const errorText = error || ' '

    return (
      <View style={styles.container}>
        <Text style={styles.placeholderTop}>{topPlaceholder}</Text>
        <Picker
          style={styles.picker}
          textStyle={styles.placeholderText}
          selectedValue={selectedValue}
          onValueChange={this.handlePick}
          // prompt={placeholder}
          >
          { (placeholder) && <Picker.Item label={placeholder} value={''} key={9999999999999999999} color='gray' /> }
          {this.showOptions()}
        </Picker>
        <Text style={errorStyle}>{errorText}</Text>
      </View>
    )
  }
}
