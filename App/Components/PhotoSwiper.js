import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import styles from './Styles/PhotoSwiperStyle'
import Swiper from 'react-native-swiper'
import R from 'ramda'

export default class PhotoSwiper extends Component {

  renderImage = ({url}) => {
    return (
      <View style={styles.slide} key={String(url)}>
        <Image style={styles.sliderImage} source={{ uri: url }} />
      </View>
    )
  }

  render () {
    let { photos } = this.props

    // if (R.length(photos) === 0) {
    //   photos = ['http://via.placeholder.com/350x150', 'http://via.placeholder.com/350x150', 'http://via.placeholder.com/350x150']
    // }

    const photosList = R.slice(0, 3, photos)
    // const addKey = (url, key, obj) => {key, url}
    // const photosToRender = R.map(photosList, addKey)

    return (
      <View style={styles.container}>
        <Swiper
          paginationStyle={styles.paginationStyle}
          style={styles.swiper}
          dotStyle={styles.dotStyle}
          activeDotStyle={styles.activeDotStyle}>
          {R.map(this.renderImage, photosList)}

        </Swiper>
      </View>
    )
  }
}
