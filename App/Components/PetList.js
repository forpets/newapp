import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, FlatList, StyleSheet } from 'react-native'
import styles from './Styles/PetListStyle'
import PetsCarouselItem from './PetsCarouselItem'

export default class PetList extends Component {
  static defaultProps = {
    horizontal: true,
    selectable: false,
    additionalContent: null
  }

  showList = () => {
    let {list, horizontal, clickFunction, additionalContent, navigate, selectable} = this.props
    let key = 0
    if (!list.length) { return }

    return (
      <FlatList
        horizontal={horizontal}
        showsHorizontalScrollIndicator={false}
        removeClippedSubviews
        data={list}
        extraData={this.props}
        renderItem={({item}) =>
          <PetsCarouselItem
            selectable={selectable}
            pet={item}
            clickFunction={clickFunction}
            navigate={navigate}
            additionalContent={additionalContent} />
        }
        keyExtractor={() => key++}
      />
    )
  }

  render () {
    return (
      <View style={styles.petsContainer}>
        {this.showList()}
      </View>
	        )
  }
}
