import React, { Component } from 'react'
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './Styles/DirectoryListStyle'
import DirectoryItem from './DirectoryItem'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import R from 'ramda'

class DirectoryList extends Component {
  render () {
    let navigate = this.props.navigate
    let {directory, fetching} = this.props

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Directorio</Text>
          <TouchableOpacity onPress={() => { this.props.navigate('DirectoriesScreen', {showList: false}) }}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={styles.rightTitle}>Ver mapa</Text>
              <IconMD name='keyboard-arrow-right' style={{fontSize: 18, textAlign: 'center', textAlignVertical: 'center', color: '#939393'}} />
            </View>
          </TouchableOpacity>

        </View>
 
        <View style={styles.directoryContainer}>

          {(!fetching)
            ? <FlatList horizontal
              showsHorizontalScrollIndicator={false}
              scrollEventThrottle={100}
              initialNumToRender={6}
              windowSize={4}
              data={directory}
              renderItem={({item}) => <DirectoryItem navigate={navigate} place={item} name={item.name} rate={item.rate} />}
              keyExtractor={(item) => item.id}
            />
            : <ActivityIndicator size={60} color='gray' />
          }
        </View>

      </View>
    )
  }
}

export default DirectoryList
