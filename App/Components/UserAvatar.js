// @flow
import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { TouchableOpacity, Image } from 'react-native'
import styles from './Styles/UserAvatarStyle'
import {Images} from '../Themes'

type Props = {
  photoURL?: string,
  noDefaultImage: boolean,
  onPress?: Function,
  style?: Object
}

export default class UserAvatar extends Component<Props> {
  static defaultProps = {
    photoURL: null,
    onPress: () => {},
    style: {},
    noDefaultImage: false,
  }

  render () {
    const {onPress, photoURL, style, noDefaultImage} = this.props
    // If no user - default photo
    // const photoURL = (.photoURL) ? user.photoURL : null
    // If no photo - default photo
    // FIXME: allow blank photo - for registration - showBlank instead of avatar
    const defaultImage = noDefaultImage ? Images.white : Images.avatar
    const photo = (photoURL) ? {uri: photoURL} : defaultImage

    return (
      <TouchableOpacity onPress={onPress}>
        <Image source={photo} style={[styles.image, style]} />
      </TouchableOpacity>
    )
  }
}
