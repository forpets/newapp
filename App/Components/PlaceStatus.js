import React, { Component } from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/PlaceStatusStyle'
import Icon from 'react-native-vector-icons/FontAwesome'
import R from 'ramda'

export default class PlaceStatus extends Component {
  static defaultProps = {
    schedule: null
  }

  render () {
    const {schedule = false} = this.props

    if (!schedule) return null

    const {days = [], h24 = null, hours = []} = schedule
    let d = new Date()
    const day = d.getDay() === 0 ? 7 : d.getDay() // UTC treats sunday as day 0

    let isOpen = false

    if (R.contains(day, days)) {
      if (h24) {
        isOpen = true
      } else {
        const [opensAt, closesAt] = hours
        let currentTime = d.getHours() + ':' + d.getMinutes
        isOpen = (opensAt <= currentTime && currentTime <= closesAt && R.contains(day, days))
      }
    }

    return (
      <View style={styles.container}>
        { isOpen &&
        <View>
          <View style={styles.rateClock}>
            <Icon name='clock-o' style={styles.clockIconOpen} />
            <Text style={styles.isOpen}> ABIERTO &nbsp; </Text>
          </View>
        </View>
        }

        { !isOpen &&
        <View>
          <View style={styles.rateClock}>
            <Icon name='clock-o' style={styles.clockIconClosed} />
            <Text style={styles.isClosed}> CERRADO &nbsp; </Text>
          </View>
        </View>
         }
      </View>
    )
  }
}
