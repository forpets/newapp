// @flow
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/DirectoryItemStyle'
import Icon from 'react-native-vector-icons/FontAwesome'
import Categories from '../Services/Categories'
import PlaceStatus from '../Components/PlaceStatus'

class DirectoryItem extends Component {
                // <Stethoscope fill='#ffffff' width={20} height={20}/>
  render () {
    let {navigate, place} = this.props

    const category = Categories.findOne(place.category)

    return (
      <TouchableOpacity onPress={() => { navigate('PlaceViewScreen', { id: place.id }) }}>

        <View style={styles.container}>
          <View style={styles.directoryItem}>

            <View style={[styles.header, {backgroundColor: category.color}]}>
              <View style={styles.icon} >
                <Image source={category.iconImage} />
              </View>

              <Text numberOfLines={1} ellipsizeMode={'tail'} style={styles.title}>{Categories.idToString(place.category).toUpperCase()}</Text>
            </View>

            <View style={styles.content}>
              <Text style={styles.itemName} numberOfLines={2} ellipsizeMode='tail'>{place.name}</Text>
              <Text style={styles.address} numberOfLines={2} ellipsizeMode='tail'>{place.address}</Text>
            </View>

            <View style={styles.footer}>
              {!place.rating
                ? <View style={styles.rate} />
                : <View style={styles.rate}>
                  <Icon name='star' style={styles.starIcon} />
                  <Text style={styles.rateNumber}>{place.rating}</Text>
                </View>
              }

              <View style={styles.rateClock}>

                <PlaceStatus schedule={place.schedule} />

                {/*   <Icon name='clock-o' style={{color: '#8ACB27'}} />
                <Text style={styles.isOpen}> ABIERTO &nbsp; </Text> */}
              </View>
            </View>

          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

export default DirectoryItem
