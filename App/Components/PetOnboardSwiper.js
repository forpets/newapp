import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/PetOnboardSwiperStyle'
import Swiper from 'react-native-swiper'
import {Images} from '../Themes'

export default class PetOnboardSwiper extends Component {

  static defaultProps = {
    onPress: () => {}
  }

  render () {

    const {onPress} = this.props

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Swiper
            paginationStyle={styles.paginationStyle}
            style={styles.swiper}
            dotStyle={styles.dotStyle}
            activeDotStyle={styles.activeDotStyle}>

            <View style={styles.slide}>
              <Image source={Images.pets.medical} style={styles.image} />
              <Text style={styles.text}>
              Desde tu perfil podes sumar{'\n'} informacion medica de tu mascota{'\n'}para brindarle un mejor cuidado
              </Text>
            </View>

            <View style={styles.slide}>
              <Image source={Images.pets.vaccines} style={styles.image} />
              <Text style={styles.text}>
              Desde tu perfil podes sumar{'\n'} informacion medica de tu mascota{'\n'}para brindarle un mejor cuidado
              </Text>
            </View>

            <View style={styles.slide}>
              <Image source={Images.pets.photos} style={styles.image} />
              <Text style={styles.text}>
              Desde tu perfil podes sumar{'\n'} informacion medica de tu mascota{'\n'}para brindarle un mejor cuidado
              </Text>
            </View>
          </Swiper>

          <TouchableOpacity onPress={onPress}>
            <Text style={styles.closeButton}>
            Omitir </Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}
