import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native'
import styles from './Styles/CategoryHeaderStyle'
import {Images} from '../Themes'
import Categories from '../Services/Categories'

export default class CategoryHeader extends Component {
  render () {
    const {categoryId} = this.props
    const category = Categories.findOne(categoryId)

    return (
      <View style={[styles.sectionHeader, {backgroundColor: category.color}]}>
        <Image source={category.iconImage} style={styles.logoImage} resizeMode='cover' />
        <Text style={styles.text1}>{category.label}</Text>
        <Image
          source={Images.directories.anuncio}
          style={styles.anuncioImage}
          resizeMode='contain'
          height={20}
        />
      </View>
    )
  }
}
