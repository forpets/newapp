import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import styles, {autocompleteInputStyle} from './Styles/GoogleAutocompleteInputStyle'
import AppConfig from '../Config/AppConfig'

  // Documentation
  // https://github.com/FaridSafi/react-native-google-places-autocomplete

  // MUST INCLUDE POWERED BY GOOGLE LOGO
  // https://developers.google.com/places/web-service/policies#logo_requirements

export default class GoogleAutocompleteInput extends Component {
  static defaultProps = {
    parentComp: null,
    fieldName: 'googleAutocompleteField',
    onGetCoordinates: () => {},
    onGetDetails: () => {},
    defaultValue: null,
    placeholder: null
  }

  constructor (props) {
    super(props)
    this.state = {showTopPlaceholder: Boolean(props.defaultValue)}
  }

  setField = (formattedAddress) => {
    const {parentComp, fieldName} = this.props
    if (parentComp && typeof parentComp.setField === 'function') {
      parentComp.setField(fieldName, formattedAddress)
    }
  }

  // Callback when we receive data from GOOGLE
  onPress = (data, details = null) => { // 'details' is provided when fetchDetails = true
    const {onGetDetails, onGetCoordinates} = this.props

    if (details) {
      const {formatted_address} = details
      console.tron.display({name: 'GMaps autocomplete', preview: formatted_address, value: {data, details}})

      this.setField(formatted_address)

      if (onGetDetails && typeof onGetDetails === 'function') { onGetDetails(details) }

      if (onGetCoordinates && typeof onGetCoordinates === 'function') {
        const {lat, lng} = details.geometry.location
        onGetCoordinates({lat, lng})
      }
    } else {
      console.tron.display({name: 'GMaps autocomplete', preview: 'No result!', value: {data, details}, important: true})
    }
  }

  getError = () : string => {
    const {fieldName, parentComp} = this.props
    return parentComp ? parentComp.getError(fieldName) : ''
  }

  handleChangeText = (text : string) => {
    this.setField(text)
    this.setState({showTopPlaceholder: Boolean(text)})
  }

  render () {
    const {handleChangeText} = this
    const {defaultValue, placeholder} = this.props

    // Error
    const error = this.getError()
    const errorText = error || ' '
    const errorBorderStyle = error ? [styles.border, styles.errorBorder] : styles.border

    // Placeholder
    const topPlaceholderStyle = !error ? styles.placeholderTop : [styles.placeholderTop, styles.placeholderTopError]
    const topPlaceholder = this.state.showTopPlaceholder ? placeholder : ' '

    return (
      <View style={styles.container}>
        <Text style={topPlaceholderStyle}>{topPlaceholder}</Text>

        <GooglePlacesAutocomplete
          placeholder={placeholder}
          minLength={3} // minimum length of text to search
          autoFocus={false}
          fetchDetails

          textInputProps={{
            autoCorrect: false,
            placeholderTextColor: 'black',
            onChangeText: handleChangeText
          }}

          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed='auto'    // true/false/undefined
          renderDescription={row => row.description} // custom description render
          onPress={this.onPress}

          getDefaultValue={() => defaultValue || ''}

          query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
            key: AppConfig.googleApiKey,
            language: 'es',
            types: 'address',
            components: 'country:ar',
            origin: 'http://mywebsite.com' // Just placeholder - google ignores it
          }}

          styles={autocompleteInputStyle}

          // currentLocation
          // currentLocationLabel='Mi ubicacion'

          nearbyPlacesAPI='GooglePlacesSearch'

            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro}
          // GooglePlacesSearchQuery={{
          // // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro}
          // // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          //   rankby: 'distance',
          //   types: 'food'}

          filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

          debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      />
        <View style={errorBorderStyle} />
        <Text style={styles.errorText}>{errorText}</Text>
      </View>
    )
  }
}
