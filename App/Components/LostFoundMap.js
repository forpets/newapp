// @flow
import React from 'react'
import MapView from 'react-native-maps'
import LostFoundMapCallout from './LostFoundMapCallout'
import Styles from './Styles/DirectoriesMapStyles'
import {Images} from '../Themes'
import { calculateRegion } from '../Lib/MapHelpers'
import AppConfig from '../Config/AppConfig'
import R from 'ramda'
import LostFound from '../Services/LostFound'

class LostFoundMap extends React.Component {
  // For full documentation, see https://github.com/lelandrichardson/react-native-maps
  static defaultProps = {
    locations: []
  }

  constructor (props : Props) {
    super(props)
    // const region = calculateRegion(locations, { latPadding: 0.05, longPadding: 0.05 })
    // const region = { latitude: -34.6156625, longitude: -58.5033382, latitudeDelta: 0.1, longitudeDelta: 0.1 }

    // FIXME: maps limit for screen

    // Go to default coordinates if do not receive coords (in case when app recently started)
    const {latitude, longitude} = props.currentLocation ? props.currentLocation : AppConfig.defaultLocation

    const region = {latitude, longitude, latitudeDelta: 0.1, longitudeDelta: 0.1}
    // console.tron.warn({props, AppConfig, region})

    this.state = {
      region,
      locations: props.locations,
      showUserLocation: true,
      selectedLocation: null,
      navigation: props.navigation
    }

    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentDidMount () {
  }

  componentWillReceiveProps (newProps: Props) {
    const newLocations = newProps.locations
    if (!R.equals(newLocations, this.state.location)) {
      this.setState({locations: newLocations})
    }
    // console.tron.log({newProps})
    // If you wish to recenter the map on new locations any time the props change, do something like this:

    // if (newProps.currentLocation) {
    //   // const region = calculateRegion(newProps.currentLocation, { latPadding: 0.1, longPadding: 0.1 })
    //   const {longitude, latitude} = newProps.currentLocation
    //   const region = {longitude, latitude, latitudeDelta: 0.01, longitudeDelta: 0.01}
    //   console.tron.log({location: newProps.currentLocation, region})
    //   this.setState({ region })
    // }
  }

  onRegionChange (newRegion) {
    // If you wish to fetch new locations when the user changes the
    // currently visible region, do something like this:

    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta / 2,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta / 2,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta / 2,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta / 2
    // }
  }

  calloutPress = (location) => {
    const {navigate} = this.props.navigation
    console.debug('Pressed report marker: ', location.id)
    // navigate('PlaceViewScreen', {id: location.id})
  }

  selectLocation = (id : string) => { this.setState({selectedLocation: id}) }

  renderMapMarkers = (location) => {
    const {selectedLocation} = this.state
    // Do not render if does not has coordinates
    if (!location.location || R.isEmpty(location.location)) {
      return
    }
    // console.debug('location for marker ', location.location)
    const [latitude, longitude] = location.location.map((val) => (parseFloat(val)))
    
    const typeConfig = LostFound.findOne(location.reportType)
    location.typeConfig = typeConfig

    // Set category marker
    let image = typeConfig.mapMarker

    if (selectedLocation && selectedLocation === location.id) {
      // TODO: get typeConfig marker if selected
      image = typeConfig.mapMarkerSelected
    }

    return (
      <MapView.Marker
        image={image}
        key={location.id}
        coordinate={{latitude, longitude}}
        onPress={() => this.selectLocation(location.id)}
      >
        <LostFoundMapCallout
          location={location}
          onPress={this.calloutPress}
          currentLocation={this.props.currentLocation}
        />
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        rotateEnabled={false}
        pitchEnabled={false}
        showTraffic={false}
        // region={this.state.region} // Want forced region?
        initialRegion={this.state.region}
        onRegionChangeComplete={this.onRegionChange}

        showsUserLocation={this.state.showUserLocation}
        showsMyLocationButton

        maxZoomLevel={20}
        minZoomLevel={12}
      >
        {this.state.locations.map((location) => this.renderMapMarkers(location))}
      </MapView>
    )
  }
}

export default LostFoundMap
