// @flow
import React, { Component } from 'react'
import styles from './Styles/LoginInputStyle'
import BasicInput from './BasicInput'

type Props = {
  parentComp: Object, // TODO: RN COMP
  fieldName: string,
  placeholder: string,
  onSubmitEditing: Function,
  secureTextEntry?: boolean,
  defaultValue?: string,
  returnKeyType?: 'done' | 'go' | 'next' | "search" | 'send',

}

export default class LoginInput extends Component<Props> {
  static defaultProps = {
    parentComp: null,
    fieldName: 'input',

    placeholder: 'Input',
    onSubmitEditing: () => {},
    maxLength: 100,
    focus: false,
    returnKeyType: 'next',
    blurOnSubmit: true,
    secureTextEntry: false,
    defaultValue: ''
  }

  getError = () : string => {
    const {fieldName, parentComp} = this.props
    return parentComp ? parentComp.getError(fieldName) : ''
  }

  isFocused = () : boolean => {
    const {fieldName, parentComp} = this.props
    return parentComp ? parentComp.isFocused(fieldName) : false
  }

  setField = (text : string) => {
    const {fieldName, parentComp} = this.props
    if (parentComp) {
      parentComp.setField(fieldName, text)
    }
  }

  render () {
    const {onSubmitEditing, defaultValue, placeholder, secureTextEntry, returnKeyType} = this.props
    const error = this.getError()

    const style = !error ? styles.inputValid : [styles.inputValid, styles.inputError]
    const errorTextStyle = styles.error
    const viewStyle = styles.viewStyle
    const placeholderColor = !error ? 'white' : 'red'

    // console.tron.log({field: this.props.fieldName, ...this.props})

    return (
      <BasicInput
        style={style}
        errorTextStyle={errorTextStyle}
        viewStyle={viewStyle}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry}
        placeholderTextColor={placeholderColor}
        returnKeyType={returnKeyType}
        error={error}
        focus={this.isFocused()}
        onChangeText={(text) => this.setField(text)}
        onSubmitEditing={onSubmitEditing}
        defaultValue={defaultValue}
      />
    )
  }
}
