// @flow
import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/PetPhotoPickerStyle'
import {Images} from '../Themes'
import ImagePicker from 'react-native-image-picker'

type Props = {
  photoUrl?: string,
  onPhotoSelected?: Function
}

type State = {
  selectedPhoto?: Object
}

var options = {
  title: 'Subir foto de tu mascota',
  takePhotoButtonTitle: 'Tomar una foto',
  chooseFromLibraryButtonTitle: 'Seleccionar una foto',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  noData: true,
  allowEditing: true,
  quality: 0.7,
  maxWidth: 1000,
  maxHeight: 1000,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

export default class PetPhotoPicker extends Component<Props, State> {
  static defaultProps = {
    photoUrl: null
  }

  constructor (props:Props) {
    super(props)
    this.state = {selecetedPhoto: null}
  }

  pickImage = () => {
    // const {pet} = this.props
    const {onPhotoSelected} = this.props

    ImagePicker.showImagePicker(options, (response) => {
      // const {petAlbumUploadRequest} = this.props

      // FIXME: add photo resize??
      if (response.didCancel) {
        console.tron.warn('User cancelled image picker')
      } else if (response.error) {
        console.tron.error('ImagePicker Error: ', response.error)
      } else {
        // console.tron.warn({msg: 'Image object selected ', resp: response})
        this.setState({ selectedPhoto: response })

        if (onPhotoSelected && typeof onPhotoSelected === 'function') {
          onPhotoSelected(response)
        }
      }
    })
  }

  render () {
    const {selectedPhoto} = this.state
    const {photoUrl} = this.props

    // Default photo vs photo provided by parent
    const defaultPhoto = !photoUrl ? Images.pets.uploadPhoto : {uri: photoUrl}
    // If user has selected a new photo
    const photo = selectedPhoto ? {uri: selectedPhoto.uri} : defaultPhoto
    const hasPhoto = (selectedPhoto || photoUrl)
    // console.tron.warn({photo})

    return (
      <TouchableOpacity onPress={this.pickImage}>
        <View style={styles.container}>
          {hasPhoto &&
            <Image source={photo} style={styles.photo} />
          }

          {!hasPhoto &&
            <View style={styles.defaultPhotoContainer}>
              <Image source={defaultPhoto} />
              <Text style={styles.headerTitle}>SUBÍ UNA FOTO DE TU MASCOTA</Text>
            </View>
          }
        </View>
      </TouchableOpacity>
    )
  }
}
