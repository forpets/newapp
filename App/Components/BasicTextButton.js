import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/BasicTextButtonStyle'

type Props = {
  text: string,
  onPress: Function,
  style?: Object
}
export default class BasicTextButton extends Component<Props> {

  static defaultProps = {
    someSetting: false,
    style: {},
    text: '',
    onPress: (t) => {}
  }

  render () {
    const {onPress, text, style} = this.props

    return (
      <TouchableOpacity style={styles.container} onPress={onPress} >
        <Text style={style}>{text}</Text>
      </TouchableOpacity>
    )
  }
}

 