import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/MenuButtonStyle'
import {Images} from '../Themes'

type Props = {
  onPress?: Function,
  disable?: boolean,
  text: string,
  image: Object
}

export default class MenuButton extends Component<Props> {
  // // Prop type warnings
  //
  // Defaults for props
  static defaultProps = {
    onPress: () => console.tron.log('Button pressed!'),
    disabled: false,
    //FIXME: default image
    image: Images.backButton,
    text: "Menu item"
  }

  render () {
    const {onPress, disabled, image, text} = this.props

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.menuItem}>
          <View style={styles.iconContainer}>
            <Image style={styles.iconSize} source={image} />
          </View>
          <Text style={styles.menuItemText}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
