import React, { Component } from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/NavBarsStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

export default class NavBars extends Component {
  static defaultProps = {
    onPress: () => {}
  }

  render () {
    return (
      <IconMD
        name='menu'
        size={25}
        onPress={this.props.onPress}
        style={{paddingLeft: 20}}
      />
    )
  }
}
