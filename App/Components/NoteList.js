import React, {Component} from 'react'
import { View, Text, Image, FlatList, ScrollView } from 'react-native'
import styles from './Styles/NoteListStyle'
import NoteItem from './NoteItem'

class NoteList extends Component {
  compare = (a, b) => {
    if (a.createdAt > b.createdAt) { return -1 }
    if (b.createdAt <= b.createdAt) { return 1 }
// a debe ser igual b
    return 0
  }

  render () {
    let {notes = [], navigate, lastItem} = this.props

    notes.sort(this.compare)

    let firstNote
    if (notes.length === 0) {
      firstNote = notes
    } else {
      firstNote = [notes[0]]
    }

    // SHOW ALL NOTES
    if (!lastItem) {
      return (
        <View style={styles.container}>
          <FlatList
            data={notes}
            extraData={this.props}
            renderItem={({item}) => <NoteItem
              note={item}
              navigate={navigate}
												/>}
            keyExtractor={(item) => item.id}
					            />
        </View>
      )

    // SHOW ONLY LAST NOTE
    } else {
      return (
        <View style={styles.container}>
          <FlatList
            data={firstNote}
            extraData={this.props}
            renderItem={({item}) => <NoteItem
              note={item}
              navigate={navigate}
												/>}
            keyExtractor={(item) => item.id}
											/>
        </View>
      )
    }
  }
}

export default NoteList
