import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/MapListSwitchStyle'
export default class MapListSwitch extends Component {
  static defaultProps = {
    onSwitchMap: null,
    defaultValue: true
  }

  constructor (props) {
    super(props)
    this.state = {showMap: props.defaultValue}
  }

  switchMap = (showMap) => {
    const {onSwitchMap} = this.props
    this.setState({showMap})
    if (onSwitchMap && typeof onSwitchMap === 'function') { onSwitchMap(showMap) }
  }

  render () {
    const {showMap} = this.state

    const mapButtonStyle = showMap ? [styles.leftButton, styles.selectedButton] : styles.leftButton
    const listButtonStyle = !showMap ? [styles.rightButton, styles.selectedButton] : styles.rightButton

    const mapTextStyle = showMap ? [styles.buttonText, styles.selectedText] : styles.buttonText
    const listTextStyle = !showMap ? [styles.buttonText, styles.selectedText] : styles.buttonText

    return (
      <View style={styles.container}>

        <TouchableOpacity onPress={() => this.switchMap(true)}>
          <View style={mapButtonStyle}>
            <Text style={mapTextStyle}>Mapa</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.switchMap(false)}>
          <View style={listButtonStyle}>
            <Text style={listTextStyle}>Lista</Text>
          </View>
        </TouchableOpacity>

      </View>
    )
  }
}
