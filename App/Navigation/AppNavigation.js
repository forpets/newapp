import React from 'react'
import { DrawerNavigator, StackNavigator } from 'react-navigation'
import PlaceClaimSuccessScreen from '../Containers/PlaceClaimSuccessScreen'
import PlaceClaimScreen from '../Containers/PlaceClaimScreen'
import ReviewSentScreen from '../Containers/ReviewSentScreen'
import ReviewNewScreen from '../Containers/ReviewNewScreen'
import PlaceCreateSuccessScreen from '../Containers/PlaceCreateSuccessScreen'
import PlaceEditScreen from '../Containers/PlaceEditScreen'
import PlaceViewScreen from '../Containers/PlaceViewScreen'
import ForgotPwSuccessScreen from '../Containers/ForgotPwSuccessScreen'
import PetVaccineNewScreen from '../Containers/PetVaccineNewScreen'
import DevScreen from '../Containers/DevScreen'
import NotesNewScreen from '../Containers/NotesNewScreen'
import PetAlbumFotoScreen from '../Containers/PetAlbumFotoScreen'
import PetsListScreen from '../Containers/PetsListScreen'
import NotesScreen from '../Containers/NotesScreen'
import PetVaccinesScreen from '../Containers/PetVaccinesScreen'
import PetMedicalInfoScreen from '../Containers/PetMedicalInfoScreen'
import PetProfileEditScreen from '../Containers/PetProfileEditScreen'
// import DirectoriesList from '../Containers/DirectoriesList'
import DirectoriesScreen from '../Containers/DirectoriesScreen'
import DirectoryFilterScreen from '../Containers/DirectoryFilterScreen'
import UserProfileEditScreen from '../Containers/UserProfileEditScreen'
import PetProfileScreen from '../Containers/PetProfileScreen'
import PetProfile from '../Containers/PetProfile'
import PetAlbums from '../Containers/PetAlbums'
import AboutUsScreen from '../Containers/AboutUsScreen'
import HomeScreen from '../Containers/HomeScreen'
import ForgotPwScreen from '../Containers/ForgotPwScreen'
import RegisterScreen from '../Containers/RegisterScreen'
import LoginScreen from '../Containers/LoginScreen'
import TermsAndCondScreen from '../Containers/TermsAndCondScreen'
import UserProfileScreen from '../Containers/UserProfileScreen'
import LostFoundScreen from '../Containers/LostFoundScreen'
import LostFoundEditScreen from '../Containers/LostFoundEditScreen'
import LostFoundFilterScreen from '../Containers/LostFoundFilterScreen'

import styles from './Styles/NavigationStyles'
import Menu from '../Containers/Menu'

export const HomeNav = StackNavigator({
  HomeScreen: { screen: HomeScreen },
  AboutUsScreen: { screen: AboutUsScreen },
  TermsAndCondScreen: { screen: TermsAndCondScreen },

  UserProfileScreen: { screen: UserProfileScreen },
  UserProfileEditScreen: { screen: UserProfileEditScreen },

  PetsListScreen: { screen: PetsListScreen },
  PetProfileEditScreen: { screen: PetProfileEditScreen },
  PetProfile: { screen: PetProfile },

  PetMedicalInfoScreen: { screen: PetMedicalInfoScreen },

  PetVaccinesScreen: { screen: PetVaccinesScreen },
  PetVaccineNewScreen: { screen: PetVaccineNewScreen },

  PetAlbums: { screen: PetAlbums },
  PetAlbumFotoScreen: { screen: PetAlbumFotoScreen },

  PetProfileScreen: { screen: PetProfileScreen },

  NotesNewScreen: { screen: NotesNewScreen },
  NotesScreen: { screen: NotesScreen },

  LostFoundScreen: { screen: LostFoundScreen },
  LostFoundEditScreen: { screen: LostFoundEditScreen },
  LostFoundFilterScreen: { screen: LostFoundFilterScreen },

  DevScreen: { screen: DevScreen }, // FIXME: disable prod

  PlaceViewScreen: { screen: PlaceViewScreen },
  PlaceCreateSuccessScreen: { screen: PlaceCreateSuccessScreen },
  PlaceEditScreen: { screen: PlaceEditScreen },
  DirectoryFilterScreen: { screen: DirectoryFilterScreen },
  DirectoriesScreen: { screen: DirectoriesScreen },

  ReviewNewScreen: { screen: ReviewNewScreen },
  ReviewSentScreen: { screen: ReviewSentScreen },

  PlaceClaimSuccessScreen: { screen: PlaceClaimSuccessScreen },
  PlaceClaimScreen: { screen: PlaceClaimScreen }
}, {
  initialRouteName: 'HomeScreen',
  // initialRouteName: 'PlaceClaimSuccessScreen',
  // initialRouteName: 'DirectoryFilterScreen',
  // initialRouteName: 'DirectoriesScreen',
//  initialRouteName: 'DevScreen',
  // initialRouteName: 'LostFoundScreen',
  headerMode: 'float'
})

export const LoginNav = StackNavigator({
  LoginScreen: { screen: LoginScreen },
  RegisterScreen: { screen: RegisterScreen },
  TermsAndCondScreen: { screen: TermsAndCondScreen },
  ForgotPwScreen: { screen: ForgotPwScreen },
  ForgotPwSuccessScreen: { screen: ForgotPwSuccessScreen }
}, {
  initialRouteName: 'LoginScreen'
})

// PRIMARY NAVIGATOR
const PrimaryNav = DrawerNavigator({
  LoginNav: { screen: LoginNav },
  HomeNav: { screen: HomeNav }

}, {
  initialRouteName: 'LoginNav',
  mode: 'modal',
  contentComponent: props => <Menu {...props} />
})

export default PrimaryNav
