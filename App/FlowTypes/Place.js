// @flow

export type Place = {
  id? : string,

  email: string,
  emergencyNote: string,
  emergencyPhone: string,
  name: string,
  owner: string, // Id of owner
  ownerAssignedAt: number,
  phone: number,
  photos: Array<PlacePhoto>,
  updateAt: number,
  website: string,
  address: string,

  location: Array<number>,

  rating?: string,
  pricing?: number,

  category: number,
  secondaryCategories: Array<number>,

  advert: boolean,
  location: Array<number>, // [lat, lng]

  public?: boolean,
  published?: boolean

}
