// @flow
export type PlaceReview = {
  userId: string,
  placeId: string,
  rating?: number,
  pricing?: number,
  comment?: string,

  createdAt: number,

  reviewed: boolean,
  userId: User
}