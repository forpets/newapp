// @flow
export type PlaceCategory = {
  value: number,
  label: string,

  iconImage: Object,
  starredImage: Object,

  mapMarker: Object,
  mapMarkerSelected: Object
}
