// @flow

export type PlacePhoto = {
  url: string,
  id?: string,
  imagePickerResponse?: Object,
  markForDelete?: boolean
}