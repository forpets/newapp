
// @flow
export type PlaceSchedule = {
  days: Array<number>,
  h24: boolean,
  hours: Array<string>
}
