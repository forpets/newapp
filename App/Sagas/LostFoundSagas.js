
import { call, put, select, take } from 'redux-saga/effects'
import R from 'ramda'
import LostFoundRedux from '../Redux/LostFoundRedux'
import { getCurrentUser } from '../Redux/UserRedux'
import Guid from '../Lib/Guid'
import NavHelper from '../Lib/NavHelper'
import StoragePaths from '../Services/StoragePaths'
import { ToastAndroid } from 'react-native'
import { getCurrentLocation, LocationTypes } from '../Redux/LocationRedux'

// Firestore path
const reportsPath = 'reports'

export function * createReport (sagaFirebase, action) {
  const { data, imagePickerResponse } = action

  data.createdAt = new Date().getTime()
  data.updatedAt = new Date().getTime()

  try {
    if (! data.location) {
      let currentLocation = yield select(getCurrentLocation)
      if (!currentLocation) {
        yield take(LocationTypes.LOCATION_SUCCESS)
        currentLocation = yield select(getCurrentLocation)
      }
      data.location = [currentLocation.latitude, currentLocation.longitude]
    }
    
    // NOTE - redux-saga-firebase does not get .key from react-native-firebase implementation on push()
    // yield call(sagaFirebase.database.update, reportsPath, data)
    const reportDoc = yield call(sagaFirebase.firestore.addDocument, reportsPath, data)

    // Upload image
    if (imagePickerResponse && imagePickerResponse.uri) {
      const image = yield call(uploadReportImage, sagaFirebase, reportDoc.id, imagePickerResponse)
      // Update report with image url
      const thisReportPath = reportsPath + '/' + reportDoc.id
      yield call(sagaFirebase.firestore.updateDocument, thisReportPath, 'image', image)
    }

    // Success
    yield put(LostFoundRedux.reportSaveSuccess())

    yield put(NavHelper.reset('LostFoundScreen'))

  } catch ({code, message, ...error}) {
    console.tron.display({name: 'createReport', preview: message, value: {code, message, action, error}, important: true})
    ToastAndroid.show('No pudimos crear nuevo reporte.\n' + code, ToastAndroid.LONG)
    yield put(LostFoundRedux.reportSaveFailure())
  }
}

export function * updateReport (sagaFirebase, action) {
  const { data, id, imagePickerResponse } = action
  const thisReportPath = reportsPath + '/' + id
  data.updatedAt = new Date().getTime()

  try {
    if (imagePickerResponse && imagePickerResponse.uri) {
      data.image = yield call(uploadReportImage, sagaFirebase, id, imagePickerResponse)
    }
    yield call(sagaFirebase.firestore.updateDocument, thisReportPath, data)
    yield put(LostFoundRedux.reportSaveSuccess())

    yield put(NavHelper.reset('LostFoundScreen'))

  } catch ({code, message}) {
    console.tron.display({name: 'updateReport', preview: message, value: {code, message}, important: true})
    yield put(LostFoundRedux.reportSaveFailure())
  }
}

export function * updateReportPhoto (sagaFirebase, action) {
  const { id, imagePickerResponse } = action
  const thisReportPath = reportsPath + '/' + id
  const updatedAt = new Date().getTime()

  try {
    if (imagePickerResponse && imagePickerResponse.uri) {
      data.image = yield call(uploadReportImage, sagaFirebase, id, imagePickerResponse)
      yield call(sagaFirebase.firestore.updateDocument, thisReportPath, 'image', image)
      // yield call(sagaFirebase.firestore.updateDocument, thisReportPath, 'updatedAt', updatedAt)
    }
    yield put(LostFoundRedux.reportSaveSuccess())

    yield put(NavHelper.reset('LostFoundScreen'))

  } catch ({code, message}) {
    console.tron.display({name: 'updateReportPhoto', preview: message, value: {code, message}, important: true})
    yield put(LostFoundRedux.reportSaveFailure())
  }
}

function * uploadReportImage (sagaFirebase, reportId, imagePickerResponse) : string {
  const { uri } = imagePickerResponse
  const mime = 'image/jpeg'
  const user = yield select(getCurrentUser)
  const uploadPath = StoragePaths.getReportProfilePhotoPath(reportId, user.uid)

  try {
    yield call(sagaFirebase.storage.uploadFile, uploadPath, uri, {contentType: mime})
    return yield call(sagaFirebase.storage.getDownloadURL, uploadPath)
  } catch ({code, message}) {
    console.tron.display({name: 'uploadReportImage', preview: 'Save error: ' + message, value: {code, message, reportId, imagePickerResponse, uploadPath}, important: true})
    yield put(LostFoundRedux.reportSaveFailure())
  }
}

export function * deleteReport (sagaFirebase, action) {
  // const {data} = action
  // const user = yield select(getCurrentUser)

  // if (data && data.length > 0) {
  //   for (let petId of data) {
  //     const pet = yield select(getPet, petId)
  //     const petProfilePhotoPath = StoragePaths.getReportProfilePhotoPath(petId, user.uid)
  //     const recordPath = '/pets/' + petId

  //     // Delete whole storage folder does not work on firebase
  //     // Not critical - on fail we do not abort the deletion process
  //     try {
  //       yield call(sagaFirebase.storage.deleteFile, petProfilePhotoPath)
  //     } catch ({code, message}) {
  //       console.tron.display({name: 'deletePetProfilePhoto', preview: 'Delete pet photo: ' + message, value: {code, message, petId, pet, petProfilePhotoPath}, important: true})
  //     }

  //     console.tron.log({album: pet.album, length: R.keys(pet.album).length})
  //     if (pet.album && R.keys(pet.album).length > 0) {
  //       const albums = yield select(getAlbumForPet, petId)
  //       for (const album of albums) {
  //         console.tron.log({deleting: 'DELETING', album})
  //         try {
  //           // Directly delete file ignoring pet.album array (anyway we are going to delete whole PET records)
  //           if (album.storagePath) yield call(sagaFirebase.storage.deleteFile, album.storagePath)
  //         } catch ({code, message}) {
  //           console.tron.display({name: 'deletePetAlbumPhoto', preview: 'Delete pet album photo: ' + message, value: {code, message, petId, pet, album}, important: true})
  //         }
  //       }
  //     }

  //     try {
  //         // Delete the pet record
  //       yield call(sagaFirebase.database.delete, recordPath)
  //     } catch ({code, message}) {
  //       console.tron.display({name: 'deletePet', preview: 'Delete pet error: ' + message, value: {code, message, petId, pet}, important: true})
  //       yield put(PetDeleteRedux.petDeleteFailure())
  //     }
  //   }

  //   yield put(PetDeleteRedux.petDeleteSuccess())
  // } else {
  //   console.tron.display({name: 'deletePet', preview: 'Delete error, list empty.', value: {}, important: true})
  //   yield put(PetDeleteRedux.petDeleteFailure())
  // }
}
