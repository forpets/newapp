
import { call, put } from 'redux-saga/effects'
import NoteSaveRedux from '../Redux/NoteSaveRedux'
import NoteDeleteRedux from '../Redux/NoteDeleteRedux'
import Guid from '../Lib/Guid'
import NavHelper from '../Lib/NavHelper'
import R from 'ramda'

export function * createNote (sagaFirebase, {petId, data}) {
  const id = Guid()
  const notePath = '/pets/' + petId + '/notes/' + id

  try {
    yield call(sagaFirebase.database.update, notePath, data)
    yield put(NoteSaveRedux.noteSaveSuccess())
    yield put(NavHelper.reset('NotesScreen'))
  } catch (error) {
    yield put(NoteSaveRedux.noteSaveFailure())
  }
}

export function * updateNote (sagaFirebase, {id, petId, data, oldNote}) {
  // Delete old note if user has changed pet
  if (oldNote.petId !== petId) {
    try {
      const oldNotePath = '/pets/' + oldNote.petId + '/notes/' + oldNote.id
      console.tron.log('deleteing ' + oldNotePath, 1)
      yield call(sagaFirebase.database.delete, oldNotePath)
    } catch ({code, message, ...error}) {
      console.tron.display({name: 'updateNote', preview: message, value: {code, message, error}, important: true})
      yield put(NoteSaveRedux.noteSaveFailure())
    }
  }

  const newNote = R.merge(oldNote, data)
  const notePath = '/pets/' + petId + '/notes/' + id
  try {
    yield call(sagaFirebase.database.patch, notePath, newNote)
    yield put(NoteSaveRedux.noteSaveSuccess())
    yield put(NavHelper.reset('NotesScreen'))
  } catch ({code, message, ...error}) {
    console.tron.display({name: 'updateNote', preview: message, value: {code, message, error}, important: true})
    yield put(NoteSaveRedux.noteSaveFailure())
  }
}

export function * deleteNote (sagaFirebase, action) {
  const {data} = action
  const notePath = '/pets/' + data.petId + '/notes/' + data.id

  try {
    yield call(sagaFirebase.database.delete, notePath)
    yield put(NoteDeleteRedux.noteDeleteSuccess())
    yield put(NavHelper.reset('NotesScreen'))
  } catch (error) {
    yield put(NoteDeleteRedux.noteDeleteFailure())
  }
}
