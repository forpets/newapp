
// @flow
import { call, put, select } from 'redux-saga/effects'
import {ToastAndroid} from 'react-native'
import {delay} from 'redux-saga'
import PlaceSearchActions, {getCurrentSearchTerm} from '../Redux/PlaceSearchRedux'
import lunr from 'lunr'
import { getFilteredPlaces, getPlaceFilter } from '../Redux/FilteredPlacesRedux'
import R from 'ramda'
import {Place} from '../FlowTypes/Place'
import Categories from '../Services/Categories'
import { sortPlaces } from './FilteredPlacesSagas'
import { getCurrentLocation } from '../Redux/LocationRedux'

export function * startPlaceSearchCf (cf, action) :void {
  let search = yield select(getCurrentSearchTerm)
  // Add asterix to search by root of word
  search += '*'

  try {
    yield put(PlaceSearchActions.placeSearchBarHide())

    const filter = yield select(getPlaceFilter)
    const currentLocation = yield select(getCurrentLocation)

    // FIND API
    const response = yield call(cf.searchPlaces, filter, currentLocation, search)

    if (response.ok) {
      yield put(PlaceSearchActions.placeSearchSuccess(response.data))
      if (R.length(response.data) < 1) {
        ToastAndroid.show('No hemos encontrado resultados para esta busqueda', ToastAndroid.LONG)
      }
    } else {
      console.tron.display({name: 'startPlaceSearch', preview: 'CF fail', value: {action, response, search}, important: true})
      ToastAndroid.show('Hemos encontrado un error durante la busqueda')
      yield put(PlaceSearchActions.placeSearchFailure())
    // yield put(PlaceSearchActions.placeSearchSuccess([]))
    }
  } catch ({code, message}) {
    console.tron.display({name: 'startPlaceSearch', preview: message, value: {code, message, action, search}, important: true})
    ToastAndroid.show('Hemos encontrado un error durante la busqueda', ToastAndroid.LONG)
    yield put(PlaceSearchActions.placeSearchFailure())
  }
}

export function * startPlaceSearch (action) :void {
  let searchTerm = yield select(getCurrentSearchTerm)
  // Add asterix to search by root of word
  searchTerm += '*'

  try {
    yield put(PlaceSearchActions.placeSearchBarHide())
    const places = yield select(getFilteredPlaces)
    const index = yield call(reindexPlaces, places)

    const searchResult = index.search(searchTerm)

    if (R.length(searchResult) > 0) {
      const getFunc = (result) => R.find(R.propEq('id', result.ref), places)
      const results = R.map(getFunc, searchResult)
      // do RESORT
      const filter = yield select(getPlaceFilter)
      const currentLocation = yield select(getCurrentLocation)
      const sorted = sortPlaces(results, filter.orderBy, currentLocation)

      yield put(PlaceSearchActions.placeSearchSuccess(sorted))
    } else {
      yield put(PlaceSearchActions.placeSearchSuccess([]))
    }
  } catch ({code, message}) {
    console.tron.display({name: 'startPlaceSearch', preview: message, value: {code, message, action}, important: true})
    ToastAndroid.show('Hemos encontrado un error durante la busqueda', ToastAndroid.LONG)
    yield put(PlaceSearchActions.placeSearchFailure())
  }
}

function * reindexPlaces (places : Array<Place>) {
  const index = yield call(lunr, function () {
    this.ref('id')
    this.field('name')
    this.field('address')
    this.field('categoryString')

    // this.use(categoryConverter)
    places.forEach(function (place) {
      this.add({...place, 'categoryString': Categories.idToString(place.category)})
    }, this)
  })

  return index
}

// const categoryConverter = function (builder) {
//   const catConv = function (token, ...other) {
//     console.tron.log({token, other})
//     return token
//   }
//   lunr.Pipeline.registerFunction(catConv, 'categoryConverter')
//   // If we want to convert search params
//   // builder.searchPipeline.before(lunr.stemmer, catConv)
//   builder.pipeline.before(lunr.stemmer, catConv)
// }
