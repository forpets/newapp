import { call, put, select, take } from 'redux-saga/effects'
import FilteredLostFoundRedux, {getLostFoundFilter} from '../Redux/FilteredLostFoundRedux'
import {ToastAndroid} from 'react-native'
import R from 'ramda'
import { getCurrentLocation, LocationTypes } from '../Redux/LocationRedux'
import GeoFunctions, {isPlaceWithinRadius} from '../Transforms/GeoFunctions'
import AppConfig from '../Config/AppConfig'

// SHOULD SEARCH TOO.....????
export function * filterLostFoundCf (cf, action) {
  let filter = yield select(getLostFoundFilter)
  let currentLocation = yield select(getCurrentLocation)

  if (!currentLocation) {
    yield take(LocationTypes.LOCATION_SUCCESS)
    currentLocation = yield select(getCurrentLocation)
  }
  if (!filter) {
    yield put(FilteredLostFoundRedux.filteredLostFoundApplyDefault())
  }
  filter = yield select(getLostFoundFilter)

  yield put(FilteredLostFoundRedux.filteredLostFoundSetMeta(currentLocation))

  // make the call to the api
  const response = yield call(cf.filterLostFound, filter, currentLocation)

  if (response.ok) {
    yield put(FilteredLostFoundRedux.filteredLostFoundSuccess(response.data))
    if (R.length(response.data) < 1) {
      ToastAndroid.show('No hemos encontrado resultados para esta búsqueda', ToastAndroid.LONG)
    }
  } else {
    __DEV__ && console.tron.log(response, 1)
    yield put(FilteredLostFoundRedux.filteredLostFoundFailure())
    ToastAndroid.show('Hemos encontrado un error durante la búsqueda', ToastAndroid.LONG)
  }
}
