
import { call, put, select, take } from 'redux-saga/effects'
import PetsActions, { getPetsArray, getPet } from '../Redux/PetsRedux'
import {getCurrentUser} from '../Redux/UserRedux'
import PetAlbumUploadRedux from '../Redux/PetAlbumUploadRedux'
import Guid from '../Lib/Guid'
import PetCreateRedux from '../Redux/PetCreateRedux'
import MedicalInfoSaveRedux from '../Redux/MedicalInfoSaveRedux'
import PetDeleteRedux from '../Redux/PetDeleteRedux'
import NavHelper from '../Lib/NavHelper'
import StoragePaths from '../Services/StoragePaths'
import R from 'ramda'
import {getAlbumForPet} from '../Redux/PetAlbumsRedux'
import { ToastAndroid } from 'react-native'

export function * triggerLoadPets (action) {
  yield put(PetsActions.petsRequest())
}

export function * syncUserPets (sagaFirebase, action) : void {
  let user = yield select(getCurrentUser)
  try {
    const ref = sagaFirebase.app.database().ref('pets').orderByChild('owner').equalTo(user.uid)
    const channel = sagaFirebase.database.channel(ref)

    while (true) {
      const snapshot = yield take(channel)
    // yield put(PetsActions.petsRequest())
    // console.tron.log({snapshot})
      yield put(PetsActions.petsSuccess({payload: snapshot.value}))

      user = yield select(getCurrentUser)
      if (!user) {
        yield put(PetsActions.petsNoUser({}))
        break
      }
    }
  } catch ({code, message, ...details}) {
    console.tron.display({name: 'syncUserPets', preview: message, value: {code, message, action, details}, important: true})
  }
}

// export function * loadPets (sagaFirebase, firebase, action) {
//   try {
//     const user = yield select(getCurrentUser)
//   // TODO: convert to WHILE user logged in - do SYNC

//     if (!user) {
//       yield put(PetsActions.petsNoUser({}))
//     } else {
//       const ref = firebase.database().ref('pets').orderByChild('owner').equalTo(user.uid)
//       const pets = yield call(sagaFirebase.database.read, ref)

//       yield put(PetsActions.petsSuccess({payload: pets}))
//     }
//   } catch ({code, message, ...meta}) {
//     console.tron.display({name: 'loadPets', preview: message, value: {code, message, action, meta}, important: true})
//     yield put(PetsActions.petsFailure())
//   }
// }

export function * uploadPetAlbumPhoto (sagaFirebase, action) {
  const {imagePickerResponse, pet} = action.data
  const {uri} = imagePickerResponse
  const mime = 'image/jpeg'
  const filename = Guid() + '.jpg'
  const currentUser = yield select(getCurrentUser)

  const uploadPath = StoragePaths.getPetAlbumPath(pet.id, currentUser.uid) + filename
  const albumPhotoRecord = '/pets/' + pet.id + '/album/' + Guid()

  try {
    yield call(sagaFirebase.storage.uploadFile, uploadPath, uri, {contentType: mime})
    const uploadUrl = yield call(sagaFirebase.storage.getDownloadURL, uploadPath)
    const albumImage = yield call(sagaFirebase.database.update, albumPhotoRecord, {image: uploadUrl, storagePath: uploadPath})

    yield put(PetAlbumUploadRedux.petAlbumUploadSuccess({payload: albumImage}))
  } catch ({code, message, ...error}) {
    console.tron.display({name: 'uploadPetAlbumPhoto', preview: message, value: {code, message, action, error}, important: true})
    yield put(PetAlbumUploadRedux.petAlbumUploadFailure())
  }
}

export function * deletePetAlbumPhoto (sagaFirebase, action) {
  const {data} = action
  const {album, petId} = data
  const {storagePath} = album
  const albumRecordPath = '/pets/' + petId + '/album/' + album.id

  try {
    yield call(sagaFirebase.database.delete, albumRecordPath)
    if (storagePath) {
      console.tron.log('Deleting file: ' + storagePath)
      yield call(sagaFirebase.storage.deleteFile, storagePath)
    }
  } catch ({code, message, ...error}) {
    console.tron.display({name: 'deletePetAlbumPhoto', preview: message, value: {code, message, action, error}, important: true})
    yield put(PetAlbumUploadRedux.petAlbumDeleteFailure())
  }
}

export function * createPet (sagaFirebase, action) {
  const {data, imagePickerResponse} = action

  const id = Guid()
  const petPath = '/pets/' + id
  data.createdAt = new Date().getTime()
  data.updatedAt = new Date().getTime()

  try {
    // If did not had pets - show onboards
    const petsArray = yield select(getPetsArray)
    const hasPets = (petsArray && petsArray.length > 0)

    // NOTE - redux-saga-firebase does not get .key from react-native-firebase implementation on push()
    yield call(sagaFirebase.database.update, petPath, data)

    // Upload image
    if (imagePickerResponse && imagePickerResponse.uri) {
      data.image = yield call(uploadPetImage, sagaFirebase, id, imagePickerResponse)
      // Update pet with image url
      yield call(sagaFirebase.database.patch, petPath, data)
    }

    // Success
    yield put(PetCreateRedux.petSaveSuccess())

    const params = {id, showOnboard: !hasPets}
    yield put(NavHelper.reset('PetProfileScreen', params))
  } catch ({code, message, ...error}) {
    console.tron.display({name: 'createPet', preview: message, value: {code, message, action, error}, important: true})
    ToastAndroid.show('No pudimos crear nueva mascota.\n' + code, ToastAndroid.LONG)
    yield put(PetCreateRedux.petSaveFailure())
  }
}

export function * updatePet (sagaFirebase, action) {
  const {data, id, imagePickerResponse} = action
  const petPath = '/pets/' + id
  data.updatedAt = new Date().getTime()

  try {
    if (imagePickerResponse && imagePickerResponse.uri) {
      data.image = yield call(uploadPetImage, sagaFirebase, id, imagePickerResponse)
    }
    yield call(sagaFirebase.database.patch, petPath, data)
    yield put(PetCreateRedux.petSaveSuccess())

    const params = {id}
    yield put(NavHelper.reset('PetProfileScreen', params))
  } catch ({code, message}) {
    console.tron.display({name: 'Pet update', preview: message, value: {code, message}, important: true})
    yield put(PetCreateRedux.petSaveFailure())
  }
}

export function * saveMedicalInfo (sagaFirebase, action) : void {
  const {data, petId} = action
  const dbPath = '/pets/' + petId + '/medicalInfo/'

  try {
    const result = yield call(sagaFirebase.database.update, dbPath, data)
    yield put(MedicalInfoSaveRedux.medicalInfoSaveSuccess(result))

    const params = {id: petId}
    yield put(NavHelper.reset('PetProfileScreen', params))
  } catch ({code, message}) {
    console.tron.display({name: 'Medical Info', preview: 'Save error: ' + message, value: {code, message}, important: true})
    yield put(MedicalInfoSaveRedux.medicalInfoSaveFailure())
  }
}

function * uploadPetImage (sagaFirebase, petId, imagePickerResponse) : string {
  const {uri} = imagePickerResponse
  const mime = 'image/jpeg'
  const user = yield select(getCurrentUser)
  const uploadPath = StoragePaths.getPetProfilePhotoPath(petId, user.uid)

  try {
    yield call(sagaFirebase.storage.uploadFile, uploadPath, uri, {contentType: mime})
    return yield call(sagaFirebase.storage.getDownloadURL, uploadPath)
  } catch ({code, message}) {
    console.tron.display({name: 'uploadPetImage', preview: 'Save error: ' + message, value: {code, message, petId, imagePickerResponse, uploadPath}, important: true})
    yield put(MedicalInfoSaveRedux.medicalInfoSaveFailure())
  }
}

export function * deletePet (sagaFirebase, action) {
  const {data} = action
  const user = yield select(getCurrentUser)

  if (data && data.length > 0) {
    for (let petId of data) {
      const pet = yield select(getPet, petId)
      const petProfilePhotoPath = StoragePaths.getPetProfilePhotoPath(petId, user.uid)
      const recordPath = '/pets/' + petId

      // Delete whole storage folder does not work on firebase
      // Not critical - on fail we do not abort the deletion process
      try {
        yield call(sagaFirebase.storage.deleteFile, petProfilePhotoPath)
      } catch ({code, message}) {
        console.tron.display({name: 'deletePetProfilePhoto', preview: 'Delete pet photo: ' + message, value: {code, message, petId, pet, petProfilePhotoPath}, important: true})
      }

      console.tron.log({album: pet.album, length: R.keys(pet.album).length})
      if (pet.album && R.keys(pet.album).length > 0) {
        const albums = yield select(getAlbumForPet, petId)
        for (const album of albums) {
          console.tron.log({deleting: 'DELETING', album})
          try {
            // Directly delete file ignoring pet.album array (anyway we are going to delete whole PET records)
            if (album.storagePath) yield call(sagaFirebase.storage.deleteFile, album.storagePath)
          } catch ({code, message}) {
            console.tron.display({name: 'deletePetAlbumPhoto', preview: 'Delete pet album photo: ' + message, value: {code, message, petId, pet, album}, important: true})
          }
        }
      }

      try {
          // Delete the pet record
        yield call(sagaFirebase.database.delete, recordPath)
      } catch ({code, message}) {
        console.tron.display({name: 'deletePet', preview: 'Delete pet error: ' + message, value: {code, message, petId, pet}, important: true})
        yield put(PetDeleteRedux.petDeleteFailure())
      }
    }

    yield put(PetDeleteRedux.petDeleteSuccess())
  } else {
    console.tron.display({name: 'deletePet', preview: 'Delete error, list empty.', value: {}, important: true})
    yield put(PetDeleteRedux.petDeleteFailure())
  }
}
