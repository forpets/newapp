import { call, put } from 'redux-saga/effects'
import PlaceClaimRedux from '../Redux/PlaceClaimRedux'
import NavHelper from '../Lib/NavHelper'

const placeClaimPath = 'placeClaims'

export function * claimPlace (sagaFirebase, action) {
  const { data } = action

  try {
    const docId = yield call(sagaFirebase.firestore.addDocument, placeClaimPath, data)

    yield put(PlaceClaimRedux.placeClaimSuccess())
    yield put(NavHelper.reset('PlaceClaimSuccessScreen', {placeId: data.placeId}))
  } catch ({code, message}) {
    console.tron.display({name: 'claimPlace', preview: message, value: {code, message, sagaFirebase, action}, important: true})
    yield put(PlaceClaimRedux.placeClaimFailure())
  }

}
