import {spawn} from 'redux-saga/effects'
import {syncAuthentication} from './UserSagas'
import AppConfig from '../Config/AppConfig'

// process STARTUP actions
export function * startup (sagaFirebase, action) {
  // Initialize Sync firebase user authentication status
  yield spawn(syncAuthentication, sagaFirebase)

  // console.tron.error({ app: sagaFirebase.app })
  // IF YOU WANT TO FORCE CRASH
  // yield sagaFirebase.app.fabric.crashlytics().crash()

  // const user = yield select(getCurrentUser)

  // If opens app - while logged out
  // if (!user) {
  //   yield call(sagaFirebase.auth.signInAnonymously)
  // }

  if (__DEV__ && console.tron) {
    // Some developer stuff
    console.tron.log('App4Pets ' + AppConfig.packageVersion, true)
  }
}
