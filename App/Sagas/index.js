import { takeLatest, all, throttle } from 'redux-saga/effects'
import CloudFunctions from '../Services/CloudFunctions'
// import FixtureAPI from '../Services/FixtureApi'
import ReduxSagaFirebase from 'redux-saga-firebase'
import firebase from 'react-native-firebase'
import GeoFire from 'geofire'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { UserTypes } from '../Redux/UserRedux'
import { PetAlbumUploadTypes } from '../Redux/PetAlbumUploadRedux'
import { PetAlbumDeleteTypes } from '../Redux/PetAlbumDeleteRedux'
import { UserRegisterTypes } from '../Redux/UserRegisterRedux'
import { PetCreateTypes } from '../Redux/PetCreateRedux'
import { NoteSaveTypes } from '../Redux/NoteSaveRedux'
import { UserUpdateTypes } from '../Redux/UserUpdateRedux'
import { MedicalInfoSaveTypes } from '../Redux/MedicalInfoSaveRedux'
import { VaccineSaveTypes } from '../Redux/VaccineSaveRedux'
import { ForgotPasswordTypes } from '../Redux/ForgotPasswordRedux'
import { VaccineDeleteTypes } from '../Redux/VaccineDeleteRedux'
import { NoteDeleteTypes } from '../Redux/NoteDeleteRedux'
import { PetDeleteTypes } from '../Redux/PetDeleteRedux'
import { PlaceAddTypes } from '../Redux/PlaceAddRedux'
import { PlaceReviewAddTypes } from '../Redux/PlaceReviewAddRedux'
import { PlaceReviewTypes } from '../Redux/PlaceReviewRedux'
import { AllUsersTypes } from '../Redux/AllUsersRedux'
import { FilteredPlacesTypes } from '../Redux/FilteredPlacesRedux'
import { PlaceClaimTypes } from '../Redux/PlaceClaimRedux'
import { PlacePhotoUploadTypes } from '../Redux/PlacePhotoUploadRedux'
import { FilteredLostFoundTypes } from '../Redux/FilteredLostFoundRedux'
import { LostFoundTypes } from '../Redux/LostFoundRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { login,
  loginFacebook, logout, loginGuest, registerUser, updateUser, syncUserProfileSaga,
  forgotPassword,
getAllUsers } from './UserSagas'
import { uploadPetAlbumPhoto, deletePetAlbumPhoto, createPet, updatePet, saveMedicalInfo, deletePet } from './PetSagas'
import { createNote, deleteNote, updateNote } from './NoteSagas'
import { saveVaccine, updateVaccine, deleteVaccine } from './VaccineSagas'
import { addPlace, updatePlace, uploadPlacePhoto } from './PlaceSagas'
import { addPlaceReview, getPlaceReview } from './PlaceReviewSagas'
import { filterPlacesCf } from './FilteredPlacesSagas'
import { PlaceSearchTypes } from '../Redux/PlaceSearchRedux'
import { startPlaceSearchCf } from './PlaceSearchSagas'
import { claimPlace } from './PlaceClaimSagas'
import { filterLostFoundCf } from './FilteredLostFoundSagas'
import { updateReportPhoto, createReport, updateReport } from './LostFoundSagas'

/* ------------- API ------------- */

// const api = DebugConfig.useFixtures ? FixtureAPI : API.create()
const cf = CloudFunctions.create()

const Firebase = firebase
const sagaFirebase = new ReduxSagaFirebase(firebase, firebase.firestore())
// __DEV__ && firebase.firestore.setLogLevel('debug')

const refPlace = firebase.database().ref('placesGeo/')
const geofirePlace = new GeoFire(refPlace)

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // Auth, User & Pet objects are being syncronized automagically using Firebase sync feature.
    takeLatest(StartupTypes.STARTUP, startup, sagaFirebase),

    throttle(1000, [
      StartupTypes.STARTUP,
      LoginTypes.LOGIN_SUCCESS,
      UserTypes.USER_SYNC_REQUEST,
      UserRegisterTypes.USER_REGISTER_SUCCESS
    ], syncUserProfileSaga, sagaFirebase),

    // Login requests
    throttle(4000, LoginTypes.LOGIN_REQUEST, login, sagaFirebase),
    takeLatest(LoginTypes.LOGIN_FACEBOOK_REQUEST, loginFacebook, sagaFirebase),

    throttle(4000, ForgotPasswordTypes.FORGOT_PASSWORD_REQUEST, forgotPassword, sagaFirebase),

    throttle(4000, UserTypes.USER_LOGIN_GUEST, loginGuest, sagaFirebase),
    throttle(4000, UserTypes.USER_LOGOUT, logout, sagaFirebase),

    throttle(4000, UserRegisterTypes.USER_REGISTER_REQUEST, registerUser, sagaFirebase),
    throttle(4000, UserUpdateTypes.USER_UPDATE_REQUEST, updateUser, sagaFirebase),

    takeLatest(AllUsersTypes.ALL_USERS_REQUEST, getAllUsers, sagaFirebase),

    // Pet CRUD
    throttle(3000, PetCreateTypes.PET_CREATE_REQUEST, createPet, sagaFirebase),
    throttle(3000, PetCreateTypes.PET_UPDATE_REQUEST, updatePet, sagaFirebase),
    takeLatest(PetDeleteTypes.PET_DELETE_REQUEST, deletePet, sagaFirebase),
    throttle(3000, MedicalInfoSaveTypes.MEDICAL_INFO_SAVE_REQUEST, saveMedicalInfo, sagaFirebase),

    // Vaccine CRUD
    throttle(2000, VaccineSaveTypes.VACCINE_SAVE_REQUEST, saveVaccine, sagaFirebase),
    takeLatest(VaccineSaveTypes.VACCINE_UPDATE_REQUEST, updateVaccine, sagaFirebase),
    takeLatest(VaccineDeleteTypes.VACCINE_DELETE_REQUEST, deleteVaccine, sagaFirebase),

    // Notes CRUD
    throttle(2000, NoteSaveTypes.NOTE_CREATE_REQUEST, createNote, sagaFirebase),
    throttle(2000, NoteSaveTypes.NOTE_UPDATE_REQUEST, updateNote, sagaFirebase),
    throttle(2000, NoteDeleteTypes.NOTE_DELETE_REQUEST, deleteNote, sagaFirebase),

    // Place CRU
    throttle(4000, PlaceAddTypes.PLACE_ADD_REQUEST, addPlace, sagaFirebase, geofirePlace),
    throttle(4000, PlaceAddTypes.PLACE_UPDATE_REQUEST, updatePlace, sagaFirebase, geofirePlace),
    throttle(2000, PlacePhotoUploadTypes.PLACE_PHOTO_UPLOAD_REQUEST, uploadPlacePhoto, sagaFirebase),

    // PlaceReviews
    throttle(4000, PlaceReviewAddTypes.PLACE_REVIEW_ADD_REQUEST, addPlaceReview, sagaFirebase),
    takeLatest(PlaceReviewTypes.PLACE_REVIEW_REQUEST, getPlaceReview, sagaFirebase),

    // Place Claims
    throttle(2000, PlaceClaimTypes.PLACE_CLAIM_REQUEST, claimPlace, sagaFirebase),

    // PetAlbums CRUD
    throttle(2000, PetAlbumUploadTypes.PET_ALBUM_UPLOAD_REQUEST, uploadPetAlbumPhoto, sagaFirebase),
    takeLatest(PetAlbumDeleteTypes.PET_ALBUM_DELETE_REQUEST, deletePetAlbumPhoto, sagaFirebase),

    // Places filtering
    throttle(2000, [
      PlaceSearchTypes.PLACE_SEARCH_REQUEST,
      PlaceSearchTypes.PLACE_SEARCH_RERUN
    ], startPlaceSearchCf, cf),

    throttle(2000, [
      FilteredPlacesTypes.FILTERED_PLACES_REQUEST,
      FilteredPlacesTypes.FILTERED_PLACES_APPLY
    ], filterPlacesCf, cf),

    // LostFound
    throttle(2000, LostFoundTypes.REPORT_CREATE_REQUEST, createReport, sagaFirebase),
    throttle(2000, LostFoundTypes.REPORT_UPDATE_REQUEST, updateReport, sagaFirebase),
    // // throttle(2000, LostFoundTypes.REPORT_DELETE_REQUEST, deleteReport, sagaFirebase),
    throttle(2000, LostFoundTypes.REPORT_PHOTO_UPLOAD_REQUEST, updateReportPhoto, sagaFirebase),
    throttle(2000, [
      FilteredLostFoundTypes.FILTERED_LOST_FOUND_REQUEST,
      FilteredLostFoundTypes.FILTERED_LOST_FOUND_APPLY
    ], filterLostFoundCf, cf)

  ])
}
