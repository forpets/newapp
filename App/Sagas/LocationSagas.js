// @flow
import { call, put, takeEvery } from 'redux-saga/effects'
import LocationRedux from '../Redux/LocationRedux'
import { locationChangeChannel, getCurrentPosition, requestLocationAccess } from '../Services/Location'
import R from 'ramda'
import AppConfig from '../Config/AppConfig'

// Initialize location watch
export function * watchLocation () : void {
  try {
    // Request for permission
    const granted = yield call(requestLocationAccess)

    if (granted) {
      yield put(LocationRedux.locationPermissionGranted())
      // Force get initial position
      const currentLocation = yield call(getCurrentPosition)
      yield call(locationChange, currentLocation)

      // Start watching for position
      const channel = yield call(locationChangeChannel)
      yield takeEvery(channel, locationChange)
    } else {
      yield put(LocationRedux.locationPermissionDenied())
      // Set default coordinates
      yield call(locationChange, {coords: AppConfig.defaultLocation})
    }
  } catch ({code, message}) {
    console.tron.display({name: 'watchLocation', preview: message, value: {code, message}, important: true})
    // FIXME: should throw some burger notification about failure
    yield put(LocationRedux.locationFailure())
    yield call(locationChange, {coords: AppConfig.defaultLocation})
  }
}

function * locationChange (location) : void {
  const coords = R.pickAll(['latitude', 'longitude'], location.coords)
  yield put(LocationRedux.locationSuccess(coords))
  // TODO: should trigger resync?
}
