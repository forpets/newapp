// @flow
import type {Place} from '../FlowTypes/Place'
import {ToastAndroid} from 'react-native'
import { call, put, take } from 'redux-saga/effects'
import PlaceRedux, { PlacePhoto } from '../Redux/PlaceRedux'
import PlaceAddRedux from '../Redux/PlaceAddRedux'
import PlacesFindAllRedux from '../Redux/PlacesFindAllRedux'
import {NavigationActions} from 'react-navigation'
import R from 'ramda'
import Guid from '../Lib/Guid'
import FilteredPlacesRedux from '../Redux/FilteredPlacesRedux'
import PlacePhotoUploadRedux from '../Redux/PlacePhotoUploadRedux'
import NavHelper from '../Lib/NavHelper'
import AppConfig from '../Config/AppConfig'

// Firestore path
const placePath = 'places'

export function * addPlace (sagaFirebase, geofirePlace, action) : void {
  // console.tron.log({sagaFirebase, geofirePlace, action})
  const { data, location } = action

  try {
    // Clone photos array
    const photos = R.clone(data.photos)
    // .. and do not upload photos object as it is
    data.public = true
    data.photos = []

    // Create new place
    const placeDoc = yield call(sagaFirebase.firestore.addDocument, placePath, {...data, location})
    const placeUid = placeDoc.id

    // Upload / delete photos
    yield processPlacePhotos(sagaFirebase, placeUid, photos)

    yield put(PlaceAddRedux.placeAddSuccess(placeUid))
    yield put(NavHelper.reset('PlaceCreateSuccessScreen'))
  } catch ({code, message}) {
    console.tron.display({name: 'Place add', preview: message, value: {code, message, sagaFirebase, geofirePlace, action}, important: true})
    yield put(PlaceAddRedux.placeAddFailure(message))
    ToastAndroid.show('No pudimos crear nuevo espacio\n' + code, ToastAndroid.LONG)
  }
}

// // TODO: Not used? - remove
// export function * findPlace (sagaFirebase, action) : void {
//   const {data} = action
//   const specificPlacePath = placePath + '/' + data
//   try {
//     const place = yield call(sagaFirebase.database.read, specificPlacePath)
//     yield put(PlaceRedux.placeSuccess(place))
//   } catch ({code, message}) {
//     console.tron.display({name: 'Place find', preview: message, value: {code, message, sagaFirebase, action}, important: true})
//     yield put(PlaceRedux.placeFailure(message))
//   }
// }

// export function * syncAllPlaces (sagaFirebase, action) : void {
//   // FIXME: try catch
//   // FIXME: filter places should access directly??? this method??
//   const query = sagaFirebase.app.firestore().collection(placePath).where('published', '==', true)
//   const channel = sagaFirebase.firestore.channel(query)

//   while (true) {
//     const snapshot = yield take(channel)
//     yield put(PlacesFindAllRedux.placesFindAllSyncing())
//     const places = collectionSnapshotToArray(snapshot)
//     yield put(PlacesFindAllRedux.placesFindAllSuccess(places))
//     // Invoke filters
//     yield put(FilteredPlacesRedux.filteredPlacesRequest())
//   }
// }

function * doUploadPlacePhoto (sagaFirebase, placeUid, imagePickerResponse) : PlacePhoto {
  const {uri} = imagePickerResponse
  const mime = 'image/jpeg'
  const uploadPath = '/places/' + placeUid + '/' + Guid()

  yield call(sagaFirebase.storage.uploadFile, uploadPath, uri, {contentType: mime})
  const url = yield call(sagaFirebase.storage.getDownloadURL, uploadPath)
  return {url, uploadPath}
}

// Uploads / deletes / keeps photos of Place and updates place object
function * processPlacePhotos (sagaFirebase, placeUid, photos:Array<PlacePhoto>) {
  let processedPhotos = []

  try {
    if (photos && photos.length > 0) {
      for (var i = 0; i < photos.length; i++) {
        const photo = photos[i]

      // Upload new photos
        if (photo.imagePickerResponse) {
          const placePhoto = yield call(doUploadPlacePhoto, sagaFirebase, placeUid, photo.imagePickerResponse)
          processedPhotos.push(placePhoto)
        } else if (photo.markForDelete) {
        // If has upload path
          if (photo.uploadPath) {
            yield call(sagaFirebase.storage.deleteFile, photo.uploadPath)
          } else {
            // Some old photo without upload path - just ignore/remove - do not add to new object
          }
        } else {
        // If not upload/ nor delete - just keep
          processedPhotos.push(photo)
        }
      }

      // Set uploaded photos in place object
      const placePhotosPath = placePath + '/' + placeUid
      yield call(sagaFirebase.firestore.updateDocument, placePhotosPath, 'photos', processedPhotos)
    }
  } catch ({code, message}) {
    console.tron.display({name: 'processPlacePhotos', preview: message, value: {code, message, sagaFirebase, photos}, important: true})
  }
}

export function * updatePlace (sagaFirebase, geofirePlace, action) : void {
  const { data, location } = action
  const placeUid = data.id
  const updatePlacePath = placePath + '/' + placeUid

  try {
    // Clone photos array
    const photos = R.clone(data.photos)
    // .. and do not upload photos object as it is
    data.photos = []

    yield call(sagaFirebase.firestore.updateDocument, updatePlacePath, {...data, location})
    // Upload / delete photos
    yield processPlacePhotos(sagaFirebase, placeUid, photos)
    yield put(PlaceAddRedux.placeAddSuccess(placeUid))

    yield put(NavHelper.reset('DirectoriesScreen'))
    ToastAndroid.show('El espacio fue actualizado exitosamente\nPronto revisaremos nuevos cambios', ToastAndroid.LONG)
  } catch ({code, message}) {
    console.tron.display({name: 'Place update', preview: message, value: {code, message, sagaFirebase, geofirePlace, action}, important: true})
    yield put(PlaceAddRedux.placeAddFailure(message))
    ToastAndroid.show('No pudimos actualizar el espacio.\n' + code, ToastAndroid.LONG)
  }
}

export function * uploadPlacePhoto (sagaFirebase, action) : void {
  const {data} = action
  const {placeId, imagePickerResponse} = data

  try {
    const snapshot = yield call(sagaFirebase.firestore.getDocument, 'places/' + placeId)
    if (snapshot && snapshot.data()) {
      const {photos = []} = snapshot.data()

      if (R.length(photos) >= AppConfig.maxPhotosForPlace) {
        yield put(PlacePhotoUploadRedux.placePhotoUploadFailure())
        ToastAndroid.show('No se puede agregar mas de ' + AppConfig.maxPhotosForPlace + ' fotos', ToastAndroid.LONG)
      } else {
        photos.push({imagePickerResponse})
        yield call(processPlacePhotos, sagaFirebase, placeId, photos)
        yield put(PlacePhotoUploadRedux.placePhotoUploadSuccess())
        ToastAndroid.show('Hemos recibido su foto.\nLa vamos a revisar y publicar pronto.', ToastAndroid.LONG)
      }
    } else {
      yield put(PlacePhotoUploadRedux.placePhotoUploadFailure())
      ToastAndroid.show('Error subiendo la foto.\No pudimos encontrar el espacio.', ToastAndroid.LONG)
    }
  } catch ({code, message}) {
    console.tron.display({name: 'uploadPlacePhoto', preview: message, value: {code, message, sagaFirebase, action}, important: true})
    ToastAndroid.show('Error subiendo la foto.\nIntenta de nuevo mas tarde.\n' + code, ToastAndroid.LONG)
    yield put(PlacePhotoUploadRedux.placePhotoUploadFailure())
  }
}
