import { call, put } from 'redux-saga/effects'
import VaccineSaveRedux from '../Redux/VaccineSaveRedux'
import {NavigationActions} from 'react-navigation'
import VaccineDeleteRedux from '../Redux/VaccineDeleteRedux'
import firebase from 'react-native-firebase'
import Guid from '../Lib/Guid'

// CREATE VACCINE
export function * saveVaccine (sagaFirebase, action) {
  const {data, petId} = action
  const id = Guid()
  const vaccinesPath = `/pets/${petId}/vaccines/${id}`
    // const result = yield call(firebase.database().ref(vaccinesPath).push, data)

  try {
    const id = yield call(sagaFirebase.database.update, vaccinesPath, data)
    const params = {id: petId}
    // TODO: if first vaccine show onboards
    // yield put(NavigationActions.navigate({routeName: 'PetVaccinesScreen', params}))
    // yield put(VaccineSaveRedux.vaccineSaveSuccess(id))
    yield put(NavigationActions.back())
    yield put(VaccineSaveRedux.vaccineSaveSuccess(id))
  } catch ({code, message}) {
    console.tron.display({name: 'Vaccine', preview: 'Save error: ' + message, value: {code, message, sagaFirebase}, important: true})
    yield put(VaccineSaveRedux.vaccineSaveFailure())
  }
}

// UPDATE VACCINE
export function * updateVaccine (sagaFirebase, action) {
  const {data, petId, petVaccineId} = action
  const vaccinesPath = `/pets/${petId}/vaccines/${petVaccineId}/`

  try {
    // Overwrite vaccine input
    const id = yield call(sagaFirebase.database.update, vaccinesPath, data)
    // const params = {id: petId}
    // yield put(NavigationActions.navigate({routeName: 'PetVaccinesScreen', params}))
    yield put(NavigationActions.back())
    yield put(VaccineSaveRedux.vaccineSaveSuccess(id))
  } catch ({code, message}) {
    console.tron.display({name: 'Vaccine', preview: 'Save error: ' + message, value: {code, message}, important: true})
    yield put(VaccineSaveRedux.vaccineSaveFailure())
  }
}

export function * deleteVaccine (sagaFirebase, action) {
  const {data} = action
  const vaccinePath = `/pets/${data.petId}/vaccines/${data.id}/`
  console.tron.error({vaccinePath})

  try {
    yield call(sagaFirebase.database.delete, vaccinePath)
    yield put(VaccineDeleteRedux.vaccineDeleteSuccess())
    yield put(NavigationActions.back())
  } catch ({code, message}) {
    console.tron.display({name: 'Vaccine', preview: 'Delete error: ' + message, value: {code, message}, important: true})
    yield put(VaccineDeleteRedux.vaccineDeleteFailure())
  }
}
