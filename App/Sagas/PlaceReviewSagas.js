// @flow
import { call, put, take } from 'redux-saga/effects'
import PlaceReviewAddRedux from '../Redux/PlaceReviewAddRedux'
import PlaceReviewRedux from '../Redux/PlaceReviewRedux'
import type {PlaceReview} from '../FlowTypes/PlaceReview'
import AllUsersRedux, {AllUsersTypes} from '../Redux/AllUsersRedux'
import { NavigationActions } from 'react-navigation'
import R from 'ramda'

const placeReviewPath = 'placeReviews'

export function * addPlaceReview (sagaFirebase, action) : void {
  const {data} = action

  try {
    yield call(sagaFirebase.firestore.addDocument, placeReviewPath, data)

    const params = {id: data.placeId}
    const resetAction = NavigationActions.reset({
      index: 2,
      actions: [
        NavigationActions.navigate({routeName: 'HomeScreen'}),
        NavigationActions.navigate({routeName: 'PlaceViewScreen', params }),
        NavigationActions.navigate({routeName: 'ReviewSentScreen'})
      ]
    })

    yield put(PlaceReviewAddRedux.placeReviewAddSuccess())
    yield put(resetAction)
  } catch ({code, message}) {
    console.tron.display({name: 'addPlaceReview', preview: message, value: {code, message, sagaFirebase, action}, important: true})
    yield put(PlaceReviewAddRedux.placeReviewAddFailure(message))
  }
}

export function * getPlaceReview (sagaFirebase, action) : void {
  const {data} = action

  try {
    const placeReviews = yield call(findReviewsForPlace, sagaFirebase, data)

    if (placeReviews && !R.isEmpty(placeReviews)) {
      const uids = R.map((item) => item.userId, placeReviews)
      yield put(AllUsersRedux.allUsersRequest(uids))
      yield take(AllUsersTypes.ALL_USERS_SUCCESS)
    }

    yield put(PlaceReviewRedux.placeReviewSuccess(placeReviews))
  } catch ({code, message}) {
    console.tron.display({name: 'getPlaceReview', preview: message, value: {code, message, sagaFirebase, action}, important: true})
    yield put(PlaceReviewRedux.placeReviewFailure(message))
  }
}

function * findReviewsForPlace (sagaFirebase, data) : Array<PlaceReview> {
  const query = sagaFirebase.firestoreDb.collection(placeReviewPath)
    .where('placeId', '==', data)
    .where('reviewed', '==', true)
  const snapshot = yield call(sagaFirebase.firestore.getCollection, query, data)
  const placeReviews:Array<PlaceReview> = []

  snapshot.forEach((doc) => {
    const place = doc.data()
    place['id'] = doc.id
    placeReviews.push(place)
  })

  return R.sort(R.descend(R.prop('createdAt')), placeReviews)
}
