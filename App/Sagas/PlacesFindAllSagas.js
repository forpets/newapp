// @flow
import type {Place} from '../FlowTypes/Place'
import { call, put } from 'redux-saga/effects'
import {ToastAndroid, InteractionManager} from 'react-native'
import PlacesFindAllActions from '../Redux/PlacesFindAllRedux'

// Firestore path
const placePath = 'places'

// DEPRECATED
function collectionSnapshotToArray (snapshot) : Array<Place> {
  const places = []
  let idx = 0
  // console.tron.log(snapshot, true)
  snapshot.forEach((doc) => {
    const place = doc.data()
    place['id'] = doc.id
    places.push(place)
    idx++
  })
  console.tron.log('forEach finish', true)

  return places
}

// DEPRECATED
export function * findAllPlaces (sagaFirebase, action) : Array<Place> {
  const {data = 50} = action // Useless now...
  try {
    const query = sagaFirebase.app.firestore().collection(placePath).where('published', '==', true).limit(data)
    console.tron.log('INIT LOAD')
    const snapshot = yield call(sagaFirebase.firestore.getCollection, query)
    // const snapshot  = R.repeat((i) => new Object({}, {'name': i}, 3000)
    console.tron.log('FINISH LOAD')

    const promise = () => new Promise((resolve, reject) => {
      InteractionManager.runAfterInteractions(() => {
        const places = collectionSnapshotToArray(snapshot)
        resolve(places)
      })
    })

    const places = yield call(promise)
    console.tron.log({places})
    yield put(PlacesFindAllActions.placesFindAllSuccess(places))

    // console.tron.log({promise, firebase: sagaFirebase.firestore.getCollection})
  } catch ({code, message}) {
    console.tron.display({name: 'findAllPlaces', preview: message, value: {code, message, sagaFirebase, action}, important: true})
    yield put(PlacesFindAllActions.placesFindAllFailure(message))

    ToastAndroid.show('No pudimos actualizar los espacios\n' + code, ToastAndroid.LONG)
    return []
  }
}
