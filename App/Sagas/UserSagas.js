
import {delay} from 'redux-saga'
import { call, put, take, select, spawn } from 'redux-saga/effects'
import LoginRedux from '../Redux/LoginRedux'
import PetsRedux from '../Redux/PetsRedux'
import UserRedux, {getCurrentUser, UserTypes} from '../Redux/UserRedux'
import UserRegisterRedux from '../Redux/UserRegisterRedux'
import { NavigationActions } from 'react-navigation'
import FBSDK, { LoginManager, AccessToken } from 'react-native-fbsdk'
import UserUpdateRedux from '../Redux/UserUpdateRedux'
import firebase from 'react-native-firebase'
import ForgotPasswordRedux from '../Redux/ForgotPasswordRedux'
import AllUsersRedux, {getAllUsersObject} from '../Redux/AllUsersRedux'
import R from 'ramda'
import {syncUserPets} from './PetSagas'
import FilteredPlacesRedux, {getFilteredPlaces} from '../Redux/FilteredPlacesRedux'
import FilteredLostFoundRedux, {getFilteredLostFound} from '../Redux/FilteredLostFoundRedux'
import { watchLocation } from './LocationSagas'
import {LocationTypes} from '../Redux/LocationRedux'
import StoragePaths from '../Services/StoragePaths'
import NavHelper from '../Lib/NavHelper'

export function * login (sagaFirebase, action) {
  try {
    // FIXME: add connection  error too
    const { data } = action
    const result = yield call(sagaFirebase.auth.signInWithEmailAndPassword, data.email, data.password)
    yield put(LoginRedux.loginSuccess())
    yield put(UserRedux.userLoggedIn(result))
    // User is being synced
  } catch ({code, message}) {
    console.tron.display({name: 'Login Error', preview: message, value: {code, message}, important: true})
    yield put(LoginRedux.loginFailure())
  }
}

export function * loginFacebook (sagaFirebase, action) {
  const result = yield call(LoginManager.logInWithReadPermissions, ['public_profile', 'email'])
  __DEV__ && console.tron.log({result}, true)
  if (result.isCanceleted) {
    // User closes popup
  } else {
    try {
      const accessToken = yield call(AccessToken.getCurrentAccessToken)
      __DEV__ && console.tron.log({accessToken}, true)
      const token = accessToken.accessToken.toString()

      const credential = yield call(firebase.auth.FacebookAuthProvider.credential, token)
      __DEV__ && console.tron.log({credential}, true)

      const user = yield call(sagaFirebase.auth.signInWithCredential, credential)
      __DEV__ && console.tron.log({user}, true)

      // Create new user if does not exists
      yield * registerNewFacebookUserIfNotExists(sagaFirebase, user)

      yield put(LoginRedux.loginSuccess())
      yield put(UserRedux.userLoggedIn(user))
    // Login success...
    } catch ({code, message}) {
      console.tron.display({name: 'FB User Login',
        preview: 'Failed to find existing FB user: ' + message,
        value: {code, message},
        important: true})
      yield put(LoginRedux.loginFailure())
    }
  }
}

function * onLogin (sagaFirebase, user) {
  yield spawn(syncUserPets, sagaFirebase) // Start syncing pets
  yield sagaFirebase.app.fabric.crashlytics().setUserIdentifier(user.uid) // Set UID for crashlytics
  yield put(NavigationActions.navigate({routeName: 'HomeNav'}))

  yield spawn(watchLocation)
  yield take(LocationTypes.LOCATION_SUCCESS)

  const filteredPlaces = yield select(getFilteredPlaces)
  // If no filtered places - find get some
  // Or if last location - was too far
  if (R.isEmpty(filteredPlaces)) {
    yield put(FilteredPlacesRedux.filteredPlacesRequest())
  }

  const filteredLostFound = yield select(getFilteredLostFound)
  // If no filtered LostFound Reports - find get some
  if (R.isEmpty(filteredLostFound)) {
    yield put(FilteredLostFoundRedux.filteredLostFoundRequest())
  }
}

function * registerNewFacebookUserIfNotExists (sagaFirebase, userCredential : Object) {
  let existingUser = null

  try {
    // Check if we have existing user
    existingUser = yield call(sagaFirebase.database.read, '/users/' + userCredential.uid)
  } catch ({code, message}) {
    console.tron.display({name: 'FB User Login',
      preview: 'Failed to find existing FB user: ' + message,
      value: {userCredential},
      important: true})
  }

  if (!existingUser) {
    __DEV__ && console.tron.warn({message: 'Facebook user does not exists in the system - continuing', userCredential})

    // IF USER DOES NOT EXISTS IN THE SYSTEM
    const {displayName, photoURL, uid, email = ''} = userCredential
    const newUserPath = '/users/' + uid
    const newUser = {
      firstname: '',
      lastname: '',
      photoURL: photoURL || '',
      uid,
      facebook: true,
      email
    }
    if (displayName) {
      newUser.firstname = displayName.split(' ')[0]
      newUser.lastname = displayName.split(' ')[1]
    }

        // Wait for user to be set - means we are logged in
    let currentUser = yield select(getCurrentUser)
    while (!currentUser) {
      currentUser = yield select(getCurrentUser)
      yield delay(400)
    }

    try {
      yield call(sagaFirebase.database.update, newUserPath, newUser)
    } catch ({code, message}) {
      console.tron.display({name: 'FB User Register',
        preview: 'Failed to registrate new FB user: ' + message,
        value: {code, message, newUser, newUserPath},
        important: true})
    }
  }
}

export function * loginGuest (sagaFirebase, action) {
  yield sagaFirebase.app.fabric.crashlytics().setUserIdentifier('Guest') // Set UID for crashlytics
  yield put(NavigationActions.navigate({routeName: 'HomeScreen'}))
  yield spawn(watchLocation)
  yield call(sagaFirebase.auth.signInAnonymously)
  yield put(FilteredPlacesRedux.filteredPlacesRequest())
  yield put(NavHelper.resetHome())
}

export function * logout (sagaFirebase, action) {
  // See syncAuthentication for sideeffects after logout
  try {
    yield call(sagaFirebase.auth.signOut)
  } catch ({code, message}) {
    console.tron.display({name: 'logout',
      preview: 'Error on signOut: ' + message,
      value: {code, message, sagaFirebase, action},
      important: true})
  }
}

export function * registerUser (sagaFirebase, action) {
  const {imagePickerResponse} = action
  const {firstname, lastname, email, password} = action.data
  const newUserData = {
    firstname,
    lastname,
    email
  }
  // console.tron.error({action})

  let user = null
  try {
    // Register user > receive authentication object
    user = yield call(sagaFirebase.auth.createUserWithEmailAndPassword, email, password)
    const newUserPath = '/users/' + user.uid

    // Wait for user to be set - means we are logged in
    let currentUser = yield select(getCurrentUser)
    while (!currentUser) {
      currentUser = yield select(getCurrentUser)
      yield delay(400)
    }

    // Upload image
    if (imagePickerResponse && imagePickerResponse.uri) {
      newUserData.photoURL = yield call(uploadUserImage, sagaFirebase, user.uid, imagePickerResponse)
    }

    console.tron.log({newUserData, newUserPath})

    // Update instead of create - ReduxSagaFirebase does not work well with RNFirebase
    yield call(sagaFirebase.database.update, newUserPath, {...newUserData, uid: user.uid})
    yield put(UserRegisterRedux.userRegisterSuccess(newUserData))
    // User will be auto-logged in by firebase now
  } catch ({code, message, ...details}) {
    console.tron.display({name: 'registerUser', preview: message, value: {code, message, details, newUserData}, important: true})
    yield put(UserRegisterRedux.userRegisterFailure(code))
  }

  // FIXME: email verification??
  // try {
  //   // yield call(user.sendEmailVerification)
  // } catch ({code, message}) {
  //   console.tron.display({name: 'Registration error', preview: message, value: {code, message}, important: true})
  // }
}

function * onLogout (sagaFirebase) {
  yield sagaFirebase.app.fabric.crashlytics().setUserIdentifier('') // Set UID for crashlytics
  yield put(NavigationActions.navigate({routeName: 'LoginNav'}))
  yield put(NavHelper.resetLoggedOut('LoginScreen'))
  yield put(PetsRedux.petsNoUser())
  yield call(logout, sagaFirebase) // FORCE LOGOUT
}

export function * updateUser (sagaFirebase, action) {
  const {imagePickerResponse, uid} = action
  const user = action.data
  const userPath = '/users/' + uid
  const updatedUser = {uid: uid, ...user}

  try {
  // Upload image
    if (imagePickerResponse && imagePickerResponse.uri) {
      const newPhoto = yield call(uploadUserImage, sagaFirebase, uid, imagePickerResponse)
      if (newPhoto) {
        updatedUser.photoURL = newPhoto
      }
    }
    yield call(sagaFirebase.database.patch, userPath, updatedUser)
    yield put(UserUpdateRedux.userUpdateSuccess({}))
    // yield put(NavigationActions.navigate({routeName: 'UserProfileScreen'}))
    yield put(NavHelper.reset('UserProfileScreen'))
  } catch ({code, message, ...details}) {
    console.tron.display({name: 'updateUser', preview: message, value: {code, message, details}, important: true})
    yield put(UserUpdateRedux.userUpdateFailure())
  }
}

function * uploadUserImage (sagaFirebase, uid, imagePickerResponse) : string {
  const {uri} = imagePickerResponse
  const mime = 'image/jpeg'
  // const uploadPath = '/images/user/' + uid + '.jpg'
  const uploadPath = StoragePaths.getUserProfilePicPath(uid)
  try {
    console.tron.log('Uploading user image: ' + uploadPath)
    yield call(sagaFirebase.storage.uploadFile, uploadPath, uri, {contentType: mime})
    return yield call(sagaFirebase.storage.getDownloadURL, uploadPath)
  } catch ({code, message, ...details}) {
    console.tron.display({name: 'updateUser', preview: message, value: {code, message, details}, important: true})
  }
}

// AUTO SYNC LOGIN / LOGOUT
export function * syncAuthentication (sagaFirebase) {
  const channel = yield call(sagaFirebase.auth.channel)
  let prevUser = null

  while (true) {
    let { error, user } = yield take(channel)
    user = (user && user.isAnonymous) ? null : user

    try {
      /// USER auth status UPDATED FROM Firebase side
      if (user) {
      // console.tron.warn('syncAuthentication LOGIN')
        __DEV__ && console.tron.display({name: 'syncAuthentication', preview: 'State: Logged in', value: {user}})
        yield put(UserRedux.userSet(user))
        yield put(UserRedux.userSyncRequest())
        if (prevUser == null) {
          yield call(onLogin, sagaFirebase, user)
        }
      } else {
        console.tron.display({name: 'syncAuthentication', preview: 'State:  Logged out'})
        if (prevUser) {
          yield call(onLogout, sagaFirebase)
        }
      }
    } catch ({code, message}) {
      console.tron.display({name: 'syncAuthentication', preview: 'Error: ' + message, value: {code, message, firebaseError: error}, important: true})
    }

    prevUser = user
  }
}

// AUTO SYNC USER PROFILE
export function * syncUserProfileSaga (sagaFirebase, action) {
  let user = yield select(getCurrentUser)

  try {
    if (user) {
      const channel = yield call(sagaFirebase.database.channel, '/users/' + user.uid)
      console.tron.display({name: 'syncUserProfile', preview: 'Channel initialized', value: {user, channel}})

      // FIXME: disable that?
    // First force sync user - in case of initial sync after registration
      const initialSyncUser = yield call(sagaFirebase.database.read, '/users/' + user.uid)
      if (initialSyncUser) { yield put(UserRedux.userSet(initialSyncUser)) }
      console.tron.display({name: 'syncUserProfile', preview: 'Initial sync', value: {initialSyncUser}})

    // Initialize channel
      while (user) {
        const userProfile = yield take(channel)
        console.tron.display({name: 'syncUserProfile', preview: 'Get synced user', value: {userProfile}})
        if (userProfile && userProfile.value) {
          yield put(UserRedux.userSet(userProfile.value))
        }
        user = yield select(getCurrentUser)
        if (!user) {
          console.tron.display({name: 'syncUserProfile', preview: 'No User. Stop Sync', value: {user}})
        }
      }
    }
  } catch ({code, message}) {
    console.tron.display({name: 'syncUserProfile', preview: 'ERROR: ' + message, value: {code, message}, important: true})
  }
}

export function * forgotPassword (sagaFirebase, action) {
  try {
    const { data } = action
    const result = yield call(sagaFirebase.auth.sendPasswordResetEmail, data)
    yield put(ForgotPasswordRedux.forgotPasswordSuccess(result))
    yield put(NavigationActions.navigate({ routeName: 'ForgotPwSuccessScreen' }))
  } catch ({code, message}) {
    console.tron.display({name: 'ForgotPW Error', preview: message, value: {code, message}, important: true})
    yield put(ForgotPasswordRedux.forgotPasswordFailure(code))
  }
}

export function * getAllUsers (sagaFirebase, action) {
  const {data} = action
  const allUsers = yield select(getAllUsersObject)

  if (data && !R.isEmpty(data)) {
    let users = allUsers ? allUsers.asMutable() : {}

    for (var i = 0; i < data.length; i++) {
      const uid = data[i]
      // Skip if we already have it
      if (R.find(R.propEq('uid', uid), users)) continue

      try {
        const user = yield call(sagaFirebase.database.read, `users/${uid}`)
        users = R.merge(users, {[user.uid]: user})
      } catch ({code, message, ...meta}) {
        console.tron.display({name: 'getAllUsers', preview: message, value: {code, message, meta}, important: true})
        yield put(AllUsersRedux.allUsersFailure(message))
      }
    }

    yield put(AllUsersRedux.allUsersSuccess(users))
  } else {
    // Put same users so PlaceReviews saga knows we're done
    yield put(AllUsersRedux.allUsersSuccess(allUsers))
  }
}
