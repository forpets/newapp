import { call, put, select, take } from 'redux-saga/effects'
import FilteredPlacesRedux, {getPlaceFilter} from '../Redux/FilteredPlacesRedux'
import PlacesFindAllRedux, { PlacesFindAllTypes } from '../Redux/PlacesFindAllRedux'
import {ToastAndroid} from 'react-native'
import R from 'ramda'
import { getCurrentLocation, LocationTypes } from '../Redux/LocationRedux'
import GeoFunctions, {isPlaceWithinRadius} from '../Transforms/GeoFunctions'
import PlaceSearchRedux, {getSearchActive} from '../Redux/PlaceSearchRedux'
import AppConfig from '../Config/AppConfig'

// SHOULD SEARCH TOO.....????
export function * filterPlacesCf (cf, action) {
  let filter = yield select(getPlaceFilter)
  let currentLocation = yield select(getCurrentLocation)

  if (!currentLocation) {
    yield take(LocationTypes.LOCATION_SUCCESS)
    currentLocation = yield select(getCurrentLocation)
  }
  if (!filter) {
    yield put(FilteredPlacesRedux.filteredPlacesApplyDefault())
  }
  filter = yield select(getPlaceFilter)

  yield put(FilteredPlacesRedux.filteredPlacesSetMeta(currentLocation))

  // make the call to the api
  const response = yield call(cf.filterPlaces, filter, currentLocation)

  if (response.ok) {
    yield put(FilteredPlacesRedux.filteredPlacesSuccess(response.data))
    if (R.length(response.data) < 1) {
      ToastAndroid.show('No hemos encontrado resultados para esta busqueda', ToastAndroid.LONG)
    }
  } else {
    __DEV__ && console.tron.log(response, 1)
    yield put(FilteredPlacesRedux.filteredPlacesFailure())
    ToastAndroid.show('Hemos encontrado un error durante la busqueda', ToastAndroid.LONG)
  }
}

// DEPRECATED CODE
/*
export function * filterPlaces (sagaFirebase, action) {
  // FIXME: try catch - failback

  console.tron.log('FETCHING PLACES', true)
  yield put(PlacesFindAllRedux.placesFindAllRequest())

  // Wait for places to load....
  const {payload} = yield take(PlacesFindAllTypes.PLACES_FIND_ALL_SUCCESS)
  // const {payload} = yield take(PlacesFindAllTypes.PLACES_FIND_ALL_SUCCESS)
  // FIXME: fetch from api
  const unfilteredPlaces = payload

  // const unfilteredPlaces = yield call(findAllPlaces, sagaFirebase)

  const searchIsActive = yield select(getSearchActive)
  let filter = yield select(getPlaceFilter)
  let currentLocation = yield select(getCurrentLocation)

  // Wait till get current location.....
  console.tron.log('GET CURRENT LOCATION', true)
  if (!currentLocation) {
    yield take(LocationTypes.LOCATION_SUCCESS)
    currentLocation = yield select(getCurrentLocation)
  }

  // No filter data present - should not happen!
  if (!filter) {
    yield put(FilteredPlacesRedux.filteredPlacesApplyDefault())
  }
  filter = yield select(getPlaceFilter)

  // From here we should call to Cloud Function to execute the search

  const {selectedCategories = []} = filter
  const conditions = {}

  if (filter.pricing) {
    conditions.pricing = R.equals(filter.pricing)
  }
  if (filter.emergency === true) {
    conditions.emergency = R.equals(true)
  }
  if (selectedCategories && R.length(selectedCategories) > 0) {
    conditions.category = (catId) => selectedCategories.includes(catId)
  }

  // Filter by conditions
  const predicate = R.where(conditions)

  // FIXME: fetch from api

  const filteredPlaces = R.filter(predicate, unfilteredPlaces)

  // Filter by radius
  console.tron.log('FILETER RADIUS', true)
  const withinRadius = (place) => isPlaceWithinRadius(place, currentLocation, filter.searchRadius * 1000)
  const filteredByDistance = R.filter(withinRadius, filteredPlaces)

  // TODO: DOING SLICE
  const slicedPlaces = filteredByDistance.slice(0, AppConfig.filteredSpacesLimit)
  // Sort
  const places = sortPlaces(slicedPlaces, filter.orderBy, currentLocation)

  // console.tron.warn({filter, conditions, currentLocation, filteredPlaces, filteredByDistance, sorted: places})
  console.tron.warn({filter, conditions, currentLocation, filteredPlacesL: filteredPlaces.length, filteredByDistanceL: filteredByDistance.length, sorted: places.length})
  yield put(FilteredPlacesRedux.filteredPlacesSuccess(places))

  // If search was active > rerun search
  if (searchIsActive) {
    yield put(PlaceSearchRedux.placeSearchRerun())
    // FIXME: should apply SORT here
  }
}
*/
// TODO: WE CAN DO IT LOCALLY??
export function sortPlaces (places:Array<Place>, orderBy: String, currentLocation) {
  // Is place within radius

  const orderByFuncs = [] // No sort by default
  const getDistFunc = (place) => GeoFunctions.getDistanceToPlace(place, currentLocation)

  switch (orderBy) {
    case 'relevance':
      orderByFuncs.push(R.descend(R.prop('rating')))
      // 2- Mayor cantidad de votantes - TODO: for this we need to laod reviews for each place - costs CPU
      orderByFuncs.push(R.ascend(getDistFunc))
      orderByFuncs.push(R.ascend(R.prop('pricing')))
      // 5- Mayor cantidad de búsquedas en analytics TODO: no tenemos analytics :-)
      break

    case 'rating':
      orderByFuncs.push(R.descend(R.prop('rating')))
      break

    case 'nearness':
    default:
      // TODO: optimize: calculate when initially loading places...
      orderByFuncs.push(R.ascend(getDistFunc))
      break
  }

  return R.sortWith(orderByFuncs, places)
}
