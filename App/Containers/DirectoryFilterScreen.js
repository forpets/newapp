import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import Categories from '../Services/Categories'
import R from 'ramda'
import CheckBox from 'react-native-check-box'
import BasicSwitch from '../Components/BasicSwitch'
import { Images } from '../Themes'
import Slider from 'react-native-slider'
import DirectoryFilterOrderBy from '../Components/DirectoryFilterOrderBy'
import styles from './Styles/DirectoryFilterScreenStyle'
import PricingFilterInput from '../Components/PricingFilterInput'
import FilteredPlacesRedux, { getPlaceFilter } from '../Redux/FilteredPlacesRedux'
import AppConfig from '../Config/AppConfig'

const categories = Categories.findAll()

class DirectoryFilterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  });

  constructor (props) {
    super(props)

    // If in some rare case we do not get placeFilter from state - use defaultFilter
    const placeFilter = this.props.placeFilter || AppConfig.defaultFilter

    this.state = {
      orderBy: placeFilter.orderBy,
      searchRadius: placeFilter.searchRadius,
      selectedCategories: placeFilter.selectedCategories,
      emergency: placeFilter.emergency,
      pricing: placeFilter.pricing
    }
  }

  handleSearchRadius = (searchRadius:number) => {
    // const searchRadius = Math.round(input)
    this.setState({searchRadius})
  }

  handleOrderBy = (orderBy:string) => {
    this.setState({orderBy})
  }

  handlePressCategoryCheckbox = (value, selected) => {
    const {selectedCategories} = this.state
    const newValue = selected ? R.append(value, selectedCategories) : R.without([value], selectedCategories)
    // const secondaryCategoriesLabels = Categories.idsToString(newValues)
    this.setState({selectedCategories: newValue})
  }

  pressSelectAllCategories = () => {
    const selectedCount = R.length(this.state.selectedCategories)
    const totalCount = R.length(categories)

    if (selectedCount === totalCount) {
      // If selected all - deselect
      this.setState({selectedCategories: []})
    } else {
      // Select all
      const selectedCategories = categories.map(cat => cat.value)
      this.setState({selectedCategories})
    }
  }

  handlePricing = (pricing : number) => {
    this.setState({pricing})
  }

  submit = () => {
    const filter = R.pickAll(['orderBy', 'searchRadius', 'selectedCategories', 'emergency', 'pricing'], this.state)
    this.props.filteredPlacesApply(filter)
    this.props.navigation.goBack()
  }

  render () {
    const {navigation} = this.props

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View style={styles.header}>
            <View style={styles.iconArrowWrapper}>
              <IconMD name='arrow-back' size={25} onPress={() => navigation.goBack()} style={styles.iconArrow} />
            </View>
            <TouchableOpacity onPress={this.submit}>
              <View style={styles.headerRightButtonWrapper}>
                <Text style={styles.headerRightButton}>OK</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.mainWrapperTitle}>
            <Text style={styles.headerTitle}>Filtrá tu busqueda dentro de la lista</Text>
          </View>

          <View style={styles.mainWrapperSubTitle}>
            <Text style={styles.subTitle}>Ordenar por</Text>
          </View>
          <DirectoryFilterOrderBy value={this.state.orderBy} onChange={this.handleOrderBy} />

          <View style={styles.mainWrapperSubTitle}>
            <Text style={styles.subTitle}>Precios</Text>
          </View>
          <PricingFilterInput value={this.state.pricing} onChange={this.handlePricing} />

          {/* *********  *RADIO DE BUSQUEDA* START ************ */}
          <View style={styles.radioBusquedaContainer}>
            <View style={styles.radioBusquedaTitleWrapper}>
              <Text style={styles.subTitle}> Radio de búsqueda </Text>
            </View>
            <View style={styles.sliderContainer}>
              <Slider
                step={1}
                value={this.state.searchRadius}
                onValueChange={this.handleSearchRadius}
                thumbTintColor={'white'}
                minimumTrackTintColor={'#8FE824'}
                trackStyle={styles.trackStyle}
                minimumValue={1}
                maximumValue={20}
                thumbStyle={styles.thumbStyle}
                thumbImage={Images.directorio.locationGuideBlack}
               />
              <View style={styles.footerContainer}>
                <View><Text style={styles.footerItem}>0 Km</Text></View>
                <View><Text style={styles.footerItemValue}>{this.state.searchRadius} Km</Text></View>
                <View><Text style={styles.footerItem}>20 km</Text></View>
              </View>
            </View>
          </View>
          {/* *********  *RADIO DE BUSQUEDA* END ************ */}

          {/* *********  *CATEGORIAS* START ************ */}
          <View style={styles.radioCategoriaContainer}>
            <View style={styles.radioCategoriaWrapperTitles}>
              <View>
                <Text style={styles.subTitle}> Categorías</Text>
              </View>
              <TouchableOpacity onPress={this.pressSelectAllCategories}>
                <Text style={styles.rightTitle}>Seleccionar todas</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.checkBoxesContainer}>
              {this.renderCheckboxes(categories)}
            </View>
          </View>
          {/* *********  *CATEGORIAS* END ************ */}

          {/* *********  *EMERGENCIAS* START ************ */}

          <View style={styles.emergencySwitcherContainer}>
            <View style={styles.emergencyTextItemWrapper}>
              <Text style={styles.emergencyTextItem}>Emergencias</Text>
            </View>
            <View style={styles.switchItemWrapper}>
              <BasicSwitch value={this.state.emergency} onValueChange={(val) => this.setState({emergency: val})} />
            </View>
          </View>
          {/* *********  *EMERGENCIAS* END ************ */}
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

  renderCheckboxes = (categories) => {
    // Group by 2 for rendering e.g.: <checkbox> <checkbox>
    const pairs = R.splitEvery(2, categories)
    return pairs.map((pair, i) => this.renderCheckboxPair(pair, i))
  }

  renderCheckboxPair = (pair, i) => {
    const [first, second] = pair
    // Do random - to force rerender all checkboxes (when receiving selected from this.state)
    const key = Math.random()

    return (
      <View key={key} style={styles.checkBoxesWrapper}>
        <View style={styles.checkBoxItem}>
          {this.renderCheckbox(first)}
        </View>
        <View style={styles.checkBoxItem}>
          {this.renderCheckbox(second)}
        </View>
      </View>
    )
  }

  renderCheckbox = (item) => {
    if (!item) return
    const selected = R.contains(item.value, this.state.selectedCategories)

    return (
      <CheckBox
        style={styles.checkbox}
        onClick={() => this.handlePressCategoryCheckbox(item.value, !selected)}
        isChecked={selected}
        rightText={item.label}
        rightTextStyle={selected ? styles.checkBoxClicked : styles.checkboxText}
        checkBoxColor={selected ? '#FF1744' : 'black'}
              />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    placeFilter: getPlaceFilter(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    filteredPlacesApply: (...args) => dispatch(FilteredPlacesRedux.filteredPlacesApply(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectoryFilterScreen)
