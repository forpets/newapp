import React from 'react'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/DirectoriesListStyle'
import { Images } from '../Themes/'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import Categories from '../Services/Categories'
import GeoFunctions from '../Transforms/GeoFunctions'
import PlaceStatus from '../Components/PlaceStatus'

class LostFoundList extends React.PureComponent {
// class LostFoundList extends React.Component {
  // shouldComponentUpdate (nextProps, nextState) {
  //   const {isFetching} = nextProps
  //   console.tron.log({nextProps, isFetching})

  //   if (isFetching) {
  //     return false
  //   }
  //   return true
  // }

  renderRow = ({item}) => {
    const {navigation, currentLocation} = this.props
    const params = {id: item.id}

    const getDateText = (dateString) => {
      const parts = dateString.split('/')
      const date = new Date(parts[2],parseInt(parts[1])-1,parts[0])
      const now = new Date();
      const days = Math.floor((now.getTime() - date.getTime()) / (1000*60*60*24))

      return 'Hace ' + days + ' días'
    }

    const onPress = () => {
      console.debug('Pressed report list item: ', item.id)
      // navigation.navigate('PlaceViewScreen', params)
    }


    const distance = (item.location && currentLocation) ? GeoFunctions.getDistanceToPlace(item, currentLocation) : null
    const distanceText = distance ? `A ${distance} km` : ''

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.row}>

          <View style={styles.sinAnuncioContainer}>
            <View style={styles.sinItemanuncio1}>
              <View style={styles.sinAnuncioWrapper1}>
                <Text style={styles.sinAnuncioBoldLabel}>{item.name}</Text>
              </View>
              <View style={styles.sinAnuncioWrapper2}>
                <Text style={styles.vetLabel}>
                  { getDateText(item.date) }
                </Text>
              </View>
              <View style={styles.sinAnuncioWrapper2}>
                <Text style={styles.vetLabel}>
                  { item.kind } - { item.gender }
                </Text>
              </View>
            </View>

            <View style={styles.sinItemanuncio2}>
              <View style={styles.sinAnuncioWrapper3}>
                <IconMD style={styles.sinAnunciolocation} name='location-on' />
              </View>
              <View style={styles.sinAnuncioWrapper4}>
                <Text style={styles.sinAnuncioLabel}>{item.address}</Text>
              </View>
              <View style={styles.sinAnuncioWrapper5}>
                <Text style={styles.sinAnuncioKmNumber}>{distanceText}</Text>
              </View>
            </View>
          </View>

          <View style={styles.footer}>
            <View style={styles.rate}>
              <Text style={styles.kmNumber}>{distanceText}</Text>
            </View>
          </View>
        </View>

      </TouchableOpacity>
    )
  }

  // Render a header?
  renderHeader = () => {}
    // <View style={[styles.label, styles.sectionHeader]}>
    //   <Image source={Images.directories.vetBagEmergency} style={styles.logoImage} />
    //   <Text style={styles.text1}> VETERINARIAS </Text>
    // </View>

  // Render a footer?
  renderFooter = () => {}

  // Show this when data is empty
  // FIXME: Read analysis - maybe show old results...
  renderEmpty = () => {
    // const {isFetching}
    return (
      <View style={styles.rowEmpty}>
        <View style={styles.labelEmptyWrapper}>
          <Text style={styles.emptyLabel}>No se han encontrando mascotas para tu búsqueda</Text>
        </View>
      </View>
    )
  }

  renderSeparator = () => <View style={styles.divider} />

  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => index

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 12

  // extraData is for anything that is not indicated in data
  // for instance, if you kept "favorites" in `this.state.favs`
  // pass that in, so changes in favorites will cause a re-render
  // and your renderItem will have access to change depending on state
  // e.g. `extraData`={this.state.favs}

  // Optimize your list if the height of each item can be calculated
  // by supplying a constant height, there is no need to measure each
  // item after it renders.  This can save significant time for lists
  // of a size 100+
  // e.g. itemLayout={(data, index) => (
  //   {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
  // )}

  render () {
    const {data, currentLocation} = this.props

    return (
      <View style={styles.container}>
        <FlatList
          scrollEventThrottle={500}
          contentContainerStyle={styles.listContent}
          data={data}
          extraData={currentLocation}
          renderItem={this.renderRow}
          keyExtractor={this.keyExtractor}
          initialNumToRender={this.oneScreensWorth}
          windowSize={7}
          ListImageComponent={this.renderImage}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    )
  }
}

export default LostFoundList
