// @flow
import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, InteractionManager } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import R from 'ramda'
import styles from './Styles/LostFoundStyle'
import validate from '../Lib/Validate'
import UserInput from '../Components/UserInput'
import RoundButton from '../Components/RoundButton'
import UserPickerInput from '../Components/UserPickerInput'
import UserDateInput from '../Components/UserDateInput'
import UserRadioButton from '../Components/UserRadioButton'
import GoogleAutocompleteInput from '../Components/GoogleAutocompleteInput'
import Background from '../Components/Background'
import LostFoundRedux, { getReport } from '../Redux/LostFoundRedux'
import { getCurrentUser } from '../Redux/UserRedux'
import PetPhotoPicker from '../Components/PetPhotoPicker'
import { Alert } from 'react-native'
import PetAttributes from '../Services/PetAttributes'

type Props = {
  pet: Pet,
  report: Report,
  navigation: Object,
  createLostFound: Function,
  updateLostFound: Function,
  owner: User
}

type Breed = {
  label: string,
  value: string
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  // mode: 'new' | 'edit',

  name: string,
  date: string,
  kind: string,
  breed: string,
  gender: string,
  description: string,
  image: string,
  imagePickerResponse?: null,

  breedsForSelect: Array<Breed>
}

class LostFoundEditScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const { onPressBack, headerTitle } = params

    return {
      title: headerTitle,
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={onPressBack}
        style={{paddingLeft: 20}}
      />
    }
  }

  constructor (props : Props) {
    super(props)
    const {pet, report, reportType} = props

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,
  
      reportType: reportType || 'found',
      pet: pet,
      name: '',
      date: null,
      age: null,
      kind: null,
      gender: null,
      breed: null,
      color: null,
      description: null,
      imagePickerResponse: null,
      address: null,
      address_components: null,
      location: null,

      breedsForSelect: PetAttributes.findBreedsFor()
    }

    // If we are creating Lost - assign values from pet
    if (!report && reportType == 'lost' && pet) {
      this.state.name = pet.name
      this.state.date = pet.date
      // this.state.age = calculate from pet.birthdate
      this.state.kind = pet.kind
      this.state.gender = pet.gender
      this.state.breed = pet.breed
      this.state.breedsForSelect = PetAttributes.findBreedsFor(this.state.kind)
      this.state.color = pet.color
      this.state.description = pet.description
    }
    // If we are editing - assign values
    if (report) {
      this.state.pet = report.pet
      this.state.name = report.name
      this.state.date = report.date
      this.state.age = report.age
      this.state.kind = report.kind
      this.state.gender = report.gender
      this.state.breed = report.breed
      this.state.breedsForSelect = PetAttributes.findBreedsFor(this.state.kind)
      this.state.color = report.color
      this.state.description = report.description
      this.state.address = report.address
      this.state.address_components = report.address_components
      this.state.location = report.location
    }
  }

  componentDidMount () {
    const {navigation} = this.props
    const {onPressBack} = this
    let headerTitle = this.props.report ? 'Editar mascota ' : 'Crear mascota '
    headerTitle += (this.props.reportType == 'lost') ? 'perdida' : 'encontrada'

    InteractionManager.runAfterInteractions(() => {
      navigation.setParams({ onPressBack, headerTitle })
    })
  }

  onPressBack = () => {
    const {touched} = this.state
    const {navigation} = this.props

    if (touched && touched.length > 0) {
      Alert.alert('', '¿Desea salir?',
        [
          {text: 'SI', onPress: () => { navigation.goBack() }},
          {text: 'NO', onPress: () => {}}
        ],
        { cancelable: true }
      )
    } else {
      navigation.goBack()
    }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    // Handle pet type change
    if (fieldName === 'kind') this.handlePetKindChange(fieldValue)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
  // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, name, date, kind, breed, description, address_components} = this.state
    let validationErrors = {}

    // Validated fields
    validationErrors.name = validate('name', name)
    validationErrors.date = validate('date', date)
    validationErrors.kind = validate('petKind', kind)
    validationErrors.breed = validate('petBreed', breed)

    validationErrors.description = validate('petDescription', description)

    // Must be precise address with street name and stuff
    // This meens we get 5 items address_components  from Google
    // console.tron.log({address_components, lenght: R.length(address_components)})
    if (!(address_components && R.length(address_components) >= 5)) {
      validationErrors.address = 'Ingrese Dirección completa (con No. de calle)'
    }

    if (isSubmit) {
      this.setState({touched: ['name', 'date', 'kind', 'breed', 'description', 'address_components'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }

  saveReport = () => {
    const {createLostFound, updateLostFound, owner, report, pet} = this.props
    // console.tron.warn({owner, state: this.state, report})
    const petUid = pet ? pet.uid : null

    if (this.formIsValid(true)) {
      const {imagePickerResponse} = this.state
      const payload = R.pick(['reportType', 'name', 'date', 'address', 'address_components', 'location', 'age', 'kind', 'gender', 'breed', 'color', 'description'], this.state)
      
      if (report) {
        updateLostFound({...payload, owner: owner.uid, pet: petUid}, imagePickerResponse, report.id)
      } else {
        createLostFound({...payload, owner: owner.uid, pet: petUid}, imagePickerResponse)
      }
    } else {
      // console.tron.error({state: this.state})
      this.focusTo(R.keys(this.state.errors)[0])
    }
  }

  handlePetKindChange = (itemValue) => {
    if (itemValue !== this.state.kind) {
      const breedsForSelect = PetAttributes.findBreedsFor(itemValue)
      this.setField('breed', null)
      this.setState({breedsForSelect})
    }
  }

  onPhotoSelected = (imagePickerResponse : Object) : void => {
    const {touched} = this.state
    touched.push('imagePickerResponse')
    this.setState({imagePickerResponse, touched}, this.formIsValid)
  }

  // Get details from autocomplete
  onGetDetails = (details) => {
    if (details) {
      // FIXME: transform address components
      let {address_components} = details

      if (address_components && R.length(address_components) > 0) {
        address_components = R.map((comp) => { return {name: comp.long_name, types: comp.types} }
        , address_components)
      }

      // console.tron.log({address_components})
      const {lat, lng} = details.geometry.location
      this.setState({location: [lat, lng], address_components}, this.formIsValid)
    }
  }

  petKinds = PetAttributes.findAllKinds()
  petGenders = PetAttributes.findAllGenders()

  render () {
    const { submitDisabled, breedsForSelect } = this.state
    const { pet, reportType} = this.props
    const thisComponent = this
    const petImage = pet ? pet.image : null
    const { onUpdatePetType, saveReport, onPhotoSelected } = this

    const date = new Date()
    const mindate = new Date()
    mindate.setDate(date.getDate()-60)

    // console.tron.log(this.state)

    return (
      <View style={styles.container}>
        <Background />
        <ScrollView style={styles.container}>
          <KeyboardAvoidingView behavior='padding'>

            <View>
              <UserInput
                style={styles.nameInput}
                parentComp={this}
                fieldName={'name'}
                placeholder='Nombre'
                defaultValue={this.state.name}
                onSubmitEditing={() => this.focusTo('name')}
              />
            </View>

            <View>
              <UserDateInput
                style={styles.date}
                parentComp={thisComponent}
                minDate={mindate}
                maxDate={date}
                fieldName='date'
                placeholder={'Fecha ' + ((reportType=='lost')?'perdido':'encontrado')}
                defaultValue={this.state.date}
              />
            </View>

            {/* Location */}
            <View style={styles.inputsWrappers}>
              <GoogleAutocompleteInput
                parentComp={this}
                fieldName={'address'}
                // onGetCoordinates={this.onGetCoordinates}
                onGetDetails={this.onGetDetails}
                placeholder='Ubicación'
                defaultValue={this.state.address}
              />
            </View>
            {/* /Location */}

            {/* Age */}
            {/* /Age */}

            <View>
              <UserPickerInput
                placeholder='Tipo de Mascota'
                fieldName='kind'
                parentComp={thisComponent}
                selectedValue={this.state.kind}
                optionsForSelect={this.petKinds}
              />
            </View>

            <View>
              <UserRadioButton
                label='Sexo'
                fieldName='gender'
                parentComp={thisComponent}
                defaultValue={this.state.gender}
                options={this.petGenders}
                buttonInnerColor='#FFD600'
              />
            </View>

            <View>
              <UserPickerInput
                placeholder='Raza'
                fieldName='breed'
                parentComp={thisComponent}
                selectedValue={this.state.breed}
                optionsForSelect={breedsForSelect}
              />
            </View>

            {/* Color colorsForSelect */}
            {/* /Color */}

            <View>
              <UserInput
                parentComp={thisComponent}
                fieldName={'description'}
                placeholder='Rasgos Distintivos'
                defaultValue={this.state.description}
                onSubmitEditing={saveReport}
                maxLength={300}
                multiline
            />
            </View>

            <Text style={styles.footerText}>Razgos distintivos de la mascota</Text>

            <PetPhotoPicker photoUrl={petImage} onPhotoSelected={onPhotoSelected} />

            {/* Dar de baja la publicación */}

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginBottom: '2%'}}>
              <RoundButton
                bgColor='#FE5582'
                bgColorDisabled='#ffea80'
                color='white'
                text='  GUARDAR  '
                onPress={saveReport}
                // disabled={submitDisabled}
                />
            </View>

          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params} = props.navigation.state
  const id = params ? params.id : null
  const reportType = params ? params.type : null

  console.debug('mapped reportType: ' + reportType)

  return {
    reportType,
    owner: getCurrentUser(state),
    report: getReport(state, id)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createLostFound: (...args) => { dispatch(LostFoundRedux.reportCreateRequest(...args)) },
    updateLostFound: (...args) => { dispatch(LostFoundRedux.reportUpdateRequest(...args)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LostFoundEditScreen)
