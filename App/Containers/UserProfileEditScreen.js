// @flow
import React, { Component } from 'react'
import { ScrollView, KeyboardAvoidingView, InteractionManager, Text } from 'react-native'
import { connect } from 'react-redux'
import {getCurrentUser} from '../Redux/UserRedux'
import UserInput from '../Components/UserInput'
import Icon from 'react-native-vector-icons/FontAwesome'
import validate from '../Lib/Validate'
import R from 'ramda'
import UserUpdateRedux from '../Redux/UserUpdateRedux'

import styles from './Styles/UserProfileEditScreenStyle'
import UserPhotoPicker from '../Components/UserPhotoPicker'

type Props = {
  user: User,
  navigation: Object,
  userUpdateRequest: Function
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  imagePickerResponse?: Object,

  firstname: string,
  lastname: string,
  address: string,
  email: string,
  phone: string
}

class UserProfileEditScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {saveProfile, submitDisabled = true} = params

    return {
      title: `Edita tu perfil`,
      headerTitleStyle: styles.headerTitleStyle,
      // headerLeft: <Icon
      //   name='arrow-left'
      //   size={20}
      //   onPress={() => navigation.goBack()}
      //   style={{paddingLeft: 20}}
      // />,
      headerRight: <Icon
        name='check'
        size={20}
        onPress={saveProfile}
        style={{paddingRight: 20, color: submitDisabled ? 'gray' : 'black'}}
      />
    }
  }

  setHeaderParams = (submitDisabled: boolean) => {
    const {params = {}} = this.props.navigation.state
    const {oldSubmitDisabled} = params
    // Avoid unnecessary render
    if (oldSubmitDisabled !== submitDisabled) {
      const saveProfile = this.saveProfile
      this.props.navigation.setParams({
        saveProfile,
        submitDisabled
      })
    }
  }

  componentDidMount () {
    const {setHeaderParams} = this
    InteractionManager.runAfterInteractions(() => {
      setHeaderParams(true)
    })
  }

  constructor (props) {
    // TODO: add support for upload image
    super(props)

    const {address, email, phone, firstname, lastname} = props.user

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      image: '',
      imagePickerResponse: null,

      firstname: firstname || null,
      lastname: lastname || null,
      address: address || null,
      phone: phone || null,
      email: email || null
    }
  }

  /// //////////////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, () => this.formIsValid(false))
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
  // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean) : boolean => {
    const {touched, firstname, lastname, address, phone} = this.state
    let validationErrors = {}

    // Validated fields
    validationErrors.firstname = validate('name', firstname)
    validationErrors.lastname = validate('lastname', lastname)
    validationErrors.address = validate('userAddress', address)
    validationErrors.phone = validate('phone', phone) // FIXME: validate user profile edit screen

    if (isSubmit) {
      this.setState({touched: ['firstname', 'lastname', 'address', 'phone'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    this.setHeaderParams(!isValid)
    return isValid
  }
  /// ///////////////////////////////////////////////////////////// EOF FORM BOILERPLATE

  saveProfile = () => {
    const {userUpdateRequest, user} = this.props

    this.props.navigation.setParams({
      saveProfile: this.saveProfile,
      submitDisabled: this.state.submitDisabled
    })

    if (this.formIsValid(true)) {
      const {firstname, lastname, address, phone, imagePickerResponse} = this.state
      userUpdateRequest({firstname, lastname, address, phone}, imagePickerResponse, user.uid)
    } else {
    }
  }

  onPhotoSelected = (imagePickerResponse : Object) : void => {
    this.setState({imagePickerResponse})
  }

  render () {
    const {user} = this.props
    const saveProfile = this.saveProfile
    const {submitDisabled} = this.state

          // <UserAvatar photoURL={photo} style={{marginLeft: 'auto', marginRight: 'auto'}} onPress={this.saveImage} />
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='padding'>

          <UserPhotoPicker photoUrl={user.photoURL} onPhotoSelected={this.onPhotoSelected} />

          <UserInput
            parentComp={this}
            fieldName={'firstname'}
            placeholder='Nombre *'
            defaultValue={this.state.firstname}
            onSubmitEditing={() => this.focusTo('lastname')}
          />

          <UserInput
            parentComp={this}
            fieldName='lastname'
            placeholder='Apellido *'
            defaultValue={this.state.lastname}
            onSubmitEditing={() => this.focusTo('address')}
          />

          <UserInput
            parentComp={this}
            maxLength={100}
            fieldName='address'
            placeholder='Direccion'
            defaultValue={this.state.address}
            onSubmitEditing={() => this.focusTo('phone')}
          />

          {/*
          <UserInput
            editable={false}
            parentComp={this}
            fieldName='email'
            placeholder='E-mail*'
            defaultValue={this.state.email}
            onSubmitEditing={() => this.focusTo('phone')}
          />
          */}

          <UserInput
            parentComp={this}
            fieldName='phone'
            placeholder='Telefono'
            keyboardType='numeric'
            returnKeyType='done'
            defaultValue={this.state.phone}
            onSubmitEditing={() => saveProfile()}
          />

        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: getCurrentUser(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userUpdateRequest: (...args) => dispatch(UserUpdateRedux.userUpdateRequest(...args))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileEditScreen)
