import React, { Component } from 'react'
import { ScrollView, Text, View, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/NotesScreenStyle'
import { getNotesArray } from '../Redux/NotesRedux'
import {Images} from '../Themes'
import NoteList from '../Components/NoteList'
import { getPetsArray } from '../Redux/PetsRedux'
import Background from '../Components/Background'
import NavBars from '../Components/NavBars';
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

class NoteView extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Notas',
      headerTitleStyle: styles.headerTitle,
      headerLeft: <NavBars onPress={() => navigation.navigate('DrawerOpen')} /> 
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      notes: []
    }
  }

  editNote = (id) => { this.props.navigation.navigate('NotesNewScreen', {id}) }

  showContent = () => {
    let {navigate} = this.props.navigation
    let {notes} = this.props

    let list = (
      <View style={styles.splashContainer}>
        <Image style={styles.image} source={Images.notes.empty} />
        <Text style={styles.title}>Oops! No hay notas todavía</Text>
        <Text style={styles.subTitle}> ¡Hacé tap en "Agregar nueva nota" </Text>
        <Text style={styles.subTitle}> para comenzar! </Text>
      </View>
        )

    if (notes.length) {
      list = <NoteList notes={notes} clickFunction={this.editNote} navigate={navigate} />
    }

    return (
      <View style={styles.container}>

        <Background />
        <ScrollView contentContainerStyle={styles.listContainer}>
          {list}
        </ScrollView>

        <View style={styles.bottomButton}>
          <TouchableOpacity onPress={() => navigate('NotesNewScreen', {id: ''})} >
            <View style={styles.bottomButtonInner}>
              <Text style={styles.bottomButtonInnerText} >Agregar nueva nota</Text>
              <IconMD name='add' style={{fontSize: 30}} />
            </View>
          </TouchableOpacity>
        </View>

      </View>
    )
  }

  render () {
    // FIXME: bhackground image
    return (
      <ScrollView style={{flex: 1}}>
        {this.showContent()}
      </ScrollView>
    )
  }
}

const mapStateToProps = state => {
  return {
    notes: getNotesArray(state),
    pets: getPetsArray(state)
  }
}

export default connect(mapStateToProps)(NoteView)
