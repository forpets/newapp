import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/ForgotPwSuccessScreenStyle'
import Background from '../Components/Background'
import {Images} from '../Themes'
import BasicTextButton from '../Components/BasicTextButton'

class ForgotPwSuccessScreen extends Component {
  static navigationOptions = {
    header: null,
    drawerLockMode: 'locked-closed'
  }

  render () {
    const {navigation} = this.props

    return (
      <View style={[styles.container, styles.background ]}>
        <Background image={Images.loginBackground} />
        <ScrollView style={[styles.container]} contentContainerStyle={styles.contentContainer}>

          <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
              <IconMD name='close' style={{fontSize: 25, color: 'white'}} />
            </TouchableOpacity>
          </View>

          <Image source={Images.login.lock} />

          <Text style={styles.text1}>¡Felicitaciones!</Text>
          <Text style={styles.text2}>Tu contrasena fue debidamente{'\n'}
          reiniciada, te enviaremos un e-mail{'\n'}confirmando esta operacion.</Text>
          <BasicTextButton style={styles.text3} text='ENTENDIDO' onPress={() => navigation.navigate('LoginScreen')} />

        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPwSuccessScreen)
