import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, View, Image } from 'react-native'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'
import styles from './Styles/PlaceCreateSuccessScreenStyle'
import RoundButton from '../Components/RoundButton'
import { Images } from '../Themes'
import NavHelper from '../Lib/NavHelper'

class PlaceCreateSuccessScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    drawerLockMode: 'locked-closed'
  })

  onPressBack = () => {
    this.props.navigation.dispatch(NavHelper.reset('PlaceEditScreen'))
  }

  onPressGoDirectories = () => {
    this.props.navigation.dispatch(NavHelper.reset('DirectoriesScreen'))
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.backArrow}>
          <TouchableOpacity onPress={this.onPressBack}>
            <IconMD name='arrow-back' style={{fontSize: 25, color: 'black'}} />
          </TouchableOpacity>
        </View>

        <View style={styles.successContainer}>

          <Image style={styles.successImage} source={Images.crearEspacio.success} />
          <Text style={styles.titleText}>Espacio creado con exito</Text>
          <Text style={styles.descriptionText}>Tu espacio está siendo evaluado, para {'\n'}
                su posterior publicación. En caso de {'\n'}
                  algún inconveniente, nos {'\n'}
                    comunicaremos con vos.
          </Text>
          <View style={styles.roundButton}>
            <RoundButton
              text='IR A DIRECTORIO'
              color='white'
              bgColor='#FF1744'
              onPress={this.onPressGoDirectories}
            />
          </View>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceCreateSuccessScreen)
