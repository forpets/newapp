// @flow
import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/PlaceClaimScreenStyle'
import UserInput from '../Components/UserInput'
import RoundButton from '../Components/RoundButton'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import { getCurrentUser } from '../Redux/UserRedux'
import R from 'ramda'
import validate from '../Lib/Validate'
import PlaceClaimRedux, { getPlaceClaimError } from '../Redux/PlaceClaimRedux'

type Props = {
  placeId: string,
  user: User,
  navigation: Object,
  placeClaimRequest: Function
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  firstname?: string,
  lastname?: string,
  email?: string,
  phone?: number
}

class PlaceClaimScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  })

  constructor (props) {
    super(props)

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      firstname: null,
      lastname: null,
      email: null,
      phone: null

      // firstname: 'test',
      // lastname: 'test',
      // email: 'asdasd@asas.as',
      // phone: 2123123
    }
  }

  /// //////////////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    fieldValue = fieldValue || null
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
    // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, firstname, lastname, email, phone} = this.state

    // FIXME:
    const validationErrors = {
      firstname: validate('name', firstname),
      lastname: validate('lastname', lastname),
      email: validate('email', email),
      phone: validate('placeClaimPhone', phone)
    }

    if (isSubmit) {
      // Mark all as touched on submit
      this.setState({touched: [ 'firstname', 'lastname', 'email', 'phone' ], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }
    /// ///////////////////////////////////////////////////////////// EOF FORM BOILERPLATE

  componentWillReceiveProps (nextProps) {
    const {error} = nextProps
    if (!this.props.error && error) {
      Alert.alert('Error', 'Solicitud de error en proceso. Por favor intente mas tarde')
    }
  }

  goBack = () => { this.props.navigation.goBack() }

  submit = () => {
    const {placeId, user} = this.props

    if (this.formIsValid(true)) {
      const data = R.pickAll(['firstname', 'lastname', 'email', 'phone'], this.state)
      this.props.placeClaimRequest({...data, userUid: user.uid, placeId})
    } else {
      // Not valid
    }
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='padding'>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.goBack} style={styles.iconArrowWrapper}>
              <IconMD name='arrow-back' size={25} style={styles.iconArrow} />
            </TouchableOpacity>

          </View>

          <View style={styles.placeClaimScreenWrapper}>

            <View>
              <Text style={styles.headerTitle}>Reportar espacio como propio</Text>
            </View>

            <View>
              <Text style={styles.hedearFooter}>
                Dejános tus datos de contacto para poder
                comunicarnos con vos y asignar este espacio
                como propiedad tuya
              </Text>
            </View>

            <View>
              <Text style={styles.subTitle}>Datos de contacto</Text>
            </View>

            <View>
              <UserInput
                parentComp={this}
                fieldName={'firstname'}
                placeholder='Nombre *'
                defaultValue={this.state.firstname}
                onSubmitEditing={() => this.focusTo('lastname')}
              />
            </View>
            <View>
              <UserInput
                parentComp={this}
                fieldName='lastname'
                placeholder='Apellido *'
                defaultValue={this.state.lastname}
                onSubmitEditing={() => this.focusTo('email')}
              />
            </View>
            <View>
              <UserInput
                parentComp={this}
                fieldName='email'
                placeholder='Email *'
                defaultValue={this.state.email}
                onSubmitEditing={() => this.focusTo('phone')}
              />
            </View>
            <View>
              <UserInput
                parentComp={this}
                fieldName='phone'
                placeholder='Teléfono *'
                defaultValue={this.state.phone}
                returnKeyType='done'
                onSubmitEditing={this.submit}
              />
            </View>

            <View style={styles.leftbuttonContainer}>
              <View style={styles.leftbuttonWrapper}>
                <RoundButton
                  text='ENVIAR'
                  color='white'
                  bgColor='#FF1744'
                  bgColorDisabled='#ffb1c0'
                  disabled={this.state.submitDisabled}
                  onPress={this.submit}
                  />
              </View>
            </View>

          </View>

        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params = {}} = props.navigation.state
  const {placeId = null} = params

  return {
    placeId,
    user: getCurrentUser(state),
    error: getPlaceClaimError(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    placeClaimRequest: (...args) => dispatch(PlaceClaimRedux.placeClaimRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceClaimScreen)
