import React, { Component } from 'react'
import { ScrollView, Text, Image, KeyboardAvoidingView, View } from 'react-native'
import { connect } from 'react-redux'
import BasicButton from '../Components/BasicButton'
import GrayButton from '../Components/GrayButton'
import BasicTextButton from '../Components/BasicTextButton'
import R from 'ramda'
import { Images } from '../Themes'
import LoginRedux from '../Redux/LoginRedux'
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/LoginScreenStyle'
import validate from '../Lib/Validate'
import LoginInput from '../Components/LoginInput'
import Background from '../Components/Background'
import UserRedux from '../Redux/UserRedux'

type Props = {
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  email: string,
  password: string,
}

class LoginScreen extends Component<Props, State> {
  static navigationOptions = {
    header: null,
    drawerLockMode: 'locked-closed'
  }

  constructor (props) {
    super(props)

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      email: '',
      password: ''
    }

    // For fast developer login
    // if (__DEV__) {
    // this.state.email = 'asdfsadf@yopmail.com'
    // this.state.password = 'Damian12!'
    // this.state.touched = ['email', 'password']
    // this.state.submitDisabled = false
    // }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, () => this.formIsValid(false))
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
    // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean) : boolean => {
    const {touched, email, password} = this.state
    let validationErrors = {}

      // Validated fields
    validationErrors.email = validate('email', email)
    validationErrors.password = validate('password', password)

    if (isSubmit) {
      this.setState({touched: ['email', 'password'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors: errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }

  // DO LOGIN
  login = () => {
    const {email, password} = this.state
    if (this.formIsValid(true)) {
      this.props.loginRequest({email, password})
    }
  }

  componentWillReceiveProps (nextProps) {
    const {loginState} = nextProps
    if (loginState && loginState.error) {
      // If we tried to Login but API returned error
      this.setState({ errors: {
        email: 'Ingrese un E-mail valido',
        password: 'Contraseña invalida. Intente nuevamente'
      } })
    }
  }

  render () {
    const {navigation} = this.props
    const {submitDisabled} = this.state

    return (
      <View style={[styles.container, styles.background]}>

        <Background image={Images.loginBackground} />

        <ScrollView style={[styles.container]}>

          <KeyboardAvoidingView behavior='padding'>
        
            <Image style={[styles.logoForPets]} source={Images.logoLogin} />
        
            <Text style={[styles.headerStyle]}>¡Te damos la {'\n'}bienvenida a ForPets!</Text>

            <LoginInput
              parentComp={this}
              fieldName='email'
              placeholder='E-mail'
              defaultValue={this.state.email}
              onSubmitEditing={() => this.focusTo('password')}
          />

            <LoginInput
              parentComp={this}
              fieldName='password'
              placeholder='Contraseña'
              returnKeyType='go'
              secureTextEntry
              defaultValue={this.state.password}
              onSubmitEditing={() => this.login()}
          />

            <GrayButton disabled={submitDisabled} onPress={() => this.login()} text='INGRESAR' />

            <BasicButton onPress={this.props.loginFacebookRequest}>
              <View>
                <Text style={styles.fbButtonStyle}>INGRESAR CON <Icon style={styles.fbimage} name='facebook' /></Text>
              </View>
            </BasicButton>

            <View style={styles.footerContainer}>
              <BasicTextButton style={styles.text1} text='Olvidé mi contraseña' onPress={() => navigation.navigate('ForgotPwScreen')} />
              <BasicTextButton style={styles.text2} text='Continuar como invitado' onPress={this.props.loginGuest} />
              <BasicTextButton style={styles.text3} text='¿No tenes una cuenta? Registrate' onPress={() => navigation.navigate('RegisterScreen')} />
            </View>

            <View style={styles.terminos} >
              <BasicTextButton
                style={styles.text4}
                text='Al registrarte, aceptas los Términos y Condiciones de Uso del servicio'
                onPress={() => navigation.navigate('TermsAndCondScreen')} />
            </View>
            <View style={ styles.space} />
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
  }

const mapStateToProps = (state) => {
  return {
    loginState: state.login,
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: (credentials) => { dispatch(LoginRedux.loginRequest(credentials)) },
    parentNav: (route) => { dispatch(NavigationActions.navigate({ routeName: route })) },
    loginGuest: () => { dispatch(UserRedux.userLoginGuest()) },
    loginFacebookRequest: () => { dispatch(LoginRedux.loginFacebookRequest()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
