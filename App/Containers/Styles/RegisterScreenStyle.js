import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  backArrow: {
    marginVertical: 10,
    marginLeft: 20
  },
  container: {
    flex: 1,
  },
  background: {
    backgroundColor: Colors.loginGreen

  },
  input: {
    height: 38,
    borderRadius: 25,
    backgroundColor: Colors.greenInputLight,
    marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    justifyContent: 'center',
    fontFamily: Fonts.normal,
    paddingLeft: 25
  },
  headerTitle: {
    ...Fonts.style.loginH1,
    color: Colors.snow,
    marginLeft: 22,
    // marginTop: 10,
    marginVertical: 5
  },
  terminos: {
    marginVertical: 20,
    fontFamily: Fonts.type.base,
    fontSize: 13,
    color: Colors.whiteCustom,
    width: '75%',
    textAlign: 'center',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  textProfilePic: {
    color: Colors.snow,
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 12,
    textAlign: 'center',
    marginVertical: 5,
    fontWeight: 'bold',
    marginBottom: 5
  },
  space:{
    height: 60
  }
})
