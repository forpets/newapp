import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

const {height, width} = Dimensions.get('window')
const itemWidth = width * 1

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
let marginHorizontal:any
let marginLeft:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 18) : ((Metrics.screenWidth <= 360) ? (fontSize = 20) : (fontSize = 20))
marginHorizontal = (Metrics.screenWidth <= 320) ? (marginHorizontal = -35) : ((Metrics.screenWidth <= 360) ? (marginHorizontal = -16) : (marginHorizontal = -22))
marginLeft = (Metrics.screenWidth <= 320) ? (marginLeft = 16) : ((Metrics.screenWidth <= 360) ? (marginLeft = 17) : (marginLeft = 18))

/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  container: {
    flex: 1
  },
  rowEmpty: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'white'
  },
  labelEmptyWrapper: {
    // flex: 1,
   /*  flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center', */
    width: moderateScale(300),
    elevation: 5,
    marginLeft: 'auto',
    marginRight: 'auto',

    marginVertical: moderateScale(20),
    borderRadius: 7
  },
  emptyLabel: {
    textAlign: 'center',
    marginHorizontal: moderateScale(8),
    marginBottom: 9,
    marginTop: 5,
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(14),
    padding: moderateScale(16)
  },
  row: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: Colors.white,
    marginVertical: Metrics.smallMargin*2,
    borderRadius: 7,
    width: '95%',
    elevation: 7,
    marginLeft: 'auto',
    marginRight: 'auto'

  },

  row2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: moderateScale(14)

  },
  services: {
    fontSize: moderateScale(8),
    fontFamily: Fonts.type.base,
    fontWeight: 'bold',
    borderRadius: 10,
    borderColor: '#E0105F',
    color: '#E0105F',
    borderWidth: 1,
    paddingTop: 3,
    paddingBottom: 2,
    paddingLeft: 5,
    paddingRight: 5
  },
  boldLabel: {
    marginTop: 6,
    color: '#444444',
    fontFamily: 'Rubik-Medium',
    fontSize,
    marginLeft: 10
  },
  label: {
    marginHorizontal: moderateScale(8),
    marginBottom: 9,
    marginTop: 5,
    fontSize: (Metrics.screenWidth <= 320) ? (fontSize = 14) : ((Metrics.screenWidth <= 360) ? (fontSize = 18) : (fontSize = 18))

  },
  listContent: {
    marginTop: Metrics.baseMargin
  },
  divider: {
    // height: 1,
    // backgroundColor: 'black'
  },
  logoImage: {
    width: 33,
    height: 28,
    marginHorizontal: 15
  },
  footer: {
    marginBottom: 2,
    marginTop: 2,
	  borderTopWidth: 1,
    borderTopColor: '#e6e6e6',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: itemWidth
  },
  rate: {
    marginLeft,
    marginVertical: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  rateNumber: {
    marginLeft: 7,
    color: '#646464',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  },
  kmNumber: {
    color: '#444444',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  //  marginTop: moderateScale(13)

  },
  sinAnuncioKmNumber: {
    color: '#B0B0B0',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14),
    marginTop: moderateScale(13),
    marginHorizontal: (Metrics.screenWidth <= 320) ? (marginHorizontal = -10) : ((Metrics.screenWidth <= 360) ? (marginHorizontal = -6) : (marginHorizontal = -22))
  },
  rateClock: {
    marginTop: 7,
    marginRight: 22,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  isOpen: {
    fontSize: 13,
    color: '#8acc27',
    marginRight: moderateScale(14),
    paddingBottom: moderateScale(5)
  },
  anuncioContainer: {
    marginBottom: moderateScale(14),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: moderateScale(6),
    marginTop: moderateScale(10)
  },
  anuncio1: {
    flex: 8
  },
  anuncioTitleWrapper: {

  },
  anuncioLabelWrapper: {

  },
  anuncio2: {
    flex: -1,
    marginHorizontal: moderateScale(-15),
    marginTop: moderateScale(-26)
  },
  anuncio3: {
    flex: 1,
    marginHorizontal: moderateScale(14),
    marginTop: moderateScale(-26)
  },
  startIcon: {
    color: '#8ACB27',
    marginRight: moderateScale(1),
    paddingBottom: moderateScale(5),
    paddingRight: -10

  },
  sinAnuncioContainer: {
   /*  flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center', */
    flex: 1,
    marginHorizontal: moderateScale(6),
    marginTop: moderateScale(10),
    width: '97%'
  },
  sinItemanuncio1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'

  },
  sinItemanuncio2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: moderateScale(18),
    marginBottom: moderateScale(18)
  },
  vetLabel: {
    // paddingTop: moderateScale(18),
    color: '#8F95E7',
    fontFamily: Fonts.type.base,
    fontSize: (Metrics.screenWidth <= 320) ? (fontSize = 9) : ((Metrics.screenWidth <= 360) ? (fontSize = 10) : (fontSize = 11))
    // marginLeft: 10
  },
  sinAnuncioLabel: {
    width: '50%'
  },

  sinAnuncioWrapper1: {
    flex: 11
  },
  sinAnuncioWrapper2: {
    flex: 4

  },
  sinAnuncioWrapper3: {
    flex: 1,
    marginHorizontal: moderateScale(7)
  },
  sinAnunciolocation: {
    fontSize: moderateScale(18)
  },
  sinAnuncioWrapper4: {
    flex: 6,
    marginLeft: moderateScale(-18)
  },
  sinAnuncioWrapper5: {
    flex: 2
    // marginRight: -32
  },
  sinAnuncioBoldLabel: {
    marginTop: 6,
    color: '#444444',
    fontFamily: 'Rubik-Medium',
    fontSize: 20,
    marginLeft: 10
  },
  placeStatusWrapper: {
    marginTop: 7,
    justifyContent: 'center',
    marginRight: moderateScale(15),
    paddingBottom: moderateScale(5)
  },
  placeStatusContainer: {

  /*   borderWidth:1, */
    marginRight: (Metrics.screenWidth <= 320) ? (marginRight = -25) : ((Metrics.screenWidth <= 360) ? (marginRight = -19) : (marginRight = -22))
    
  }

})
