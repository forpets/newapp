import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  tituloGaleria: {
    marginHorizontal: 20,
    marginVertical: 5,
    marginBottom: 10
  },
  tituloGaleriaItem: {
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(18)
  },
  emergencyTextItem: {
    marginHorizontal: 20,
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(20)

  },
  emergencySwitcherContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: -14
  },
  switchItemWrapper: {
    marginHorizontal: moderateScale(20)
  },
  crearEspacioButton: {
    marginLeft: 'auto',
    width: '60%',
    marginRight: 'auto'
  },
  placePhotoPicker: {
    marginHorizontal: 16,
    marginVertical: 10,
    marginBottom: 20
  },
  inputsWrappers: {
    marginHorizontal: moderateScale(-5),
    marginVertical: moderateScale(-2)
  },
  inputsObservacioneEmergencyWrappers: {
    marginHorizontal: moderateScale(-4)

  }

})
