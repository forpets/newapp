import { StyleSheet, Dimensions } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
const {width, height} = Dimensions.get('window')
const butonWidth = width * 0.35
const butonHeigth = width * 0.15
const containerHeight = width * 0.25
const containerWidth = width * 0.25

export default StyleSheet.create({
  container: {
    flex: 1
  },
  container2: {
    flex: 1
  },
  headerTitleStyle: {
    ...Fonts.style.header,
    color: '#8F95E7'
  },
  headerTitle: {
    fontSize: 28,
    color: '#8F98E5',
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 40,
    marginLeft: 10
  },
  title: {
    fontSize: 20,
    color: 'black',
    marginBottom: 20,
    fontFamily: Fonts.type.rubikMedium

  },
  subTitle: {
    fontSize: 17,
    color: '#ababab',
    textAlign: 'center'
  },
  startButton: {
    backgroundColor: '#8F95E7',
    marginTop: 20,
    width: butonWidth,
    height: butonHeigth,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    flexDirection: 'row'
  },
  startButtonText: {
    color: '#ffffff',
    fontWeight: 'bold'
  },
  splashContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    marginTop: 90,
    marginBottom: 50
  },
  petAvatar: {
    width: containerWidth,
    height: containerHeight,
    marginTop: 0
  },
  plusText: {
    fontSize: 60,
    color: 'white',
    flex: 1
  },
  plusContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: containerWidth,
    height: containerHeight,
    backgroundColor: '#8F96E7'
  },
  flatListContainer: {

    borderWidth: 0
  },
  crossContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  }
})
