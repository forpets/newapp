import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    paddingLeft: 10, //FIXME: centered?
    paddingTop: 10,
    // marginHorizontal: 'auto',
    // borderWidth: 2
  },
  containerEmpty: {
    flex: 1,
    marginTop: 150,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'

    // borderColor: 'red',
    // borderWidth: 10
  },
  headerTitle: {
    ...Fonts.style.header,
    color: Colors.rosyRed
  },
  image: {
    // padding: 20,
    // width: 350,
    // height: 210
  },
  text1: {
    paddingTop: 40,
    fontFamily: Fonts.type.rubikMedium,
    fontSize: Fonts.size.h3
    // fontWeight: 'bold'
  },
  text2: {
    padding: 20,
    fontFamily: Fonts.type.base,
    textAlign: 'center',
    fontSize: Fonts.size.h6,
    color: 'gray'
  },
  button: {
    borderWidth: 1,
    marginVertical: 20
  }

})
