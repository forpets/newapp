import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: 'white'
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    height: moderateScale(600)

  },
  titleWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%'

  },
  iconArrow: {
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(20)
  },
  imageWrapper: {
    marginTop: 150
  },
  iconSuccess: {
  },
  textWrapper: {
  },
  thanksText: {
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(20),
    textAlign: 'center'
  },
  roundButtonWrapper: {
    marginBottom: 150,
    width: '46%'

  }

})
