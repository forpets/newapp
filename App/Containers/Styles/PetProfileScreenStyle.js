import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

let {width, height} = Dimensions.get('window')

// FIXME: convert to percentage
const itemWidth = width * 0.40
const itemTextWidth = width * 0.25
const itemHeight = height * 0.25

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
  },
  headerTitle: {
    ...Fonts.style.header,
    color: Colors.rosyRed
  },
  menuCont: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0
  },
  row: {
    marginHorizontal: moderateScale(12),
    // marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  menuItem: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    elevation: 5,
    height: scale(180),
    width: scale(142),
    backgroundColor: 'white',
    borderRadius: 3,
    borderColor: '#ddd',
    paddingTop: 30,
    //paddingBottom: 12
  },
  menuItemText: {
   // marginTop: moderateScale(5),
    marginBottom: moderateScale(20),
    color: '#444444',
    fontSize: moderateScale(15),
    fontWeight: 'bold',
    fontFamily: 'Rubik-Regular',
    width: itemTextWidth,
    textAlign: 'center'
  },
  divider: {
    borderBottomWidth: 0.3,
    borderTopWidth: 0.3,
    borderColor: Colors.lightGray,
    width: scale(142),
    marginTop:scale(30),
    marginBottom: 2,
    paddingLeft: 0,
    paddingRight: 0
  },
  imageResizing: {
    resizeMode: 'contain',
    height: 60
  },
  updatedAt: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    marginLeft: 30,
    color: '#CCC'
  },
  petCarouselItemContainer:{
    marginHorizontal: 20,
    marginTop: 20
  }


})
