import {Fonts} from '../../Themes'
import {StyleSheet, Dimensions} from 'react-native'

const {width, height} = Dimensions.get('window')

let bottomButtonHeight = height * 0.1

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: '#ababab',
    marginBottom: 20
  },
  headerTitleStyle: {
    ...Fonts.style.header
  },
  subTitle: {
    fontSize: 17,
    color: '#ababab',
    textAlign: 'center'
  },
  // Bottom button
  bottomButton: {
    zIndex: 4,
    position: 'absolute',
    left: 0,
    bottom: 0,

    width: '100%',
    height: bottomButtonHeight,
    backgroundColor: '#6cd5c2',

    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomButtonInner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width * 0.9
  },
  bottomButtonInnerText: {
    color: '#444444',
    fontSize: 18,
    fontWeight: 'bold'
  },

  splashContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    backgroundColor: 'white',
    height: height - 56
  },
  listContainer: {
    paddingBottom: 60
  },
  image: {
    marginTop: 90,
    marginBottom: 50
  }
})

export default styles
