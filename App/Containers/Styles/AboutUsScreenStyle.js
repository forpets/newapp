import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: 'white'
  },
  title: {
    fontFamily: Fonts.type.rubikBold,
    fontSize: Fonts.size.h1 - 3,
    color: '#444444',
    marginTop: 10,
    marginHorizontal: 20
  },
  image: {
    marginTop: 18,
    marginBottom: 40,
    marginHorizontal: 20,
    width: '35%',
    height: '6%'
  },
  description: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 80,
    fontSize: 14,
    fontFamily: Fonts.type.base,
    lineHeight: 25,
    justifyContent: 'center',
    width: '90%'
  }
})
