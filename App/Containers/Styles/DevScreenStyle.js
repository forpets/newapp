import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  container: {
    // borderWidth: 1,
    flex: 1,
    height: '100%',
    width: '100%'
  }
})
