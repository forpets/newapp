import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: 'white'
  },
  placeClaimScreenWrapper: {
    flex: 1
  },
  headerTitle: {
    flex: 1,
    fontSize: Fonts.size.h1,
    fontFamily: Fonts.type.rubikBold,
    color: '#444444',
    marginTop: moderateScale(15),
    marginBottom: moderateScale(36),
    marginLeft: moderateScale(20)
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(20)
  },
  iconArrowWrapper: {

  },
  iconArrow: {

  },
  hedearFooter: {
    marginHorizontal: moderateScale(20),
    fontFamily: Fonts.type.base,
    textAlign: 'left',
    width: '90%',
    marginBottom: moderateScale(26),
    lineHeight: moderateScale(20)
  },
  subTitle: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.rubikBold,
    color: '#444444',
    marginLeft: moderateScale(20)
  },
  leftbuttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  leftbuttonWrapper: {
    width: '46%'
  }
})
