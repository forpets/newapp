import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
  },
  headerTitleStyle: {
    ...Fonts.style.header,
    color: '#FFD600'
  },
  placeholder: {
    color: 'black'
  },
  footerText: {
    marginLeft: 25,
    color: Colors.lightGray,
    fontFamily: Fonts.type.base,
    fontSize: 12
  },
  button: {

  },
  emptySpace:{
    height: 100,
    width: '100%'
  },
  inputsWrappers: {
    marginHorizontal: moderateScale(-5),
    marginVertical: moderateScale(-2)
  }
})
