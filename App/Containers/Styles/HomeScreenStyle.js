
import { StyleSheet, Dimensions } from 'react-native'
import {ApplicationStyles, Metrics, Fonts} from '../../Themes'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

const {height, width} = Dimensions.get('window')
const placesContainerHeight = height * 0.23
const itemContainerWidth = width * 0.90
const containerHeight = height * 0.25
const containerWidth = height * 0.68

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  placesContainer: {
    height: placesContainerHeight,
    backgroundColor: '#e4e4e4'
  },
  directoryContainer: {
    marginTop: moderateScale(20),
    marginHorizontal: moderateScale(8)
  },

  emptyPetContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 12,
    marginLeft: 20,
    backgroundColor: '#FFFFFF',
    borderColor: '#EAECEE',
    borderRadius: 8,
    elevation: 4,
    borderWidth: 1,
    marginTop: -5,
    width: itemContainerWidth,
    height: placesContainerHeight
  },
  emptyPetSubContainer: {
    marginLeft: 40,
    marginRight: 40,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  emptyPetImage: {
    resizeMode: 'contain',
    marginLeft: 30,
    width: 60
		// height:70,
		// marginTop:30,
  },
  emptyPetText: {
    fontSize: 16,
    fontFamily: Fonts.type.base,
    color: '#7D7D7D',
		// marginTop:30,
    textAlign: 'center'
  },
  emptyPetButton: {
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    height: 30,
    width: 100,
    backgroundColor: '#9ADF00'
  },
  emptyPetTextButton: {
    fontSize: 10,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  emptyBigContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 1,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: 10,
    marginTop: 10,
    elevation: 6,
    borderRadius: 6,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 15,
    height: containerHeight,
    width: containerWidth * 0.80,
    backgroundColor: 'white'

  },
  emptyNotesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: '#FFFFFF',
    paddingLeft: 15,
    borderRadius: 10
  },
  emptyNotesImageWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  emptyNotesImage: {
    width: (Metrics.screenWidth < 400) ? 50 : 70,
    height: (Metrics.screenWidth < 400) ? 50 : 70
  },
  emptyNotesText: {
    fontSize: moderateScale(16),
    fontFamily: Fonts.type.base,
    color: '#7D7D7D',
		// marginTop:30,
    textAlign: 'center'
  },
  emptyNotesButton: {
    marginTop: moderateScale(5),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    height: 30,
    width: 100,
    backgroundColor: '#9ADF00'
  },
  emptyNotesTextButton: {
    fontSize: moderateScale(12),
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  emptyNotesSubContainer: {
    flex: 4,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: moderateScale(40)
  },

  notesTitle: {
    textAlignVertical: 'center',
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(20),
    color: '#343434'
    // borderWidth: 1
  },
  notesRightTitle: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: 'Roboto-Regular',
    color: '#939393',
    fontSize: moderateScale(13)
  },

  notesContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: moderateScale(20),
   /*  marginTop: verticalScale(6) */
    marginTop: (Metrics.screenHeight < 800) ? 12 : 2
    // borderWidth: 1
  },

  notesListContainer: {
/*     marginHorizontal: moderateScale(26) */

  },
  ultmaNotaVerTodas: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  }
})

export default styles
