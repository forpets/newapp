import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'
import { moderateScale } from 'react-native-size-matters/lib/scalingUtils'
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  successContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
     marginVertical: moderateScale(40)
  },
  titleText: {
    marginTop: moderateScale(20),
    fontSize: moderateScale(24),
    fontFamily: Fonts.type.rubikMedium,
    color: '#343434'
  },
  descriptionText: {
    marginTop: moderateScale(20),
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    color: '#343434',
    fontSize: moderateScale(16),
    lineHeight: 24

  },
  roundButton: {
    marginVertical: moderateScale(10),
    width: '53%'
  },
  successImage: {
    height: 100,
    width: 100,
    marginVertical: moderateScale(40)

    // backgroundColor: '#FF1744'
  },
  backArrow: {
    // flex: 1,
    // flexDirection: 'column',
    marginVertical: 10,
    marginLeft: 20
  }
})
