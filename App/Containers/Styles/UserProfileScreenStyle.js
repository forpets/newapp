import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors } from '../../Themes/'

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 24) : ((Metrics.screenWidth <= 360) ? (fontSize = 26) : (fontSize = 26))

/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  name: {
    fontFamily: Fonts.type.bold,
    fontSize,
    borderBottomWidth: 1,
    borderColor: '#DCDCDC',
    width: '95%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft: 20,
    marginBottom: 50,
    paddingBottom: 15,
    color: Colors.loginGray
  },
  headerTitleStyle: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 20
  },
  iconHouse: {
    paddingRight: 30,
    marginLeft: 20,
    fontSize: 26,
    color: Colors.iconGreen
  },
  iconEmail: {
    paddingRight: 30,
    marginLeft: 22,
    fontSize: Metrics.icons.small,
    color: Colors.iconGreen
  },
  iconPhone: {
    paddingRight: 30,
    marginLeft: 20,
    fontSize: 28,
    color: Colors.iconGreen
  },
  item: {
    // marginHorizontal: Metrics.marginHorizontal,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    width: '80%',
    marginLeft: 'auto',
    marginRight: 'auto',
  /*   borderColor: 'red',
    borderWidth: 1 */
  },
  text: {
    textAlign: 'left',
    fontSize: (Metrics.screenWidth <= 320) ? (fontSize = 12) : ((Metrics.screenWidth <= 360) ? (fontSize = 14) : (fontSize = 16)),
    fontFamily: Fonts.type.base,
/*     borderColor: 'blue',
    borderWidth: 1, */
    width: '72%',
    color: '#343434'
  },
  avatar: {
    marginVertical: 40,
    marginLeft: 'auto',
    marginRight: 'auto'
  }
})
