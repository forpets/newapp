import { ApplicationStyles, Fonts, Colors } from '../../Themes/'
import {StyleSheet, Dimensions} from 'react-native'

const {width, height} = Dimensions.get('window')

let componentWidth = width * 0.9

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'white'
  },
  headerTitleStyle: {
    ...Fonts.style.header
  },

  innerContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',

    width: '100%',
    // width: componentWidth
  },
  title: {
    fontSize: 27,
    color: 'black',
    fontFamily: 'Rubik-Medium',
    marginVertical: 22,
    width: '100%'
  },
  description: {
    textAlignVertical: 'top',
    width: '100%',
    height: '100%',
    fontFamily: 'Rubik-Regular',
    fontSize: 20,
    color: 'black'
  },
  petSelector: {
    // borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginLeft: 5,
    marginTop: 30
  },
  petSelectorText: {
    // marginTop: 5,
    marginLeft: 15,
    fontSize: 17,
    fontFamily: Fonts.type.rubikMedium,
    color: '#6CD5C2'
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    height: height * 0.07,
    width: width * 0.3,
    // backgroundColor: '#6CD5C2',
    backgroundColor: 'red',
    marginTop: height * 0.05
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
    width: width * 0.3
  },
  modalItem: {},
  modalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 0,
    // backgroundColor: 'rgba(120,120,120,0.5)',
    width: width,
    height: height
  },
  modal: {
    height: height,
    width: width,
    marginTop: 0,
    backgroundColor: '#ffffff'
  },
  liTitle: {
    fontWeight: 'bold',
    fontFamily:Fonts.type.base,
    flexDirection: 'column',
    fontSize: 30,
    color: '#6CD5C2',
    marginTop: 5,
    marginBottom: 20,
    marginHorizontal: 15
    // backgroundColor: '#f2f2f2',
  },
  li: {
    flexDirection: 'column',
    alignItems: 'center'
    // backgroundColor: '#f2f2f2',
  },
  liRow: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomWidth: 0.6,
    borderColor: Colors.headerGrayLight,
    paddingBottom: 10,
    paddingTop: 10,
    marginHorizontal: 15
  },
  liImage: {
    height: 60,
    width: 60,
    borderRadius: 100
  },
  liText: {
    marginTop: 20,
    marginBottom: 20,
    fontWeight: 'bold',
    fontSize: 15,
    borderWidth: 0,
    marginTop: 5,
    marginLeft: 10,
    fontSize: 17,
    color: 'black'
  },
  rightMenuContainer: {
    position: 'absolute',
    top: 20,
    right: 10,
    width: 50,
    height: 50,
    alignItems: 'flex-end'
  },
  rightMenu: {
    marginTop: 0
  },
  arrowBack:{
    marginHorizontal:15, 
    marginVertical:20,
    fontSize: 20,
    color: 'black'
  }
})

export default styles
