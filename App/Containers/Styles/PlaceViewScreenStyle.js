import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let width:any
let height:any
width = (Metrics.screenWidth <= 320) ? (width = 32) : ((Metrics.screenWidth <= 360) ? (width = 44) : (width = 42))
height = (Metrics.screenWidth <= 320) ? (height = 20) : ((Metrics.screenWidth <= 360) ? (height = 24) : (height = 26))

/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    marginTop: -2
  },
  mainCotainer: {
   // flex: 1
    backgroundColor: 'white'
  },
  section: {
    width: '100%'
  // height: '100%'
  },
  section1: {

  },
  section2: {

  },
  section3: {
    marginBottom: moderateScale(50),
    flex: 1
  },
  sliderContainer: {
    height: 300
  },
  testSlider: {
    width: '100%'
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 38,
    marginBottom: 20
  },
  titleWrapper: {
    marginRight: 100
  },
  title: {
    marginHorizontal: 20,
    textAlign: 'left',
    fontFamily: Fonts.type.rubikBold,
    fontSize: moderateScale(30),
    color: '#8F95E7'
  },
  iconAnuncioWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'

  },
  iconAnuncio: {
    height: 20,
    width: 77
   // backgroundColor: '#8F95E7'
  },
  vetPetsContainer: {
    // borderWidth: 1,
    flex: 1,
    marginBottom: 12,
    marginLeft: 20
  },
  vetPetsContainerContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  veteplaceholderItem: {
    // flex: 3,
    // width: '20%'
    marginRight: moderateScale(10)
  },
  veteplaceholderText: {
    color: '#8F95E7',
    textAlign: 'center',
    borderRadius: 15,
    borderWidth: 1,
    paddingTop: 3,
    paddingBottom: 2,
    paddingHorizontal: 10,
    borderColor: '#8F95E7',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  },
  petShopItem: {
    flex: 3,
    paddingLeft: 20
  },
  emptyColumnItem: {
    flex: 6
  },
  petShopText: {
    color: '#F5A623',
    textAlign: 'center',
    borderRadius: 15,
    borderWidth: 1,
    paddingTop: 3,
    paddingBottom: 2,
    borderColor: '#F5A623',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  },
  footersvetPetsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: moderateScale(10)
  },
  starItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: 20
  },
  imagStarContainerItem: {
    flex: 9

  },
  imagRatingContainerItem: {
    flex: 65,
    alignSelf: 'center'
  },

  ratingNumberText: {
    flex: 1,
    color: '#525252',
    fontSize: moderateScale(12),
    fontFamily: Fonts.type.base
  /*   borderColor: 'red',
    borderWidth: 1 */
  },
  relojItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  imagRelojContainerItem: {
  //  flex: -520

  },
  imagReloj: {
    fontSize: moderateScale(13),
    marginHorizontal: moderateScale(5),
    color: '#7AC51E'
  },
  imagOpenContainerItem: {
    marginRight: 20
  },
  ratingOpenText: {
    color: '#7AC51E',
    fontSize: moderateScale(13),
    fontFamily: Fonts.type.base
  },
  currencyContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: 10

  },
  currencyWrapper: {
    width,
    height
  },
  currency: {
    width: '100%',
    height: '100%'
  },
  timeContainer: {
    marginVertical: 15,
    paddingBottom: 10
  },
  footersvetPetsText: {
    marginHorizontal: 20,
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16),
    color: '#525252'
  },
  dividerLine: {
    borderWidth: 0.5,
    borderColor: 'gray',
    width: '100%',
    marginVertical: 14
  },
  halfDivider: {
    marginHorizontal: moderateScale(102),
    borderWidth: 0.5,
    borderColor: 'gray',
    width: '85%',
    marginVertical: 14
  },
  addAndEditmainContainer: {
    // marginVertical: moderateScale(-20)
  },
  iconAndTextMainContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 36
  },
  iconContainer: {
    marginRight: 15,
    marginVertical: 8
  },
  iconTextContainer: {
    width: '85%'
  },
  emergencyImage: {
    width: moderateScale(25),
    height: moderateScale(28)
  },
  iconContacts: {
    fontSize: moderateScale(25),
    color: 'gray'
  },
  iconEdit: {
    width: moderateScale(23),
    height: moderateScale(23),
    marginLeft: moderateScale(2)
  },
  textContacts: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(15),
    color: 'gray',
    marginHorizontal: moderateScale(22),
    width: '80%'
  },
  iconTextContainerNumber: {
    flex: 1
  },
  iconTextContainerEmergency: {
    flex: 1,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'red',
    paddingLeft: -20,
    paddingRight: -20,
    marginHorizontal: 20,
    paddingTop: 5,
    paddingBottom: 5
  },
  textContactsEmergency: {
    color: 'white',
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    fontSize: 16
  },
  descriptionContainer: {
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(30)

  },
  titleDescription: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 24,
    color: '#343434'
  },
  descriptionText: {
    marginTop: moderateScale(9),
    fontFamily: Fonts.type.base,
    fontSize: 16,
    color: '#343434',
    lineHeight: 30,
    textAlign: 'left'
  },
  pictureScrollerContainer: {
    // borderWidth: 1,
    // height: 150,
    // width: 250,
    marginHorizontal: 20,
    marginVertical: 10

  },
  pictureScroller: {

  },
  comentariosContainer: {
    marginVertical: moderateScale(20),
    marginHorizontal: moderateScale(20)
  },
  comentariosTitle: {
    color: '#8F95E7',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(20)
  },
  starIcon: {
    color: '#7AC51E',
    fontSize: 17,
    textAlignVertical: 'top'
  },
  emergencyNoteContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: moderateScale(20)
  },
  emergencyNote: {
    fontFamily: Fonts.type.base,
    fontStyle: 'italic',
    textAlign: 'center',
    color: 'gray'
  },
  placeReviewsWrapper: {
    marginVertical: moderateScale(20)
  },

  topButtonsContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    position: 'absolute',
    top: 20,
    zIndex: 100
  },
  topIcon: {
    fontSize: 25,
    color: 'white',
    textShadowColor: 'black',
    textShadowRadius: 3,
    textShadowOffset: {width: 2, height: 2}
  },
  topLeftIcon: {
    paddingLeft: 20
  },
  topRightIcon: {
    paddingRight: 20
  },
  placeStatusWrapper: {

  }

})
