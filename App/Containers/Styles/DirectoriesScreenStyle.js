import { StyleSheet } from 'react-native'
import { Fonts, ApplicationStyles, Metrics, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  mapContainer: {
    // height: Metrics.screenHeight + 300
    flex: 1
    // flexDirection: 'column'
  },
  overlaySwitch: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    width: '100%',

    position: 'absolute',
    bottom: '5%',
    zIndex: 4
  },
  headerTitleStyle: {
    ...Fonts.style.header,
    backgroundColor: 'white'
  },
  viewContainer: {

    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    left: 0,
    // flex: 1,
    height: Metrics.screenHeight - 55,
    zIndex: 3
    // flexDirection: 'column'
  }
})
