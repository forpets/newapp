import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors} from '../../Themes/'
import colors from '../../Themes/Colors'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1
  },
  background: {
    backgroundColor: colors.loginGreen
  },
  logoForPets: {
    // borderWidth: 1,
    marginTop: moderateScale(40),
    marginBottom: moderateScale(10),
    marginLeft: moderateScale(20),
    width: '45%',
    height: '12%',
    resizeMode: 'contain'
  },
  headerStyle: {
    ...Fonts.style.loginH1,
    fontSize: moderateScale(32),
    marginHorizontal: moderateScale(20),
    marginTop: moderateScale(14),
    marginBottom: moderateScale(44),
    flex: 1,
    justifyContent: 'center',
    color: colors.snow
  },
  fbimage: {
    fontSize: moderateScale(15)
  },
  fbButtonStyle: {
    textAlign: 'center',
    fontFamily: Fonts.normal,
    fontSize: Fonts.size.medium,
    fontWeight: 'bold',
    color: colors.snow
  },
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingTop: moderateScale(12),
    paddingBottom: moderateScale(35),
    marginHorizontal: moderateScale(2)
  },
  text1: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(12),
    // width: '100%',
    color: colors.snow,
    fontWeight: 'bold',
    width: moderateScale(150)
  },
  text2: {
    fontSize: moderateScale(12),
    fontFamily: Fonts.type.base,
    // width: '100%',
    color: 'gray'
  },
  text3: {
    fontSize: moderateScale(12),
    fontFamily: Fonts.type.base,
    paddingTop: moderateScale(10),
    width: '100%',
    color: colors.snow,
    fontWeight: 'bold'

  },
  terminos: {
    // fontFamily: Fonts.type.base,
    paddingTop: 15,
    borderTopWidth: 1,
    borderTopColor: '#EEE',
    marginBottom: 70,
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto'

  },
  text4: {
    fontFamily: Fonts.type.base,
    width: '75%',
    color: colors.snow,
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  space: {
    height: moderateScale(200)
  }
})
