import { StyleSheet, Dimensions } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import {ApplicationStyles, Metrics} from '../../Themes'
// TODO: add metrics
const {height, width} = Dimensions.get('window')

const componentWidth = width * 0.85
// const avatarWH = width * 0.35
const componentLeftPadding = width * 0.07

/** ************ Media Queries Block Starts ***************/
/* sony_xperia_z2 , Samsung S8 =>> 360  width
  nexus S => 320 width
  nexus 5X and pixel 2 Pixel_XL  ===>>  411.42 width
*/

let fontSize:any
fontSize = (Metrics.screenWidth <= 320) ? (fontSize = 18) : ((Metrics.screenWidth <= 360) ? (fontSize = 22) : (fontSize = 24))
/** ************ Media Queries Block Ends ***************/

export default StyleSheet.create({
  container: {
    height: height,
    backgroundColor: '#444444'
  },
  subContainer: {
    // borderWidth: 1,
    height: height,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  menuItemsContainer: {
    paddingLeft: moderateScale(20)

    // paddingTop: 15,
    // borderWidth: 1
  },
  paddingContainer: {
    paddingBottom: verticalScale(20)
  },
  divider: {
    alignSelf: 'stretch',
    borderBottomWidth: 2,
    borderColor: 'gray',
    marginTop: verticalScale(5),
    marginBottom: verticalScale(17)
  },
  menu: {
    paddingLeft: componentLeftPadding,
    paddingTop: 5
  },
  menuItem: {
    flexDirection: 'row',
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10)
  },
  menuItemTextDisabled: {
    color: 'gray',
    marginLeft: moderateScale(-45),
    fontSize: moderateScale(17)
  },
  menuItemText: {
    color: '#ffffff',
    marginLeft: moderateScale(-45),
    fontSize: moderateScale(17)
  },
  iconContainer: {
    width: moderateScale(85)
  },
  userName: {
    color: '#ffffff',
    fontSize: moderateScale(20)
  },
  avatar: {
    flex: 1,
    // height: avatarWH,
    // width: avatarWH,
    // borderRadius: avatarWH,
    marginBottom: moderateScale(20),
    marginTop: moderateScale(40)
    // borderWidth: 1
  },
  userInfo: {
    marginBottom: moderateScale(20)
  },
  menuContainer: {
    backgroundColor: '#444444'
  },
  displayName: {
    fontSize,
    color: '#ccc',
    marginLeft: moderateScale(20),
    marginRight: moderateScale(20),
    marginBottom: moderateScale(5)
  },
  iconSize: {
    resizeMode: 'contain',
    width: moderateScale(20)
  }
})
