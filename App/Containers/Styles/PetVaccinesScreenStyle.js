import {Fonts} from '../../Themes'
import { StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')
const butonWidth = width * 0.35
const butonHeigth = width * 0.15
const titleWidth = width * 0.65

// FIXME: hacer more responsive

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subcontainer: {
    flex: 1,
    // borderWidth: 1,
    // marginTop: '3%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    // height: '100%',

  },
  headerTitleStyle: {
    ...Fonts.style.header,
    color: '#00B0FF',
    // borderWidth: 1,
  },
  title: {
    fontFamily: 'Rubik-Medium',
    // borderWidth: 1,
    color: 'black',
    fontSize: 20,
    marginTop: '10%',
    textAlign: 'center'
  },
  subtitle: {
    // borderWidth: 1,
    color: '#656565',
    marginTop: '5%',
    textAlign: 'center',
  },
  startButton: {
    flexDirection: 'row',
    marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    borderWidth: 1,
    width: 100,
    height: 100
  }
})

export default styles
