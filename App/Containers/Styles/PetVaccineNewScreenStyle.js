import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
    padding: 20,
    height: 800
  },
  mainContainer: {

  },
  header: {
    flex: 1,
    justifyContent: 'flex-start'
/*     borderColor: 'red',
    borderWidth: 1 */
  },
  wrapperForInputs: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: -10,
    marginLeft: -24

  },
  startButton: {
    width: '100%',
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // borderColor: 'red',
    // borderWidth: 1
  },
  startButtonWrap: {
    width: '50%',
    // borderWidth: 1
  },
  YesOrNoRadioButton: {
  /*  flex:1 */
  },
  inputContainers: {
    marginLeft: -24,
    marginTop: 30,
    marginBottom: 30,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerTitleStyle: {
    ...Fonts.style.header,
    color: '#00B0FF'
  },
  vaccineSelector: {
    marginVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: Colors.headerGrayLight,
    fontFamily: 'Rubik-Regular',
    color: '#363636',
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 20
  },
  heading: {
    marginTop: 20,
    marginBottom: 12,
    fontFamily: 'Rubik-Medium',
    fontSize: 24,
    marginVertical: 10,
    color: '#363636'
  },
  textWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  text: {
    color: '#363636',
    fontSize: 20,
    lineHeight: 33
  },
  applicationDate: {
    paddingTop: 10
  },
  date1InputWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  /*   marginVertical: 20, */
 /*    paddingTop: 5,
    paddingBottom: 20 */
  },
  userRadioButtonWrapper: {
    flex: 1
   /*  borderColor: 'orange',
    borderWidth: 1 */
  /*   flex: 1, */
 /*    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center', */
/*     width: '100%',
    height: '100%' */
   /*  marginTop: 60 */
  },
  nextApplicationDate: {

  },
  yesInput: {
    flex: 1,
    width: '100%'
  /*   borderColor: 'blue',
    borderWidth: 1 */
    // flex:1,
/*     borderColor: 'red',
    borderWidth: 1 */
  }

})
