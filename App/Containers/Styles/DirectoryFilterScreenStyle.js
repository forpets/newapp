import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: 'white'
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: moderateScale(20)
  },
  iconArrowWrapper: {
    marginHorizontal: moderateScale(20)
  },
  headerRightButtonWrapper: {
    marginHorizontal: moderateScale(20)
  },
  headerRightButton: {
    fontFamily: Fonts.type.rubikMedium,
    color: '#e4348c',
    fontSize: moderateScale(14)
  },
  headerTitleStyle: {
    ...Fonts.style.header
  },
  mainWrapperTitle: {
    padding: moderateScale(20)
  },
  headerTitle: {
    fontFamily: Fonts.type.rubikBold,
    fontSize: moderateScale(30),
    color: '#343434'
  },
  mainWrapperSubTitle: {
    padding: moderateScale(20),
    marginBottom: moderateScale(-5)
  },
  subTitle: {
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(20),
    color: '#343434'

  },


  orderByWrapperWithoutRightWidth: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: moderateScale(26),
    paddingBottom: moderateScale(26)
  },

  

  radioBusquedaContainer: {
    padding: moderateScale(20)
  },
  radioBusquedaTitleWrapper: {
    marginTop: moderateScale(15),
    marginBottom: moderateScale(10)
  },
  sliderContainer: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  trackStyle: {
    height: moderateScale(8)
  },
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  footerItem: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(15)
  },
  footerItemValue: {
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(15),
    color: '#9B9B9B'
  },
  checkbox: {
    flex: 1,
    padding: 10,
    width: 170
  },
  checkBoxesWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'

  },
  radioCategoriaContainer: {

  },
  radioCategoriaWrapperTitles: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: moderateScale(20)
  },
  rightTitle: {
    fontFamily: Fonts.type.bold,
    fontSize: moderateScale(14),
    color: '#FD5381'
  },
  checkBoxesContainer: {
    marginHorizontal: moderateScale(14)
  /*   flex: 1,
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center' */
  },
  emergencySwitcherContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: moderateScale(20)
  },
  emergencyTextItem: {
    marginHorizontal: 20,
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(20)

  },
  switchItemWrapper: {
    marginHorizontal: moderateScale(20)
  },
  thumbStyle: {
    flex: 1,
    height: 30,
    width: 30,
    backgroundColor: 'transparent'
  },
  checkBoxClicked: {
    fontFamily: Fonts.type.rubikBold,
    fontSize: moderateScale(14),
    // borderWidth: 1,
  },
  checkboxText: {
    // borderWidth: 1,
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  }
})
