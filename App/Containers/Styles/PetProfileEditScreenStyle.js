import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerTitleStyle: {
    ...Fonts.style.header,
    color: '#FFD600'
  },
  placeholder: {
    color: 'black'
  },
  footerText: {
    marginLeft: 25,
    color: Colors.lightGray,
    fontFamily: Fonts.type.base,
    fontSize: 12
  },
  button: {

  },
  emptySpace:{
    height: 100,
    width: '100%'
  }
})
