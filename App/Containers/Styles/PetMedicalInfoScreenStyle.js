import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1
  },
  headerTitle: {
    color: '#00C853'
  },
  footInput:{
    color: 'gray',
    marginVertical: -7, // MD has 16
    marginHorizontal: Metrics.section,
    fontFamily: Fonts.type.base,
    fontSize: 12,
    height: 50
  },
  emptySpace:{
    height: 100,
    width: '100%'
  },
  inputPesoContainer:{
    flex:1, 
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputPeso:{
    flex:7,
    width: '100%',

  },
  textKg:{
    flex:1,
    zIndex: 999,
    right: Metrics.section,
    position: 'absolute' 
  }
})
