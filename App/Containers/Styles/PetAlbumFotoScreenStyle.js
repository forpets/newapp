import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
const {width, height} = Dimensions.get('window');
const containerHeight = width*.25;
const containerWidth = width*.25;

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container:{

  },
  photoAlbum: {
//    width: containerWidth,
//    height: containerHeight,
    width: width,
    height:height,

  },
})
