import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  name: {
    ...Fonts.style.h2,
    borderBottomWidth: 1,
    paddingLeft: 10,
    marginBottom: 20
  },
  input: {

  },

  headerTitleStyle: {
    ...Fonts.style.header
  }

})
