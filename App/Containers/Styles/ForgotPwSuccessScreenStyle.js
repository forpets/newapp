import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  background: {
    backgroundColor: Colors.loginGreen
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '90%',
    marginRight: 'auto',
    marginLeft: 'auto',
    marginTop: 20,
  },
  text1: {
    // borderWidth: 1,
    color: Colors.snow,
    fontFamily: Fonts.type.rubikMedium,
    fontSize: 32,
    marginVertical: 50
  },
  text2: {
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    color: Colors.snow,
    fontSize: 18
  },
  text3: {
    marginTop: 20,
    fontFamily: Fonts.type.base,
    width: '100%',
    color: Colors.snow,
    fontWeight: 'bold',
    marginBottom: '40%'
  }
})
