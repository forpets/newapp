import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  background: {
    backgroundColor: Colors.loginGreen
  },
  header: {
    flex: 1,
    justifyContent: 'flex-start',
    width: '75%',
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 50
  },
  text1: {
    ...Fonts.style.loginH1,
    color: Colors.snow,
    fontFamily: Fonts.type.bold,
    marginTop: 50,
    marginBottom: 10
  },
  text2: {
    fontFamily: Fonts.type.base,
    color: Colors.snow,
    fontSize: 18
  },
  text3: {
    fontFamily: Fonts.type.base,
    color: Colors.snow,
    fontSize: 18,
    marginBottom: 20
  },
  text4: {
    fontFamily: Fonts.type.base,
    fontSize: 18,
    color: Colors.snow
  },
  inputAndButton: {
    flex: 1,
    justifyContent: 'flex-start'

  }
})
