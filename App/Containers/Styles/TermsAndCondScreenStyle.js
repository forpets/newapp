import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: 'white'
  },
  headerTitle: {
    flex: 1,
    fontSize: Fonts.size.h1,
    fontFamily: Fonts.type.rubikBold,
    color: '#444444',
    marginTop: 15,
    marginBottom: 80,
    marginLeft: 20
  },
  subTitle: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.rubikBold,
    color: '#444444',
    marginLeft: moderateScale(20)
  },
  description: {
    marginHorizontal: 20,
    fontFamily: Fonts.type.base,
    textAlign: 'left',
    width: '90%',
    lineHeight: 25
  }

})
