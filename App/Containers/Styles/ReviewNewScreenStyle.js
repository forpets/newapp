import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts } from '../../Themes/'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: 'white'
  },
  mainContainer: {
    flex: 1
  /*   justifyContent: 'center',
    alignItems: 'center' */
  },
  titleAndSubtitleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingBottom: moderateScale(20)
  },
  titleWrapper: {
    marginVertical: moderateScale(20),
    marginHorizontal: moderateScale(16)
  },
  title: {
    color: '#343434',
    fontFamily: Fonts.type.rubikBold,
    fontSize: moderateScale(26)

  },
  subTitleWrapper: {
    marginVertical: moderateScale(10),
    marginHorizontal: moderateScale(16)
  },
  subTitle: {
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(16)
  },

/*   PRICE STYLING SECTION  */
  priceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  pricingIconsContainer: {
    marginRight: 3
  },
  priceTextWrapper: {
    flex: 1,
    marginHorizontal: moderateScale(16)
  },
  subTitlePrice: {
    color: '#FD5381',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16)
  },

  questionWrapper: {
    marginVertical: moderateScale(20),
    marginHorizontal: moderateScale(16)
  },
  question: {
    color: '#ABABAB',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(14)
  },

/*  SERVICES (ATENCIÓN)   STYLING section */

  serviceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: moderateScale(15),
    marginBottom: moderateScale(-3)
  },
  serviceTextWrapper: {
    flex: 1,
    marginHorizontal: moderateScale(16)
  },
  subTitleServices: {
    color: '#FD5381',
    fontFamily: Fonts.type.base,
    fontSize: moderateScale(16)
  },

  starWrapper1: {

  },
  starWrapper2: {

  },
  starWrapper3: {

  },
  starWrapper4: {

  },
  starWrapper5: {

  },

/*  COMMENTS STYLING section */

  commentsContainer: {

  },
  subTitleCommentsWrapper: {
    marginHorizontal: moderateScale(16)
  },
  subCommentsTitle: {
    color: '#343434',
    fontFamily: Fonts.type.rubikMedium,
    fontSize: moderateScale(16)
  },
  input: {
    marginHorizontal: moderateScale(-8)

  },
/* FOOTER STYLING section */
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  cancelarButtonContainer: {
    width: '45%'
  },
  enviarButtonContainer: {
    width: '40%'
  }

})
