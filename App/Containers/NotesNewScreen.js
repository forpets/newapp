import React, { Component } from 'react'
import { ScrollView,
  View,
  Text,
  TextInput,
  Image,
  Modal,
  FlatList,
  Alert,
  TouchableOpacity, InteractionManager} from 'react-native'
import { connect } from 'react-redux'
import {Images} from '../Themes'
import styles from './Styles/NotesNewScreenStyle'
import { getPetsArray } from '../Redux/PetsRedux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {getNote} from '../Redux/NotesRedux'
import NoteSaveRedux from '../Redux/NoteSaveRedux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import NoteDeleteRedux from '../Redux/NoteDeleteRedux'

class NotesNewScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {onPressBack, headerRight, headerTitle} = params

    // FIXME:
    // If new note - save headerRight
    // If old note (has id) - save on go back (show popup??), delete on right side

    return {
      title: headerTitle,
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={onPressBack}
        style={{paddingLeft: 20}}
    />,
      headerRight: headerRight
    }
  };

  constructor (props) {
    super(props)
    const {note = {}, pets} = props

    const currentPet = note ? pets.find((pet) => pet.id === note.petId) : {notes: []}

    this.state = {
      titleTouched: false,
      touched: false,
      validTitle: false,

      id: (note && note.id) || '',
      description: (note && note.description) || '',
      title: (note && note.title) || '',
      currentPet,
      showModal: false,
      petList: [],
      editing: false
    }
  }

  componentDidMount () {
    console.tron.log({props: this.props})

    InteractionManager.runAfterInteractions(() => {
      this.props.navigation.setParams({
        headerRight: this.headerRight(),
        headerTitle: this.headerTitle(),
        onPressBack: this.headerOnPressBack
      })
    })
  }

  headerTitle = () => {
    const {note} = this.props
    return (!note) ? 'Nueva nota' : 'Notas'
  }

  headerOnPressBack = () => {
    const {note, navigation} = this.props
    if (note && this.state.touched) {
      Alert.alert('', '¿Deseas guardar esta nota?',
        [
            {text: 'NO', onPress: () => navigation.goBack()},
            {text: 'GUARDAR', onPress: () => this.saveNote()}
        ],
          { cancelable: true }
        )
    } else {
      navigation.goBack()
    }
  }

  headerRight = () => {
    const {note} = this.props

    // If editing current note
    if (note) {
      return <IconMD
        name='delete'
        size={25}
        onPress={this.deleteNote}
        style={{paddingRight: 20}}
      />
    } else {
      return <IconMD
        name='check'
        size={25}
        onPress={this.saveNote}
        style={{paddingRight: 20}}
      />
    }
  }

  _handleTitle = (title) => {
    let validTitle = !!title.length
    this.setState({titleTouched: true, touched: true, validTitle, title})
  }

  _handleDescription = description => {
    this.setState({touched: true, description})
  }

  deleteNote = () => {
    const {note, deleteNote} = this.props
    if (note) {
      Alert.alert('', '¿Deseas eliminar esta nota?',
        [
          {text: 'CANCELAR', onPress: () => {}},
          {text: 'ELIMINAR', onPress: () => deleteNote(note)}
        ],
        { cancelable: false }
      )
    }
  }

  saveNote = () => {
    let {title, description, currentPet, id} = this.state

    if (!(!currentPet.id && !title.length && !description.length)) {
      if (!currentPet.id) {
        Alert.alert('', 'Seleccione una mascota',
          [
          {text: 'OK', onPress: () => {}}
          // {text: 'ELIMINAR', onPress: () => deleteNote(note)}
          ],
        { cancelable: true }
      )
        return
      }

      if (!title.length) {
        Alert.alert('', 'Ingrese un título',
          [
          {text: 'OK', onPress: () => {}}
          // {text: 'ELIMINAR', onPress: () => deleteNote(note)}
          ],
        { cancelable: true }
      )
        return
      }
      let createdAt = Date()

      if (id) {
        this.props.updateNote(id, currentPet.id, {title, description, createdAt}, this.props.note)
      } else {
        this.props.createNote(currentPet.id, {title, description, createdAt})
      }
    }
  }

  showContent = () => {
    let {titleTouched, validTitle, currentPet} = this.state
    let selectorText = 'Etiquetar mascota'

    if (currentPet.name) {
      selectorText = currentPet.name
    }

    return (
      <View style={styles.innerContainer}>

        <TouchableOpacity onPress={this.toggleModal}>
          <View style={styles.petSelector}>
            <Image source={Images.notes.step} />
            <Text style={styles.petSelectorText}>{selectorText}</Text>
          </View>
        </TouchableOpacity>

        <TextInput
          style={styles.title}
          placeholder='Título'
          underlineColorAndroid='transparent'
          placeholderTextColor='#444444'
          value={this.state.title}
          onChangeText={this._handleTitle}
           />

        { titleTouched &&
            !validTitle &&
            <Text style={styles.error}>Ingrese un titulo para la nota</Text> }

        <TextInput
          multiline
          style={styles.description}
          placeholder='Nota'
          underlineColorAndroid='transparent'
          placeholderTextColor='#444444'
          value={this.state.description}
          onChangeText={this._handleDescription}
        />

      </View>
    )
  }

  toggleModal = () => { this.setState({showModal: !this.state.showModal}) }

  printImage = (image) => {
    if (image) {
      return <Image source={{uri: image}} style={styles.liImage} />
    }
    return <Image source={Images.petsDefaultImage} style={styles.liImage} />
  }

  showModal = () => {
    let petList = this.props.pets
    let key = 0

    return (

      <Modal
        transparent={false}
        presentationStyle='fullScreen'
        visible={this.state.showModal}
        onRequestClose={() => { this.setState({ showModal: false }) }}
          >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>

            <ScrollView>
              <View style={styles.backArrow}>
                <TouchableOpacity onPress={() => this.setState({showModal: false})}>
                  <IconMD name='close' style={styles.arrowBack} size={30} />
                </TouchableOpacity>
              </View>
              <Text style={styles.liTitle}>Asignar nota a ...</Text>
              <FlatList
                showsHorizontalScrollIndicator={false}
                removeClippedSubviews
                data={petList}
                extraData={this.props}
                renderItem={({item}) => <View style={styles.modalItem}>
                  <TouchableOpacity onPress={() => {
                    let currentPet = petList.filter(pet => pet.id === item.id)[0]
                    this.setState({showModal: false, touched: true, currentPet})
                  }}>
                    <View style={styles.liRow}>
                      {this.printImage(item.image)}
                      <Text style={styles.liText}>{item.name}</Text>
                    </View>
                  </TouchableOpacity>
                </View>}
                keyExtractor={() => key++}
                />
            </ScrollView>

          </View>
        </View>
      </Modal>)
  }

  render () {
    return (
      <View style={styles.container}>
        {this.showContent()}
        {this.showModal()}
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params = {}} = props.navigation.state
  // console.tron.log({recei: 'rec', params})

  return {
    pets: getPetsArray(state),
    note: getNote(state, params.id)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createNote: (...args) => { dispatch(NoteSaveRedux.noteCreateRequest(...args)) },
    updateNote: (...args) => { dispatch(NoteSaveRedux.noteUpdateRequest(...args)) },
    deleteNote: (...args) => { dispatch(NoteDeleteRedux.noteDeleteRequest(...args)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesNewScreen)
