import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import Categories from '../Services/Categories'
import R from 'ramda'
import CheckBox from 'react-native-check-box'
import BasicSwitch from '../Components/BasicSwitch'
import UserPickerInput from '../Components/UserPickerInput'
import UserRadioButton from '../Components/UserRadioButton'
import validate from '../Lib/Validate'
import { Images } from '../Themes'
import Slider from 'react-native-slider'
import styles from './Styles/DirectoryFilterScreenStyle'
import FilteredLostFoundRedux, { getLostFoundFilter } from '../Redux/FilteredLostFoundRedux'
import AppConfig from '../Config/AppConfig'
import PetAttributes from '../Services/PetAttributes'

class LostFoundFilterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  });

  constructor (props) {
    super(props)

    const lostFoundFilter = this.props.lostFoundFilter || AppConfig.defaultLostFoundFilter
    console.debug(lostFoundFilter)

    this.state = {
      searchRadius: lostFoundFilter.searchRadius,
      kind: lostFoundFilter.kind,
      breed: lostFoundFilter.breed,
      gender: lostFoundFilter.gender,
      breedsForSelect: lostFoundFilter.breedsForSelect || PetAttributes.findBreedsFor(lostFoundFilter.kind),
      types: {
        lost: { selected: lostFoundFilter.types.lost.selected, label: 'Perdido' },
        found: { selected: lostFoundFilter.types.found.selected, label: 'Encontrado' }
      },

      submitDisabled: true,
      fieldFocused: '',
      errors: {},
      touched: []
    }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    // Handle pet type change
    if (fieldName === 'kind') this.handlePetKindChange(fieldValue)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
  // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, kind, breed} = this.state
    let validationErrors = {}

    // Validated fields
    validationErrors.kind = validate('petKind', kind)
    validationErrors.breed = validate('petBreed', breed)

    if (isSubmit) {
      this.setState({touched: ['kind', 'breed'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }

  handleSearchRadius = (searchRadius:number) => {
    // const searchRadius = Math.round(input)
    this.setState({searchRadius})
  }

  handlePetKindChange = (itemValue) => {
    if (itemValue !== this.state.kind) {
      const breedsForSelect = PetAttributes.findBreedsFor(itemValue)
      this.setField('breed', null)
      this.setState({breedsForSelect})
    }
  }

  submit = () => {
    const filter = R.pickAll(['searchRadius', 'types', 'kind', 'breed', 'gender'], this.state)
    this.props.filteredLostFoundApply(filter)
    this.props.navigation.goBack()
  }

  petKinds = PetAttributes.findAllKinds()
  petGenders = PetAttributes.findAllGenders()

  render () {
    const {navigation} = this.props
    const thisComponent = this

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View style={styles.header}>
            <View style={styles.iconArrowWrapper}>
              <IconMD name='arrow-back' size={25} onPress={() => navigation.goBack()} style={styles.iconArrow} />
            </View>
            <TouchableOpacity onPress={this.submit}>
              <View style={styles.headerRightButtonWrapper}>
                <Text style={styles.headerRightButton}>OK</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.mainWrapperTitle}>
            <Text style={styles.headerTitle}>Filtrá tu búsqueda dentro de la lista</Text>
          </View>

          <View>
            <UserPickerInput
              placeholder='Especie'
              fieldName='kind'
              parentComp={thisComponent}
              selectedValue={this.state.kind}
              optionsForSelect={this.petKinds}
            />
          </View>

          <View>
            <UserPickerInput
              placeholder='Raza'
              fieldName='breed'
              parentComp={thisComponent}
              selectedValue={this.state.breed}
              optionsForSelect={this.state.breedsForSelect}
            />
          </View>

          <View>
            <UserRadioButton
              label='Sexo'
              fieldName='gender'
              parentComp={thisComponent}
              defaultValue={this.state.gender}
              options={this.petGenders}
              buttonInnerColor='#FFD600'
            />
          </View>

          {/* *********  *RADIO DE BUSQUEDA* START ************ */}
          <View style={styles.radioBusquedaContainer}>
            <View style={styles.radioBusquedaTitleWrapper}>
              <Text style={styles.subTitle}> Radio de búsqueda </Text>
            </View>
            <View style={styles.sliderContainer}>
              <Slider
                step={1}
                value={this.state.searchRadius}
                onValueChange={this.handleSearchRadius}
                thumbTintColor={'white'}
                minimumTrackTintColor={'#8FE824'}
                trackStyle={styles.trackStyle}
                minimumValue={1}
                maximumValue={20}
                thumbStyle={styles.thumbStyle}
                thumbImage={Images.directorio.locationGuideBlack}
              />
              <View style={styles.footerContainer}>
                <View><Text style={styles.footerItem}>0 Km</Text></View>
                <View><Text style={styles.footerItemValue}>{this.state.searchRadius} Km</Text></View>
                <View><Text style={styles.footerItem}>20 km</Text></View>
              </View>
            </View>
          </View>
          {/* *********  *RADIO DE BUSQUEDA* END ************ */}

          {/* *********  *Types* START ************ */}
          <View style={styles.radioCategoriaContainer}>
            <View style={styles.radioCategoriaWrapperTitles}>
              <View>
                <Text style={styles.subTitle}>Estado</Text>
              </View>
            </View>
            <View style={styles.checkBoxesContainer}>
              <View style={styles.checkBoxesWrapper}>
                <View style={styles.checkBoxItem}>
                  <CheckBox
                    style={styles.checkbox}
                    onClick={() => this.state.types.found.selected = !this.state.types.found.selected}
                    isChecked={this.state.types.found.selected}
                    rightText={this.state.types.found.label}
                    rightTextStyle={this.state.types.found.selected ? styles.checkBoxClicked : styles.checkboxText}
                    checkBoxColor={this.state.types.found.selected ? '#FF1744' : 'black'}
                  />
                </View>
                <View style={styles.checkBoxItem}>
                  <CheckBox
                    style={styles.checkbox}
                    onClick={() => this.state.types.lost.selected = !this.state.types.lost.selected}
                    isChecked={this.state.types.lost.selected}
                    rightText={this.state.types.lost.label}
                    rightTextStyle={this.state.types.lost.selected ? styles.checkBoxClicked : styles.checkboxText}
                    checkBoxColor={this.state.types.lost.selected ? '#FF1744' : 'black'}
                  />
                </View>
              </View>
            </View>
          </View>
          {/* *********  *Types* END ************ */}
          
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    lostFoundFilter: getLostFoundFilter(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    filteredLostFoundApply: (...args) => dispatch(FilteredLostFoundRedux.filteredLostFoundApply(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LostFoundFilterScreen)
