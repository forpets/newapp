import React, { Component } from 'react'
import { ScrollView, Text, View, Image } from 'react-native'
import { connect } from 'react-redux'
import {Images} from '../Themes'
import RoundButton from '../Components/RoundButton'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/ReviewSentScreenStyle'
import NavHelper from '../Lib/NavHelper'

class ReviewSentScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  });

  goBack = () => {
    this.props.navigation.goBack()
  }

  pressContinue = () => {
    // resets to directory list
    this.props.pressContinue()
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.mainContainer}>
          <View style={styles.titleWrapper}>
            <IconMD name='arrow-back' size={25} onPress={this.goBack} style={styles.iconArrow} />
          </View>

          <View style={styles.imageWrapper}>
            <Image style={styles.iconSuccess} source={Images.directorio.thanksForTheComments} />
          </View>

          <View style={styles.textWrapper}>
            <Text style={styles.thanksText}>¡Gracias por tu valoración y comentario!</Text>
          </View>

          <View style={styles.roundButtonWrapper}>
            <RoundButton
              bgColor='#FF1744'
              color='white'
              text='CONTINUAR'
              onPress={this.pressContinue}
            />
          </View>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pressContinue: () => dispatch(NavHelper.reset('DirectoriesScreen', {showList: true}))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewSentScreen)
