import React, { Component } from 'react'
import { ScrollView, View, InteractionManager, Alert } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/DirectoriesScreenStyle'
import DirectoriesMap from '../Components/DirectoriesMap'
import DirectoriesList from './DirectoriesList'
import NavBars from '../Components/NavBars'
import DirectorySearchBar from '../Components/DirectorySearchBar'
import MapListSwitch from '../Components/MapListSwitch'
import LocationRedux, {
  getCurrentLocation,
  getShouldShowLocationAccessDeniedAlert
} from '../Redux/LocationRedux'
import { getFilteredPlaces, getFilteredPlacesFetching } from '../Redux/FilteredPlacesRedux'
import PlaceSearchRedux,
{ getSearchFinished, getCurrentSearchTerm, getSearchHistory, getShowSearchBar, getSearchedPlaces, getPlaceSearchFetching } from '../Redux/PlaceSearchRedux'
import R from 'ramda'

class DirectoriesScreen extends Component {
  static navigationOptions = ({navigation, ...others}) => {
    const {dispatch} = navigation
    const {params = {}} = navigation.state
    const {searchActive, currentSearchTerm} = params

    const title = currentSearchTerm || 'Directorio'

    return {
      title,
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <NavBars onPress={() => navigation.navigate('DrawerOpen')} />,
      headerRight:
  <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
    {!(searchActive) && // Show - search icon
      <Icon
        name='search'
        onPress={() => dispatch(PlaceSearchRedux.placeSearchBarShow())}
        style={{paddingRight: 15, fontSize: 20}}
                    />
      }
    {(searchActive) && // Show - X - cancel search
      <IconMD
        name='close'
        onPress={() => dispatch(PlaceSearchRedux.placeSearchClear())}
        style={{paddingRight: 15, fontSize: 25}}
                    />
      }
    <Icon
      name='filter' onPress={() => navigation.navigate('DirectoryFilterScreen')}
      style={{paddingRight: 20, fontSize: 20}}
                  />
  </View>
    }
  }

  constructor (props) {
    super(props)
    this.state = { showMap: true }
    const {params = {}} = props.navigation.state
    if (params.showList) this.state.showMap = false
  }

  componentDidMount () {
    const {setHeaderParams} = this
    InteractionManager.runAfterInteractions(() => { setHeaderParams(this.props) })
  }

  // Updates header params
  setHeaderParams = (propsSource) => {
    const {searchActive, currentSearchTerm} = propsSource
    this.props.navigation.setParams({ searchActive, currentSearchTerm })
  }

  shouldComponentUpdate (nextProps, nextState) {
    // If location changed - update distance to place
    const oldLoc = this.props.currentLocation
    const newLoc = this.props.newLocation
    if (!R.equals(oldLoc, newLoc)) { return true }
    // If places changed - update everything
    // if (!R.equals(this.props.places, nextProps.places)) { return true } // Too heavy on CPU
    if (this.props.places !== nextProps.places) return true
    // if (this.props.searchActive) return true
    // return true
  }

  componentWillReceiveProps (newProps : Props) {
    const {shouldShowLocationAccessDeniedAlert, locationAccessDeniedAlertDisplayed} = newProps
    if (shouldShowLocationAccessDeniedAlert) {
      Alert.alert('Atención', 'No se podrán mostrar los espacios cercanos a su ubicación')
      locationAccessDeniedAlertDisplayed()
    }

    // Update header only if props changed in order to avoid render loop
    const newP = R.pickAll(['searchActive', 'currentSearchTerm'], newProps)
    const oldP = R.pickAll(['searchActive', 'currentSearchTerm'], this.props)
    if (newP.searchActive !== oldP.searchActive || newP.currentSearchTerm !== oldP.currentSearchTerm) {
      this.setHeaderParams(newProps)
    }
  }

  render () {
    const {showMap} = this.state
    const {places, currentLocation, isFetching} = this.props

    // console.tron.log(this.state)
    // console.tron.log({places})
    const zStyleMap = {zIndex: showMap ? 3 : 1}
    const zStyleList = {zIndex: !showMap ? 3 : 1}

    return (
      <View style={styles.mapContainer}>

        <DirectorySearchBar
          show={this.props.showSearchBar}
          hideModalFunc={this.props.placeSearchBarHide}
          searchHistory={this.props.searchHistory}
          startSearchFunc={this.props.placeSearchRequest}
        />

        <View style={styles.overlaySwitch}>
          <MapListSwitch onSwitchMap={(showMap) => this.setState({showMap})} defaultValue={showMap} />
        </View>

        <View style={[styles.viewContainer, zStyleList]}>
          <ScrollView style={styles.mapContainer}>
            <DirectoriesList
              navigation={this.props.navigation}
              data={places}
              currentLocation={currentLocation}
              // isFetching={isFetching}
            />
          </ScrollView>
        </View>

        <View style={[styles.viewContainer, zStyleMap]}>
          <ScrollView style={styles.mapContainer}>
            <DirectoriesMap
              navigation={this.props.navigation}
              locations={places}
              currentLocation={currentLocation}
              />
          </ScrollView>
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const searchActive = getSearchFinished(state)
  const places = searchActive ? getSearchedPlaces(state) : getFilteredPlaces(state)
  // console.tron.log({searchActive})
  // const isFetching= searchActive ? getPlaceSearchFetching(state) : getFilteredPlacesFetching(state)

  return {
    places,
    // isFetching, 
    currentLocation: getCurrentLocation(state),
    shouldShowLocationAccessDeniedAlert: getShouldShowLocationAccessDeniedAlert(state),
    searchActive,
    currentSearchTerm: getCurrentSearchTerm(state),
    searchHistory: getSearchHistory(state),
    showSearchBar: getShowSearchBar(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    locationAccessDeniedAlertDisplayed: () => dispatch(LocationRedux.locationAccessDeniedAlertDisplayed()),
    placeSearchRequest: (...args) => dispatch(PlaceSearchRedux.placeSearchRequest(...args)),
    placeSearchBarHide: () => dispatch(PlaceSearchRedux.placeSearchBarHide())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectoriesScreen)
