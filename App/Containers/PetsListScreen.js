import React, { Component } from 'react'
import {View, Image, ScrollView, Text, InteractionManager, Alert} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {Images} from '../Themes'
import RoundButton from '../Components/RoundButton'
import styles from './Styles/PetsListScreenStyle'
import { getPetsArray } from '../Redux/PetsRedux'
import { getCurrentUser, getIsGuestUser } from '../Redux/UserRedux'
import PetList from '../Components/PetList'
import Background from '../Components/Background'
import { Menu, MenuOptions, MenuOption, MenuTrigger, MenuContext } from 'react-native-popup-menu'
import NavBars from '../Components/NavBars'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import PetDeleteRedux from '../Redux/PetDeleteRedux'

class PetsListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {headerRight} = params

    return {
      title: `Mis mascotas`,
      headerTitleStyle: styles.headerTitle,
      headerLeft: <NavBars onPress={() => navigation.navigate('DrawerOpen')} />,
      // onPress={() => navigation.navigate('UserProfileEditScreen')}
      headerRight: headerRight

    }
  };

  constructor (props) {
    super(props)
    this.state = {
      deleting: false,
      deletingList: [],
      nameDeletingList: []
    }
  }

  componentDidMount () {
    const {navigation} = this.props
    const {headerRight} = this

    InteractionManager.runAfterInteractions(() => {
      navigation.setParams({
        headerRight: headerRight()
      })
    })
  }

  headerRight = (isDeleting = false) => {
    // const {deleting} = this.state
    const {navigation} = this.props

    if (isDeleting) {
      // const {nameDeletingList} = this.state
      return <IconMD name='delete' size={25} style={{paddingRight: 20}}
        onPress={() => Alert.alert('Eliminar perfil de mascota?',
                      '¿Esta seguro que desea borrar el perfil? ',
          [
                        {text: 'Cancelar', onPress: () => this.cancelSelected()},
                        {text: 'Borrar', onPress: () => this.deleteSelected()} ],
                      { cancelable: true }
                    )}
            />
    } else {
      return <Menu>
        <MenuTrigger >
          <Icon name='ellipsis-v' size={20} style={{paddingRight: 20}} />
        </MenuTrigger>
        <MenuOptions >

          <MenuOption onSelect={() => navigation.navigate(`PetProfileEditScreen`)} >
            <Text style={{margin: 10, fontFamily: 'Rubik-Regular', fontSize: 15}}>Crear Mascota</Text>
          </MenuOption>
          <MenuOption onSelect={this.toggleDeleting} >
            <Text style={{margin: 10, fontFamily: 'Rubik-Regular', fontSize: 15}}>Eliminar Mascota</Text>
          </MenuOption>

        </MenuOptions>
      </Menu>
    }
  }

  toggleDeleting = () => {
    this.setState({deleting: !this.state.deleting})
    this.props.navigation.setParams({
      headerRight: this.headerRight(!this.state.deleting)
    })
  }

  // showRightButton = () => {
  // 	let isLoggedIn = this.props.isLoggedIn;

  // 	if(this.state.deleting){
  // 		let {deletingList, nameDeletingList} = this.state;
  // 		console.log(deletingList);
  // 		return (
  // 			<View style={styles.rightMenuContainer}>
  //         <TouchableHighlight
  //           onPress={()=> 		Alert.alert(
  // 						  'Eliminar perfil de mascota?',
  // 						  '¿Esta seguro que desea borrar el perfil de'+nameDeletingList,
  // 						  [
  // 						    {text: 'Cancelar', onPress: () => this.cancelSelected()},
  // 						    {text: 'Borrar', onPress: () => this.deletSelected()},
  // 						  ],
  // 						  { cancelable: false }
  // 						)  }
  //           underlayColor= '#ffffff'>
  //   				<View style={styles.rightMenu}>
  //   					<Image source={require('./trash.png')}/>
  //   				</View>
  //         </TouchableHighlight>
  //       </View>
  // 		)
  // 	}
  // 	if(isLoggedIn){
  // 			return (
  // 	      <View style={styles.rightMenuContainer}>
  // 	        <TouchableHighlight
  // 	          onPress={()=>this.toggleMenu()}
  // 	          underlayColor= '#ffffff'>
  // 	  				<View style={styles.rightMenu}>
  // 	  					<MenuPoints style={styles.menuPoints}/>
  // 	  				</View>
  // 	        </TouchableHighlight>
  // 	      </View>
  // 				)
  // 		}else{
  // 			return null
  // 		}
  // }

  deleteSelected = () => {
    let {deletingList} = this.state
    // console.tron.log({deletingList: deletingList})

    if (deletingList && deletingList.length > 0) {
      this.props.petDeleteRequest(deletingList)
    }
    this.cancelSelected()
  }

  cancelSelected = () => {
    let {nameDeletingList, deletingList, deleting} = this.state
    deleting = false
    deletingList = []
    nameDeletingList = []
    this.setState({nameDeletingList, deletingList, deleting})
    this.props.navigation.setParams({ headerRight: this.headerRight(deleting) })
  }

  selectToDelete = (id, name, selected) => {
    let deletingList, nameDeletingList
    if (this.state.deletingList.includes(id)) {
      deletingList = this.state.deletingList.filter(item => item !== id)
      nameDeletingList = this.state.nameDeletingList.filter(item => item !== ' ' + name)
    } else {
      deletingList = this.state.deletingList.concat([id])
      nameDeletingList = this.state.nameDeletingList.concat([' ' + name])
    }
    this.setState({deletingList, nameDeletingList})
  }

  // getAdditionalContent = (item) => {
  // 	if(!this.state.deleting)return null;
  // 	if(this.state.deletingList.includes(item.id) ){
  // 			return (<View style={styles.deletingItemSelected}>
  // 				<CheckerIcon/>
  // 			</View>)
  // 	}
  // 	return (
  // 		<View style={styles.deletingItem}></View>
  // 	)
  // }

  showContent = () => {
    const { navigate } = this.props.navigation
    let {petList} = this.props
    let {deleting} = this.state

    if (petList && petList.length) {
      let clickFunction = deleting ? this.selectToDelete : null

      return <PetList list={petList}
        clickFunction={clickFunction}
        selectable={deleting}
        navigate={navigate}
        horizontal={false}
        additionalContent={this.getAdditionalContent}
    />
    } else {
      return this.showNoPets()
    }
  }

  // endEditing = ()=>{
  // 	this.setState({
  // 		editing: false
  // 	});
  // }

  // showHeader = () => {
  // 	let { editing } = this.state;
  // 	let { navigate } = this.props.navigation;
  // 	if(editing){
  // 		return (<Header
  // 			navigate={navigate}
  // 			logo={false}
  // 			text='Mis mascotas'
  // 			mainButton="goBack"
  // 			callback={this.endEditing}/>);
  // 	}
  // 	return (<Header logo={false} text='Mis mascotas' navigate={navigate}/>);
  // }

  showNoPets = () => {
    const {navigate} = this.props.navigation
    const {isGuestUser} = this.props

    const onPress = isGuestUser ? () => navigate('RegisterScreen') : () => navigate('PetProfileEditScreen')

    return (
      <View style={styles.containerEmpty}>
        <Image style={styles.image} source={Images.pets.noPets} />
        <Text style={styles.text1}>Cargá acá tus mascotas</Text>
        <Text style={styles.text2}>Cargá la información que necesitas {'\n'}tener a mano de tus mascotas</Text>
        <RoundButton bgColor='red' color='white' text='EMPEZAR' onPress={() => { onPress() }} />
      </View>

    )
  }

  render () {
    const {pets} = this.props
    const {showNoPets, showContent} = this
    const hasPets = (pets && pets.length > 0)

    return (
      <View style={styles.container}>
        <Background />
        {!hasPets && showNoPets()}
        {hasPets && showContent()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pets: getPetsArray(state),
    petList: getPetsArray(state),
    // pets: [],
    // petList: [],
    currentUser: getCurrentUser(state),
    isGuestUser: getIsGuestUser(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    petDeleteRequest: (...args) => dispatch(PetDeleteRedux.petDeleteRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetsListScreen)
