// @flow
import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, InteractionManager, Alert } from 'react-native'
import { connect } from 'react-redux'
import RoundButton from '../Components/RoundButton'
import styles from './Styles/PetVaccineNewScreenStyle'
import Background from '../Components/Background'
import UserDateInput from '../Components/UserDateInput'
import UserRadioButton from '../Components/UserRadioButton'
import R from 'ramda'
import validate from '../Lib/Validate'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import {getPet} from '../Redux/PetsRedux'
import * as Vaccines from '../Services/Vaccines'
import type Vaccine from '../Services/Vaccines'
import VaccineSaveRedux, {getVaccine, getVaccinesArray} from '../Redux/VaccineSaveRedux'
import VaccineDeleteRequest from '../Redux/VaccineDeleteRedux'

type Props = {
  pet?: Pet,
  petVaccine?: PetVaccine,
  navigation: Object,
  vaccines: Array<Vaccines>,
  vaccineSaveReques?: Function
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  step: number,
  nextApplication: 'yes' | 'no',

  vaccineId: number,
  applicationDate: string,
  nextApplicationDate: string,
}

const yesNo = [
      {value: 'yes', label: 'SI'},
      {value: 'no', label: 'NO'}
]

class PetVaccineNewScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {headerTitle, backCallback, headerRight} = params

    return {
      title: headerTitle,
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={() => navigation.goBack()}
        style={{paddingLeft: 20}}
      />,
      headerRight
    }
  }

  constructor (props) {
    super(props)
    const {petVaccine, pet, navigation} = this.props

    if (petVaccine) {
      // Edit existing vaccine
      const {vaccineId, applicationDate, nextApplicationDate} = petVaccine
      this.state = {
        fieldFocused: '',
        touched: [],
        errors: {},
        submitDisabled: true,

        step: 2,
        nextApplication: petVaccine.nextApplicationDate ? 'yes' : 'no',
        vaccineId,
        applicationDate,
        nextApplicationDate
      }
    } else if (pet) {
      // Create new vaccine for pet
      this.state = {
        fieldFocused: '',
        touched: [],
        errors: {},
        submitDisabled: true,

        step: 1,
        nextApplication: 'no',
        vaccineId: 0,
        applicationDate: '',
        nextApplicationDate: ''
      }
    } else {
      // FIXME: should hit error and redicrect back
      navigation.goBack()
    }
  }

  componentDidMount () {
    const {navigation} = this.props
    const {headerTitle, headerRight, backCallback} = this

    InteractionManager.runAfterInteractions(() => {
      navigation.setParams({
        headerTitle: headerTitle(),
        headerRight: headerRight(),
        backCallback})
    })
  }

  headerRight = () => {
    const {petVaccine, vaccineDeleteRequest} = this.props

    if (petVaccine) {
      return <IconMD
        name='delete'
        size={25}
        onPress={() => Alert.alert('', '¿Eliminar vacuna?',
          [
              {text: 'CANCELAR'},
              {text: 'ELIMINAR', onPress: () => vaccineDeleteRequest(petVaccine)}
          ],
            { cancelable: false }
          )}
        style={{paddingRight: 20}}
      />
    }
  }
  headerTitle = () => this.props.petVaccine ? 'Editar vacuna' : 'Nueva vacuna'

  /// /////////////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
      // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean= false) : boolean => {
    const {touched, vaccineId, applicationDate, nextApplication, nextApplicationDate} = this.state
    let validationErrors = {}

        // Validated fields
    validationErrors.vaccineId = validate('idPresent', vaccineId)
    validationErrors.applicationDate = validate('vaccineApplicationDate', applicationDate)
    // validationErrors.nextAppliactionDate = validate('vaccineApplicationDate', nextApplicationDate)
    if (nextApplication && nextApplication !== 'no') {
      validationErrors.nextApplicationDate = validate('vaccineApplicationDate', nextApplicationDate)
      if (nextApplicationDate && nextApplicationDate <= applicationDate) {
        validationErrors.nextApplicationDate = 'NO PUEDE SER!'
      }
    }

    if (isSubmit) {
      this.setState({touched: ['vaccineId', 'applicationDate', 'nextApplicationDate'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }
  /// /////////////////////////////////////////////////////////// END OF FORM BOILERPLATE

  step1 = () => {
    const goStep2 = (id: number) => { this.setState({step: 2, vaccineId: id}) }
    const {vaccines} = this.props

    const button = (vaccine : Vaccine) =>
      <TouchableOpacity onPress={() => goStep2(vaccine.id)} key={vaccine.id}>
        <Text style={styles.vaccineSelector}>{vaccine.name}</Text>
      </TouchableOpacity>

    return (
      <View style={{ width: '100%' }}>
        <Text style={styles.heading}>¿Qué vacuna vas a cargar?</Text>
        {R.isEmpty(vaccines) && <Text>Ya tenes todas las vacunas cargadas</Text>}
        {vaccines.map((vaccine:Vaccine) => button(vaccine))}
      </View>
    )
  }

  step2 = (parentComp) => {
    const {saveVaccine} = this
    const vaccine = Vaccines.findOne(this.state.vaccineId)

    if (!vaccine) { return <Text>Error: no existe esa vacuna</Text> }

    const onPressRadioButton = (val) => {
      if (val === 'yes') {
        this.setState({nextApplication: val})
      } else {
        this.setState({nextApplication: val, nextApplicationDate: null})
      }
    }

    return (
      <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false} >
        <View style={styles.header}>
          <Text style={styles.heading}>{vaccine.name}</Text>
          <View style={styles.textWrapper}>
            <Text style={styles.text}>{vaccine.description}</Text>
          </View>
        </View>

        <View style={styles.wrapperForInputs}>

          <View style={styles.date1InputWrapper}>
            <UserDateInput
              parentComp={parentComp}
              maxDate={new Date()}
              fieldName='applicationDate'
              placeholder='Fecha de aplicación'
              defaultValue={this.state.applicationDate} />
          </View>

          <View style={styles.userRadioButtonWrapper}>
            <UserRadioButton
              parentComp={parentComp}
              label='Tiene próxima dosis?'
              fieldName='nextApplication'
              style={styles.YesOrNoRadioButton}
              onPress={onPressRadioButton}
              defaultValue={this.state.nextApplication}
              options={yesNo}
              buttonInnerColor='#00B0FF'
          />
          </View>

          { this.state.nextApplication === 'yes' &&
          <View style={styles.yesInput} >
            <UserDateInput
              style={styles.nextApplicationDate}
              parentComp={parentComp}
              minDate={new Date()}
              fieldName='nextApplicationDate'
              placeholder='Fecha de próxima dosis'
              defaultValue={this.state.nextApplicationDate} />
          </View>
          }

          <View style={styles.startButton}>
            <View style={styles.startButtonWrap}>
              <RoundButton
                text='GUARDAR'
                color='white'
                bgColor='#00B0FF'
                onPress={saveVaccine}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }

  saveVaccine = () => {
    const {vaccineId, applicationDate, nextApplicationDate, errors, touched} = this.state

    if (this.formIsValid(true)) {
      const {petVaccine, petVaccineId, pet, petId, vaccineSaveRequest, vaccineUpdateRequest} = this.props
      // If already existing entry
      if (petVaccine) {
        // console.tron.warn({props: this.props})
        vaccineUpdateRequest({vaccineId, applicationDate, nextApplicationDate}, petId, petVaccineId)
      } else {
        vaccineSaveRequest({vaccineId, applicationDate, nextApplicationDate}, petId)
      }
    } else {
      console.tron.warn({applicationDate, nextApplicationDate, errors, touched})
    }
  }

  render () {
    const parentComp = this
    const {step} = this.state
    const {step1, step2} = this

    return (
      <View style={styles.container}>
        <Background />
        {step === 1 && step1()}
        {step === 2 && step2(parentComp)}
      </View>
    )
  }
}

const mapStateToProps = (state, props) : Props => {
  const {petId, petVaccineId} = props.navigation.state.params
  const petVaccinesIds = R.map(R.prop('vaccineId'), getVaccinesArray(state, petId))
  const allVaccines = Vaccines.findAll()
  const vaccines = R.reject((v) => R.contains(v.id, petVaccinesIds), allVaccines)

  return {
    pet: getPet(state, petId),
    petVaccine: getVaccine(state, petId, petVaccineId),
    vaccines,
    petId,
    petVaccineId
    // petVaccines: getVaccinesArray(state, petId)
  }
}

const mapDispatchToProps = (dispatch) : Props => {
  return {
    vaccineSaveRequest: (...args) => dispatch(VaccineSaveRedux.vaccineSaveRequest(...args)),
    vaccineUpdateRequest: (...args) => dispatch(VaccineSaveRedux.vaccineUpdateRequest(...args)),
    vaccineDeleteRequest: (...args) => dispatch(VaccineDeleteRequest.vaccineDeleteRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetVaccineNewScreen)
