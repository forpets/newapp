import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { ScrollView, View, Text } from 'react-native'
import styles from './Styles/MenuStyle'
import MenuButton from '../Components/MenuButton'
import {Images} from '../Themes'
import { connect } from 'react-redux'
import UserRedux, { getCurrentUser, getIsGuestUser } from '../Redux/UserRedux'
import UserAvatar from '../Components/UserAvatar'

class Menu extends Component {
  render () {
    const {user, isGuestUser} = this.props
    let {navigate} = this.props.navigation

    const userOnPress = user ? () => navigate('UserProfileScreen') : () => {}
    const photoURL = user ? user.photoURL : null
    const name = user ? user.firstname + ' ' + user.lastname : 'Usuario Invitado'

    const onPressMyProfile = isGuestUser ? () => { navigate('LoginScreen') } : () => { navigate('UserProfileScreen') }
    const onPressNotes = isGuestUser ? () => { navigate('RegisterScreen') } : () => { navigate('NotesScreen') }
    const onPressPetsList = isGuestUser ? () => { navigate('RegisterScreen') } : () => { navigate('PetsListScreen') }
    const onPressLostFound = isGuestUser ? () => { navigate('RegisterScreen') } : () => { navigate('LostFoundScreen') }

    // TODO: Menu has 2 states - user logged in && guest user
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>

          <View style={styles.avatar} >
            <UserAvatar photoURL={photoURL} onPress={userOnPress} style={{marginLeft: 20, marginBottom: 10}} />
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.displayName}>{name}</Text>
          </View>

          <View style={[styles.menuItemsContainer, styles.paddingContainer]}>
            <MenuButton text='Inicio' onPress={() => navigate('HomeScreen')} image={Images.homeIcon} />
            <MenuButton text='Ver mapa' onPress={() => navigate('DirectoriesScreen')} image={Images.menuDirectorio} />
            <MenuButton text='Mascotas' onPress={onPressPetsList} image={Images.menuMascotas} />
            <MenuButton text='Notas' onPress={onPressNotes} image={Images.menuNotas} />
            <MenuButton text='Perdidos y Encontrados' onPress={onPressLostFound} image={Images.menuLostFound} />
            <MenuButton text='Mi Perfil' onPress={onPressMyProfile} image={Images.menuMiPerfil} />
            {!isGuestUser && <MenuButton text='Crear nuevo espacio' onPress={() => navigate('PlaceEditScreen')} image={Images.menuCrearEspacio} />}
          </View>

          <View style={styles.divider} />

          <View style={[styles.menuItemsContainer, styles.paddingContainer ]} >
            <MenuButton text='Acerca de' onPress={() => navigate('AboutUsScreen')} image={Images.menuAcercaDe} />
            <MenuButton text='Términos & Condiciones' onPress={() => navigate('TermsAndCondScreen')} image={Images.menuTyc} />
            {isGuestUser && <MenuButton text='Iniciar Sesión' onPress={() => navigate('LoginScreen')} image={Images.menuCerrarSesion} />}
            {!isGuestUser && <MenuButton text='Cerrar Sesión' onPress={() => this.props.logoutRequest()} image={Images.menuCerrarSesion} />}
          </View>
        </View>

      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: getCurrentUser(state),
    isGuestUser: getIsGuestUser(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logoutRequest: () => { dispatch(UserRedux.userLogout()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
