import React, { Component } from 'react'
import { ScrollView, Text, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {getPetsArray} from '../Redux/PetsRedux'
import {getIsGuestUser, getCurrentUser} from '../Redux/UserRedux'
import PetsCarousel from '../Components/PetsCarousel'
import { Images } from '../Themes'
import Background from '../Components/Background'
import NoteList from '../Components/NoteList'
import styles from './Styles/HomeScreenStyle'
import { getNotesArray } from '../Redux/NotesRedux'
import DirectoryList from '../Components/DirectoryList'
import NavBars from '../Components/NavBars'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import { getFilteredPlaces, getFilteredPlacesFetching } from '../Redux/FilteredPlacesRedux'

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({

    headerLeft:
  <View style={{flexDirection: 'row', alignItems: 'center'}}>
    <NavBars onPress={() => navigation.navigate('DrawerOpen')} />
    <Image source={Images.headerLogo} style={{marginLeft: 30, width: 100, height: 20}} />
  </View>
  });

  showEmptyPets = (show) => {
    let {isLoggedIn} = this.props

    if (show) {
      return (
        <View style={styles.emptyPetContainer} >
          <Image source={Images.petsEmpty} style={styles.emptyPetImage} />
          <View style={styles.emptyPetSubContainer} >
            <Text style={styles.emptyPetText}> {isLoggedIn ? 'Carga acá tu primer \nmascota.' : 'Registrate para \ncargar una mascota'}</Text>
            <TouchableOpacity style={styles.emptyPetButton}
              onPress={() => { this.props.navigation.navigate(isLoggedIn ? 'PetProfileEditScreen' : 'RegisterScreen') }} >
              <Text style={styles.emptyNotesTextButton}>{isLoggedIn ? 'CARGAR' : 'REGISTRARSE'}</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }

  showEmptyNotes = (show) => {
    let { isLoggedIn, pets } = this.props
    const {navigate} = this.props.navigation

    if (show) {
      return (
        <View style={styles.emptyBigContainer}>
          <View style={styles.emptyNotesContainer}>
            <View style={styles.emptyNotesImageWrapper}>
              <Image source={Images.notesEmpty} style={styles.emptyNotesImage} />
            </View>
            <View style={styles.emptyNotesSubContainer} >
              <View>
                <Text style={styles.emptyNotesText}>
                  { isLoggedIn ? (pets.length === 0
                    ? 'Carga al menos una\nmascota para comenzar\na subir notas' : 'Comenzá a\nsubir notas de tus\nmascotas') : 'Carga al menos una\nmascota para comenzar\na subir notas'}
                </Text>
              </View>
              <View>
                { (isLoggedIn) &&
                <TouchableOpacity style={styles.emptyNotesButton} onPress={() => { navigate('NotesScreen') }}>
                  <Text style={styles.emptyNotesTextButton}> CARGAR </Text>
                </TouchableOpacity>
            }
              </View>
            </View>
          </View>
        </View>
      )
    }
  }

  render () {
    let navigate = this.props.navigation.navigate
    let {pets, notes, places, placesFetching} = this.props

    return (
      <View style={styles.container}>

        <Background />
        <ScrollView styles={styles.container}>

          <PetsCarousel list={pets} navigate={navigate} />
          {this.showEmptyPets(pets.length === 0)}

          <View style={styles.directoryContainer} >
            <DirectoryList navigate={navigate} directory={places} fetching={placesFetching} />
          </View>

          <View style={styles.notesContainer}>
            <Text style={styles.notesTitle}>Última nota</Text>

            <View>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('NotesScreen') }}>
                {!(notes.length === 0) &&
                <View style={{flexDirection: 'row'}} >
                  <Text style={styles.notesRightTitle}>Ver todas</Text>
                  <IconMD name='keyboard-arrow-right' style={{fontSize: 18, textAlign: 'center', textAlignVertical: 'center', color: '#939393'}} />
                </View>
              }
              </TouchableOpacity>
            </View>

          </View>

          <View style={styles.notesListContainer}>
            <NoteList notes={notes} navigate={navigate} lastItem />
            {this.showEmptyNotes(notes.length === 0)}
          </View>

        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    pets: getPetsArray(state),
    isLoggedIn: getCurrentUser(state),
    notes: getNotesArray(state),
    places: getFilteredPlaces(state),
    placesFetching: getFilteredPlacesFetching(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
