// @flow
import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, InteractionManager } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import R from 'ramda'
import styles from './Styles/PetProfileEditScreenStyle'
import validate from '../Lib/Validate'
import UserInput from '../Components/UserInput'
import RoundButton from '../Components/RoundButton'
import UserPickerInput from '../Components/UserPickerInput'
import UserDateInput from '../Components/UserDateInput'
import UserRadioButton from '../Components/UserRadioButton'
import Background from '../Components/Background'
import PetCreateRedux from '../Redux/PetCreateRedux'
import { getCurrentUser } from '../Redux/UserRedux'
import {getPet} from '../Redux/PetsRedux'
import PetPhotoPicker from '../Components/PetPhotoPicker'
import { Alert } from 'react-native'
import PetAttributes from '../Services/PetAttributes'

type Props = {
  pet: Pet,
  navigation: Object,
  createPet: Function,
  updatePet: Function,
  owner: User
}

type Breed = {
  label: string,
  value: string
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  // mode: 'new' | 'edit',

  name: string,
  birthdate: string,
  kind: string,
  breed: string,
  gender: string,
  description: string,
  image: string,
  imagePickerResponse?: null,

  breedsForSelect: Array<Breed>
}

class PetProfileEditScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {onPressBack} = params

    return {
      title: 'Sobre mi mascota',
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={onPressBack}
        style={{paddingLeft: 20}}
  /> }
  };

  constructor (props : Props) {
    super(props)
    const {pet} = props

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      name: '',
      birthdate: null,
      kind: null,
      breed: null,
      gender: null,
      description: null,
      imagePickerResponse: null,

      breedsForSelect: PetAttributes.findBreedsFor()
    }

    // If we are editing pet - assign values
    if (pet) {
      this.state.name = pet.name
      this.state.birthdate = pet.birthdate
      this.state.kind = pet.kind
      this.state.breed = pet.breed
      this.state.breedsForSelect = PetAttributes.findBreedsFor(this.state.kind)
      this.state.gender = pet.gender
      this.state.description = pet.description
    }
  }

  componentDidMount () {
    const {navigation} = this.props
    const {onPressBack} = this
    InteractionManager.runAfterInteractions(() => {
      navigation.setParams({ onPressBack })
    })
  }

  onPressBack = () => {
    const {touched} = this.state
    const {navigation} = this.props

    if (touched && touched.length > 0) {
      Alert.alert('', '¿Desea salir?',
        [
          {text: 'SI', onPress: () => { navigation.goBack() }},
          {text: 'NO', onPress: () => {}}
        ],
        { cancelable: true }
      )
    } else {
      navigation.goBack()
    }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    // Handle pet type change
    if (fieldName === 'kind') this.handlePetKindChange(fieldValue)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
     // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, name, birthdate, kind, breed, description} = this.state
    let validationErrors = {}

    // Validated fields
    validationErrors.name = validate('name', name)
    validationErrors.birthdate = validate('petBirthdate', birthdate)
    validationErrors.kind = validate('petKind', kind)
    validationErrors.breed = validate('petBreed', breed)

    validationErrors.description = validate('petDescription', description)

    if (isSubmit) {
      this.setState({touched: ['name', 'birthdate', 'kind', 'breed'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }

  savePet = () => {
    const {createPet, updatePet, owner, pet} = this.props
    // console.tron.warn({owner, state: this.state, pet})

    if (this.formIsValid(true)) {
      const {name, birthdate, kind, breed, gender, description, imagePickerResponse} = this.state

      if (pet) {
        updatePet({name, birthdate, kind, breed, gender, description, owner: owner.uid}, imagePickerResponse, pet.id)
      } else {
        createPet({name, birthdate, kind, breed, gender, description, owner: owner.uid}, imagePickerResponse)
      }
    } else {
      // console.tron.error({state: this.state})
    }
  }

  handlePetKindChange = (itemValue) => {
    if (itemValue !== this.state.kind) {
      const breedsForSelect = PetAttributes.findBreedsFor(itemValue)
      this.setField('breed', null)
      this.setState({breedsForSelect})
    }
  }

  onPhotoSelected = (imagePickerResponse : Object) : void => {
    const {touched} = this.state
    touched.push('imagePickerResponse')
    this.setState({imagePickerResponse, touched}, this.formIsValid)
  }

  petKinds = PetAttributes.findAllKinds()
  petGenders = PetAttributes.findAllGenders()

  render () {
    const {submitDisabled, breedsForSelect} = this.state
    const {pet} = this.props
    const thisComponent = this
    const petImage = pet ? pet.image : null
    const {onUpdatePetType, savePet, onPhotoSelected} = this

    const date = new Date()
    const minBirthdate = new Date().setFullYear(date.getFullYear() - 99)

    // console.tron.log(this.state)

    return (
      <View style={styles.container}>
        <Background />
        <ScrollView style={styles.container}>
          <KeyboardAvoidingView behavior='padding'>

            <PetPhotoPicker photoUrl={petImage} onPhotoSelected={onPhotoSelected} />
            <View>
              <UserInput
                style={styles.nameInput}
                parentComp={this}
                fieldName={'name'}
                placeholder='Nombre'
                defaultValue={this.state.name}
                onSubmitEditing={() => this.focusTo('name')}
            />
            </View>

            <View>
              <UserDateInput
                style={styles.birthDate}
                parentComp={thisComponent}
                minDate={minBirthdate}
                maxDate={date}
                fieldName='birthdate'
                placeholder='Fecha de nacimiento'
                defaultValue={this.state.birthdate} />
            </View>

            <View>
              <UserPickerInput
                placeholder='Tipo de Mascota'
                fieldName='kind'
                parentComp={thisComponent}
                selectedValue={this.state.kind}
                optionsForSelect={this.petKinds}
              />
            </View>

            <View>
              <UserPickerInput
                placeholder='Raza'
                fieldName='breed'
                parentComp={thisComponent}
                selectedValue={this.state.breed}
                optionsForSelect={breedsForSelect}
              />
            </View>

            <View>
              <UserRadioButton
                label='Sexo'
                fieldName='gender'
                parentComp={thisComponent}
                defaultValue={this.state.gender}
                options={this.petGenders}
                buttonInnerColor='#FFD600'
          />
            </View>

            <View>
              <UserInput
                parentComp={thisComponent}
                fieldName={'description'}
                placeholder='Rasgos Distintivos'
                defaultValue={this.state.description}
                onSubmitEditing={savePet}
                maxLength={300}
                multiline
            />
            </View>

            <Text style={styles.footerText}>Razgos distintivos de tu mascota</Text>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginBottom: '2%'}}>
              <RoundButton
                bgColor='#FFD600'
                bgColorDisabled='#ffea80'
                color='white'
                text='  GUARDAR  '
                onPress={savePet}
                // disabled={submitDisabled}
                />
            </View>

          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params} = props.navigation.state
  const id = params ? params.id : null

  return {
    owner: getCurrentUser(state),
    pet: getPet(state, id)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createPet: (...args) => { dispatch(PetCreateRedux.petCreateRequest(...args)) },
    updatePet: (...args) => { dispatch(PetCreateRedux.petUpdateRequest(...args)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetProfileEditScreen)
