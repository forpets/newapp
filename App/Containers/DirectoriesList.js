import React from 'react'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/DirectoriesListStyle'
import { Images } from '../Themes/'
import CategoryHeader from '../Components/CategoryHeader'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import Categories from '../Services/Categories'
import GeoFunctions from '../Transforms/GeoFunctions'
import PlaceStatus from '../Components/PlaceStatus'

class DirectoriesList extends React.PureComponent {
// class DirectoriesList extends React.Component {
  // shouldComponentUpdate (nextProps, nextState) {
  //   const {isFetching} = nextProps
  //   console.tron.log({nextProps, isFetching})

  //   if (isFetching) {
  //     return false
  //   }
  //   return true
  // }

  renderRow = ({item}) => {
    const {navigation, currentLocation} = this.props
    const params = {id: item.id}

    const category = Categories.findOne(item.category)

    const distance = (item.location && currentLocation) ? GeoFunctions.getDistanceToPlace(item, currentLocation) : null
    const distanceText = distance ? `A ${distance} km` : ''

    return (
      <TouchableOpacity onPress={() => navigation.navigate('PlaceViewScreen', params)}>
        <View style={styles.row}>
          {(item.advert) && <CategoryHeader categoryId={item.category} />}

          {(item.advert) &&
          <View style={styles.anuncioContainer}>
            <View style={styles.anuncio1}>
              <View style={styles.anuncioTitleWrapper}>
                <Text style={styles.boldLabel}>{item.name}</Text>
              </View>
              <View style={styles.anuncioLabelWrapper}>
                <Text style={styles.label}>{item.address}</Text>
              </View>
            </View>
            <View style={styles.anuncio2}>
              {(item.rating) && <Icon name='star' style={{color: '#8ACB27'}} />}
            </View>
            <View style={styles.anuncio3}>
              <Text style={styles.rateNumber}>{item.rating}</Text>
            </View>
          </View>
          }

          {!(item.advert) &&
          <View style={styles.sinAnuncioContainer}>
            <View style={styles.sinItemanuncio1}>
              <View style={styles.sinAnuncioWrapper1}>
                <Text style={styles.sinAnuncioBoldLabel}>{item.name}</Text>
              </View>
              <View style={styles.sinAnuncioWrapper2}>
                <Text style={[ styles.vetLabel, {color: category.color} ]}>
                  {Categories.idToString(item.category, {uppercase: true})}
                </Text>
              </View>
            </View>

            <View style={styles.sinItemanuncio2}>
              <View style={styles.sinAnuncioWrapper3}>
                <IconMD style={styles.sinAnunciolocation} name='location-on' />
              </View>
              <View style={styles.sinAnuncioWrapper4}>
                <Text style={styles.sinAnuncioLabel}>{item.address}</Text>
              </View>
              <View style={styles.sinAnuncioWrapper5}>
                <Text style={styles.sinAnuncioKmNumber}>{distanceText}</Text>
              </View>
            </View>
          </View>
          }

          {(item.advert && false) && // FIXME: must implement later...
          <View style={styles.row2}>
            <Text style={styles.services}>ATENCIÓN MÉDICA</Text>
            <Text style={styles.services}>DELIVERY ALIMENTOS</Text>
            <Text style={styles.services}>ESPECIALIDADES MÉDICAS</Text>
          </View>
            }

          <View style={styles.footer}>
            {(item.advert) &&
            <View style={styles.rate}>
              <Text style={styles.kmNumber}>{distanceText}</Text>
            </View>
          }

            {!(item.advert) &&
            <View style={styles.rate}>
              <Icon name='star' style={{color: item.rating ? '#8ACB27' : 'gray'}} />
              <Text style={styles.rateNumber}>{item.rating}</Text>
            </View>
             }
            <View style={styles.rateClock}>
              <View style={styles.placeStatusWrapper}>
                <View style={styles.placeStatusContainer}>
                  <PlaceStatus schedule={item.schedule} />
                </View>
              </View>
            </View>

          </View>
        </View>

      </TouchableOpacity>
    )
  }

  // Render a header?
  renderHeader = () => {}
    // <View style={[styles.label, styles.sectionHeader]}>
    //   <Image source={Images.directories.vetBagEmergency} style={styles.logoImage} />
    //   <Text style={styles.text1}> VETERINARIAS </Text>
    // </View>

  // Render a footer?
  renderFooter = () => {}

  // Show this when data is empty
  // FIXME: Read analysis - maybe show old results...
  renderEmpty = () => {
    // const {isFetching}
    return (
      <View style={styles.rowEmpty}>
        <View style={styles.labelEmptyWrapper}>
          <Text style={styles.emptyLabel}>No se han encontrando lugares para tu búsqueda</Text>
        </View>
      </View>
    )
  }

  renderSeparator = () => <View style={styles.divider} />

  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => index

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 12

  // extraData is for anything that is not indicated in data
  // for instance, if you kept "favorites" in `this.state.favs`
  // pass that in, so changes in favorites will cause a re-render
  // and your renderItem will have access to change depending on state
  // e.g. `extraData`={this.state.favs}

  // Optimize your list if the height of each item can be calculated
  // by supplying a constant height, there is no need to measure each
  // item after it renders.  This can save significant time for lists
  // of a size 100+
  // e.g. itemLayout={(data, index) => (
  //   {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
  // )}

  render () {
    const {data, currentLocation} = this.props

    return (
      <View style={styles.container}>
        <FlatList
          scrollEventThrottle={500}
          contentContainerStyle={styles.listContent}
          data={data}
          extraData={currentLocation}
          renderItem={this.renderRow}
          keyExtractor={this.keyExtractor}
          initialNumToRender={this.oneScreensWorth}
          windowSize={7}
          ListImageComponent={this.renderImage}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    )
  }
}

export default DirectoriesList
