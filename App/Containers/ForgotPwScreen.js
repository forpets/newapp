import React, { Component } from 'react'
import { TouchableOpacity, ScrollView, Text, View, KeyboardAvoidingView, Alert } from 'react-native'
import { connect } from 'react-redux'
import R from 'ramda'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/ForgotPwScreenStyle'
import GrayButton from '../Components/GrayButton'
import validate from '../Lib/Validate'
import LoginInput from '../Components/LoginInput'
import ForgotPasswordRedux from '../Redux/ForgotPasswordRedux'

type State = {
  fieldFocused: string,
  submitDisabled: boolean,
  touched: Array,
  errors: Object,

  email: string,
}

type Props = {}

class ForgotPwScreen extends Component<Props, State> {
  static navigationOptions = {
    header: null,
    drawerLockMode: 'locked-closed'
  }

  constructor (props) {
    super(props)
    this.state = {
      fieldFocused: '',
      email: '',
      touched: [],
      errors: {},
      submitDisabled: true
    }
  }

  componentWillReceiveProps (nextProps) {
    // Get email already in use error
    console.tron.warn({nextProps})
    const {errorMsg} = nextProps
    if (errorMsg) {
      if (errorMsg === 'auth/user-not-found') {
        this.setState({errors: {email: 'El usuario con este email no existe'}})
      }
    }
  }

  /// /////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, () => this.formIsValid(false))
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
   // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean) : boolean => {
    const { touched, email } = this.state
    let validationErrors = {}

     // Validated fields
    validationErrors.email = validate('email', email)

    if (isSubmit) {
      this.setState({touched: ['email'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }
  /// /////////////////////////////////////////////////// END OF FORM BOILERPLATE

  resetPassword = () => {
    console.tron.warn(this.state)

    if (this.formIsValid(true)) {
      const {email} = this.state
      const {forgotPasswordRequest} = this.props
      forgotPasswordRequest(email)
    } else {
      // Shows errors
    }
  }

  render () {
    const {navigation} = this.props

    return (
      <ScrollView style={[styles.container, styles.background ]}>
        <KeyboardAvoidingView behavior='position'>

          <View style={styles.header}>
            <View>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <IconMD name='arrow-back' style={{fontSize: 25, color: 'white'}} />
              </TouchableOpacity>
            </View>

            <Text style={styles.text1}>Reiniciar {'\n'}contraseña</Text>
            <Text style={styles.text2}>¿Olvidaste tu contraseña?</Text>
            <Text style={styles.text3}>¡No hay problema! Acá podés {'\n'}generar una nueva.</Text>
            <Text style={styles.text4}>Verifica tu dirección de email</Text>
          </View>

          <View style={styles.inputAndButton}>

            <LoginInput
              parentComp={this}
              fieldName='email'
              placeholder='E-mail * '
              defaultValue={this.state.email}
              onSubmitEditing={() => this.resetPassword()}
            />

            <GrayButton
              text='ENVIAR INSTRUCCIONES'
              disabled={this.state.submitDisabled}
              onPress={() => this.resetPassword()}
          />
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    errorMsg: state.forgotPassword.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPasswordRequest: (...args) => dispatch(ForgotPasswordRedux.forgotPasswordRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPwScreen)
