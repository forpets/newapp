// @flow
import React, { Component } from 'react'
import { ScrollView, Text, View, Image, FlatList, TouchableOpacity, Share, Linking, ActivityIndicator, Alert } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import {Images} from '../Themes'
import PhotoSwiper from '../Components/PhotoSwiper'
import {getPlace} from '../Redux/PlacesFindAllRedux'
import R from 'ramda'
import styles from './Styles/PlaceViewScreenStyle'
import PlaceReviews from '../Components/PlaceReviews'
import { getCurrentUser } from '../Redux/UserRedux'
import type {Place} from '../FlowTypes/Place'
import PlaceReviewRedux, { getPlaceReviewsArray, getPlaceReviewsIsFetching } from '../Redux/PlaceReviewRedux'
import type {PlaceReview} from '../FlowTypes/PlaceReview'
import { getAllUsersObject } from '../Redux/AllUsersRedux'
import Categories from '../Services/Categories'
import { getCurrentLocation } from '../Redux/LocationRedux'
import GeoFunctions from '../Transforms/GeoFunctions'
import PlaceViewMap from '../Components/PlaceViewMap'
import PlaceStatus from '../Components/PlaceStatus'
import ScheduleToString from '../Transforms/ScheduleToString'
import ImagePicker from 'react-native-image-picker'
import type PlacePhoto from '../Redux/PlaceRedux'
import AppConfig from '../Config/AppConfig'
import PlacePhotoUploadRedux from '../Redux/PlacePhotoUploadRedux'

type Props = {
  place: Place,
  navigation: Object,
  placeReviewRequest: Function,
  placeReviews: Array<PlaceReview>,
  placeReviewsIsFetching: boolean,
  allUsers: Array<UserPublic>,
  currentLocation: Location,
  user: User,
  placePhotoUploadRequest: Function
}

const pickerOptions = {
  title: 'Subir foto de espacio',
  takePhotoButtonTitle: 'Tomar una foto',
  chooseFromLibraryButtonTitle: 'Seleccionar una foto',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  allowEditing: true,
  noData: true,
  quality: 0.7,
  maxWidth: 1000,
  maxHeight: 1000,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}
class PlaceViewScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      header: null
    }
  }

  componentDidMount () {
    const {place, placeReviewRequest} = this.props
    // Load place reviews
    if (place) {
      placeReviewRequest(place.id)
    }
  }

  openMap = (address) => {
    const query = encodeURI(address)
    Linking.openURL('http://maps.google.com/maps?daddr=' + query)
  }
  openEmail = (email) => { Linking.openURL('mailto:' + email) }
  openBrowser = (url) => { Linking.openURL(url) }
  openPhone = (phone) => { Linking.openURL('tel:' + phone) }

  render () {
    const {navigation, user, placeReviews} = this.props
    let {place, currentLocation} = this.props
    // If place id not found - show ...
    if (!place) { return (<View><ActivityIndicator size='large' color='gray' /></View>) }

    let {photos = [], secondaryCategories} = place
    const userCanEdit = user ? (user.uid === place.owner || place.public) : false
    const category = Categories.findOne(place.category)

    const shareContent = {
      message: place.name + ' - ' + category.label + '\n' + place.address // FIXME: barrio
      // title: 'Check out this amazing Place for your pet!'
    }
    const shareOptions = {
      dialogTitle: 'Compartir Espacio'
    }
    const distance = (place.location && currentLocation) ? GeoFunctions.getDistanceToPlace(place, currentLocation) : null
    const distanceText = distance ? `A ${distance} km de distancia` : ''

              // <View style={styles.emptyColumnItem} />

    return (
      <ScrollView style={styles.container}>
        <View style={styles.mainCotainer}>

          <View style={styles.topButtonsContainer}>
            <IconMD onPress={() => navigation.goBack()}
              name='arrow-back'
              style={[styles.topIcon, styles.topLeftIcon]}
              />
            <IconMD onPress={() => Share.share(shareContent, shareOptions)}
              name='share'
              style={[styles.topIcon, styles.topRightIcon]}
              />
          </View>

          <View style={[styles.section, styles.section1]}>

            { /* TOP PHOTO SLIDER / MAP if no photos */ }
            <View style={styles.sliderContainer}>
              { (R.length(photos) > 0)
                ? <PhotoSwiper photos={photos} />
                : <PlaceViewMap place={place} />
              }
            </View>

            { /* NAME */ }
            <View style={styles.titleContainer}>
              <View style={styles.titleWrapper}>
                <Text style={[styles.title/* , {color: category.color} */]}>{place.name}</Text>
              </View>
              <View style={styles.iconAnuncioWrapper}>
                {(place.advert) && <Image source={Images.directorio.anuncio} style={styles.iconAnuncio} /> }
              </View>
            </View>

            { /* SECONDARY CATEGORIES */ }
            { (secondaryCategories && !R.isEmpty(secondaryCategories)) &&
              <ScrollView horizontal
                style={styles.vetPetsContainer}
                contentContainerStyle={styles.vetPetsContainerContent}
                showsHorizontalScrollIndicator={false}
              >
                {secondaryCategories.map((item, i) => {
                  const cat = Categories.findOne(item)
                  return (<View style={styles.veteplaceholderItem} key={i}>
                    <Text style={[styles.veteplaceholderText, { borderColor: cat.color, color: cat.color }]}>
                      {Categories.idToString(item)}
                    </Text>
                  </View>)
                }
                )}
              </ScrollView>
            }

            { /* RATING */ }
            <View style={styles.footersvetPetsContainer}>
              {place.rating &&
                <View style={styles.starItem}>
                  <View style={styles.imagStarContainerItem} >
                    <IconMD name='star' style={styles.starIcon} />
                  </View>
                  <View style={styles.imagRatingContainerItem} >
                    <View><Text style={styles.ratingNumberText}>{place.rating}</Text></View>
                  </View>
                </View>}

              { /* OPEN / CLOSED */ }
              <View style={styles.relojItem}>
                <View style={styles.placeStatusWrapper}>
                  <PlaceStatus schedule={place.schedule} />
                </View>
                {/*  <View style={styles.imagRelojContainerItem}>
                  <IconMD style={styles.imagReloj} name='schedule' />
                </View>
                <View style={styles.imagOpenContainerItem}>
                  <Text style={styles.ratingOpenText}>ABIERTO</Text>
                </View> */}
              </View>
            </View>

            { /* PRICING */ }
            { (parseInt(place.pricing) > 0 && parseInt(place.pricing) <= 3) &&
            <View style={styles.currencyContainer}>
              <View style={styles.currencyWrapper}>
                <Image style={styles.currency} source={Images.directorio[`currencyFilter${place.pricing}`]} resizeMode='contain' />
              </View>
            </View>
            }

            { /* SCHEDULE */ }
            <View style={styles.timeContainer}>
              <View style={styles.footersvetPetsItem}>
                <Text style={styles.footersvetPetsText}>{ScheduleToString(place.schedule)}</Text>
              </View>
            </View>

            { /* DISTANCE */ }
            <View style={styles.kmContainer}>
              <View style={styles.footersvetPetsItem}>
                <Text style={styles.footersvetPetsText}>{ distanceText }</Text>
              </View>
            </View>

          </View>

          <View style={styles.dividerLine} />

          <View style={[styles.section, styles.section2]}>

            <TouchableOpacity onPress={() => this.openMap(place.address)}>
              <View style={styles.iconAndTextMainContainer}>
                <View style={styles.iconContainer}>
                  <IconMD style={styles.iconContacts} name='place' />
                </View>
                <View style={styles.iconTextContainer}>
                  <Text style={styles.textContacts}>{place.address}</Text>
                </View>
              </View>
            </TouchableOpacity>

            { place.phone
              ? <TouchableOpacity onPress={() => this.openPhone(place.phone)}>
                <View style={styles.iconAndTextMainContainer}>
                  <View style={styles.iconContainer}>
                    <IconMD style={styles.iconContacts} name='local-phone' />
                  </View>
                  <View style={styles.iconTextContainer}>
                    <Text style={styles.textContacts}>{place.phone}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            : null }

            { place.email
              ? <TouchableOpacity onPress={() => this.openEmail(place.email)}>
                <View style={styles.iconAndTextMainContainer}>
                  <View style={styles.iconContainer}>
                    <IconMD style={styles.iconContacts} name='email' />
                  </View>
                  <View style={styles.iconTextContainer}>
                    <Text style={styles.textContacts} >{place.email}</Text>
                  </View>
                </View>
              </TouchableOpacity>
              : null}

            { place.website
            ? <TouchableOpacity onPress={() => this.openBrowser(place.website)}>
              <View style={styles.iconAndTextMainContainer}>
                <View style={styles.iconContainer}>
                  <IconMD style={styles.iconContacts} name='language' />
                </View>
                <View style={styles.iconTextContainer}>
                  <Text style={styles.textContacts} >{place.website}</Text>
                </View>
              </View>
            </TouchableOpacity>
            : null}

            { (place.emergency && (place.emergencyNote || place.emergencyPhone))
              ? <TouchableOpacity onPress={() => place.emergencyPhone ? this.openPhone(place.emergencyPhone) : {}} >
                <View style={styles.iconAndTextMainContainer}>
                  <View style={styles.iconContainer}>
                    <Image style={styles.emergencyImage} source={Images.directorio.emergencia} />
                  </View>
                  <View style={styles.iconTextContainerNumber}>
                    <Text style={styles.textContacts} >
                      {place.emergencyPhone || 'Telefono no disponible'}
                    </Text>
                  </View>
                  <View style={styles.iconTextContainerEmergency}>
                    <Text style={styles.textContactsEmergency}>EMERGENCIAS</Text>
                  </View>
                </View>

                {(place.emergencyNote)
                ? (
                  <View style={styles.emergencyNoteContainer}>
                    <Text style={styles.emergencyNote}>{place.emergencyNote}</Text>
                  </View>
                )
                : null}

              </TouchableOpacity>
            : null}

            { place.description
            ? <View style={styles.descriptionContainer}>
              <Text style={styles.titleDescription}>Descripción</Text>
              <Text style={styles.descriptionText}>{place.description}</Text>
            </View>
                  : null}

            {this.renderPhotos(photos)}

          </View>

          { userCanEdit &&
          <View style={styles.addAndEditmainContainer}>
            <TouchableOpacity onPress={this.pickPhoto}>
              <View style={styles.iconAndTextMainContainer}>
                <View style={styles.iconContainer} >
                  <IconMD style={styles.iconContacts} name='camera-alt' />
                </View>
                <View style={styles.iconTextContainer}>
                  <Text style={styles.textContacts} >Añadir una foto</Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('PlaceEditScreen', {id: place.id})}>
              <View style={styles.iconAndTextMainContainer}>
                <View style={styles.iconContainer}>
                  <Image style={styles.iconEdit} source={Images.directorio.editarEspacio} />
                </View>
                <View style={styles.iconTextContainer}>
                  <Text style={styles.textContacts} >Editar espacio</Text>
                </View>
              </View>
            </TouchableOpacity>

            <View style={styles.dividerLine} />
          </View>
          }

          <View style={[styles.section, styles.section3]}>
            <View style={styles.comentariosContainer}>
              <Text style={styles.comentariosTitle}>Comentarios</Text>
            </View>

            <PlaceReviews
              navigation={navigation}
              place={place}
              isFetching={this.props.placeReviewsIsFetching}
              reviews={this.props.placeReviews}
              allUsers={this.props.allUsers}
              currentUser={this.props.user}
            />

          </View>

        </View>

      </ScrollView>
    )
  }

  renderPhotos = (photos) => {
    if (photos && photos.length > 0) {
      return (
        <View style={styles.pictureScrollerContainer}>
          <FlatList
            horizontal
            windowSize={5}
            initialNumToRender={6}
            data={photos}
            renderItem={({item}) => this.renderPhotoItem(item)}
            keyExtractor={(item, index) => index} />
        </View>
      )
    }
  }

  renderPhotoItem = ({url}) => {
    // console.tron.log({url})
    return (
      <Image source={{uri: url}} style={{height: 130, width: 150, marginRight: 10}} />
    )
  }

  pickPhoto = () => {
    const {photos = [], id} = this.props.place
    const maxPhotos = AppConfig.maxPhotosForPlace

    if (photos && photos.length >= maxPhotos) {
      Alert.alert('', 'No se puede agregar mas que ' + maxPhotos + ' fotos.',
      [ {text: 'OK', onPress: () => {}} ], { cancelable: false })
      return
    }

    ImagePicker.showImagePicker(pickerOptions, (response) => {
      if (response.didCancel) {
        console.tron.warn('User cancelled image picker')
      } else if (response.error) {
        console.tron.error('ImagePicker Error: ', response.error)
      } else {
        this.props.placePhotoUploadRequest({imagePickerResponse: response, placeId: id})
      }
    })
  }
}

const mapStateToProps = (state, props) => {
  const {params = {}} = props.navigation.state
  const {id} = params
  // const id = '9IqR827QuRJF2aWKfKK6'

  return {
    place: getPlace(state, id),
    user: getCurrentUser(state),
    allUsers: getAllUsersObject(state),
    placeReviews: getPlaceReviewsArray(state),
    placeReviewsIsFetching: getPlaceReviewsIsFetching(state),
    currentLocation: getCurrentLocation(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    placeReviewRequest: (...args) => dispatch(PlaceReviewRedux.placeReviewRequest(...args)),
    placePhotoUploadRequest: (...args) => dispatch(PlacePhotoUploadRedux.placePhotoUploadRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceViewScreen)
