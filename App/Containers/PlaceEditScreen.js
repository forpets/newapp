import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Alert, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import validate from '../Lib/Validate'
import R from 'ramda'
import UserInput from '../Components/UserInput'
import RoundButton from '../Components/RoundButton'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import PlaceAddRedux, { getPlaceAddError } from '../Redux/PlaceAddRedux'
import styles from './Styles/PlaceEditScreenStyle'
import { getCurrentUser } from '../Redux/UserRedux'
import BasicSwitch from '../Components/BasicSwitch'
import PlacePhotoPicker from '../Components/PlacePhotoPicker'
import GoogleAutocompleteInput from '../Components/GoogleAutocompleteInput'
import {getPlace} from '../Redux/PlacesFindAllRedux'
import PlaceCategoriesSelect from '../Components/PlaceCategoriesSelect'
import ScheduleSelect from '../Components/ScheduleSelect'
import Categories from '../Services/Categories'
import {PlaceSchedule} from '../FlowTypes/PlaceSchedule'
import ScheduleToString from '../Transforms/ScheduleToString'

class PlaceEditScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {headerTitle} = params

    return {
      title: headerTitle,
      drawerLockMode: 'locked-closed',
      headerLeft: <IconMD
        onPress={() => navigation.goBack()}
        name='arrow-back'
        style={{paddingLeft: 20, fontSize: 25, color: 'black'}} />
    }
  }

  constructor (props) {
    super(props)

    let {place = {}} = props
    place = place || {}
    const photos = place.photos ? place.photos.asMutable() : []

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      name: place.name || null,
      email: place.email || null,
      address: place.address || null,
      phone: place.phone || null,
      website: place.website || null,
      description: place.description || null,

      emergency: place.emergency || null,
      emergencyPhone: place.emergencyPhone || null,
      emergencyNote: place.emergencyNote || null,

      location: place.location || [],
      address_components: place.address_components || [],

      category: place.category || null,
      secondaryCategories: place.secondaryCategories || [],
      secondaryDays: [],
      photos,

      schedule: null || place.schedule, // If does not provide schdule - ScheduleSelect component is going to generate default
      scheduleLabel: place.schedule ? ScheduleToString(place.schedule) : '', // If does not provide schdule - ScheduleSelect component is going to generate default

      showCategoriesSelect: false,
      showScheduleSelect: false,

      categoryLabel: place.category ? Categories.idToString(place.category) : null, // Transcient field
      secondaryCategoriesLabels: place.secondaryCategories ? Categories.idsToString(place.secondaryCategories) : []  // Transcient field
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.error && nextProps.error) {
      Alert.alert('Error', 'Solicitud de error en proceso. Por favor intente mas tarde')
    }
  }

  componentDidMount () {
    const headerTitle = this.props.place ? 'Editar espacio' : 'Crear espacio'
    this.props.navigation.setParams({headerTitle})
  }

  /// //////////////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched, secondaryCategories } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    fieldValue = fieldValue || null

    this.setState({[fieldName]: fieldValue, touched, fieldFocused: '', secondaryCategories}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
    // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, name, email, address, category, phone, website, schedule,
      description, emergency, emergencyPhone, emergencyNote, location,
    address_components, secondaryCategories } = this.state

    const validationErrors = {
      name: validate('placeName', name),
      email: validate('placeEmail', email),
      address: validate('placeAddress', address),
      // FIXME: , schedule
      phone: validate('placePhone', phone),
      website: validate('placeWebsite', website),
      description: validate('placeDescription', description),
      category: validate('placeCategory', category)
    }

    // Must be precise address with street name and stuff
    // This meens we get 5 items address_components  from Google
    // console.tron.log({address_components, lenght: R.length(address_components)})
    if (!(address_components && R.length(address_components) >= 5)) {
      validationErrors.address = 'Ingrese Dirreccion preciso (con el No de la calle)'
    }

    // IF emergency checked - must enter at least number
    if (emergency) {
      validationErrors.emergencyNote = validate('placeDescription', emergencyNote)
      if (emergencyPhone) {
        validationErrors.emergencyPhone = validate('placePhone', emergencyPhone)
      } else {
        validationErrors.emergencyPhone = 'Ingrese un teléfono válido'
      }
    }

    // If address does not have coordinates
    // If editing location - we do not load location data IGNORE VALIDATION
    // See Place update saga
    if (!(location && location.length > 0) && !this.props.place) {
      validationErrors.address = 'Ingrese Dirrecion válido'
    }

    // console.tron.log({schedule, emptyDays: R.isEmpty(schedule.days), emptyHours: R.isEmpty(schedule.hours)} )
    if (schedule) {
      if (!schedule.days || R.isEmpty(schedule.days)) {
        validationErrors.schedule = 'Por favor establezca un horario de apertura'
      } else if (!schedule.h24 && (!schedule.hours || R.isEmpty(schedule.hours))) {
        validationErrors.schedule = 'Por favor establezca el rango de horario de trabajo'
      }
    }

    if (isSubmit) {
      // Mark all as touched on submit
      this.setState({touched: [
        'name', 'email', 'address', 'phone', 'website', 'description',
        'emergency', 'emergencyPhone', 'emergencyNote',
        'category', 'secondaryCategories', 'schedule'
      ],
        errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }
    /// ///////////////////////////////////////////////////////////// EOF FORM BOILERPLATE

  submitPlace = () => {
    if (this.formIsValid(true)) {
      const {placeAddRequest, placeUpdateRequest, place} = this.props
      const {location} = this.state

      // Filter values from state
      const values = R.pickAll([ 'name', 'email', 'address', 'phone', 'website', 'description',
        'emergency', 'emergencyPhone', 'emergencyNote', 'photos', 'address_components',
        'category', 'secondaryCategories', 'schedule'
      ], this.state)
      // Set empty values which are null
      const data = R.map((val) => val || '', values)

      if (place) {
        placeUpdateRequest({...data, id: place.id}, location)
      } else {
        placeAddRequest({...data}, location)
      }
    } else {
    }
  }

  // Region changed in Map
  // onRegionChange = (newRegion) => {
  //   this.setState({ location: [ newRegion.longitude, newRegion.latitude ] }, this.formIsValid)
  // }
  // Get coordinates from autocomplete
  // onGetCoordinates = ({lat, lng}) => { this.setState({location: [lat, lng]}) }

  // Get details from autocomplete
  onGetDetails = (details) => {
    if (details) {
      // FIXME: transform address components
      let {address_components} = details

      if (address_components && R.length(address_components) > 0) {
        address_components = R.map((comp) => { return {name: comp.long_name, types: comp.types} }
        , address_components)
      }

      // console.tron.log({address_components})
      const {lat, lng} = details.geometry.location
      this.setState({location: [lat, lng], address_components}, this.formIsValid)
    }
  }

  updatePhotos = (photos) => { this.setState({photos}) }

  handleSecondaryCategories = (value, selected) => {
    const {secondaryCategories} = this.state
    // Add / remove secondary category
    const newValues = selected ? R.append(value, secondaryCategories) : R.without([value], secondaryCategories)
    // Generate label for input
    const secondaryCategoriesLabels = Categories.idsToString(newValues)
    this.setState({secondaryCategories: newValues, secondaryCategoriesLabels})
  }

  isChecked = (value, selected) => {
    const {secondaryDays} = this.state
    // Add / remove secondary category
    const newValues = selected ? R.append(value, secondaryDays) : R.without([value], secondaryDays)

    this.setState({secondaryDays: newValues})
  }

  handleCategory = (value) => {
    const categoryLabel = Categories.idToString(value)
    // Subcategory cannot include CATEGORY
    const secondaryCategories = R.reject(R.equals(value), this.state.secondaryCategories)
    this.setField('categoryLabel', categoryLabel)
    this.setState({category: value, secondaryCategories})
  }

  handleSchedule = (schedule : PlaceSchedule) : void => {
    this.setField('schedule', schedule)
    this.setField('scheduleLabel', ScheduleToString(schedule))
  }

  render () {
    const {secondaryCategoriesLabels, secondaryDays, secondaryCategories} = this.state

    return (
      <ScrollView style={styles.container}>

        <PlaceCategoriesSelect
          show={this.state.showCategoriesSelect}
          hideModalFunc={() => this.setState({showCategoriesSelect: false})}

          mainValue={this.state.category}
          secondaryValues={this.state.secondaryCategories}

          onChangeMain={this.handleCategory}
          onPressSecondary={this.handleSecondaryCategories}
        />

        <ScheduleSelect
          show={this.state.showScheduleSelect}
          hideModalFunc={() => this.setState({showScheduleSelect: false})}

          value={this.state.schedule}
          onChange={this.handleSchedule}
         />

        <KeyboardAvoidingView behavior='padding'>

          { /* <View style={{height: 200, marginHorizontal: 20, borderWidth: 1, flex: 1}}>
            <SetLocationMap onRegionChangeCallback={this.onRegionChange} />
            <Text style={{color: 'gray'}}>{location[0]}{'\n'}{location[1]}</Text>
              </View> */ }
          <View style={styles.inputsWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'name'}
              placeholder='Nombre *'
              defaultValue={this.state.name}
          />
          </View>
          <View style={styles.inputsWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'email'}
              placeholder='Email '
              defaultValue={this.state.email}
          />
          </View>

          <View style={styles.inputsWrappers}>
            <GoogleAutocompleteInput
              parentComp={this}
              fieldName={'address'}
              // onGetCoordinates={this.onGetCoordinates}
              onGetDetails={this.onGetDetails}
              placeholder='Dirección *'
              defaultValue={this.state.address}
           />
          </View>

          <TouchableOpacity onPress={() => this.setState({showCategoriesSelect: true})}>
            <View style={styles.inputsWrappers} >
              <UserInput
                parentComp={this}
                value={this.state.categoryLabel} // Force value to categoryLabel
                fieldName={'category'}
                placeholder='Categoría *'
                editable={false}
                overlayItem={<View style={{position: 'absolute', right: 25, marginTop: 5}}>
                  <IconMD name='keyboard-arrow-right' style={{fontSize: 18, textAlign: 'center', textAlignVertical: 'center', color: '#939393'}} />
                </View>}
                />
            </View>
          </TouchableOpacity>

          {(secondaryCategories && !R.isEmpty(secondaryCategories)) &&
            <TouchableOpacity onPress={() => this.setState({showCategoriesSelect: true})}>
              <View style={styles.inputsWrappers} >
                <UserInput
                  parentComp={this}
                  value={this.state.secondaryCategoriesLabels} // Force value to categoryLabel
                  fieldName={'secondaryCategory'}
                  placeholder='Otras categorías'
                  editable={false}
                  overlayItem={<View style={{position: 'absolute', right: 25, marginTop: 5}}>
                    <IconMD name='keyboard-arrow-right' style={{fontSize: 18, textAlign: 'center', textAlignVertical: 'center', color: '#939393'}} />
                  </View>}
                  />
              </View>
            </TouchableOpacity>
          }

          <View style={styles.inputsWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'phone'}
              placeholder='Teléfono '
              defaultValue={this.state.phone}
          />
          </View>

          <View style={styles.inputsWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'website'}
              placeholder='Sitio Web'
              defaultValue={this.state.website}
          />
          </View>

          <TouchableOpacity onPress={() => this.setState({showScheduleSelect: true})}>
            <View style={styles.inputsWrappers}>
              <UserInput
                parentComp={this}
                fieldName={'schedule'}
                value={this.state.scheduleLabel} // Force value to categoryLabel
                placeholder='Horarios de atención'
                editable={false}
                overlayItem={<View style={{position: 'absolute', right: 25, marginTop: 5}}>
                  <IconMD name='keyboard-arrow-right' style={{fontSize: 18, textAlign: 'center', textAlignVertical: 'center', color: '#939393'}} />
                </View>}
            />
            </View>
          </TouchableOpacity>

          <View style={styles.inputsWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'description'}
              multiline
              maxLength={500}
              placeholder='Descripción '
              defaultValue={this.state.description}
          />
          </View>
          <View style={styles.tituloGaleria}>
            <Text style={styles.tituloGaleriaItem}>Galería de fotos</Text>
          </View>

          <View style={styles.placePhotoPicker}>
            <PlacePhotoPicker onUpdate={this.updatePhotos} defaultValue={this.state.photos} />
          </View>

          <View style={styles.emergencySwitcherContainer}>
            <View style={styles.emergencyTextItemWrapper}>
              <Text style={styles.emergencyTextItem}>Emergencias</Text>
            </View>
            {/*  <Text style={{color: 'gray'}}>{R.toString(this.state.emergency)}</Text> */}

            <View style={styles.switchItemWrapper}>
              <BasicSwitch value={this.state.emergency} onValueChange={(val) => this.setField('emergency', val)} />
            </View>
          </View>

          { this.state.emergency &&
          <View style={styles.inputsObservacioneEmergencyWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'emergencyPhone'}
              placeholder='Teléfono de emergencia'
              defaultValue={this.state.emergencyPhone}
          />
          </View>
          }

          { this.state.emergency &&
          <View style={styles.inputsObservacioneEmergencyWrappers}>
            <UserInput
              parentComp={this}
              fieldName={'emergencyNote'}
              placeholder='Observaciones'
              multiline
              maxLength={300}
              defaultValue={this.state.emergencyNote}
            />
          </View>
          }
          <View style={styles.crearEspacioButton}>
            <RoundButton
              text={this.props.place ? 'GUARDAR ESPACIO' : 'CREAR ESPACIO'}
              color='white'
              bgColor='#FF1744'
              onPress={this.submitPlace}
          />
          </View>

          <View style={{height: 50}} />

        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params} = props.navigation.state
  const id = params ? params.id : null
  // const id = '-L1Wl4uOsh5kFTZlSWKc'

  return {
    place: getPlace(state, id),
    user: getCurrentUser(state),
    error: getPlaceAddError(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    placeAddRequest: (...args) => dispatch(PlaceAddRedux.placeAddRequest(...args)),
    placeUpdateRequest: (...args) => dispatch(PlaceAddRedux.placeUpdateRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceEditScreen)
