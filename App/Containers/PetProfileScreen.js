import React, { Component } from 'react'
import { ScrollView, Text, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
import {getPet} from '../Redux/PetsRedux'
import PetsCarouselItem from '../Components/PetsCarouselItem'
import Icon from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal'

// FIXME: add new pet

// Styles
import styles from './Styles/PetProfileScreenStyle'
import {Images} from '../Themes'
import Background from '../Components/Background'
import PetOnboardSwiper from '../Components/PetOnboardSwiper'

class PetProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Perfil de mi mascota',
    headerTitleStyle: styles.headerTitle
  //   headerLeft: <Icon
  //     name='arrow-left'
  //     size={20}
  //     onPress={() => navigation.goBack()}
  //     style={{paddingLeft: 20}}
  // />
  });

  constructor (props) {
    super(props)
    this.state = {isModalVisible: props.showOnboard}
  }

  closeModal = () => {
    this.setState({isModalVisible: false})
  }

  render () {
    const {navigate} = this.props.navigation
    const {pet} = this.props
    const {id} = pet
    const {isModalVisible} = this.state
    const {closeModal} = this

    const options = {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: 'numeric',
      timeZone: 'America/Buenos_Aires'
    }
    const updatedAt = pet.updatedAt
    ? new Date(pet.updatedAt).toLocaleDateString('es-AR', options)
     : null

    return (
      <View style={styles.container} >

        <Background />

        <ScrollView style={styles.container} >
          <View style={styles.petCarouselItemContainer}>
            <PetsCarouselItem
              pet={pet}
              navigate={navigate}
              clickFunction={() => {}}
            />

          </View>

          <View style={styles.row}>
            <View style={styles.menuItem}>
              <TouchableOpacity style={styles.menuCont} onPress={() => { navigate('PetProfileEditScreen', {id}) }}>
                <Image style={styles.imageResizing} source={Images.pets.about} />
                <View style={styles.divider} />
                <Text style={styles.menuItemText}>Sobre {'\n'}mi mascota</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.menuItem}>
              <TouchableOpacity style={styles.menuCont} onPress={() => { navigate('PetMedicalInfoScreen', {id}) }}>
                <Image style={styles.imageResizing} source={Images.pets.medical} />
                <View style={styles.divider} />
                <Text style={styles.menuItemText}>Información médica</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.row}>

            <View style={styles.menuItem}>
              <TouchableOpacity style={styles.menuCont} onPress={() => navigate('PetVaccinesScreen', {id})}>
                <Image style={styles.imageResizing} source={Images.pets.vaccines} />
                <View style={styles.divider} />
                <Text style={styles.menuItemText}>Información {'\n'}de vacunas</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.menuItem}>
              <TouchableOpacity style={styles.menuCont} onPress={() => navigate('PetAlbums', {id})}>
                <Image style={styles.imageResizing} source={Images.pets.photos} />

                <View style={styles.divider} />
                <Text style={styles.menuItemText}>Album {'\n'}de fotos</Text>
              </TouchableOpacity>
            </View>
          </View>
          {(updatedAt) && <Text style={styles.updatedAt}>Actualizado{'\n'}{updatedAt}</Text>}
        </ScrollView>

        <Modal isVisible={isModalVisible}>
          <PetOnboardSwiper onPress={closeModal} />
        </Modal>

      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {id, showOnboard = false} = props.navigation.state.params
  return {
    pet: getPet(state, id),
    showOnboard: showOnboard
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetProfileScreen)
