// @flow
import React, { Component } from 'react'
import { View, ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import {getCurrentUser} from '../Redux/UserRedux'
import UserAvatar from '../Components/UserAvatar'
import styles from './Styles/UserProfileScreenStyle'
import NavBars from '../Components/NavBars'

type Props = {
  user: User
}

class UserProfileScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: `Perfil`,
    headerTitleStyle: styles.headerTitle,
    headerLeft: <NavBars onPress={() => navigation.navigate('DrawerOpen')} />,
    headerRight: <IconMD
      name='mode-edit'
      size={25}
      onPress={() => navigation.navigate('UserProfileEditScreen')}
      style={{paddingRight: 20}}
    />
  });

  render () {
    const {user} = this.props

    if (!user) return (<View />)

    const phone = user.phone || 'Sin teléfono'
    const email = user.email
    const address = user.address || 'Sin dirección'

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>

          <UserAvatar photoURL={user.photoURL} style={styles.avatar} />

          <Text style={styles.name}>{user.firstname + ' ' + user.lastname}</Text>

          <View style={styles.item}>
            <Icon name='home' style={styles.iconHouse} />
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.text} >{address}</Text>
          </View>
          <View style={styles.item}>
            <Icon name='envelope' style={styles.iconEmail} />
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.text}>{email}</Text>
          </View>
          <View style={styles.item}>
            <Icon name='phone' style={styles.iconPhone} />
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.text}>{phone}</Text>
          </View>

        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state): Props => {
  return {
    user: getCurrentUser(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileScreen)
