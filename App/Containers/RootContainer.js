// @flow
import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import UserActions, {getCurrentUser} from '../Redux/UserRedux'
import ReduxPersist from '../Config/ReduxPersist'
import Spinner from 'react-native-loading-spinner-overlay'
import styles from './Styles/RootContainerStyles'
import type {State} from '../Redux'
import { MenuContext } from 'react-native-popup-menu'

type Props = {
  fetching: boolean,
  fetchingText: string,
  // getCurrentUser: User,
  startup: Function,
  userLoggedIn: Function,
  userClear: Function
}

class RootContainer extends Component<Props> {
  componentDidMount () {
    // if redux persist is not active fire startup action
    const props = this.props
    if (!ReduxPersist.active) {
      props.startup()
    }
  }

  render () {
    // We are hiding status bar in styles.xml (bugs with modals in RN)
    return (
      <MenuContext >
        <View style={styles.applicationView}>
          <ReduxNavigation />

          <Spinner
            overlayColor={'rgba(0,0,0,0.7)'}
            visible={this.props.fetching}
            textContent={this.props.fetchingText}
            textStyle={{color: '#FFF'}}
          />
        </View>
      </MenuContext>
    )
  }
}

//
// When to show Activity indicator?
//
const getFetching = (state : State) : boolean => {
  if (state.login && state.login.fetching) { return true }
  if (state.userUpdate && state.userUpdate.fetching) { return true }
  if (state.userRegister && state.userRegister.fetching) { return true }

  if (state.pets && state.pets.fetching) { return true }
  if (state.petCreate && state.petCreate.fetching) { return true }
  if (state.petAlbumUpload && state.petAlbumUpload.fetching) { return true }
  if (state.vaccineSave && state.vaccineSave.fetching) { return true }
  if (state.medicalInfoSave && state.medicalInfoSave.fetching) { return true }

  if (state.placeAdd && state.placeAdd.fetching) { return true }
  if (state.place && state.place.fetching) { return true }
  // if (state.placesFindAll && state.plaesFindAll.fetching) { return true }

  if (state.noteSave && state.noteSave.fetching) { return true }
  if (state.noteDelete && state.noteDelete.fetching) { return true }

  if (state.placeReviewAdd && state.placeReviewAdd.fetching) { return true }

  if (state.lostFound && state.lostFound.fetching) { return true }
  if (state.filteredPlaces && state.filteredPlaces.fetching) { return true }
  if (state.placeSearch && state.placeSearch.fetching) { return true }
  if (state.placeClaim && state.placeClaim.fetching) { return true }
  if (state.placePhotoUpload && state.placePhotoUpload.fetching) { return true }

  if (state.filteredLostFound && state.filteredLostFound.fetching) { return true }

  return false
}

const getFetchingText = (state: State) : string => {
  if (state.placeSearch.fetching) {
    return 'Buscando...'
  } else {
    return 'Cargando...'
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) : Props => ({
  startup: () => dispatch(StartupActions.startup()),
  userLoggedIn: (user) => dispatch(UserActions.userLoggedIn(user)),
  userClear: () => dispatch(UserActions.userClear())
})

const mapStateToProps = (state) : Props => {
  return {
    fetching: getFetching(state),
    fetchingText: getFetchingText(state)
    // getCurrentUser: getCurrentUser(state)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)
