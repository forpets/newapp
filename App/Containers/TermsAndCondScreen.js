import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/TermsAndCondScreenStyle'
import Icon from 'react-native-vector-icons/FontAwesome'

class TermsAndCondScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // headerLeft: <Icon
      //   name='arrow-left'
      //   size={20}
      //   onPress={() => navigation.goBack()}
      //   style={{paddingLeft: 20}}
      // />
    }
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position' style={styles.container}>

          <Text style={styles.headerTitle}>Términos y Condiciones</Text>
          <Text style={styles.subTitle}>USO Y RESTRICCIONES.
            {'\n'}
            {'\n'}
          </Text>

          <Text style={styles.description}>
La utilización de la aplicación ("APP") expresa la adhesión plena y sin reservas del usuario a los presentes TÉRMINOS Y CONDICIONES DE USO. A través de la APP, el usuario accederá y/o utilizará diversos contenidos, puestos a disposición de los usuarios por Madelaine Ekserciyan (“LA TITULAR”).

Si Ud. no acepta en forma total los presentes TÉRMINOS Y CONDICIONES DE USO, LA TITULAR le ruega no utilizar y/o descargar la APP, ya que, de hacerlo, se entenderá que Ud. ha aceptado en forma total estos TÉRMINOS Y CONDICIONES DE USO.

LA TITULAR no garantiza la disponibilidad y continuidad de la operación de la APP y de los Contenidos, ni la utilidad de la APP o Contenidos en relación con ninguna actividad específica. LA TITULAR no será responsable por ningún daño o pérdida de cualquier naturaleza que pueda ser causado debido a la falta de disponibilidad o continuidad de operación de la APP  y/o de los Contenidos.

LA TITULAR se reserva el derecho a denegar, restringir, condicionar o retirar el acceso y/o descarga de la APP en cualquier momento y sin necesidad de preaviso, por iniciativa propia o a instancia de un tercero, a quienes incumplan estos TÉRMINOS Y CONDICIONES DE USO y/o los que, en su caso, resulten de aplicación.

LA TITULAR no se hace responsable por el contenido de los comentarios vertidos por los usuarios en la APP.

Los TÉRMINOS Y CONDICIONES DE USO podrán ser modificados en todo o en parte únicamente por LA TITULAR y dichos cambios e implementaciones tendrán vigencia a partir del momento mismo en que sean publicados o insertados en la APP o desde que sean notificados por cualquier medio, lo que ocurra primero.
Por ello, LA TITULAR le sugiere que cada vez que decida ingresar a la APP visite estos TÉRMINOS Y CONDICIONES DE USO y compruebe que no hayan sido modificados. Las violaciones de estos TÉRMINOS Y CONDICIONES DE USO generarán el derecho a LA TITULAR a suspender o impedir que los usuarios las hubieran realizado, por acción u omisión, puedan acceder  y/o utilizar y/o descargar la APP.
{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>CONTENIDOS.</Text>
            {'\n'}
            {'\n'}

LA TITULAR se reserva el derecho a modificar unilateralmente, en cualquier momento y sin aviso previo, el contenido, presentación y configuración de LA APP, así como las condiciones requeridas para el acceso y/o utilización y/o descarga de la misma.

LA TITULAR no garantiza la precisión, licitud, fiabilidad y utilidad de los CONTENIDOS y excluye toda responsabilidad por los daños y perjuicios de toda naturaleza que pudieran deberse a la transmisión, difusión, almacenamiento, puesta a disposición, recepción, obtención o acceso a los CONTENIDOS.

Los CONTENIDOS se ofrecen para la información de los usuarios, debiendo ser utilizados con los fines para los cuales fueron creados y puestos a su disposición.
{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>PROPIEDAD INTELECTUAL.</Text>
            {'\n'}
            {'\n'}
Los derechos de propiedad intelectual respecto de los Contenidos, las marcas, software, nombre de domino de la APP, así como los derechos de uso y explotación de los mismos, incluyendo su divulgación, publicación, reproducción, distribución y transformación, son propiedad exclusiva de LA TITULAR.

No obstante, ante la posible falencia en los  controles de calidad, si el usuario encontrase textos y/o imágenes que pudiesen pertenecer a terceros y dichos autores no fueron citados, deberá comunicarse a la brevedad y con carácter previo, con LA TITULAR, a efectos de recibir las aclaraciones correspondientes.

El usuario no adquiere ningún derecho de propiedad intelectual por el simple uso y/o descarga de los Contenidos de la APP y en ningún momento dicho uso y/o descarga será considerado como una autorización ni licencia para utilizar los Contenidos con fines distintos a los que se contemplan en los presentes TÉRMINOS Y CONDICIONES DE USO.
{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>USOS PERMITIDOS.</Text>
            {'\n'}
            {'\n'}
El uso y/o descarga y/o aprovechamiento de los Contenidos de la APP es exclusiva responsabilidad del usuario, quien en todo caso deberá servirse de ellos acorde a las funcionalidades permitidas en la propia APP y a los usos autorizados en los presentes TÉRMINOS Y CONDICIONES, por lo que el usuario se obliga a utilizarlos de modo tal que no atenten contra la legislación vigente en la República Argentina, las normas de uso y convivencia en Internet ni la legislación vigente en el país en que el usuario se encuentre al usarlos y/o descargarlos, la moral, las buenas costumbres y los derechos de terceros.

La APP es para el uso personal del usuario por lo que no podrá comercializar ni difundir de manera alguna los Contenidos.
{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>PROHIBICIONES.</Text>
            {'\n'}
            {'\n'}
El usuario no tiene el derecho de colocar o utilizar los Contenidos de la APP en aplicaciones, sitios o páginas web propias o de terceros sin autorización previa y por escrito de LA TITULAR. Asimismo, el usuario no tendrá el derecho de limitar o impedir a cualquier otro usuario el uso y/o acceso  y/o descarga de la APP.

Los usuarios se obligan a usar los Contenidos en forma diligente, correcta y lícita y, en particular, se comprometen a no reproducir o copiar, distribuir, permitir el acceso del público a través de cualquier modalidad de comunicación pública, transformar o modificar los CONTENIDOS, a menos que se cuente con la autorización expresa y escrita de LA TITULAR y/o del titular de los correspondientes derechos; no suprimir, eludir o manipular o violar, por cualquier medio, los derechos intelectuales y/o industriales y demás datos identificativos de los derechos de LA TITULAR incorporados a los CONTENIDOS, así como los dispositivos técnicos de protección que pudieren contener los CONTENIDOS; no emplear los CONTENIDOS y, en particular, la información de cualquier clase obtenida a través de la APP para remitir publicidad, comunicaciones con fines de venta directa o con cualquier otra clase de finalidad comercial, mensajes no solicitados dirigidos a una pluralidad de personas con independencia de su finalidad, así como comercializar o divulgar de cualquier otro modo dicha información.
{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>GARANTÍA DE INDEMNIDAD.</Text>
            {'\n'}
            {'\n'}
Los usuarios prestan expresa conformidad a indemnizar y mantener indemne a LA TITULAR y/o a cualquiera de sus dependientes, licenciantes, proveedores y licenciatarios, de y contra toda pérdida, gasto, daño y costos que se originen por cualquier violación a las leyes y normas aplicables, y a los presentes TÉRMINOS Y CONDICIONES DE USO cuando dicha violación sea llevada a cabo, por acción u omisión, por el usuario y/o por cualquier persona que acceda al APP utilizando la cuenta y/o dispositivos técnicos pertenecientes a los mismos.

Los usuarios responderán por los daños y perjuicios de toda naturaleza que LA TITULAR y/o la APP puedan sufrir como consecuencia del incumplimiento de cualquiera de las obligaciones a las que queda sometido al aceptar estos TÉRMINOS Y CONDICIONES DE USO.

Los usuarios prestan expresa conformidad de mantener indemne a LA TITULAR respecto de los comentarios por ellos vertidos en la APP.

{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>CONFIDENCIALIDAD.</Text>
            {'\n'}
            {'\n'}

LA TITULAR se obliga a mantener confidencial la información que reciba del usuario que tenga dicho carácter conforme a las disposiciones legales aplicables a la República Argentina.

LA TITULAR no asume ninguna obligación de mantener confidencial cualquier otra información que el usuario le proporcione, ya sea al descargar y/o utilizar la APP o en cualquier otro momento posterior, incluyendo aquella información que el usuario proporcione a través de comentarios, foros, chats, video conferencias.

{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>USO DE LA INFORMACIÓN NO CONFIDENCIAL.</Text>
            {'\n'}
            {'\n'}

Mediante el uso de la APP, el usuario autoriza a LA TITULAR a utilizar, publicar, reproducir, divulgar, comunicar públicamente y transmitir la información no confidencial.

{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>NOMBRES DE USUARIO Y CLAVES DE ACCESO.</Text>
            {'\n'}
            {'\n'}

En todo momento, el usuario es el responsable único y final de mantener en secreto la identificación de su usuario, contraseñas personales, claves de acceso y/o de descarga y otros datos confidenciales con los cuales tenga acceso y/o descargue los Contenidos de la APP.

{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>NOTIFICACIONES y COMUNICACIONES</Text>
            {'\n'}
            {'\n'}

A los fines que los usuarios puedan tomar contacto con LA TITULAR se considerarán válidas las comunicaciones dirigidas a madelaine.eks@hotmail.com.

Las notificaciones y comunicaciones cursadas  por LA TITULAR a la casilla de correo electrónico que surja como dirección de correo del usuario se considerarán eficaces y plenamente válidas.

Asimismo se considerarán eficaces las comunicaciones que consistan en avisos y/o mensajes insertos por LA TITULAR en la APP, o que envíe LA TITULAR a los usuarios por cualquier medio de comunicación, que tengan por finalidad informar a los usuarios sobre determinada circunstancia.

Si Ud. tiene alguna consulta que realizar sobre la interpretación de estos TERMINOS Y CONDICIONES DE USO, sugerencias o ayuda, envíe un e-mail a LA TITULAR: madelaine.eks@hotmail.com o escriba a Lavalle 1772, 2° Piso, Oficina 12, C.A.B.A., C. P. 1048

{'\n'}
            {'\n'}
            <Text style={styles.subTitle}>LEYES APLICABLES Y JURISDICCIÓN.</Text>
            {'\n'}
            {'\n'}

Para la interpretación, cumplimiento y ejecución de los presentes TERMINOS Y CONDICIONES DE USO, LA TITULAR y los usuarios acuerdan que serán aplicables las leyes a la República Argentina y competentes los Tribunales Nacionales en lo Civil y Federal de la Ciudad Autónoma de Buenos Aires,  renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera corresponderles en razón de sus domicilios presentes o futuros.
{'\n'}
            {'\n'}
            {'\n'}
          </Text>
        </KeyboardAvoidingView>

      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TermsAndCondScreen)
