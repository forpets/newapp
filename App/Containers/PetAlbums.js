import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, View, Image, FlatList, BackAndroid } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { connect } from 'react-redux'
import {getPet} from '../Redux/PetsRedux'
import styles from './Styles/PetAlbumsStyle'
import { getAlbumForPet } from '../Redux/PetAlbumsRedux'
import PetAlbumUploadRedux from '../Redux/PetAlbumUploadRedux'
import Background from '../Components/Background'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

var options = {
  title: 'Subir foto de tu mascota',
  takePhotoButtonTitle: 'Tomar una foto',
  chooseFromLibraryButtonTitle: 'Seleccionar una foto',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  allowEditing: true,
  noData: true,
  quality: 0.7,
  maxWidth: 1000,
  maxHeight: 1000,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

class PetAlbums extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Album de fotos ',
    headerTitleStyle: styles.headerTitleStyle,
    headerLeft: <IconMD
      name='arrow-back'
      size={25}
      onPress={() => navigation.goBack()}
      style={{paddingLeft: 20}}
  />
  });

  constructor (props) {
    super(props)
    this.state = {}
  }

  saveImage = () => {
    const {pet} = this.props

    ImagePicker.showImagePicker(options, (response) => {
      const {petAlbumUploadRequest} = this.props

      if (response.didCancel) {
        console.tron.warn('User cancelled image picker')
      } else if (response.error) {
        console.tron.warn('ImagePicker Error: ', response.error)
      } else {
        console.tron.warn({msg: 'Image object selected ', resp: response})
        if (response.uri) { petAlbumUploadRequest({imagePickerResponse: response, pet}) }
        this.setState({ avatarSource: response })
      }
    })
  }

  render () {
    const {pet, album, navigation, petAlbumUpload} = this.props
    const {navigate} = navigation
    var listAlbum = []

    // Obtenemos el resultado de upload
    const {fetching, error, payload} = petAlbumUpload
    // FIXME: if fetching - vamos a mostrar algo
    // FIXME: if error -m ostra popup o algo

    if (album && album.length > 0) {
      listAlbum = album.slice()

      // console.tron.log('album length ' + album.length + ' listAlbum length ' + listAlbum.length)

      if (listAlbum.length === album.length) {
        listAlbum.unshift({id: 'Nuevo', image: 'x'})
      }

      return (
        <View style={styles.container}>
          <Background />
          <ScrollView contentContainerStyle={styles.container2} >
            <FlatList
              style={styles.flatListContainer}
              numColumns={4}
              data={listAlbum}
              extraData={this.props}
              renderItem={({item}) => <TouchableOpacity style={styles.crossContainer} onPress={item.id === 'Nuevo' ? this.saveImage : () => navigate('PetAlbumFotoScreen', {idPet: pet.id, albumItem: item})}>
                { item.id === 'Nuevo'
                                              ? <View style={styles.plusContainer}>
                                                <IconMD name='add' style={{color: 'white', fontSize: 60}} />
                                              </View>
                                              : <Image source={{uri: item.image}} style={styles.petAvatar} />
                                              }
              </TouchableOpacity>
                                 }
              keyExtractor={(item) => item.id}
                  />
          </ScrollView>
        </View>
      )
    } else {
      return (

        <View style={styles.container}>
          <Background />
          <ScrollView style={styles.container}>
            <View style={styles.splashContainer}>
              <Image style={styles.image} source={require('../Images/Mascotas/mascotas_empty_fotos.png')} />
              <Text style={styles.title}>No tenés fotos todavía</Text>
              <Text style={styles.subTitle}>
                Cargá todas las fotos de tu mascota para{'\n'}que las puedas ver cuando quieras
              </Text>
              <TouchableOpacity onPress={this.saveImage}>
                <View style={styles.startButton}>
                  <Text style={styles.startButtonText} >EMPEZAR</Text>
                </View>
              </TouchableOpacity>
            </View>

          </ScrollView>
        </View>
      )
    }
  }
}

const mapStateToProps = (state, props) => {
  const {id = false} = props.navigation.state.params
  return {
    pet: getPet(state, id),
    album: getAlbumForPet(state, id),
    petAlbumUpload: state.petAlbumUpload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    petAlbumUploadRequest: (data) => { dispatch(PetAlbumUploadRedux.petAlbumUploadRequest(data)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetAlbums)
