// @flow
import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, Alert } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/ReviewNewScreenStyle'
import UserInput from '../Components/UserInput'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import RoundButton from '../Components/RoundButton'
import R from 'ramda'
import validate from '../Lib/Validate'
import ReviewPricingInput from '../Components/ReviewPricingInput'
import ReviewRatingInput from '../Components/ReviewRatingInput'
import PlaceReviewAddRedux, { getPlaceReviewAddError } from '../Redux/PlaceReviewAddRedux'
import { getCurrentUser } from '../Redux/UserRedux'

type Props = {
  placeId: string,
  user: User,
  navigation: Object,
  placeReviewAddRequest: Function,
  error: boolean
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  rating?: number,
  pricing?: number,
  comment?: string,
}

class ReviewNewScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  });

  constructor (props) {
    super(props)

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true
    }
  }

  /// //////////////////////////////////////////////////////////// FORM BOILERPLATE
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    fieldValue = fieldValue || null
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
    // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean = false) : boolean => {
    const {touched, rating, pricing, comment} = this.state

    // FIXME:
    const validationErrors = {
      rating: validate('reviewRating', rating),
      pricing: validate('reviewPricing', pricing),
      comment: validate('reviewComment', comment)
    }

    // if (!pricing && !rating && !comment) {
    // Rating or Comment required - on order not to show empy reviews
    if (!rating && !comment) {
      validationErrors.comment = 'Escribi tu comentario'
    }

    if (isSubmit) {
      // Mark all as touched on submit
      this.setState({touched: [ 'rating', 'pricing', 'comment' ], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }
    /// ///////////////////////////////////////////////////////////// EOF FORM BOILERPLATE

  componentWillReceiveProps (nextProps) {
    const {error} = nextProps
    if (!this.props.error && error) {
      Alert.alert('Error', 'Solicitud de error en proceso. Por favor intente mas tarde')
    }
  }

  goBack = () => {
    if (this.state.touched.length > 0) {
      Alert.alert('Atención', 'Si cancela se perderán los datos', [
        {text: 'SI', onPress: () => this.props.navigation.goBack()},
        {text: 'NO', style: 'cancel'}
      ])
    } else {
      this.props.navigation.goBack()
    }
  }

  submit = () => {
    if (this.formIsValid(true)) {
      const {user, placeId, placeReviewAddRequest} = this.props
      const data = R.pickAll(['rating', 'pricing', 'comment'], this.state)
      placeReviewAddRequest({...data, userId: user.uid, placeId})
    } else {
      // Button should be disabled
      console.tron.log(this.state.validationErrors)
    }
  }

  handlePricingInput = (pricing) => { this.setField('pricing', pricing) }
  handleRatingInput = (rating) => { this.setField('rating', rating) }

  render () {
    const {placeId} = this.props

    if (!placeId) {
    // FIXME: no place ID - alert and go back
      return (<View><Text>Not found</Text></View>)
    }

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View style={styles.mainContainer}>

            <View style={styles.titleWrapper}>
              <IconMD name='arrow-back' size={25} onPress={this.goBack} style={styles.iconArrow} />
            </View>

            {/*  here STARTS the header section */}
            <View style={styles.titleAndSubtitleContainer}>
              <View style={styles.titleWrapper}>
                <Text style={styles.title}>Dejá un comentario</Text>
              </View>
              <View style={styles.subTitleWrapper}>
                <Text style={styles.subTitle}>Valoración</Text>
              </View>
            </View>
            {/*  here ENDS the header section */}

            {/*  here STARTS the PRICE section */}
            <View style={styles.priceContainer}>
              <View style={styles.priceTextWrapper}>
                <Text style={styles.subTitlePrice}>Precio</Text>
              </View>

              <View style={styles.pricingIconsContainer}>
                <ReviewPricingInput onPress={this.handlePricingInput} value={this.state.pricing} />
              </View>

            </View>

            <View style={styles.questionWrapper}>
              <Text style={styles.question}>¿Qué valor le darías al servicio/lugar </Text>
            </View>
            {/*  here ENDS the PRICE section */}

            {/*  here STARTS the SERVICES (ATENCIÓN) section */}
            <View style={styles.serviceContainer}>
              <View style={styles.serviceTextWrapper}>
                <Text style={styles.subTitleServices}>Atención</Text>
              </View>

              <ReviewRatingInput onPress={this.handleRatingInput} value={this.state.rating} />

            </View>
            <View style={styles.questionWrapper}>
              <Text style={styles.question}>¿Lo recomendarías? Dejales tu puntaje</Text>
            </View>
            {/*  here ENDS the SERVICES (ATENCIÓN) section */}

            {/*  here STARTS the COMMENTS section */}
            <View style={styles.commentsContainer}>
              <View style={styles.subTitleCommentsWrapper}>
                <Text style={styles.subCommentsTitle}>Comentario</Text>
              </View>
              <View style={styles.input}>
                <UserInput
                  placeholderTextColor='#ABABAB'
                  parentComp={this}
                  fieldName={'comment'}
                  hideTopPlaceholder
                  placeholder='Escribí tu comentario acá '
                  multiline
                  maxLength={300}
                  />
              </View>
            </View>
            {/*  here ENDS the COMMENTS section */}

            {/*  here STARTS the FOOTER section */}
            <View style={styles.footerContainer}>
              <View style={styles.cancelarButtonContainer}>
                <RoundButton
                  onPress={this.goBack}
                  bgColor='white'
                  color='gray'
                  text='CANCELAR'
                 />
              </View>
              <View style={styles.enviarButtonContainer}>
                <RoundButton
                  disabled={this.state.submitDisabled}
                  onPress={this.submit}
                  bgColor='#FF1744'
                  bgColorDisabled='#ffb1c0'
                  color='white'
                  text='ENVIAR'
                 />
              </View>

            </View>
            {/*  here ENDS the FOOTER section */}

          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params = {}} = props.navigation.state
  const {placeId = null} = params

  return {
    placeId,
    user: getCurrentUser(state),
    error: getPlaceReviewAddError(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    placeReviewAddRequest: (...args) => dispatch(PlaceReviewAddRedux.placeReviewAddRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewNewScreen)
