import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View } from 'react-native'
import { connect } from 'react-redux'
import { Menu, MenuOptions, MenuOption, MenuTrigger, MenuContext } from 'react-native-popup-menu'
import styles from './Styles/DevScreenStyle'
import UserInput from './../Components/UserInput'
import UserRadioButton from '../Components/UserRadioButton'
import UserDateInput from '../Components/UserDateInput'
import PetPhotoPicker from '../Components/PetPhotoPicker'
import GoogleAutocompleteInput from '../Components/GoogleAutocompleteInput'
import MapListSwitch from '../Components/MapListSwitch'
import PlaceCategoriesSelect from '../Components/PlaceCategoriesSelect'
import ScheduleSelect from '../Components/ScheduleSelect'
import PlaceStatus from '../Components/PlaceStatus'

const diagnosticBorder = {borderWidth: 1, borderColor: 'red', padding: 0, margin: 0}

class DevScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  })

        // <PlaceCategoriesSelect />
  render () {
    return (
      <ScrollView style={styles.container}>
        <PlaceStatus  />
      </ScrollView>
    )
  }

  albumRender () {
    const diagnosticBorder = {borderWidth: 1, borderColor: 'red', padding: 0, margin: 0}

    const genders = [
      {value: 'hembra', label: 'HEMBRA'},
      {value: 'macho', label: 'MACHO'}
    ]

    return (
      <ScrollView style={styles.container}>

        <Text>DevScreen</Text>

        <Text>UserInput</Text>
        <View style={diagnosticBorder}>
          <UserInput
            parentComp={mockComponentDefault}
            placeholder='Placeholder asd'
          />
        </View>

        <View style={diagnosticBorder}>
          <UserInput
            parentComp={mockComponentError}
            placeholder='Placeholder asd'
        />
        </View>

        <View style={diagnosticBorder}>
          <UserInput
            parentComp={mockComponentError}
            placeholder='Placeholder asd'
            defaultValue='Some value'
          />
        </View>

        <Text>UserDateInput</Text>
        <View style={diagnosticBorder}>
          <UserDateInput
            parentComp={mockComponentDefault}
            fieldName='birthdate'
            placeholder='Fecha de nacimiento'
            defaultValue='12/12/2017' />
        </View>

        <View style={diagnosticBorder}>
          <UserDateInput
            parentComp={mockComponentDefault}
            fieldName='birthdate'
            placeholder='Fecha de nacimiento'
            defaultValue='' />
        </View>

        <View style={diagnosticBorder}>
          <UserDateInput
            parentComp={mockComponentError}
            fieldName='birthdate'
            placeholder='Fecha de nacimiento'
            defaultValue='' />
        </View>

        <View style={diagnosticBorder}>
          <UserDateInput
            style={styles.birthDate}
            parentComp={mockComponentDefault}
            fieldName='birthdate'
            placeholder='Fecha de nacimiento'
            defaultValue='' />
        </View>

        <Text>User Radio Button</Text>
        <View style={diagnosticBorder}>
          <UserRadioButton
            label='Sexo'
            fieldName='gender'
            parentComp={mockComponentDefault}
            defaultValue='hembra'
            options={genders}
            buttonInnerColor='#ffd102'
    />
        </View>

      </ScrollView>

    )
  }
 }

const mockComponentError = {
  getError: (fieldName : string) => 'Some asda serror:',
  isFocused: (fieldName : string) => false,
  setField: (fieldName : string, fieldValue : string) => { }
}

const mockComponentDefault = {
  getError: (fieldName : string) => '',
  isFocused: (fieldName : string) => false,
  setField: (fieldName : string, fieldValue : string) => { }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DevScreen)
