import React, { Component } from 'react'
import { View, TouchableOpacity, ScrollView, Text, KeyboardAvoidingView, Alert } from 'react-native'
import { connect } from 'react-redux'
import GrayButton from '../Components/GrayButton'
import LoginInput from '../Components/LoginInput'
import BasicTextButton from '../Components/BasicTextButton'
import validate from '../Lib/Validate'
import R from 'ramda'
import UserAvatar from '../Components/UserAvatar'
// import Icon from 'react-native-vector-icons/FontAwesome'
import UserRegisterRedux from '../Redux/UserRegisterRedux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/RegisterScreenStyle'
import UserPhotoPicker from '../Components/UserPhotoPicker'

type Props = {}

type State = {
  fieldFocused: string,
  touched: Array,
  errors: Object,
  submitDisabled: boolean,
  imagePickerResponse: Object,

  firstname: string,
  lastname: string,
  email: string,
  password: string,
  passwordConfirm: string
}

class RegisterScreen extends Component<Props, State> {
  static navigationOptions = {
    header: null,
    drawerLockMode: 'locked-closed'
  }

  constructor (props) {
    super(props)
    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,
      imagePickerResponse: null
    }

    // development only 
    if (__DEV__ && true) {
      this.state.firstname = 'What'
      this.state.lastname = 'What'
      this.state.email = '4pets4@yopmail.com'
      this.state.password = 'damian'
      this.state.passwordConfirm = 'damian'
    }
  }

  componentWillReceiveProps (nextProps) {
    // Get email already in use error
    const {userRegisterError} = nextProps

    // FIXME: SHOW GLOBAL ERRORES?
    // FIXME: bugfix this one... if repeats with email in use... does not show error
    if (userRegisterError) {
      // errorMsg = 'No se puede registrar nuevo usuario'
      if (userRegisterError === 'auth/email-already-in-use') {
        this.setState({errors: {email: 'Email esta ocupado'}})
      }
    }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    this.setState({[fieldName]: fieldValue, touched, fieldFocused: ''}, () => this.formIsValid(false))
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
    // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean) : boolean => {
    const {touched, firstname, lastname, email, password, passwordConfirm} = this.state
    let validationErrors = {}

      // Validated fields
    validationErrors.firstname = validate('name', firstname)
    validationErrors.lastname = validate('lastname', lastname)
    validationErrors.email = validate('email', email)
    validationErrors.password = validate('password', password)
    validationErrors.passwordConfirm = validate('password', passwordConfirm)

    if (password !== passwordConfirm && !validationErrors.passwordConfirm) {
      validationErrors.passwordConfirm = 'No son iguales'
      validationErrors.password = 'No son iguales'
    }

    if (isSubmit) {
      this.setState({touched: ['email', 'firstname', 'lastname', 'passwordConfirm', 'password'], errors: validationErrors})
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors})
    }

    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    this.setState({submitDisabled: !isValid})
    return isValid
  }

  register = () => {
    const {registerUser} = this.props

    if (this.formIsValid(true)) {
      const {firstname, lastname, email, password, imagePickerResponse} = this.state
      registerUser({firstname, lastname, email, password}, imagePickerResponse)
    }
  }

  onPhotoSelected = (imagePickerResponse : Object) : void => {
    this.setState({imagePickerResponse})
  }

  render () {
    const {navigation, userRegisterError} = this.props
    const {submitDisabled} = this.state
    const {register} = this
    // FIXME: show register errror

    // console.tron.warn(JSON.stringify(this.state))

    return (
      <ScrollView style={[styles.container, styles.background]}>
        <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={30}>

          <View style={styles.backArrow}>
            <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
              <IconMD name='arrow-back' style={{fontSize: 25, color: 'white'}} />
            </TouchableOpacity>
          </View>

          <Text style={styles.headerTitle}>Registrate {'\n'}en ForPets</Text>

          <UserPhotoPicker onPhotoSelected={this.onPhotoSelected}
            bottomComponent={<Text style={styles.textProfilePic}>CARGAR FOTO</Text>}
            containerStyle={{width: 100, height: 100, elevation: 0}}
            showDefaultImage={false}
          />

          <LoginInput
            parentComp={this}
            fieldName='firstname'
            placeholder='Nombre *'
            defaultValue={this.state.firstname}
            onSubmitEditing={() => this.focusTo('lastname')}
          />

          <LoginInput
            parentComp={this}
            fieldName='lastname'
            placeholder='Apellido *'
            defaultValue={this.state.lastname}
            onSubmitEditing={() => this.focusTo('email')}
          />

          <LoginInput
            parentComp={this}
            fieldName='email'
            placeholder='E-mail *'
            defaultValue={this.state.email}
            onSubmitEditing={() => this.focusTo('password')}
          />

          <LoginInput
            parentComp={this}
            fieldName='password'
            placeholder='Contraseña *'
            defaultValue={this.state.password}
            secureTextEntry
            onSubmitEditing={() => this.focusTo('passwordConfirm')}
          />

          <LoginInput
            parentComp={this}
            fieldName='passwordConfirm'
            placeholder='Repetir Contraseña *'
            secureTextEntry
            defaultValue={this.state.passwordConfirm}
            onSubmitEditing={register}
          />

          <BasicTextButton
            onPress={() => navigation.navigate('TermsAndCondScreen')}
            text='Al registrarte aceptas los Terminos y Condiciones de uso del servicio'
            style={styles.terminos}
           />

          <GrayButton style={styles.button} text='INGRESAR' onPress={register} />
          <View style={styles.space} />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userRegisterError: state.userRegister.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerUser: (...args) => dispatch(UserRegisterRedux.userRegisterRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
