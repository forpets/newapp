import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, Image, View, InteractionManager } from 'react-native'
import { connect } from 'react-redux'
import {Images} from '../Themes'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import {getVaccinesArray} from '../Redux/VaccineSaveRedux'
import styles from './Styles/PetVaccinesScreenStyle'
import { getPet } from '../Redux/PetsRedux'
import RoundButton from '../Components/RoundButton'
import Background from '../Components/Background'
import PetVaccinesList from '../Components/PetVaccinesList'
import R from 'ramda'

class PetVaccinesScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state

    const {addNewVaccine} = params

    return {
      title: `Información de Vacunas`,
      headerTitleStyle: styles.headerTitleStyle,
      headerRight: <IconMD
        name='add'
        size={25}
        onPress={addNewVaccine}
        style={{paddingRight: 20}}
        />
    }
  }

  componentDidMount () {
    const {addNewVaccine, props} = this
    InteractionManager.runAfterInteractions(() => {
      props.navigation.setParams({addNewVaccine})
    })
  }

  addNewVaccine = () => {
    this.props.navigation.navigate('PetVaccineNewScreen', {petId: this.props.pet.id})
  }

  render () {
    let {vaccines, navigation} = this.props
    const byDate = R.comparator((a, b) => a.applicationDate < b.applicationDate);
    vaccines = R.sort(byDate, vaccines)

    const SUBTITLE = 'Comenza a cargar las vacunas de tu\nmascota para llevar un mejor control\nde su salud'

    // If no vaccines present
    if (vaccines.length === 0) {
      return (
        <View style={styles.container}>
          <Background />
          <View style={styles.subcontainer}>
            <Image source={Images.pets.vaccines} style={styles.image} />
            <Text style={styles.title}>No tenes info todavia</Text>
            <Text style={styles.subtitle}>{SUBTITLE}</Text>
            <View style={styles.startButton}>
              <RoundButton
                text='CARGAR VACUNAS'
                color='white'
                bgColor='#00B0FF'
                onPress={this.addNewVaccine}
              />
            </View>

          </View>
        </View>
      )
    } else {
      // Vaccines present!
      return (
        <View style={styles.container}>
          <Background />
          <PetVaccinesList data={vaccines} navigation={navigation} />
        </View>
      )
    }
  }
}

const mapStateToProps = (state, props) => {
  const {id} = props.navigation.state.params
  return {
    pet: getPet(state, id),
    vaccines: getVaccinesArray(state, id)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetVaccinesScreen)
