// @flow
import React, { Component } from 'react'
import { ScrollView, Text, View, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/PetMedicalInfoScreenStyle'
import R from 'ramda'
import validate from '../Lib/Validate'
import UserInput from '../Components/UserInput'
import UserPickerInput from '../Components/UserPickerInput'
import RoundButton from '../Components/RoundButton'
import Background from '../Components/Background'
import { getPet } from '../Redux/PetsRedux'
import MedicalInfoSaveRedux from '../Redux/MedicalInfoSaveRedux'
import PetAttributes from '../Services/PetAttributes'

type Props = {
  pet: Pet,
  navigation: Object,
  medicalInfoSave: Function,
  petCastratedOptions: Array<Object>,
  petFoods: Array<Object>
}

type State = {
  fieldFocused: string,
  touched: Array<string>,
  errors: Object,
  submitDisabled: boolean,

  weight: string,
  color: string,
  food: string,
  veterinaryInfo: string,
  castrated: string,
  disease: string,
  treatment: string,
  medicine: string,
  alergies: string,

}

class PetMedicalInfoScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Información médica',
      headerTitleStyle: styles.headerTitle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={() => navigation.goBack()}
        style={{paddingLeft: 20}}
      /> }
  };

  constructor (props) {
    super(props)
    const {pet, navigation} = props

    if (!pet) {
      console.tron.error('PetMedicalInfoScreen - pet not found')
      navigation.goBack()
    }

    const {medicalInfo = {}} = pet

    this.state = {
      fieldFocused: '',
      touched: [],
      errors: {},
      submitDisabled: true,

      weight: medicalInfo.weight || null,
      color: medicalInfo.color || null,
      food: medicalInfo.food || null,
      veterinaryInfo: medicalInfo.veterinaryInfo || null,
      castrated: medicalInfo.castrated || null,
      disease: medicalInfo.disease || null,
      treatment: medicalInfo.treatment || null,
      medicine: medicalInfo.medicine || null,
      alergies: medicalInfo.alergies || null
    }
  }

  // BASIC FORM STUFF
  isFocused = (fieldName : string) => { return (this.state.fieldFocused === fieldName) }
  focusTo = (fieldName : string) => { this.setState({fieldFocused: fieldName}) }
  setField = (fieldName : string, fieldValue : string) => {
    let { touched } = this.state
    if (touched.indexOf(fieldName) === -1) touched.push(fieldName)
    let fieldVal = fieldValue

    if (fieldName === 'weight') {
      fieldVal = (fieldVal && typeof fieldVal === 'string') ? fieldVal.replace(',', '.') : null
    }
    this.setState({[fieldName]: fieldVal, touched, fieldFocused: ''}, this.formIsValid)
  }
  getError = (fieldName: string) => { return this.state.errors[fieldName] }
     // VALIDATION PROCESSOR
  formIsValid = (isSubmit: boolean= false) : boolean => {
    const {touched, weight, color, food, veterinaryInfo, castrated, disease, treatment, medicine, alergies} = this.state
    let validationErrors = {}

       // Validated fields
    validationErrors.weight = validate('petWeight', weight)
    validationErrors.veterinaryInfo = validate('petVeterinaryInfo', veterinaryInfo)
    validationErrors.disease = validate('petVeterinaryInfo', disease)
    validationErrors.treatment = validate('petVeterinaryInfo', treatment)
    validationErrors.medicine = validate('petVeterinaryInfo', medicine)
    validationErrors.alergies = validate('petVeterinaryInfo', alergies)

    if (isSubmit) {
      this.setState({
        touched: ['weight', 'color', 'food', 'veterinaryInfo', 'castrated', 'disease', 'treatment', 'medicine', 'alergies'],
        errors: validationErrors,
        submitDisabled: false
      })
    } else {
      const errors = R.pick(touched, validationErrors)
      this.setState({errors, submitDisabled: false})
    }
    const isValid = R.isEmpty(R.reject(R.isNil, validationErrors))
    // this.setState({submitDisabled: !isValid})
    return isValid
  }

  save = () => {
    const {medicalInfoSave, pet} = this.props

    if (this.formIsValid(true)) {
      const data = R.pick(['weight', 'color', 'food', 'veterinaryInfo', 'castrated', 'disease', 'treatment', 'medicine', 'alergies'], this.state)
      medicalInfoSave(data, pet.id)
    } else {
      console.tron.display({name: 'MedicalInfo', important: true, preview: 'Form invalid', value: {state: this.state}})
    }
  }

  render () {
    const {submitDisabled} = this.state
    const {updateBreeds, save} = this

    return (
      <View style={styles.container}>
        <Background />
        <ScrollView style={[styles.container, {paddingTop: 10}]}>

          <KeyboardAvoidingView behavior='padding'>
            <View style={styles.inputPesoContainer}>
              <View style={styles.inputPeso}>
                <UserInput
                  parentComp={this}
                  fieldName='weight'
                  placeholder='Peso'
                  keyboardType='numeric'
                  defaultValue={this.state.weight}
                  maxLength={6}
                />

              </View>
              <View style={styles.textKg}><Text>Kg</Text></View>
            </View>

            <UserPickerInput
              placeholder='Color'
              fieldName='color'
              parentComp={this}
              selectedValue={this.state.color}
              optionsForSelect={this.props.petColors}
            />

            <UserPickerInput
              placeholder='Alimento'
              fieldName='food'
              parentComp={this}
              selectedValue={this.state.food}
              optionsForSelect={this.props.petFoods}
            />

            <UserInput
              parentComp={this}
              fieldName='veterinaryInfo'
              placeholder='Veterinario de cabecera'
              defaultValue={this.state.veterinaryInfo}
              maxLength={300}
              multiline
            />

            <Text style={styles.footInput}>
            Cargá todos los datos de contacto que tengas{'\n'}
            (Nombre, teléfono , e-mail, etc )
            </Text>

            <UserPickerInput
              placeholder='Castrado'
              fieldName='castrated'
              parentComp={this}
              selectedValue={this.state.castrated}
              optionsForSelect={this.props.petCastratedOptions}
        />

            <UserInput
              parentComp={this}
              fieldName='disease'
              placeholder='Enfermedad'
              defaultValue={this.state.disease}
              onSubmitEditing={() => this.focusTo('treatment')}
              maxLength={300}
              multiline
        />
            <UserInput
              parentComp={this}
              fieldName='treatment'
              placeholder='Tratamiento médico'
              defaultValue={this.state.treatment}
              onSubmitEditing={() => this.focusTo('medicine')}
              maxLength={300}
              multiline
        />
            <UserInput
              parentComp={this}
              fieldName='medicine'
              placeholder='Medicación'
              defaultValue={this.state.medicine}
              onSubmitEditing={() => this.focusTo('alergies')}
              maxLength={300}
              multiline
        />

            <UserInput
              parentComp={this}
              fieldName='alergies'
              placeholder='Alergias'
              defaultValue={this.state.alergies}
              maxLength={300}
              multiline
              returnKeyType='done'
        />

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginBottom: '2%'}}>
              <RoundButton
                bgColor='#00C853'
                bgColorDisabled='#99ffc3'
                color='white'
                disabled={submitDisabled}
                text='  GUARDAR  '
                onPress={save} />
            </View>

          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {id = false} = props.navigation.state.params
  const pet = getPet(state, id)
  return {
    pet,
    petFoods: PetAttributes.findAllFoods(),
    petCastratedOptions: PetAttributes.findAllCastratedOptions(),
    petColors: PetAttributes.findColorsFor(pet.kind)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    medicalInfoSave: (...args) => dispatch(MedicalInfoSaveRedux.medicalInfoSaveRequest(...args))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetMedicalInfoScreen)
