import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image } from 'react-native'
import { connect } from 'react-redux'
import {Images} from '../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AboutUsScreenStyle'

class AboutUsScreen extends Component {
  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position' style={{backgroundColor: 'white'}}>

          <Text style={styles.title}>Acerca de</Text>
          <Image style={styles.image} resizeMethod='scale' resizeMode='contain' source={Images.headerLogo} />
          <Text style={styles.description}>
          forPets es la aplicación integral para los dueños de mascotas.
          {'\n'}
          {'\n'}
En forPets vas a encontrar los espacios petfriendly más cercanos: restaurantes, bares, alojamientos, veterinarias, petshops, paseadores y muchos servicios más. Tu comunidad

          {'\n'}
          {'\n'}
¿Perdiste a tu mejor amigo? Por eso creamos Perdidos y Encontrados, una herramienta que te brinda forPets para facilitarte el reencuentro con él.

          {'\n'}
          {'\n'}
Sumamos funciones continuamente para mejorar tu experiencia en la aplicación y enriquecer cada vez más la relación con tu mascota. Entendemos que a nuestras mascotas hay que brindarles lo que se merecen, y desde forPets asumimos el compromiso.

          {'\n'}
          {'\n'}
Inspirada en Marli, un cariñoso bulldog inglés que regaló alegrías hasta ganarse el amor de todos. Nos enseñó lo importante que es compartir la vida con ellos, por eso con forPets, te invitamos a que lo hagas con tu mascota.
          </Text>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutUsScreen)
