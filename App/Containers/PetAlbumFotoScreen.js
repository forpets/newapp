import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, Alert, InteractionManager } from 'react-native'
import { connect } from 'react-redux'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/PetAlbumFotoScreenStyle'
import PetAlbumDeleteRedux from '../Redux/PetAlbumDeleteRedux'

class PetAlbumFotoScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state
    const {deletePhoto} = params

    return {
      title: '',
      headerTitleStyle: styles.headerTitle,
      headerLeft: <IconMD
        name='arrow-back'
        size={25}
        onPress={() => navigation.goBack()}
        style={{paddingLeft: 20}}
    />,
      headerRight: <IconMD
        name='delete'
        size={25}
    // FIXME: context menu eliminar mascota
        onPress={() => Alert.alert('', '¿Eliminar foto?',
          [
                  {text: 'CANCELAR', onPress: () => {}},
                  {text: 'ELIMINAR', onPress: deletePhoto}
          ],
                { cancelable: false }
              )}
        style={{paddingRight: 20}}
    />
    }
  };

  componentDidMount () {
    // const {setHeaderParams} = this
    InteractionManager.runAfterInteractions(() => {
      this.props.navigation.setParams({ deletePhoto: this.deletePhoto })
    })
  }1

  constructor (props) {
    super(props)
    this.state = {}
    // TODO: enter with navigation parameter
  }

  deletePhoto = () => {
    const {navigation} = this.props
    const {params = {}} = this.props.navigation.state
    const {idPet, albumItem} = params

    if (idPet && albumItem) {
      this.props.petAlbumDeleteRequest({petId: idPet, album: albumItem})
    }
    navigation.goBack()
  }

  render () {
    const {params = {}} = this.props.navigation.state
    const {idPet, albumItem} = params

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Image source={{uri: albumItem.image}} style={styles.photoAlbum} />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    petAlbumDeleteRequest: (data) => { dispatch(PetAlbumDeleteRedux.petAlbumDeleteRequest(data)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PetAlbumFotoScreen)
