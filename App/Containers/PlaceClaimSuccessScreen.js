import React, { Component } from 'react'
import { ScrollView, Text, Image, TouchableOpacity, KeyboardAvoidingView, View } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/PlaceClaimSuccessScreenStyle'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'
import { Images } from '../Themes'
import RoundButton from '../Components/RoundButton'
import { getCurrentUser } from '../Redux/UserRedux'
import NavHelper from '../Lib/NavHelper'

class PlaceClaimSuccessScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: 'locked-closed',
    header: null
  })

  goBackToPlace = () => {
    // FIXME: WHAT HAPPENS IF WE DO NOT HAVE THIS PLACE IN STORAGE
    // FIXME: PlaceRequest - hit local store > miss > load from cloud
    // this.props.navigation.dispatch(NavHelper.reset('PlaceViewScreen', {id: this.props.placeId}))
    this.props.navigation.dispatch(NavHelper.reset('DirectoriesScreen'))
  }

  goToDirectories = () => {
    // directories
    this.props.navigation.dispatch(NavHelper.reset('DirectoriesScreen'))
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.backArrow}>
          <TouchableOpacity onPress={this.goBackToPlace}>
            <IconMD name='arrow-back' style={{fontSize: 25, color: 'black'}} />
          </TouchableOpacity>
        </View>

        <View style={styles.successContainer}>

          <Image style={styles.successImage} source={Images.crearEspacio.success} />
          <Text style={styles.descriptionText}>En breve nos {'\n'}
                comunicaremos con vos{'\n'}
                 para indicarte los pasos a {'\n'}
                    seguir.
          </Text>
          <View style={styles.roundButton}>
            <RoundButton
              text='CONTINUAR'
              color='white'
              bgColor='#FF1744'
              onPress={this.goToDirectories}
            />
          </View>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state, props) => {
  const {params = {}} = props.navigation.state
  const {placeId = null} = params

  return {
    placeId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceClaimSuccessScreen)
