import React, { Component } from 'react'
import { ScrollView, View, InteractionManager, Alert } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import {default as IconMD} from 'react-native-vector-icons/MaterialIcons'

import styles from './Styles/DirectoriesScreenStyle'

import LostFoundMap from '../Components/LostFoundMap'
import LostFoundList from './LostFoundList'

import RoundButton from '../Components/RoundButton'
import NavBars from '../Components/NavBars'
import MapListSwitch from '../Components/MapListSwitch'
import LocationRedux, {
  getCurrentLocation,
  getShouldShowLocationAccessDeniedAlert
} from '../Redux/LocationRedux'

import { getFilteredLostFound, getFilteredLostFoundFetching } from '../Redux/FilteredLostFoundRedux'

import R from 'ramda'

class LostFoundScreen extends Component {
  static navigationOptions = ({navigation, ...others}) => {
    const {dispatch} = navigation
    const {params = {}} = navigation.state
    // const {searchActive, currentSearchTerm} = params

    const title = 'Perdidos y encontrados'

    return {
      title,
      headerTitleStyle: styles.headerTitleStyle,
      headerLeft: <NavBars onPress={() => navigation.navigate('DrawerOpen')} />,
      headerRight:
        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
          <Icon
            name='filter' onPress={() => navigation.navigate('LostFoundFilterScreen')}
            style={{paddingRight: 20, fontSize: 20}}
          />
        </View>
    }
  }

  constructor (props) {
    super(props)
    this.state = { showMap: true }
    const {params = {}} = props.navigation.state
    if (params.showList) this.state.showMap = false
  }

  componentDidMount () {
    const {setHeaderParams} = this
    InteractionManager.runAfterInteractions(() => { setHeaderParams(this.props) })
  }

  // Updates header params
  setHeaderParams = (propsSource) => {
    // const {searchActive, currentSearchTerm} = propsSource
    // this.props.navigation.setParams({ searchActive, currentSearchTerm })
  }

  shouldComponentUpdate (nextProps, nextState) {
    // Check state for relevant update flags
    // if (getLostFoundUpdate()) {
    //   updatedLostFound()
    //   return true
    // }
    if (this.props.reports !== nextProps.reports) return true
    if (this.state.showMap !== nextState.showMap) return true
    return false
  }

  componentWillReceiveProps (newProps : Props) {
    const {shouldShowLocationAccessDeniedAlert, locationAccessDeniedAlertDisplayed} = newProps
    if (shouldShowLocationAccessDeniedAlert) {
      Alert.alert('Atención', 'No se podrán mostrar los espacios cercanos a su ubicación')
      locationAccessDeniedAlertDisplayed()
    }

    // // Update header only if props changed in order to avoid render loop
    // const newP = R.pickAll(['searchActive', 'currentSearchTerm'], newProps)
    // const oldP = R.pickAll(['searchActive', 'currentSearchTerm'], this.props)
    // if (newP.searchActive !== oldP.searchActive || newP.currentSearchTerm !== oldP.currentSearchTerm) {
    //   this.setHeaderParams(newProps)
    // }
  }

  newReport () {
    this.props.navigation.navigate('LostFoundEditScreen')
  }

  render () {
    const {showMap} = this.state
    const {reports, currentLocation, isFetching} = this.props

    // console.tron.log(this.state)
    // console.tron.log({reports})
    const zStyleMap = {zIndex: showMap ? 3 : 1}
    const zStyleList = {zIndex: !showMap ? 3 : 1}
    const NewButtonStyle = {
      flex: 1, flexDirection: 'row', justifyContent: 'flex-end',
      // width: 'auto',
      zIndex: 5,
      position: 'absolute',
      right: '3.5%',
      bottom: '4.2%'
    }

    return (
      <View style={styles.mapContainer}>

        <View style={styles.overlaySwitch}>
          <MapListSwitch onSwitchMap={(showMap) => this.setState({showMap})} defaultValue={showMap} />
        </View>

        <View style={NewButtonStyle}>
          <RoundButton
            bgColor='#FE5582'
            bgColorDisabled='#CC85A0'
            color='white'
            text='   +   '
            onPress={this.newReport.bind(this)}
          />
        </View>

        <View style={[styles.viewContainer, zStyleList]}>
          <ScrollView style={styles.mapContainer}>
            <LostFoundList
              navigation={this.props.navigation}
              data={reports}
              currentLocation={currentLocation}
              // isFetching={isFetching}
            />
          </ScrollView>
        </View>

        <View style={[styles.viewContainer, zStyleMap]}>
          <ScrollView style={styles.mapContainer}>
            <LostFoundMap
              navigation={this.props.navigation}
              locations={reports}
              currentLocation={currentLocation}
            />
          </ScrollView>
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const reports = getFilteredLostFound(state)

  return {
    reports,
    isFetching: getFilteredLostFoundFetching(state), 
    currentLocation: getCurrentLocation(state),
    shouldShowLocationAccessDeniedAlert: getShouldShowLocationAccessDeniedAlert(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    locationAccessDeniedAlertDisplayed: () => dispatch(LocationRedux.locationAccessDeniedAlertDisplayed())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LostFoundScreen)
