import validatejs from 'validate.js'
import validation from './ValidationRules'

export default function validate (fieldName, value) {
  const val = value || null // If empty string treat as null
  // const val = value

  const formValues = {[fieldName]: val}
  const formFields = {[fieldName]: validation[fieldName]}
  const result = validatejs(formValues, formFields, {fullMessages: false})
  // console.tron.display({name: 'Validation', preview: value + ' > ' + fieldName, value: {formValues, formFields, result} })

  if (result) {
    // console.tron.warn({result, 'resu': result[fieldName][0]})
    return result[fieldName][0]
  }

  return null
}
