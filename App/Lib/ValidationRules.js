// http://validatejs.org/#validators
import Categories from '../Services/Categories'
import R from 'ramda'

//
// IMPORTANT
//
// While updating local rules - do not forget update Firebase/Firestore rules too (see firestore.rules...)

// spaces between only
// const textOnlyPattern = '^([A-Za-z\u00C0-\u00FF]+ )+[A-Za-z\u00C0-\u00FF]+$|^[A-Za-z\u00C0-\u00FF]+$'
const textOnlyPattern = /^[A-Za-z\u00C0-\u00FF\s]+$/

export default {
  name: {
    presence: { message: 'Ingrese  nombre' },
    length: { minimum: 3, maximum: 30, message: 'Entre 3 y 30 caracteres' },
    format: {pattern: textOnlyPattern, flags: 'i', message: 'Solo letras y espacios'}
  },

  lastname: {
    presence: { message: 'Ingrese apellido' },
    length: { minimum: 3, maximum: 30, message: 'Entre 3 y 30 caracteres' },
    format: {pattern: textOnlyPattern, flags: 'i', message: 'Solo letras y espacios'}
  },

  phone: {
    numericality: {strict: true, onlyInteger: true, greaterThan: 10000, lessThan: 10000000000, message: 'Ingrese un teléfono válido'}
  },

  userAddress: {
    // presence: { message: 'Ingrese una dirección válida' },
    length: { minimum: 3, maximum: 150, message: 'Calle, Nro. Entre 3 y 150 caracteres' }
  },

  //
  // PET
  petName: {
    presence: { message: 'Ingrese nombre' },
    length: { minimum: 3, maximum: 50, message: 'Entre 3 y 30 caracteres' },
    format: {pattern: textOnlyPattern, flags: 'i', message: 'Solo letras y espacios'}
  },
  petDescription: {
    length: { minimum: 3, maximum: 300, message: 'Entre 3 y 300 caracteres' }
  },
  petWeight: {
    length: { minimum: 1, maximum: 6, message: 'Entre 1 y 6 caracteres' },
    // Crashes
    // format: {pattern: /^\d+(\.\d*[1-9])?$/, message: 'Tiene que ser un número'}
    // format: {pattern: /^\d{1,3}+(,\d{1,2}+)*$/, message: 'Tiene que ser un número'}
    numericality: {strict: false, onlyInteger: false, greaterThan: 0, lessThan: 999, message: 'Tiene que ser un número hasta 999.99'}
  },
  petVeterinaryInfo: {
    length: { minimum: 0, maximum: 300, message: 'Entre 1 y 300 caracteres' }
  },
  petBirthdate: {
    presence: {message: 'Debe seleccionar fecha de nacimiento'},
    length: { minimum: 6, message: 'Debe seleccionar fecha de nacimiento' }
  },
  petKind: {
    presence: {message: 'Debe seleccionar tipo de mascota'},
    length: { minimum: 1, message: 'Debe seleccionar tipo de mascota' }
  },
  petBreed: {
    presence: {message: 'Debe seleccionar raza de mascota'},
    length: { minimum: 1, message: 'Debe seleccionar raza de mascota' }
  },

  // LostFound
  date: {
    presence: {message: 'Debe seleccionar la fecha'},
    length: { minimum: 6, message: 'Debe seleccionar la fecha' }
  },

  generalString: {
    length: { maximum: 50, minimum: 3, message: 'Entre 3 y 50 caracteres' }
  },

  email: {
    presence: { message: 'Ingrese un E-mail válido' },
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: 'Ingrese un E-mail valido'
    },
    length: { maximum: 80, minimum: 3, message: 'Entre 3 y 80 caracteres' }
  },

  password: {
    presence: { message: 'Contraseña inválida. Intente nuevamente' },
    // length: { minimum: 5, message: 'Contraseña invalida. Min 5 caracteres' }
    length: { minimum: 5, maximum: 30, message: 'Entre 5 y 30 caracteres' }
  },

  idPresent: {
    numericality: {strict: true, onlyInteger: true, greaterThan: 0, message: 'Debe elegir una Vacuna'}
  },

  vaccineApplicationDate: {
    length: { minimum: 6, message: 'Debe seleccionar una fecha para la vacuna' }
  },

  //
  // Places
  //
  placeName: {
    presence: { message: 'Ingresar nombre' },
    length: { minimum: 3, maximum: 50, message: 'Entre 3 y 50 caracteres' }
  },
  placeEmail: {
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: 'Ingrese un E-mail válido'
    },
    length: { maximum: 80, minimum: 3, message: 'Entre 3 y 80 caracteres' }
  },
  placeAddress: {
    presence: { message: 'Ingrese una dirección válida' },
    length: { minimum: 3, maximum: 200, message: 'Calle, Nro. Entre 3 y 200 caracteres' }
  },
  placePhone: {
    numericality: {strict: true, onlyInteger: true, greaterThan: 10000, lessThan: 10000000000, message: 'Ingrese un teléfono válido'}
  },
  placeWebsite: {
    url: { message: 'Ingrese un Sitio Web válido' },
    length: { minimum: 0, maximum: 100, message: 'Ingrese un Sitio Web válido' }
  },
  placeDescription: {
    length: { minimum: 0, maximum: 500, message: 'Ingrese una descripción válida' }
  },
  placeCategory: {
    presence: { message: 'Seleccioná la categoria principal' },
    numericality: {strict: true, onlyInteger: true, greaterThan: 0, lessThan: R.length(Categories.findAll()) + 1, message: 'Seleccioná la categoria principal'}
  },


  //
  // Place Review
  //
  reviewRating: {
    numericality: {greaterThan: 0, lessThan: 6, message: 'Evalúe la atención por favor'}
  },
  reviewPricing: {
    numericality: {greaterThan: 0, lessThan: 4, message: 'Evalúe los precios'}
  },
  reviewComment: {
    length: { minimum: 0, maximum: 500, message: 'Ingrese un comentario válido' }
  },

  //
  // PlaceClaim
  //
  placeClaimPhone: {
    presence: {message: 'Ingrese un teléfono válido'},
    numericality: {strict: true, onlyInteger: true, greaterThan: 10000, lessThan: 10000000000, message: 'Ingrese un teléfono válido'}
  }

}
