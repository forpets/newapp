// @flow
import {NavigationActions} from 'react-navigation'

export const resetHome = () => {
  return NavigationActions.reset({
    index: 0,
    actions: [ NavigationActions.navigate({routeName: 'HomeScreen'}) ]
  })
}

export const reset = (routeName: string, params : Object = {}) => {
  return NavigationActions.reset({
    index: 1,
    actions: [
      NavigationActions.navigate({routeName: 'HomeScreen'}),
      NavigationActions.navigate({routeName, params})
    ]
  })
}

export const resetLoggedOut = (routeName: string, params : Object = {}) => {
  return NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({routeName, params})
    ]
  })
}

export default {reset, resetLoggedOut, resetHome}
