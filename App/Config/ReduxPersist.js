import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import { AsyncStorage } from 'react-native'
import AppConfig from './AppConfig'

// More info here:  https://shift.infinite.red/shipping-persistant-reducers-7341691232b1

const REDUX_PERSIST = {
  active: true,
  reducerVersion: AppConfig.packageVersion, // Should clear reducer-persistence every time we update package version
  storeConfig: {
    storage: AsyncStorage,
    // blacklist: ['login', 'search', 'nav', 'userRegister', 'petAlbums', 'petAlbumDelete', 'petAlbumUpload', 'petCreate', 'noteSave', 'userUpdate', 'vaccineSave', 'forgotPassword'],

    whitelist: ['user', 'pets', 'placeSearch', 'filteredPlaces', 'filteredLostFound', 'lostFound'],

    // Optionally, just specify the keys you DO want stored to persistence. An empty array means 'don't store any reducers'
    transforms: [immutablePersistenceTransform]
  }
}

export default REDUX_PERSIST
