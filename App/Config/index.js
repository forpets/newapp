import { Text } from 'react-native'
import DebugConfig from './DebugConfig'
import AppConfig from './AppConfig'
import './Polyfills'
import firebase from 'react-native-firebase'

// Allow/disallow font-scaling in app
Text.defaultProps.allowFontScaling = AppConfig.allowTextFontScaling

if (__DEV__) {
  // If ReactNative's yellow box warnings are too much, it is possible to turn
  // it off, but the healthier approach is to fix the warnings.  =)
  console.disableYellowBox = !DebugConfig.yellowBox
} else {
  // IN PRODUCTION - ALL DISPLAY ERRORS STUFF - GOES TO CRASHLYTICS
  // console.tron.log('Handling errors')
  console.tron = {
    display: onDisplayError,
    log: () => {},
    error: () => {},
    warn: () => {}
  }
  // Does not function?
  // handleUncaughtErrors()
}

// Send serious errors to crashlytics
function onDisplayError ({name, preview, value, important}) {
  if (!important) { return }
  const error = `${name}: ${preview}. Data: ${JSON.stringify(value)}`
  firebase.fabric.crashlytics().recordError(error)
}

// Make crashlytics handle errors
function handleUncaughtErrors () {
  if (ErrorUtils) {
    // console.tron.log('App is handling uncaught errors')

    const previousHandler = ErrorUtils.getGlobalHandler()

    ErrorUtils.setGlobalHandler((error, isFatal) => {
      // console.log(`Handle error. Is fatal? <${isFatal}>`)
      error.message = `ErrorUtils: handled ${isFatal ? 'fatal' : 'non-fatal'} error: ${error.message}`
      console.tron.log(error)
      if (isFatal) {
        console.tron.log('Recording Fatal')
        firebase.fabric.crashlytics().recordError(1, error.message)
      } else {
        // TODO: SHOULD LOG ONLY??
        console.tron.log('Recording non Fatal')
        firebase.fabric.crashlytics().recordError(0, error.message)
      }
      if (previousHandler) {
        previousHandler(error, isFatal)
      }
    })
  }
}
