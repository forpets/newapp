// Simple React Native specific changes
import '../I18n/I18n'
const Package = require('../../package.json')

export default {
  packageVersion: Package.version,
  allowTextFontScaling: true, // font scaling override - RN default is on
  googleApiKey: 'AIzaSyCl8y2rekilzoQ5IRKXFKc6VIjuzSp_MWg', // Google Places API
  googleReferral: 'app4pets.com.ar', // Does not work - google does not support in this case

  filteredSpacesLimit: 30,
  maxPhotosForPlace: 10,

  defaultFilter: {
    orderBy: 'nearness', // relevance / rating
    searchRadius: 5,
    selectedCategories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
    emergency: null,
    pricing: null
  },
  
  defaultLostFoundFilter: {
    searchRadius: 5,
    kind: null,
    breed: null,
    gender: null,
    types: {
      lost: { selected: true },
      found: { selected: true }
    }
  },

  // Default location to show when user does not provide LocationAccess or Error
  // Obelisco en CABA
  defaultLocation: {
    latitude: -34.6037132,
    longitude: -58.3816179
  },

  // FULL firebase configuration is in google-services.json
  Firebase: {
    appUrl: 'https://dazzling-torch-8669.firebaseapp.com'
  }
}

console.ignoredYellowBox = [
  // Component does not check for styles from StyleSheet
  'Warning: Failed prop type: Invalid prop ',
  'Warning: Failed prop type: Invalid prop ',
  'Warning: Failed prop type: Invalid prop `rightTextStyle` of type `number` supplied to `CheckBox`',
  'Warning: Failed prop type: Invalid prop `leftTextStyle` of type `number` supplied to `CheckBox`'
]
