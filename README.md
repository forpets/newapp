#  App4Pets

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

* Standard compliant React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)

## :arrow_up: How to Setup

**Step 1:** git clone this repo:
**Step 2:** cd to the cloned repo:
**Step 3:** Install the Application with `yarn` 
**Step 4:** Install flow types `flow-typed install`
**Step 5:** Prepare your editor - install `Flow` and `StandardJS` extensions

## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for Android
    * run `react-native run-android`


### Developer commands

'Shake' Menu
```
adb shell input keyevent 82
```

App crashes? Check logs
```
adb logcat
```
https://developer.android.com/studio/command-line/logcat.html

Kill on port
```
fuser -k 8081/tcp
```

Reactotron physical dev
```
adb reverse tcp:9090 tcp:9090
```

Do not forget to update version before new release
```
npm version 1.2.3
```

### Run emulator in Linux

```
LD_PRELOAD=/usr/lib64/libstdc++.so.6 primusrun ~/Android/Sdk/tools/emulator @Nexus_5X_API_21 -gpu host 

LD_PRELOAD=/usr/lib64/libstdc++.so.6 primusrun ~/Android/Sdk/tools/emulator @Nexus_6P_API_27 -gpu host -sdcard ~/sdcard.img  

LD_PRELOAD=/usr/lib64/libstdc++.so.6 primusrun ~/Android/Sdk/tools/emulator @Nexus_5X_API_24 -gpu host -sdcard ~/sdcard.img  
```

### Update app icon

```
npm install -g yo generator-rn-toolbox

# Use high resolution PNG (e.g. 200x200)
# Do not use SVG as it generates white background

yo rn-toolbox:assets --icon ./icon.png
```

### Update firebase/firestore rules
```
npm -g install firebase-tools
```

If `does not have permission to` > add `--unsafe-perm`

```
firebase deploy --only firestore
firebase deploy --only database
firebase deploy --only functions
```


### Test cloud functions locally
```
firebase serve --only functions
```

### Hot Reload

 echo 999999 | sudo tee -a /proc/sys/fs/inotify/max_user_watches  && echo 999999 | sudo tee -a  /proc/sys/fs/inotify/max_queued_events && echo 999999 | sudo tee  -a /proc/sys/fs/inotify/max_user_instances && 


# Deployment on Client account

## Facebook

* Set Facebook API key in strings.xml

## Google 

* Set Google Maps API key in `AndroidManifest.xml` and `AppConfig.js`
* Enable Google Maps Geocoding API
* Enable Google Maps Places API
* Enable billing and verify your identity in Google API Console to increase limit FREE (https://developers.google.com/places/web-service/usage)



## :no_entry_sign: Standard Compliant

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)
This project adheres to Standard.  Our CI enforces this, so we suggest you enable linting to keep your project compliant during development.

**Understanding Linting Errors**

The linting rules are from JS Standard and React-Standard.  [Regular JS errors can be found with descriptions here](http://eslint.org/docs/rules/), while [React errors and descriptions can be found here](https://github.com/yannickcr/eslint-plugin-react).

## :closed_lock_with_key: Secrets

This project uses [react-native-config](https://github.com/luggit/react-native-config) to expose config variables to your javascript code in React Native. You can store API keys
and other sensitive information in a `.env` file:

```
API_URL=https://myapi.com
GOOGLE_MAPS_API_KEY=abcdefgh
```

and access them from React Native like so:

```
import Secrets from 'react-native-config'

Secrets.API_URL  // 'https://myapi.com'
Secrets.GOOGLE_MAPS_API_KEY  // 'abcdefgh'
```

The `.env` file is ignored by git keeping those secrets out of your repo.

### Get started:
1. Copy .env.example to .env
2. Add your config variables
3. Follow instructions at [https://github.com/luggit/react-native-config#setup](https://github.com/luggit/react-native-config#setup)
4. Done!

### Source maps

Parse source maps to get usable info. Useful for reading the madness from Hockeyapp or other crash storing logs

```
ignite ignite-source-map lookup ./someFolder/index.ios.map 32:75
ignite ignite-source-map lookup ./someFolder/index.ios.map stackTrace.txt
```
