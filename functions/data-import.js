const admin = require('./node_modules/firebase-admin')
const serviceAccount = require('./data-import-key.json')

// const allData = require('./data-import/placesImport20180123.json')
const allData = require('./data-import/placesImport20180130.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://dazzling-torch-8669.firebaseio.com'
})

console.log('Initializing admin: ' + admin)
const collection = 'places'

//
// TODO: import in batches 
//
// const data = allData.slice(0, 50)
// const data = allData.slice(51, 100)
// const data = allData.slice(100, 150)
// const data = allData.slice(50, 51)
// const data = allData.slice(150, 200)
// const data = allData.slice(200, 250)

const timestamp = new Date().getTime()

console.log('Data length: ' + data.length)
console.log('Data sample: ' + JSON.stringify(data[0]))

// return 1
data && data.forEach((item, index) => {
  admin.firestore()
                  .collection(collection)
                  // .doc(docTitle)
                  // .set(nestedContent[docTitle])
                  .add({
                    createdAt: timestamp,
                    updatedAt: timestamp,
                    owner: null,
                    public: true,
                    published: true,

                    ignoreFunc: true, // Do not trigger cloud func

                    // TODO: set date
                    imported: '20180130',

                    email: item.email || '',
                    phone: item.phone || '',
                    name: item.name,
                    address: item.address,
                    location: [item.lat, item.long],
                    category: item.category
                  })
                  .then((ref) => {
                    console.log(index + ' Item: ' + item.name + ' added! id:  ' + ref.id)
                  })
                  .catch((error) => {
                    console.error(' >> Idx: ' + index + ', name: ' + item.name + ' Error writing document: ', error)
                  })
})
