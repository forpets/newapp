'use strict'

// HAVE IN MIND INFINITE LOOPS !!! (onUpdate > update > onUpdate > ....)

const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase)
// Moving to rambda might help - https://github.com/selfrefactor/rambda
const R = require('ramda')
const geolib = require('geolib')
// Stemming ?? https://github.com/MihaiValentin/lunr-languages
const lunr = require('lunr')

const MIN_SEARCH_RADIUS = 5 // km
const MAX_SEARCH_RADIUS = 20 // km
const MAX_PLACES_RESULT = 60 // records
const MAX_REPORTS_RESULT = 60 // records
const orderByModes = ['nearness', 'relevance', 'rating']

exports.onCreatePlacess = functions.firestore.document('places/{placeId}').onCreate((event) => {
// const onCreatePlacess = functions.firestore.document('places/{placeId}').onCreate((event) => {

  // console.log("Event params: " + JSON.stringify(event.params))
  // if (event.data.previous) return false // SHould not happen
  // Magic key to ignore func when doing mass imports
  // if (event.data.data().ignoreFunc) {
  //   console.warn('ignoreFunc - abort')
  //   return false
  // }

  const timestamp = new Date().getTime()
  console.log('Creating place place: ' + JSON.stringify(event.data.data()) + ' timestamp ' + timestamp)

  return event.data.ref.set({
    createdAt: timestamp,
    updatedAt: timestamp,
    owner: null,
    public: true,
    published: true
  }, {merge: true})
})

exports.onUpdatePlace = functions.firestore.document('places/{placeId}').onUpdate((event) => {
  if (!event.data.previous) {
    console.warn('Previous data does not exist')
    return false
  }

  // Ignore rating/pricing updates
  if (event.data.data().rating || event.data.data().pricing) { return false }
  const margin = 60 * 1000 // 60 secs
  // Make sure we do not invoke onUpdate loop  - add margin
  const oldTimestamp = event.data.previous.data().updatedAt + margin
  const timestamp = new Date().getTime()

  if (oldTimestamp < timestamp) {
    return event.data.ref.set({ updatedAt: timestamp }, {merge: true})
  } else {
    console.log(`Old timestamp too recent. (${oldTimestamp} < ${timestamp})`)
    return false
  }
})

// Automatically add timestamp and reviewed
exports.onCreatePlaceReview = functions.firestore.document('placeReviews/{reviewsId}').onCreate((event) => {
  // if (event.data.previous) return false
  const timestamp = new Date().getTime()
  return event.data.ref.set({ createdAt: timestamp, reviewed: true }, {merge: true})
})

// Recalculate reviews when REVIEWED=true
exports.updatePlaceRatings = functions.firestore.document('placeReviews/{reviewsId}').onUpdate((event) => {
  if (!event.data.previous) {
    console.error('Previous data does not exist')
    return false
  }

  const wasReviewed = event.data.previous.data().reviewed
  const reviewed = event.data.data().reviewed
  const placeId = event.data.data().placeId

  if (!wasReviewed && reviewed && placeId) {
    const userId = event.data.data().userId
    // In case the place does not have ratings - these values are being assigned
    const newRating = event.data.data().rating
    const newPricing = event.data.data().pricing
    const updateData = {}
    if (newPricing) { updateData.pricing = newPricing }
    if (newRating) { updateData.rating = newRating }
    // console.log('>>>>>>> Initial data: ', JSON.stringify(updateData))

    // find all placeReviews
    return admin.firestore().collection('placeReviews').where('reviewed', '==', true).where('placeId', '==', placeId).get()
    .then(snapshot => {
      const placeReviews = []
      snapshot.forEach(doc => { placeReviews.push(doc.data()) })

      // if we have previous published palce reviews - recalculate
      if (placeReviews && R.length(placeReviews) > 0) {
        // console.log('Recalculating. PlaceReviews count: ', R.length(placeReviews), ', newRating:', newRating, ' newPricing:',newPricing)

        // TODO: fix it - because now we already receive the current record!
        // If for some reason we have duplicate - remove the new one
        // const duplicateReviewFound = R.find(R.propEq('userId', userId), placeReviews)
        // if (duplicateReviewFound && ) {
        //   console.warn('Duplicate PlaceReview found. Deleting. userId: ' + userId + ' placeId: ' + placeId)
        //   return admin.firestore().collection(`places`).doc(placeId).delete()
        // }

        if (newRating) {
          const ratings = R.map((rev) => rev.rating, placeReviews)
          const cleanRatings = R.reject(R.isNil, ratings)
          // cleanRatings.push(updateData.rating) // should be already there
          const mean = Math.round(R.mean(cleanRatings) * 10) / 10
          updateData.rating = mean
          console.log('Previous ratings: ' + JSON.stringify(cleanRatings))
          // console.log('Updating rating: ' + updateData.rating, ' CleanRatings: ', JSON.stringify(cleanRatings), ' Mean: ', mean, ' Ratings: ', JSON.stringify(ratings))
        }

        if (newPricing) {
          const pricings = R.map((rev) => rev.pricing, placeReviews)
          const cleanPricings = R.reject(R.isNil, pricings)
          // cleanPricings.push(updateData.pricing) // should be already there
          updateData.pricing = Math.round(R.mean(cleanPricings))
          console.log('Previous pricings: ' + JSON.stringify(cleanPricings))
        }
      }
      console.log('Added data. PlaceId: ', placeId, ' ', JSON.stringify({newRating, newPricing}), ' Updated data: ', JSON.stringify(updateData))

      return admin.firestore().collection(`places`).doc(placeId).update(updateData, {merge: true})
    }).catch(err => { console.error('Error getting placeReviews (did not update place): ' + err) })
  } else {
    return false
  }
})

// FIXME: on delete place - delete reviews

// PLACES SEARCH PLACES
/*
 curl -X POST https://us-central1-dazzling-torch-8669.cloudfunctions.net/filterPlaces -H "Content-Type:application/json" --data '{ "filter" : { "orderBy": "nearness", "searchRadius": 5, "selectedCategories": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], "emergency": null, "pricing": null }, "location": { "latitude": -34.5878, "longitude": -58.321999 } }'
*/

//
//
// FILTER PLACES >>>>>> PRODUCTION
//
//
exports.filterPlaces = functions.https.onRequest((req, res) => {
  if (!req.body) { res.status(400).send('No params received') }
  const {filter, location, debug, search = false} = req.body
  // if (debug) console.log('Body: ' + JSON.stringify(req.body))
  if (!filter) { res.status(400).send('No filter defined') }
  if (!location) { res.status(400).send('No location defined') }
  const radius = parseInt(filter.searchRadius)

  const placeFilter = {
    emergency: filter.emergency,
    pricing: filter.pricing,
    orderBy: R.contains(filter.orderBy, orderByModes) ? filter.orderBy : 'nearness',
    searchRadius: (radius >= MIN_SEARCH_RADIUS && radius <= MAX_SEARCH_RADIUS) ? radius : MIN_SEARCH_RADIUS,
    selectedCategories: filter.selectedCategories || null // By default gets all
  }
  // If we need to take into account distance
  const orderByDistanceMode = (placeFilter.orderBy === 'nearness' || placeFilter.orderBy === 'relevance')
  // Get center location
  const centerLocation = R.map((i) => parseFloat(i), R.pickAll(['latitude', 'longitude'], location))

  if (debug) {
    console.log('Receiving params: \n ' + JSON.stringify(filter) + '\n' +
    JSON.stringify(location) +
    '\n Filtered filter: ' + JSON.stringify(placeFilter) +
    '\n Center location: ' + JSON.stringify(centerLocation) +
    '\n Search: ' + search)
  }
  // TODO: REGEXP search text??

  let ref = admin.firestore().collection('places').where('published', '==', true)
  if (placeFilter.emergency === true) { ref = ref.where('emergency', '==', true) }
  if (placeFilter.pricing) { ref = ref.where('pricing', '==', filter.pricing) }

  ref.get()
    .then(snapshot => {
      const predicate = getPredicate(placeFilter, debug)
      const places = []
      // let count = 0

      // FILTER
      snapshot.forEach(doc => {
        // Do not limit results if we are searching or sorting by distance - only filter
        // if (count > MAX_PLACES_RESULT && !search && !orderByDistanceMode) return
        const place = doc.data()
        if (predicate(place)) {
          place['id'] = doc.id
          places.push(place)
          // count++
        }
      })

      let result = []
      // DO SEARCH
      if (search) {
        const index = getLunrIndex(places)
        if (debug) console.log('Indexing places for search. Length: ', R.length(index))
        const searchResult = index.search(search)

        if (R.length(searchResult) > 0) {
          if (debug) console.log('Search places "', search, '" found count:', R.length(searchResult))
          const getFunc = (result) => R.find(R.propEq('id', result.ref), places)
          result = R.map(getFunc, searchResult)
        }
      } else {
        result = places
      }

      let unsortedResult = []

      // CALCULATE AND FILTER DISTANCE
      if (orderByDistanceMode) {
        const {searchRadius} = placeFilter
        const distanceFilter = (item) => (item._distance && item._distance <= searchRadius * 1000) // to meters

        const func = (item) => {
          item._distance = getDistance(item.location, centerLocation)
          if (distanceFilter(item)) unsortedResult.push(item)
        }
        R.forEach(func, result)
        // for (const item in result) {
        // }
      } else {
        unsortedResult = result
      }

      // SORT
      const sortedResult = sortPlaces(unsortedResult, placeFilter.orderBy).slice(0, MAX_PLACES_RESULT)

      if (debug) { console.log('Fetched ', snapshot.size, ' places. Filtered cat: ', R.length(result), 'UnsortedResult: ', R.length(unsortedResult), ' sorted: ', R.length(sortedResult)) }

      res.status(200).send(JSON.stringify(sortedResult))
    }).catch(err => {
      res.status(500).send('Error: ' + err)
    })
})

//
//
// FILTER PLACES DEVELOPMENT
//
//
// FIXME: DEV FIXME: DEV

exports.filterPlacesDev = functions.https.onRequest((req, res) => {
  if (!req.body) { res.status(400).send('No params received') }
  const {filter, location, debug, search = false} = req.body
  // if (debug) console.log('Body: ' + JSON.stringify(req.body))
  if (!filter) { res.status(400).send('No filter defined') }
  if (!location) { res.status(400).send('No location defined') }
  const radius = parseInt(filter.searchRadius)

  const placeFilter = {
    emergency: filter.emergency,
    pricing: filter.pricing,
    orderBy: R.contains(filter.orderBy, orderByModes) ? filter.orderBy : 'nearness',
    searchRadius: (radius >= MIN_SEARCH_RADIUS && radius <= MAX_SEARCH_RADIUS) ? radius : MIN_SEARCH_RADIUS,
    selectedCategories: filter.selectedCategories || null // By default gets all
  }
  // If we need to take into account distance
  const orderByDistanceMode = (placeFilter.orderBy === 'nearness' || placeFilter.orderBy === 'relevance')
  // Get center location
  const centerLocation = R.map((i) => parseFloat(i), R.pickAll(['latitude', 'longitude'], location))

  if (debug) {
    console.log('Receiving params: \n ' + JSON.stringify(filter) + '\n' +
    JSON.stringify(location) +
    '\n Filtered filter: ' + JSON.stringify(placeFilter) +
    '\n Center location: ' + JSON.stringify(centerLocation) +
    '\n Search: ' + search)
  }
  // TODO: REGEXP search text??

  let ref = admin.firestore().collection('places').where('published', '==', true)
  if (placeFilter.emergency === true) { ref = ref.where('emergency', '==', true) }
  if (placeFilter.pricing) { ref = ref.where('pricing', '==', filter.pricing) }

  ref.get()
    .then(snapshot => {
      const predicate = getPredicate(placeFilter, debug)
      const places = []
      // let count = 0

      // FILTER
      snapshot.forEach(doc => {
        // Do not limit results if we are searching or sorting by distance - only filter
        // if (count > MAX_PLACES_RESULT && !search && !orderByDistanceMode) return
        const place = doc.data()
        if (predicate(place)) {
          place['id'] = doc.id
          places.push(place)
          // count++
        }
      })

      let result = []
      // DO SEARCH
      if (search) {
        const index = getLunrIndex(places)
        if (debug) console.log('Indexing places for search. Length: ', R.length(index))
        const searchResult = index.search(search)

        if (R.length(searchResult) > 0) {
          if (debug) console.log('Search places "', search, '" found count:', R.length(searchResult))
          const getFunc = (result) => R.find(R.propEq('id', result.ref), places)
          result = R.map(getFunc, searchResult)
        }
      } else {
        result = places
      }

      let unsortedResult = []

      // CALCULATE AND FILTER DISTANCE
      if (orderByDistanceMode) {
        const {searchRadius} = placeFilter
        const distanceFilter = (item) => (item._distance && item._distance <= searchRadius * 1000) // to meters

        const func = (item) => {
          item._distance = getDistance(item.location, centerLocation)
          if (distanceFilter(item)) unsortedResult.push(item)
        }
        R.forEach(func, result)
        // for (const item in result) {
        // }
      } else {
        unsortedResult = result
      }

      // SORT
      const sortedResult = sortPlaces(unsortedResult, placeFilter.orderBy).slice(0, MAX_PLACES_RESULT)

      if (debug) { console.log('Fetched ', snapshot.size, ' places. Filtered cat: ', R.length(result), 'UnsortedResult: ', R.length(unsortedResult), ' sorted: ', R.length(sortedResult)) }

      res.status(200).send(JSON.stringify(sortedResult))
    }).catch(err => {
      res.status(500).send('Error: ' + err)
    })
})

function getPredicate (filter, debug) {
  const {selectedCategories = []} = filter
  const conditions = {}

  // Not needed anymore - transfered this logic to Firestore query
  // if (filter.pricing) { conditions.pricing = R.equals(filter.pricing) }
  // if (filter.emergency === true) { conditions.emergency = R.equals(true) }

  if (selectedCategories && R.length(selectedCategories) > 0) {
    conditions.category = (catId) => selectedCategories.includes(catId)
  }

  const predicate = R.where(conditions)
  return predicate
}

function getDistance (location, currentLocation) {
  // TODO: GEOHASHES??? - GEOFIRE??
  // const location1 = getCoordsFromPlace(place)
  const [latitude, longitude] = location

  if (!latitude || !longitude) {
    // console.warn('Location1 has malformed data: ' + JSON.stringify(location))
    return false
  }
  const location1 = {latitude, longitude}
  const location2 = extractLocation(currentLocation)

  if (!location2) {
    // console.warn('Location2 has malformed data: ' + JSON.stringify(currentLocation))
    return false
  }

  return geolib.getDistanceSimple(location1, location2)
}

function sortPlaces (places, orderBy) {
  const orderByFuncs = [] // No sort by default

  const ratingFunc = (item) => item.rating || 0
  const pricingFunc = (item) => item.pricing || 1000

  switch (orderBy) {
    case 'relevance':

      // 2- Mayor cantidad de votantes - TODO: need to cache reviews
      orderByFuncs.push(R.ascend(pricingFunc))
      orderByFuncs.push(R.ascend(R.prop('_distance')))
      orderByFuncs.push(R.descend(ratingFunc))
      // 5- Mayor cantidad de búsquedas en analytics TODO: no tenemos analytics :-)
      break

    case 'rating':
      orderByFuncs.push(R.descend(ratingFunc))
      break

    case 'nearness':
    default:
      orderByFuncs.push(R.ascend(R.prop('_distance')))
      break
  }

  return R.sortWith(orderByFuncs, places)
}

// function isPlaceWithinRadius (location, currentLocation, radius) {
//   // TODO: GEOHASHES??? - GEOFIRE??
//   // const location1 = getCoordsFromPlace(place)
//   const [latitude, longitude] = location

//   if (!latitude || !longitude) {
//     // console.warn('Location1 has malformed data: ' + JSON.stringify(location))
//     return false
//   }
//   const location1 = {latitude, longitude}
//   const location2 = extractLocation(currentLocation)

//   if (!location2) {
//     // console.warn('Location2 has malformed data: ' + JSON.stringify(currentLocation))
//     return false
//   }

//   return geolib.isPointInCircle(location1, location2, radius)
// }

function extractLocation (location) {
  if (!location) {
    // console.error('extractLocation: Received falsy location. <Location>:' + JSON.stringify(location))
    return null
  }
  const { latitude, longitude } = location
  if (!longitude || !latitude) {
    // console.error('extractLocation: Received place with malformed location data. <Location>:' + JSON.stringify(location))
    return null
  }

  return {latitude, longitude}
}

// Could implement caching, however GC Functions instances does not live long - not sure about performance gains...
function getLunrIndex (places) {
  const index = lunr(function () {
    this.ref('id')
    this.field('name')
    this.field('address')
    this.field('categoryString')

    places.forEach(function (place) {
      place.categoryString = idToString(place.category)
      this.add(place)
    }, this)
  })

  return index
}

function idToString (id) {
  const cat = findOne(id)
  let label = cat ? cat.label : ''
  return label
}

function findAll () {
  return [
    {value: 1, label: 'Veterinaria' },
    {value: 2, label: 'Peluquería' },
    {value: 3, label: 'Paseadores' },
    {value: 4, label: 'Alojamientos' },
    {value: 5, label: 'Entrenadores' },
    {value: 6, label: 'Refugio' },
    {value: 7, label: 'Guarderías' },
    {value: 8, label: 'Cuidadores' },
    {value: 9, label: 'Restaurants/Bares' },
    {value: 10, label: 'Pet Shops' },
    {value: 11, label: 'Plazas' }
  ]
}

function findOne (id) {
  return findAll().find((item) => item.value === id)
}



/*
 curl -X POST https://us-central1-dazzling-torch-8669.cloudfunctions.net/filterLostFound -H "Content-Type:application/json" --data '{ "filter" : { "searchRadius": 10, "types": { "lost": { "selected": true, "label": "Perdido" }, "found": { "selected": true, "label": "Encontrado" } }, "kind": "gato", "breed": "C8", "gender": "hembra" }, "location": { "latitude": -34.5878, "longitude": -58.321999 } }'
 curl -X POST https://us-central1-dazzling-torch-8669.cloudfunctions.net/filterLostFound -H "Content-Type:application/json" --data '{ "filter" : { "searchRadius": 20, "types": { "lost": { "selected": true, "label": "Perdido" }, "found": { "selected": true, "label": "Encontrado" } }, "kind": null, "breed": null, "gender": null }, "location": { "latitude": -34.4634328, "longitude": -58.549907 }, "debug": true }'
*/
//
//
// FILTER LostFound >>>>>> PRODUCTION
//
//
exports.filterLostFound = functions.https.onRequest((req, res) => {
  if (!req.body) { res.status(400).send('No params received') }
  req.body.debug = true
  const {filter, location, debug, search = false} = req.body
  // if (debug) console.log('Body: ' + JSON.stringify(req.body))
  if (!filter) { res.status(400).send('No filter defined') }
  if (!location) { res.status(400).send('No location defined') }
  const radius = parseInt(filter.searchRadius)

  const reportFilter = {
    kind: filter.kind,
    breed: filter.breed,
    gender: filter.gender,
    searchRadius: (radius >= MIN_SEARCH_RADIUS && radius <= MAX_SEARCH_RADIUS) ? radius : MIN_SEARCH_RADIUS,
  }
  // If we need to take into account distance
  // Get center location
  const centerLocation = R.map((i) => parseFloat(i), R.pickAll(['latitude', 'longitude'], location))

  if (debug) {
    console.log('Receiving params: \n ' + JSON.stringify(filter) + '\n' +
      JSON.stringify(location) +
      '\n Filtered filter: ' + JSON.stringify(reportFilter) +
      '\n Center location: ' + JSON.stringify(centerLocation)
    )
  }
  // TODO: REGEXP search text??

  let ref = admin.firestore().collection('reports')//.where('published', '==', true)
  if (reportFilter.kind) { ref = ref.where('kind', '==', reportFilter.kind) }
  if (reportFilter.breed) { ref = ref.where('breed', '==', reportFilter.breed) }
  if (reportFilter.gender) { ref = ref.where('gender', '==', reportFilter.gender) }

  ref.get()
    .then(snapshot => {
      const result = []
      // let count = 0
      if (debug) { console.log('Fetched ', snapshot.size) }
      
      // FILTER
      snapshot.forEach(doc => {
        const report = doc.data()
        report['id'] = doc.id
        result.push(report)
      })
      
      let unsortedResult = []
      
      // CALCULATE AND FILTER DISTANCE
      const {searchRadius} = reportFilter
      const distanceFilter = (item) => ((item._distance || item._distance === 0) && item._distance <= searchRadius * 1000) // to meters
      
      const func = (item) => {
        // if (debug) { console.log('location ', R.length(item.location), ' -- ', item.location) }
        if (! item.location || 2 != R.length(item.location)) { return }

        item._distance = getDistance(item.location, centerLocation)
        // if (debug) { console.log('distance ', item._distance) }
        if (distanceFilter(item)) unsortedResult.push(item)
      }
      // if (debug) { console.log('result ', result) }
      R.forEach(func, result)

      // SORT
      const sortedResult = unsortedResult.slice(0, MAX_REPORTS_RESULT) //sortPlaces(unsortedResult, reportFilter.orderBy).slice(0, MAX_REPORTS_RESULT)

      if (debug) { console.log('Fetched ', snapshot.size, ' reports. Filtered cat: ', R.length(result), 'UnsortedResult: ', R.length(unsortedResult), ' sorted: ', R.length(sortedResult)) }

      res.status(200).send(JSON.stringify(sortedResult))
    }).catch(err => {
      res.status(500).send('Error: ' + err)
    })
})
