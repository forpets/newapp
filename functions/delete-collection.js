
// TODO: HANDLE WITH CARE
//
//
//
//
//

const admin = require('./node_modules/firebase-admin')
const serviceAccount = require('./data-import-key.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://dazzling-torch-8669.firebaseio.com'
})

console.log('INitialize app ' + admin)

deleteCollection(admin.firestore(), 'places     a', 30)
.then((any) => { console.log('Success ' + JSON.stringify(any)) })
.catch((error) => {
  console.log('>>> ERROR: ' + JSON.stringify(error))
})

function deleteCollection (db, collectionPath, batchSize) {
  var collectionRef = db.collection(collectionPath)
  var query = collectionRef.where('imported', '==', '20180130').limit(batchSize)
// admin.firestore().collection(collection).where('imported','==','20180130')

  return new Promise((resolve, reject) => {
    deleteQueryBatch(db, query, batchSize, resolve, reject)
  })
}

function deleteQueryBatch (db, query, batchSize, resolve, reject) {
  query.get()
      .then((snapshot) => {
        console.log('Snapshot length: ' + snapshot.size + ' Batch size: ' + batchSize)
          // When there are no documents left, we are done
        if (snapshot.size == 0) {
          return 0
        }

          // Delete documents in a batch
        var batch = db.batch()
        snapshot.docs.forEach((doc) => {
          batch.delete(doc.ref)
          console.log('Deleting: ' + doc.ref)
        })

        return batch.commit().then(() => {
          return snapshot.size
        })
      }).then((numDeleted) => {
        if (numDeleted === 0) {
          resolve()
          return
        }

          // Recurse on the next process tick, to avoid
          // exploding the stack.
        process.nextTick(() => {
          console.log('Next tick')
          deleteQueryBatch(db, query, batchSize, resolve, reject)
        })
      })
      .catch(reject)
}
